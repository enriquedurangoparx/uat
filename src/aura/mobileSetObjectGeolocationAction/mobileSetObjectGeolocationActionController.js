({
    doInit : function(component, event, helper) {
        let objCoordinates = {
            currentLatitude : undefined,
            currentLongitude : undefined,
            existingLatitude : undefined,
            existingLongitude : undefined
        }
        if (navigator.geolocation) 
        {
            navigator.geolocation.getCurrentPosition((position) => {
                objCoordinates.currentLatitude = position.coords.latitude;
                objCoordinates.currentLongitude = position.coords.longitude;
                component.set("v.objectCoordinates", objCoordinates);

                let action = component.get("c.getObjectData");
                action.setParams({
                    "recId" : component.get("v.recordId")
                });
                
                action.setCallback(this, function(response) 
                {
                    let state = response.getState();
                    if (state === "SUCCESS") 
                    {
                        
                        let resp = JSON.parse(response.getReturnValue());
                        console.log('resp: ', resp);
                        if (resp.Geolocation__Latitude__s && resp.Geolocation__Longitude__s)
                        {
                            objCoordinates.existingLatitude = resp.Geolocation__Latitude__s;
                            objCoordinates.existingLongitude = resp.Geolocation__Longitude__s;
                            component.set("v.objectCoordinates", objCoordinates);
                            component.set("v.alreadyHasCoordinates", true);
                        }
                        
                        component.set("v.hasErrors", false);
                    } 
                    else if (state === "ERROR") 
                    {
                        helper.setErrorMessage(component, response, false, '');
                    } 
                    else 
                    {
                        helper.setErrorMessage(component, response, true, $A.get("$Label.c.GeneralLabelUnknownError"));
                    }
                });
                $A.enqueueAction(action);
            });
        } 
        else 
        {
            component.set("v.hasErrors", true, $A.get("$Label.c.SetObjectGeodataUnsupportedBrowser"));
        }
    },

    updateRecord : function(component, event, helper)
    {
        let action = component.get("c.setObjectCoordinates");
        action.setParams({
            "latitude" : component.get("v.objectCoordinates").currentLatitude,
            "longitude" : component.get("v.objectCoordinates").currentLongitude,
            "recId" : component.get("v.recordId")
        });
        
        action.setCallback(this, function(response) 
        {
            let state = response.getState();
            if (state === "SUCCESS") 
            {
                helper.showSuccessToast($A.get("$Label.c.GeneralLabelSuccessUpdateMessage"));
                $A.get('e.force:refreshView').fire();
//                $A.get("e.force:closeQuickAction").fire();
				var sObjectEvent = $A.get("e.force:navigateToSObject");
				sObjectEvent.setParams({
					"recordId": component.get("v.recordId"),
					"slideDevName": "detail"
				});
				sObjectEvent.fire();
//				sforce.one.back(true);
                component.set("v.hasErrors", false);
            } 
            else if (state === "ERROR") 
            {
                helper.setErrorMessage(component, response, false, '');
            } 
            else 
            {
                helper.setErrorMessage(component, response, true, $A.get("$Label.c.GeneralLabelUnknownError"));
            }
        });
        $A.enqueueAction(action);
    }, 

    closeModal : function(component, event, helper) 
    {
//        $A.get("e.force:navigationcomplete").fire();
		var sObjectEvent = $A.get("e.force:navigateToSObject");
		sObjectEvent.setParams({
			"recordId": component.get("v.recordId"),
			"slideDevName": "detail"
		});
		sObjectEvent.fire();
//		sforce.one.back(true);
    }
})