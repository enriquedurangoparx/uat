({
    setErrorMessage : function(component, response, isCustomError, customErrorMessage)
    {
        component.set("v.hasErrors", true);
        if (!isCustomError)
        {
            let errors = response.getError();
            if (errors) {
                component.set("v.errorMessage", errors[0].message);
                
            }
        }
        else
        {
            component.set("v.errorMessage", $A.get("$Label.c.GeneralLabelUnknownError"));
        }
		
    },
    
    showSuccessToast : function(message) {
		var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": $A.get("$Label.c.GeneralLabelSuccess"),
            "type": "success",
            "message": "" + message
        });
        toastEvent.fire();
	},
})