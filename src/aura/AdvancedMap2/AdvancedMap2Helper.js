/**
 * Created by yoon on 28.01.19.
 */
({
    reloadMap : function (component, event, helper) {


        // console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!');

        var objectList = component.get('v.objectList');
        var geoField = component.get('v.GeoLocationField');
        var columns = component.get('v.columns');
        // console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        var mapMarkers= [];
        if (geoField)
        {
            var latitude = geoField.substring(0, geoField.length -1) + 'Latitude__s';
            var longitude = geoField.substring(0, geoField.length -1) + 'Longitude__s';
            objectList.forEach(function(object) 
            {
                var mapMarker = {};
                if (component.get('v.mapMarkerTitle'))
                {
                   mapMarker.title = helper.extract(object, component.get('v.mapMarkerTitle'));
                }
                mapMarker.location = 
                {
                    'Latitude': helper.extract(object, latitude),
                    'Longitude': helper.extract(object, longitude)
                }
                mapMarker.description = '';
                if (mapMarker.location.Latitude)
                {
                    columns.forEach(function(column) {
                         // console.log(column)
                         if (column.displayInMap)
                         {
                            var v = helper.extract(object, column.fieldName);
                            // console.log(v);
                            if (column.linkFieldName)
                            {
                                mapMarker.description += '<a target="_top" href="'+ window.location.origin + '/' + helper.extract(object, column.linkFieldName) +'"><b>' + column.label + ':</b> ' + v + '</a><br/>';
                            }
                            else if (column.type.includes('DATE'))
                            {
                                mapMarker.description += '<b>' + column.label + ':</b> ' + $A.localizationService.formatDate(v) + '<br/>';
                            }
                            else
                            {
                                mapMarker.description += '<b>' + column.label + ':</b> ' + v + '<br/>';
                            }
                         }
                    })
                    mapMarkers.push(mapMarker);
                }
            })            
        }


        component.set('v.mapMarkers', []);
        if(mapMarkers.length > 0)
        {
          component.set('v.mapMarkers', mapMarkers);
          //component.set('v.zoomLevel', 16);

        }

        component.set('v.center', {
            location: {
                City: 'Mühlhausen'
            }
        });

        console.log("Map Centered Mühlhausen!!!");
    },
    extract: function(object, fieldName)
    {
        if (typeof object[fieldName] !== "undefined")
        {
            return object[fieldName]
        }
        var val = object;
        fieldName.split('.').forEach(function(part)
        {
            if (typeof val === "undefined" || val === null)
            {
                val = {};
            }
            val = val[part];
        });
        return val;
    },

})