/**
 * Created by ille on 2019-03-13.
 */
({
    error: null,
    distributionColumns: ['DistributionForIdentification__c', 'DistributionForOrderAcquisition__c', 'DistributionForExecution__c', 'DistributionForReporting__c'],
    percentageColumns: ['PercentageForIdentification__c', 'PercentageForOrderAcquisition__c', 'PercentageForExecution__c', 'PercentageForReporting__c', 'SharePercentage__c'],

    initialize: function (component) {
        const helper = this;
        helper.loadFeeDistributionSettings(component)
            .then($A.getCallback(() => {
                return helper.loadFeeSharingDescription(component);
            }))
            .then($A.getCallback(() => {
                return helper.loadOpportunity(component, helper);
            }))
            .then($A.getCallback(() => {
                return helper.loadFeeSharing(component);
            }))
            .catch($A.getCallback(something => {
                if (helper.error){
                    helper.releaseError();
                }
                else{
                    console.log(something);
                }
            }));
    },

    /**
     * A Promise wrapper around apex method call.
     *
     * @param action
     * @returns {Promise<any>}
     */
    executeAction: function(action) {
        const helper = this;
        return new Promise($A.getCallback((resolve, reject) => {
            action.setCallback(helper, $A.getCallback(response => {
                if (helper.responseSuccessful(response)){
                    resolve(response);
                }
                else{
                    reject(response, action);
                }
            }));

            $A.enqueueAction(action);
        }));
    },

    //Call this function to handle HttpRequest errors!
    responseSuccessful: function (response) {
        return response.getState() === 'SUCCESS';
    },

    loadFeeSharingDescription: function(component) {
        const loadFeeSharingDescriptionAction = component.get('c.getSObjectDescription');
        loadFeeSharingDescriptionAction.setParam('sObjectName', 'FeeSharing__c');

        return this.executeAction(loadFeeSharingDescriptionAction)
            .then($A.getCallback(response => {
                component.set('v.feeSharingDescription', response.getReturnValue());
            }));
    },

    /**
     * This function loads FeeDistributionSetting__mdt
     * @param component
     */
    loadFeeDistributionSettings: function (component) {
        const selectFeeDistributionSettingAction = component.get('c.selectFeeDistributionSetting');
        const helper = this;
        return helper.executeAction(selectFeeDistributionSettingAction)
            .then($A.getCallback(response => {
                component.set('v.feeDistributionSetting', response.getReturnValue());
                if (component.get('v.feeDistributionSetting')){
                    component.set('v.disableComponent', false);
                }
            }));
    },

    /**
     * Loads list of FeeSharing__c records by calling apex controller.
     *
     * @param component
     * @returns {*|Promise<T | never>}
     */
    loadFeeSharing: function(component) {
        const selectFeeSharingAction = component.get('c.selectFeeSharing');
        selectFeeSharingAction.setParam('opportunityId', component.get('v.recordId'));

        const helper = this;
        return helper.executeAction(selectFeeSharingAction)
            .then($A.getCallback(response => {
                component.set('v.feeSharing', response.getReturnValue());
                if (component.get('v.feeSharing') && component.get('v.feeSharing')[0]){
                    //substituting previously stored FeeDistributionSetting__c record with instance of FeeSharing__c record
                    //since we're planning to use fields which names match
                    const feeDistributionSetting = component.get('v.feeDistributionSetting');
                    helper.distributionColumns.forEach(column => {
                        if (component.get('v.feeSharing')[0][column]){
							feeDistributionSetting[column] = component.get('v.feeSharing')[0][column];
						}
                    });

                    component.set('v.feeDistributionSetting', feeDistributionSetting);
                }

                component.get('v.feeSharing').forEach(feeSharing => {
                	helper.distributionColumns.forEach(distributionColumn => {
                		if (!feeSharing[distributionColumn]){
							feeSharing[distributionColumn] = component.get('v.feeDistributionSetting')[distributionColumn];
						}
					});
                });
                helper.recalculateSum(component);
            }));
    },

    loadOpportunity: function(component, helper){
        const selectOpportunityAction = component.get('c.selectOpportunity');
        selectOpportunityAction.setParam('recordId', component.get('v.recordId'));

        return helper.executeAction(selectOpportunityAction)
            .then($A.getCallback((response => {
                component.set('v.opportunityRecord', response.getReturnValue());

                //if Opportunity owned by a User w/o "OS Fee Share" custom permission - simply copying MaxAmountOfWorkersForNonOsOwned__c to MaxAmountOfWorkers__c
                //which will ensure all the validations as for number of records.
                if (!component.get('v.opportunityRecord.OSFeeShare__c')){
                    component.set('v.feeDistributionSetting.MaxAmountOfWorkers__c', component.get('v.feeDistributionSetting.MaxAmountOfWorkersForNonOsOwned__c'));
                }
            })));
    },

    baseToast: function(message) {
        return $A.get('e.force:showToast').setParams({
            'duration': '10000',
            'message': message
        });
    },

    baseError: function(message) {
        return this.baseToast(message).setParams({
            type: 'error'
        });
    },

    /**
     * Shows a toast error previously stored via storeError method.
     */
    releaseError: function()
    {
        if (this.error && this.error.fire)
        {
            this.error.fire();
        }

        this.error = null;
    },

    /**
     * Stores an error which then can be shown ("released") via releaseError method.
     * @param error
     */
    storeError: function(error)
    {
        this.error = error;
    },

    closeComponent: function () {
        $A.get('e.force:closeQuickAction').fire();
    },

    onCancel: function(component) {
        this.initialize(component);
        // this.closeComponent();
    },

    onNew: function (component) {
        const feeSharing = component.get('v.feeSharing');
        if (feeSharing.length < component.get('v.feeDistributionSetting.MaxAmountOfWorkers__c'))
        {
            const newFeeSharing = {Id: null, Opportunity__c: component.get('v.recordId')};
            this.distributionColumns.forEach(column => {
                newFeeSharing[column] = component.get('v.feeDistributionSetting')[column];
            });

            this.percentageColumns.forEach(column => {
                newFeeSharing[column] = 0;
            });

            feeSharing.push(newFeeSharing);
        }

        component.set('v.feeSharing', feeSharing);
    },

    onDelete: function (component, event) {
        const feeSharing = component.get('v.feeSharing');
        const index = event.getSource().get('v.value');
        console.log('...feeSharing:' + feeSharing);
        if (feeSharing && feeSharing[index])
        {
            feeSharing.splice(index, 1);
        }

        component.set('v.feeSharing', feeSharing);
    },

    onSave: function (component) {
        const helper = this;
        if (!helper.validateData(component))
        {
            helper.releaseError();
            return;
        }

        console.log(JSON.stringify(component.get('v.feeSharing')));
        const saveFeeSharingAction = component.get('c.commitFeeSharing');
        var opportunityId = component.get('v.recordId');
        saveFeeSharingAction.setParam('feeSharings', JSON.stringify(component.get('v.feeSharing')));
        saveFeeSharingAction.setParam('opportunityId', opportunityId);

        helper.executeAction(saveFeeSharingAction)
            .then($A.getCallback(response => {
                component.set('v.feeSharing', response.getReturnValue());
                helper.baseToast($A.get('$Label.c.SuccessMessage')).setParams({'type': 'success'}).fire();
            }))
            .catch($A.getCallback(response => {
                console.log('...error: ' + response.getError());
                helper.handleErrors([].concat(response.getError()));
            }));
    },

    /**
     * Validates data before performing save operation
     * @param component
     */
    validateData: function (component) {
        const feeSharing = component.get('v.feeSharing');
        const helper = this;

        try{
            if (component.get('v.opportunityRecord.OSFeeShare__c')){
                feeSharing.forEach(eachFeeSharing => {
                    helper.percentageColumns.forEach(column => {
                        if (!eachFeeSharing[column] && eachFeeSharing[column] !== 0 && column !== 'SharePercentage__c'){
                            helper.storeError(helper.baseError($A.get('$Label.c.SumValidationErrorMessage')));
                            throw true;
                        }
                    });
                });
            }

            helper.percentageColumns.forEach(column => {
                if (component.get('v.sumFeeSharing')[column] > 100){
                    helper.storeError(helper.baseError($A.get('$Label.c.SumValidationErrorMessage')));
                    throw true;
                }
            });
        }
        catch (e){
            return false;
        }

        return true;
    },

    recalculateSum: function (component) {
        const feeSharing = component.get('v.feeSharing');
        const sumFeeSharing = {};
        const helper = this;

        var sumOverallPercent = 0;
        var sumWeightedSharePerUser = 0;
        var sumSharePerUser = 0;

        feeSharing.forEach(eachFeeSharing => {
            eachFeeSharing.OverallPercentage__c = 0;
            eachFeeSharing.WeightedSharePerUser__c = 0;
            eachFeeSharing.SharePerUser__c = 0;

            eachFeeSharing.OverallPercentage__c += parseFloat(eachFeeSharing.PercentageForIdentification__c ? eachFeeSharing.PercentageForIdentification__c : 0);
            eachFeeSharing.OverallPercentage__c += parseFloat(eachFeeSharing.PercentageForOrderAcquisition__c ? eachFeeSharing.PercentageForOrderAcquisition__c : 0);
            eachFeeSharing.OverallPercentage__c += parseFloat(eachFeeSharing.PercentageForExecution__c ? eachFeeSharing.PercentageForExecution__c : 0);
            eachFeeSharing.OverallPercentage__c += parseFloat(eachFeeSharing.PercentageForReporting__c ? eachFeeSharing.PercentageForReporting__c : 0);
            eachFeeSharing.OverallPercentage__c = eachFeeSharing.OverallPercentage__c.toFixed(5);
            sumOverallPercent += parseFloat(eachFeeSharing.OverallPercentage__c ? eachFeeSharing.OverallPercentage__c : 0);

            eachFeeSharing.WeightedSharePerUser__c = eachFeeSharing.OverallPercentage__c * component.get('v.opportunityRecord').WeightedBusinessLine__c;
            eachFeeSharing.WeightedSharePerUser__c = eachFeeSharing.WeightedSharePerUser__c.toFixed(2) / 100;
            sumWeightedSharePerUser += parseFloat(eachFeeSharing.WeightedSharePerUser__c ? eachFeeSharing.WeightedSharePerUser__c : 0);

            eachFeeSharing.SharePerUser__c = eachFeeSharing.OverallPercentage__c * component.get('v.opportunityRecord').ShareBusinessLine__c;
            eachFeeSharing.SharePerUser__c = eachFeeSharing.SharePerUser__c.toFixed(2) / 100;
            sumSharePerUser += parseFloat(eachFeeSharing.SharePerUser__c ? eachFeeSharing.SharePerUser__c : 0);

            helper.percentageColumns.forEach(field => {
                sumFeeSharing[field] = parseFloat(sumFeeSharing[field] ? sumFeeSharing[field] : 0) + parseFloat(eachFeeSharing[field] ? eachFeeSharing[field] : 0);
            });
        });


        ['Identification', 'OrderAcquisition', 'Execution', 'Reporting'].forEach(field => {
            var maxValue;
            if(field === 'Identification') maxValue = component.get('v.feeDistributionSetting.DistributionForIdentification__c');
            if(field === 'OrderAcquisition') maxValue = component.get('v.feeDistributionSetting.DistributionForOrderAcquisition__c');
            if(field === 'Execution') maxValue = component.get('v.feeDistributionSetting.DistributionForExecution__c');
            if(field === 'Reporting') maxValue = component.get('v.feeDistributionSetting.DistributionForReporting__c');

            sumFeeSharing[field + 'Class'] = helper.matchClass(sumFeeSharing['PercentageFor' + field + '__c'], maxValue, component);
        });

        sumFeeSharing['SharePercentageClass'] = helper.matchClass(sumFeeSharing['SharePercentage__c'], 100, component);
        sumFeeSharing['sumOverallPercentClass'] = helper.matchClass(sumOverallPercent, 100, component);
        sumFeeSharing['sumWeightedSharePerUserClass'] = helper.matchClass(sumWeightedSharePerUser, component.get('v.opportunityRecord').WeightedBusinessLine__c, component);
        sumFeeSharing['sumSharePerUserClass'] = helper.matchClass(sumSharePerUser, component.get('v.opportunityRecord').ShareBusinessLine__c, component);

        component.set('v.sumFeeSharing', sumFeeSharing);
        component.set('v.sumOverallPercent', sumOverallPercent);
        component.set('v.sumWeightedSharePerUser', sumWeightedSharePerUser);
        component.set('v.sumSharePerUser', sumSharePerUser);
        component.set('v.feeSharing', feeSharing);

        if (feeSharing.length === 0)
        {
            helper.onNew(component);
        }

        console.log(JSON.stringify(sumFeeSharing, null, '\t'));
    },


    matchClass: function (percentage, maxValue, component) {
        if (percentage > maxValue)
        {
            return 'sum-red';
        }
        else if (percentage === maxValue)
        {
            return 'sum-green'
        }

        return 'sum-yellow';
    },

    handleErrors: function(errors)
    {
        const helper = this;
        const messages = [];

        errors.forEach(error => {

            if (error.fieldErrors)
            {
                Object.keys(error.fieldErrors).forEach(fieldName => {
                    helper.baseError(error.fieldErrors[fieldName][0].message).fire();
                });
            }

            if (error.message)
            {
                messages.push(error.message);
            }

            if (error.pageErrors)
            {
                error.pageErrors.forEach(function(pageError){
                    messages.push(pageError.message);
                });
            }
        });

        messages.forEach(message => {
            helper.baseError(message).fire();
        });
    }
})