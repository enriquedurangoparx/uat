({
    init: function (component, event, helper) {
        var action = component.get("c.getLocations");
        action.setParam("currentId", component.get("v.recordId"));
        action.setCallback(this, function(response) {
            console.log('response'+response);
            var state = response.getState();
            console.log(state);
            if (state == "SUCCESS") {
                var obj =response.getReturnValue() ;
                console.log(obj);
                component.set('v.mapMarkers',obj);
            }
            
        });
        $A.enqueueAction(action); 
    }
})