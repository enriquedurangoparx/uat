<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Expense_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ExpenseCompactLayout</fullName>
        <fields>DateSpent__c</fields>
        <fields>Type__c</fields>
        <fields>DocumentAmountGross__c</fields>
        <label>Expense Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Records to track the expenses of the employees</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Expenses</relationshipLabel>
        <relationshipName>Expenses</relationshipName>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Add_Expenses_to_Invoice__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If checked, a flow is launched, which collects further expenses.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, a flow is launched, which collects further expenses.</inlineHelpText>
        <label>Add Expenses to Invoice</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ArrivalLocation__c</fullName>
        <externalId>false</externalId>
        <label>Arrival Location</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BillingType__c</fullName>
        <externalId>false</externalId>
        <label>Billing Type</label>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Billable</fullName>
                    <default>false</default>
                    <label>Billable</label>
                </value>
                <value>
                    <fullName>Internal</fullName>
                    <default>true</default>
                    <label>Internal</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Clear_Invoice__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If checked, a flow will clear the invoice value. Used in invoice cancelation process.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, a flow will clear the invoice value. Used in invoice cancelation process.</inlineHelpText>
        <label>Clear Invoice</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Comment__c</fullName>
        <externalId>false</externalId>
        <label>Comment</label>
        <length>32000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>DateSpent__c</fullName>
        <externalId>false</externalId>
        <label>Date Spent</label>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>DocumentAmountGross__c</fullName>
        <externalId>false</externalId>
        <label>Document Amount Gross</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DocumentAmountNet__c</fullName>
        <externalId>false</externalId>
        <formula>DocumentAmountGross__c / (1 + VAT__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Document Amount Net</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ExpenseAccount__c</fullName>
        <externalId>false</externalId>
        <label>Expense Account</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ExpenseAccount__c.Status__c</field>
                <operation>equals</operation>
                <value>Open, Ready for Approval, Approved by Manager</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ExpenseAccount__c</referenceTo>
        <relationshipLabel>Expenses</relationshipLabel>
        <relationshipName>Expenses</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>KmRefundAmountTotal__c</fullName>
        <externalId>false</externalId>
        <label>Km Refund Amount Total</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>KmTotal__c</fullName>
        <externalId>false</externalId>
        <label>Km Total</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ON_AddToTransactionTable__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Determines whether to show the transaction on the invoice, deselected by default.</description>
        <externalId>false</externalId>
        <inlineHelpText>Determines whether to show the transaction on the invoice, deselected by default.</inlineHelpText>
        <label>JustOn: Add To Transaction Table</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ON_Invoice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Links to the related invoice after the object records have been successfully billed. Is empty by default.</description>
        <externalId>false</externalId>
        <inlineHelpText>Links to the related invoice after the object records have been successfully billed. Is empty by default.</inlineHelpText>
        <label>JustOn: Invoice</label>
        <referenceTo>ONB2__Invoice__c</referenceTo>
        <relationshipLabel>Expenses</relationshipLabel>
        <relationshipName>Expenses</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OpportunityLineItemScheduleId__c</fullName>
        <externalId>false</externalId>
        <label>Opportunity Line Item Schedule Id</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Expenses</relationshipLabel>
        <relationshipName>Expenses</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Opportunity Line Item should be from the same Opportunity as the Expense.</errorMessage>
            <filterItems>
                <field>OpportunityLineItem__c.Opportunity__c</field>
                <operation>equals</operation>
                <valueField>$Source.Opportunity__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>OpportunityLineItem__c</referenceTo>
        <relationshipLabel>Expenses</relationshipLabel>
        <relationshipName>Expenses</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>RefundAmountGross__c</fullName>
        <defaultValue>0.0</defaultValue>
        <externalId>false</externalId>
        <label>Refund Amount Gross</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>RefundAmountNet__c</fullName>
        <defaultValue>0.0</defaultValue>
        <externalId>false</externalId>
        <label>Refund Amount Net</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>StartingLocation__c</fullName>
        <externalId>false</externalId>
        <label>Starting Location</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Travel - Train</fullName>
                    <default>false</default>
                    <label>Travel - Train</label>
                </value>
                <value>
                    <fullName>Travel - Taxi</fullName>
                    <default>false</default>
                    <label>Travel - Taxi</label>
                </value>
                <value>
                    <fullName>Travel - Gasoline</fullName>
                    <default>false</default>
                    <label>Travel - Gasoline</label>
                </value>
                <value>
                    <fullName>Travel - Rental Car</fullName>
                    <default>false</default>
                    <label>Travel - Rental Car</label>
                </value>
                <value>
                    <fullName>Travel - Parking</fullName>
                    <default>false</default>
                    <label>Travel - Parking</label>
                </value>
                <value>
                    <fullName>Travel - Airplane</fullName>
                    <default>false</default>
                    <label>Travel - Airplane</label>
                </value>
                <value>
                    <fullName>Travel - Public Transport</fullName>
                    <default>false</default>
                    <label>Travel - Public Transport</label>
                </value>
                <value>
                    <fullName>Hotel</fullName>
                    <default>false</default>
                    <label>Hotel</label>
                </value>
                <value>
                    <fullName>Entertainment Expenses (Client)</fullName>
                    <default>false</default>
                    <label>Entertainment Expenses (Client)</label>
                </value>
                <value>
                    <fullName>Entertainment Expenses (Employee)</fullName>
                    <default>false</default>
                    <label>Entertainment Expenses (Employee)</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Expenses</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>VAT__c</fullName>
        <externalId>false</externalId>
        <label>VAT (%)</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <gender>Feminine</gender>
    <label>Spesen</label>
    <nameField>
        <displayFormat>EXP-{000000}</displayFormat>
        <label>Expense No.</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Spesen</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Closed_Expenses_can_not_be_edited</fullName>
        <active>false</active>
        <description>As soon an expense is marked as closed, an edit is only allowed for the dedicated personal with special permissions.</description>
        <errorConditionFormula>($User.Id != ExpenseAccount__r.ExpenseKeeper__c &amp;&amp; NOT($Permission.ManageExpenses)) || 
!ISPICKVAL(ExpenseAccount__r.Status__c, &quot;Open&quot;)</errorConditionFormula>
        <errorMessage>Expense is closed. An edit is not allowed anymore.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Opportunity_Is_Mandatory_For_Billable</fullName>
        <active>true</active>
        <description>In case the Expense record is billable, then the Opportunity needs to be linked.</description>
        <errorConditionFormula>ISPICKVAL(BillingType__c, &apos;Billable&apos;) &amp;&amp; ISBLANK(Opportunity__c)</errorConditionFormula>
        <errorMessage>For billable expenses is an Opportunity required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>User_Change_For_Non_Open_EXA_Validation</fullName>
        <active>true</active>
        <description>Ensure the standard user (user without the custom permissions &quot;Manage Finances&quot; and &quot;Manage Expenses&quot;) is not able to change any values on the fields, if the status on the Expense Account is not &quot;Open&quot;</description>
        <errorConditionFormula>NOT($Permission.ManageExpenses) 
&amp;&amp; NOT($Permission.ManageFinances) 
&amp;&amp; NOT(ISPICKVAL(ExpenseAccount__r.Status__c, &apos;Open&apos;))
&amp;&amp; NOT(ISNEW())</errorConditionFormula>
        <errorMessage>The related Expense Account is already closed for further processing. It is not possible to edit the fields.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
