<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>ValuationRecordPage</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object für saving Valuation details.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>ConflictCheckCompleted__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Conflict Check Completed</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ConflictExistent__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Conflict Existent</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ConflictReason__c</fullName>
        <externalId>false</externalId>
        <label>Conflict Reason</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>ConflictExistent__c</controllingField>
            <restricted>true</restricted>
            <valueSetName>Conflict_Reason</valueSetName>
            <valueSettings>
                <controllingFieldValue>checked</controllingFieldValue>
                <valueName>Transaction via Colliers</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>checked</controllingFieldValue>
                <valueName>Sales by CI Letting Department</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>checked</controllingFieldValue>
                <valueName>Application for Exclusive Mandate by CI is running</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>checked</controllingFieldValue>
                <valueName>Pitch through other CI BL</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>CustomerName__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.Account.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Customer Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExternalId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>ExternalId</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Inspection__c</fullName>
        <externalId>false</externalId>
        <label>Inspection</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Drive by</fullName>
                    <default>false</default>
                    <label>Drive by</label>
                </value>
                <value>
                    <fullName>Full site inspection</fullName>
                    <default>false</default>
                    <label>Full site inspection</label>
                </value>
                <value>
                    <fullName>​​​​​Desktop</fullName>
                    <default>false</default>
                    <label>​​​​​Desktop</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>InvoiceRecipient__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Invoice Recipient</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Valuations</relationshipLabel>
        <relationshipName>Valuations</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>InvoicedProduct__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Invoiced Product</label>
        <referenceTo>OpportunityLineItem__c</referenceTo>
        <relationshipLabel>Valuations</relationshipLabel>
        <relationshipName>Valuations</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>MarketValueToSpecialAssumptions__c</fullName>
        <externalId>false</externalId>
        <label>Market Value to special assumptions</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>MarketValue__c</fullName>
        <externalId>false</externalId>
        <label>Market Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>MortgageLendingValue__c</fullName>
        <externalId>false</externalId>
        <label>Mortgage Lending Value (MLV)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Object__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Object</label>
        <referenceTo>PropertyObject__c</referenceTo>
        <relationshipLabel>Valuations</relationshipLabel>
        <relationshipName>Valuations</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Valuations</relationshipLabel>
        <relationshipName>Valuations</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>PurposeOfUse__c</fullName>
        <externalId>false</externalId>
        <label>Purpose Of Use</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Purchase</fullName>
                    <default>false</default>
                    <label>Purchase</label>
                </value>
                <value>
                    <fullName>Sales purpose</fullName>
                    <default>false</default>
                    <label>Sales purpose</label>
                </value>
                <value>
                    <fullName>Internal</fullName>
                    <default>false</default>
                    <label>Internal</label>
                </value>
                <value>
                    <fullName>Financing</fullName>
                    <default>false</default>
                    <label>Financing</label>
                </value>
                <value>
                    <fullName>Accounting</fullName>
                    <default>false</default>
                    <label>Accounting</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ReportCreator__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Report Creator</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Valuations</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ReportFormat__c</fullName>
        <externalId>false</externalId>
        <label>Report Format</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Indication</fullName>
                    <default>false</default>
                    <label>Indication</label>
                </value>
                <value>
                    <fullName>One Pager</fullName>
                    <default>false</default>
                    <label>One Pager</label>
                </value>
                <value>
                    <fullName>Short Report</fullName>
                    <default>false</default>
                    <label>Short Report</label>
                </value>
                <value>
                    <fullName>Full Report</fullName>
                    <default>false</default>
                    <label>Full Report</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>In process</fullName>
                    <default>false</default>
                    <label>In process</label>
                </value>
                <value>
                    <fullName>Review</fullName>
                    <default>false</default>
                    <label>Review</label>
                </value>
                <value>
                    <fullName>Review approved</fullName>
                    <default>false</default>
                    <label>Review approved</label>
                </value>
                <value>
                    <fullName>Draft sent</fullName>
                    <default>false</default>
                    <label>Draft sent</label>
                </value>
                <value>
                    <fullName>Draft approved</fullName>
                    <default>false</default>
                    <label>Draft approved</label>
                </value>
                <value>
                    <fullName>Final</fullName>
                    <default>false</default>
                    <label>Final</label>
                </value>
                <value>
                    <fullName>Final sent</fullName>
                    <default>false</default>
                    <label>Final sent</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Update__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indicates if this is an updated valuation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates if this is an updated valuation.</inlineHelpText>
        <label>Update</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ValuationDate__c</fullName>
        <externalId>false</externalId>
        <label>Valuation Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ValuationDocuments__c</fullName>
        <externalId>false</externalId>
        <label>Valuation Documents</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>ValuationLanguage__c</fullName>
        <externalId>false</externalId>
        <label>Valuation Language</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>German</fullName>
                    <default>false</default>
                    <label>German</label>
                </value>
                <value>
                    <fullName>English</fullName>
                    <default>false</default>
                    <label>English</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ValuationMethod__c</fullName>
        <externalId>false</externalId>
        <label>Valuation Method</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Income approach</fullName>
                    <default>false</default>
                    <label>Income approach</label>
                </value>
                <value>
                    <fullName>DRC</fullName>
                    <default>false</default>
                    <label>DRC</label>
                </value>
                <value>
                    <fullName>Comparison approach</fullName>
                    <default>false</default>
                    <label>Comparison approach</label>
                </value>
                <value>
                    <fullName>DCF</fullName>
                    <default>false</default>
                    <label>DCF</label>
                </value>
                <value>
                    <fullName>Investment Method</fullName>
                    <default>false</default>
                    <label>Investment Method</label>
                </value>
                <value>
                    <fullName>Term &amp; Reversion</fullName>
                    <default>false</default>
                    <label>Term &amp; Reversion</label>
                </value>
                <value>
                    <fullName>Hardcore Top Slice</fullName>
                    <default>false</default>
                    <label>Hardcore Top Slice</label>
                </value>
                <value>
                    <fullName>Residual value</fullName>
                    <default>false</default>
                    <label>Residual value</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>ValuationSoftware__c</fullName>
        <externalId>false</externalId>
        <label>Valuation Software</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Excel</fullName>
                    <default>false</default>
                    <label>Excel</label>
                </value>
                <value>
                    <fullName>Argus Enterprise</fullName>
                    <default>false</default>
                    <label>Argus Enterprise</label>
                </value>
                <value>
                    <fullName>Lora</fullName>
                    <default>false</default>
                    <label>Lora</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>ValuationStandard__c</fullName>
        <externalId>false</externalId>
        <label>Valuation Standard</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Red Book</fullName>
                    <default>false</default>
                    <label>Red Book</label>
                </value>
                <value>
                    <fullName>White Book</fullName>
                    <default>false</default>
                    <label>White Book</label>
                </value>
                <value>
                    <fullName>BauGB</fullName>
                    <default>false</default>
                    <label>BauGB</label>
                </value>
                <value>
                    <fullName>BelWertV</fullName>
                    <default>false</default>
                    <label>BelWertV</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <label>Valuation</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>Alle</label>
    </listViews>
    <nameField>
        <label>Valuation Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Valuations</pluralLabel>
    <recordTypeTrackFeedHistory>false</recordTypeTrackFeedHistory>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>ValuationReadOnlyRecordType</fullName>
        <active>true</active>
        <label>Valuation Read Only Record Type</label>
        <picklistValues>
            <picklist>ConflictReason__c</picklist>
            <values>
                <fullName>Application for Exclusive Mandate by CI is running</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pitch through other CI BL</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Sales by CI Letting Department</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Transaction via Colliers</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Inspection__c</picklist>
            <values>
                <fullName>%E2%80%8B%E2%80%8B%E2%80%8B%E2%80%8B%E2%80%8BDesktop</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Drive by</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Full site inspection</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>PurposeOfUse__c</picklist>
            <values>
                <fullName>Accounting</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Financing</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Internal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Purchase</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Sales purpose</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ReportFormat__c</picklist>
            <values>
                <fullName>Full Report</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Indication</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>One Pager</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Short Report</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Final</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Final sent</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ValuationLanguage__c</picklist>
            <values>
                <fullName>English</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>German</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ValuationMethod__c</picklist>
            <values>
                <fullName>Comparison approach</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>DCF</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>DRC</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Hardcore Top Slice</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Income approach</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Investment Method</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Residual value</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Term %26 Reversion</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ValuationSoftware__c</picklist>
            <values>
                <fullName>Argus Enterprise</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Excel</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Lora</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ValuationStandard__c</picklist>
            <values>
                <fullName>BauGB</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>BelWertV</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Red Book</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>White Book</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Valuation_Record_Type</fullName>
        <active>true</active>
        <label>Valuation Record Type</label>
        <picklistValues>
            <picklist>ConflictReason__c</picklist>
            <values>
                <fullName>Application for Exclusive Mandate by CI is running</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pitch through other CI BL</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Sales by CI Letting Department</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Transaction via Colliers</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Inspection__c</picklist>
            <values>
                <fullName>%E2%80%8B%E2%80%8B%E2%80%8B%E2%80%8B%E2%80%8BDesktop</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Drive by</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Full site inspection</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>PurposeOfUse__c</picklist>
            <values>
                <fullName>Accounting</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Financing</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Internal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Purchase</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Sales purpose</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ReportFormat__c</picklist>
            <values>
                <fullName>Full Report</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Indication</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>One Pager</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Short Report</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Draft approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Draft sent</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Final</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Final sent</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>In process</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Review</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Review approved</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ValuationLanguage__c</picklist>
            <values>
                <fullName>English</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>German</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ValuationMethod__c</picklist>
            <values>
                <fullName>Comparison approach</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>DCF</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>DRC</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Hardcore Top Slice</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Income approach</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Investment Method</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Residual value</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Term %26 Reversion</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ValuationSoftware__c</picklist>
            <values>
                <fullName>Argus Enterprise</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Excel</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Lora</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>ValuationStandard__c</picklist>
            <values>
                <fullName>BauGB</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>BelWertV</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Red Book</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>White Book</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ConflictReasonIfConflictExistent</fullName>
        <active>true</active>
        <description>If the checkbox &quot;Conflict Existent&quot; is active, the user must select a Conflict Reason</description>
        <errorConditionFormula>AND(
 ConflictExistent__c = TRUE,
 ISPICKVAL( ConflictReason__c , &quot;&quot;) 
)</errorConditionFormula>
        <errorDisplayField>ConflictReason__c</errorDisplayField>
        <errorMessage>Please define the reason for the conflict.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>FieldEditOnlyByOpportunityOwner</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
Opportunity__r.Owner.Id &lt;&gt; $User.Id,
OR(
ISCHANGED( ConflictCheckCompleted__c ),
ISCHANGED( ConflictExistent__c ),
ISCHANGED( ConflictReason__c )
)
)</errorConditionFormula>
        <errorMessage>Only the Opportunity Owner can edit this field.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>OpportunityReadOnly</fullName>
        <active>true</active>
        <description>Prevents to edit the opportunity lookup, if status=final</description>
        <errorConditionFormula>AND(
RecordType.Id =&quot;0129E000000aCNg&quot;,
ISCHANGED(Opportunity__c)
)</errorConditionFormula>
        <errorMessage>Please note that the record cannot be edited as this valuation is set to &quot;Final&quot; or &quot;Final sent&quot;.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
