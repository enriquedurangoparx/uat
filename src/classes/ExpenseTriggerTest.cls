/**
 * @author ile
 * @copyright PARX
 */

@istest
private class ExpenseTriggerTest
{
    @testsetup
    private static void setup()
    {
        insert new List<ONB2__TriggerSettings__c>{
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceBeforeInsert'
            ),
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceBeforeUpdate'
            ),
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceLineItemBeforeInsert'
            ),
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceLineItemBeforeUpdate'
            )
        };

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    private static User generateUser()
    {
        User testUser = UnitTestDataTest.buildUser();

        insert testUser;
        return testUser;
    }

    private static Map<String, SObject> prepareData()
    {
        OpportunityHandlerTest.initializeProducts();// Needed to setup product, pricebook and pricebookentry.

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity project = UnitTestDataTest.buildOpportunity('Project Opportunity');
        project.PrimaryProperty__c = propertyObject.Id;
        project.BillingType__c = 'Time and Material';
        project.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
        project.ProjectStart__c = Date.today();
        project.CloseDate = Date.today().addDays(1);
        insert project;

        ProjectRole__c projectRole = new ProjectRole__c(Project__c = project.Id, DailyRate__c = 10, TargetAmountHours__c = 8, StartDate__c = System.today().addDays(-1), EndDate__c = System.today().addDays(10));
        insert projectRole;

        Product2 productA = new Product2(Name = 'A', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        Product2 productB = new Product2(Name = 'B', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        Product2 productC = new Product2(Name = 'C', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true, Family = 'Other', ProductCode = 'Other');
        insert new List<Product2>{productA, productB, productC};

        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.Product2Id = productA.Id;
        standardPricebookEntry.UnitPrice = 100;
        insert standardPricebookEntry;

        Pricebook2 valuationPricebook = [SELECT Id FROM Pricebook2 WHERE Name = :OpportunityHandler.VALUATION_PRICEBOOKNAME];

        PricebookEntry pricebookEntryA = standardPricebookEntry;//new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productA.Id, UnitPrice = 1, IsActive = true);
        //PricebookEntry pricebookEntryB = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productB.Id, UnitPrice = 1, IsActive = true);
        PricebookEntry pricebookEntryB = [SELECT Id FROM PriceBookEntry WHERE Pricebook2Id = :valuationPricebook.Id];//new PricebookEntry(Pricebook2Id = valuationPricebook.Id, Product2Id = productB.Id, UnitPrice = 1, IsActive = true);

        //insert new List<PricebookEntry>{pricebookEntryA, pricebookEntryB};

        Opportunity testOpportunity = project;


        Account accountA = UnitTestDataTest.buildAccount('Account A');
        insert accountA;

        OpportunityLineItem opportunityProduct = new OpportunityLineItem(PricebookEntryId = pricebookEntryB.Id, OpportunityId = testOpportunity.Id, Quantity = 1, UnitPrice = 10);
        insert opportunityProduct;

        ONB2__Template__c templateA = new ONB2__Template__c();
        insert templateA;

        ONB2__Invoice__c invoiceA = new ONB2__Invoice__c(Name = 'A', ONB2__Account__c = accountA.Id, ONB2__Template__c = templateA.Id);
        ONB2__Invoice__c invoiceB = new ONB2__Invoice__c(Name = 'A', ONB2__Account__c = accountA.Id, ONB2__Template__c = templateA.Id);
        insert new List<ONB2__Invoice__c>{invoiceA, invoiceB};

        ExpenseAccount__c expenseAccount = new ExpenseAccount__c(ExpenseKeeper__c = UserInfo.getUserId(), Year__c = System.today().year(), Month__c = System.now().format('MMMM'), Status__c = 'Open');
        insert expenseAccount;

        OpportunityLineItem__c opportunityLineItem = new OpportunityLineItem__c(Name = 'Test OpportunityLineItem', Product__c = productC.Id, OpportunityProductId__c = opportunityProduct.Id, Opportunity__c = testOpportunity.Id);
        insert opportunityLineItem;

        Expense__c expense = new Expense__c(Account__c = accountA.Id, ExpenseAccount__c = expenseAccount.Id, Product__c = opportunityLineItem.Id, 
            Opportunity__c = testOpportunity.Id, DateSpent__c = System.today(), DocumentAmountGross__c = 100, VAT__c = 10, Type__c = 'Other');

        insert expense;

        return new Map<String, SObject>{
            'Account' => accountA,
            'Product A' => productA,
            'Product B' => productB,
            'Price Book Entry A' => pricebookEntryA,
            'Price Book Entry B' => pricebookEntryB,
            'Opportunity' => testOpportunity,
            'Opportunity Product' => opportunityProduct,
            'OpportunityLineItem' => opportunityLineItem,
            'Invoice A' => invoiceA,
            'Invoice B' => invoiceB,

            'Project' => project,
            'Project Role' => projectRole,
            'Expense' => expense,
            'Expense Account' => expenseAccount
        };
    }

    @istest
    static void shouldValidateAccessOnExpenseDelete()
    {
        User testUser = generateUser();
        PermissionSetAssignment psAssignment;
        for (PermissionSet each : [select Name from PermissionSet where Name = 'ManageExpenses'])
        {
            psAssignment = new PermissionSetAssignment(AssigneeId = testUser.Id, PermissionSetId = each.Id);
        }

        insert psAssignment;

        Test.startTest(); if (true)
        {

            System.runAs(testUser)
            {
                Map<String, SObject> objectMap = prepareData();

                Account accountRecord = (Account) objectMap.get('Account');// new Account(Name = 'Test Account');
//                insert accountRecord;

                Opportunity opportunityRecord = (Opportunity) objectMap.get('Opportunity');// UnitTestDataTest.buildOpportunity('Test Opportunity');
//                opportunityRecord.AccountId = accountRecord.Id;
//                opportunityRecord.FrameworkContractPanel__c = 'United States';
//                insert opportunityRecord;

                ExpenseAccount__c expenseAccount = (ExpenseAccount__c) objectMap.get('Expense Account');

                Expense__c expense = new Expense__c(Account__c = accountRecord.Id, ExpenseAccount__c = expenseAccount.Id,
                    Product__c = [select Id from OpportunityLineItem__c where OpportunityProductId__c = :objectMap.get('Opportunity Product').Id limit 1].get(0).Id,
                    Opportunity__c = opportunityRecord.Id, DateSpent__c = System.today(), DocumentAmountGross__c = 100, VAT__c = 10, Type__c = 'Other');

                insert expense;
                delete expense;
            }

        }
        Test.stopTest();
    }

    @istest
    private static void shouldMaintainSchedulesRevenueAndAssociation()
    {
        Map<String, SObject> objectMap = prepareData();
        OpportunityLineItemSchedule lineItemSchedule;
        Expense__c expense;
        Test.startTest(); if (true)
        {
            lineItemSchedule = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today().addDays(10), Type = 'Revenue');
            insert lineItemSchedule;

            expense = ((Expense__c) objectMap.get('Expense')).clone(false, true);
            expense.Product__c = [select Id from OpportunityLineItem__c where OpportunityProductId__c = :objectMap.get('Opportunity Product').Id limit 1].get(0).Id;

            insert expense;
        }
        Test.stopTest();

        OpportunityLineItemSchedule existingLineItemSchedule = [select Id from OpportunityLineItemSchedule where Id = :lineItemSchedule.Id limit 1];
        Expense__c updatedExpense = [select OpportunityLineItemScheduleId__c from Expense__c where Id = :expense.Id limit 1];

        System.assertEquals(updatedExpense.OpportunityLineItemScheduleId__c, existingLineItemSchedule.Id);
        
    }

    public static testMethod void testSetProduct() 
    {

        Map<String, SObject> objectMap = prepareData();
        Expense__c expense = (Expense__c)objectMap.get('Expense');
        OpportunityLineItem__c opportunityLineItem = (OpportunityLineItem__c)objectMap.get('OpportunityLineItem');

        Expense__c updatedExpense = [
            SELECT Id, Product__c
            FROM Expense__c
            WHERE Id =: expense.Id
        ];

        system.assertEquals(updatedExpense.Product__c, opportunityLineItem.Id);

        delete opportunityLineItem;
        expense.Product__c = null;

        try {
            update expense;
        }
        catch (Exception e) 
        {
            system.assertEquals(e.getMessage().indexOf(Label.No_product_Other_found_in_the_Opportunity_Please_create_a_new_Opportunity_Pro) != -1, true);
        }

    }

}