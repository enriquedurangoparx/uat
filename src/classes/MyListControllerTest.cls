@IsTest
public with sharing class MyListControllerTest {
    @TestSetup
    static void setup(){
        // create a test user with the right profile
        User testUser = new User(FirstName='Peter',
            LastName='Tester',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Colliers Investment'].Id,
            Email='peter.tester@acme.ace',
            UserName='peter.tester@acme.ace',
            CompanyName='Acme Test Corp.',
            Alias='Tester1',
            TimeZoneSidKey = 'Europe/Berlin',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'de',
            LocaleSidKey = 'de_DE_EURO'
        );
        insert testUser;

        User testUser2 = new User(FirstName='Petra',
            LastName='Testerli',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Colliers Investment'].Id,
            Email='petra.testerli@acme.ace',
            UserName='petra.testerli@acme.ace',
            CompanyName='Acme Test Corp.',
            Alias='Tester2',
            TimeZoneSidKey = 'Europe/Berlin',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'de',
            LocaleSidKey = 'de_DE_EURO'
        );
        insert testUser2;
        User testUser3 = new User(FirstName='Ernst',
            LastName='Testerov',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Colliers Investment'].Id,
            Email='ernst.testerov@acme.ace',
            UserName='ernst.testerov@acme.ace',
            CompanyName='Acme Test Corp.',
            Alias='Tester2',
            TimeZoneSidKey = 'Europe/Berlin',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'de',
            LocaleSidKey = 'de_DE_EURO'
        );
        insert testUser3;
        // create Brand Object
        Brand__c brand = new Brand__c(Name='TestBrand', Type__c='Investor');
        insert brand;

        // create Accounts and Contacts for Assigning to Investors List
        List<Account> testAccounts = new List<Account>();
        List<Contact> testContacts = new List<Contact>();
        Account testAccount1 = new Account(Name='Apex Test Kunde 1', Brand__c=brand.Id, OwnerId=testUser.Id);
        testAccounts.add(testAccount1);
        Account testAccount2 = new Account(Name='Account 2 Apex Test-Class', Brand__c=brand.Id, OwnerId=testUser.Id);
        testAccounts.add(testAccount2);
        insert testAccounts;
        Contact testContactAccount1 = new Contact(Salutation='Mr.', FirstName='Arno', LastName='Tester', AccountId=testAccount1.Id);
        testContacts.add(testContactAccount1);
        Contact testContact2Account1 = new Contact(Salutation='Ms.', FirstName='Elfriede', LastName='Testermann', AccountId=testAccount1.Id);
        testContacts.add(testContact2Account1);
        Contact testContactAccount2 = new Contact(Salutation='Ms.', FirstName='Wilhelma', LastName='Test-Hammer', AccountId=testAccount2.Id);
        testContacts.add(testContactAccount2);
        insert testContacts;

        // create an InvestorsList__c Object for testing purposes
        List<InvestorsList__c> testInvestorsList = new List<InvestorsList__c>();
        InvestorsList__c testIL = new InvestorsList__c(Name='Test Investors List', Description__c='Test Investors List description', OwnerId=testUser.Id);
        testInvestorsList.add(testIL);
        InvestorsList__c testIL2 = new InvestorsList__c(Name='Second Test Investors List', Description__c='Second Test Investors List description', OwnerId=testUser2.Id);
        testInvestorsList.add(testIL2);
        InvestorsList__c testIL3 = new InvestorsList__c(Name='Third Test Investors List', Description__c='Third Test Investors List description', OwnerId=testUser3.Id);
        testInvestorsList.add(testIL3);
        insert testInvestorsList;
        // share the entries with user 1
        List<InvestorsList__Share> sharingEntries = new List<InvestorsList__Share>();
        InvestorsList__Share testIL2Share = new InvestorsList__Share(AccessLevel='Read', UserOrGroupId=testUser.Id, ParentId=testIL2.Id);
        sharingEntries.add(testIL2Share);
        InvestorsList__Share testIL3Share = new InvestorsList__Share(AccessLevel='Read', UserOrGroupId=testUser.Id, ParentId=testIL3.Id);
        sharingEntries.add(testIL3Share);
        insert sharingEntries;

        // create InvestorsListItems for testing
        List<InvestorsListItem__c> testInvestorsListItems = new List<InvestorsListItem__c>();
        InvestorsListItem__c testILI1 = new InvestorsListItem__c(Account__c=testAccount1.Id, InvestorsList__c=testIL.Id);
        testInvestorsListItems.add(testILI1);
        InvestorsListItem__c testILI2 = new InvestorsListItem__c(Account__c=testAccount2.Id, InvestorsList__c=testIL.Id);
        testInvestorsListItems.add(testILI2);
        insert testInvestorsListItems;

        // create InvestorsListItemContacts
        List<InvestorsListItemContact__c> testInvestorsListItemContacts = new List<InvestorsListItemContact__c>();
        InvestorsListItemContact__c testILIC1 = new InvestorsListItemContact__c(InvestorsListItem__c=testILI1.Id, Contact__c=testContactAccount1.Id);
        testInvestorsListItemContacts.add(testILIC1);
        InvestorsListItemContact__c testILIC2 = new InvestorsListItemContact__c(InvestorsListItem__c=testILI1.Id, Contact__c=testContact2Account1.Id);
        testInvestorsListItemContacts.add(testILIC2);
        InvestorsListItemContact__c testILIC3 = new InvestorsListItemContact__c(InvestorsListItem__c=testILI2.Id, Contact__c=testContactAccount2.Id);
        testInvestorsListItemContacts.add(testILIC3);
        insert testInvestorsListItemContacts;
    }

    @IsTest
    public static void testGetInvestors(){
        User usr = [SELECT Id, FirstName, LastName FROM User WHERE FirstName = 'Peter' AND CompanyName = 'Acme Test Corp.'];
        if(usr != null){
             System.runAs(usr) {
                // test without "my own" filter
                List<MyListController.investorsResult> result1 = MyListController.getInvestors(null, null);
                System.assertEquals(3, result1.size(), 'Result for my own list should only be 3.');
                // test with filter
                List<MyListController.investorsResult> result2 = MyListController.getInvestors(null, 'myOwnList');
                System.assertEquals(1, result2.size(), 'Result for my own list should only be 1.');
                // test with search
                List<MyListController.investorsResult> result3 = MyListController.getInvestors('Third', null);
                System.assertEquals(1, result3.size(), 'Result for search "Third" should only be 1.');
            }
        }
    }

    @IsTest
    public static void testSearchAccountOrContact(){
        List<Object> accountSearchResult = MyListController.searchAccountOrContact('Account', 'Apex Test ', null, 100);
        //System.debug('accountSearchResult: ' + accountSearchResult.size());
        System.assertEquals(1, accountSearchResult.size());
        List<Object> contactSearchResult = MyListController.searchAccountOrContact('Contact', 'Arno', null, 100);
        //System.debug('contactSearchResult: ' + contactSearchResult.size());
        System.assertEquals(1, contactSearchResult.size());
    }

    @IsTest
    public static void testSaveInvestorsListItems(){
        User usr = [SELECT Id, FirstName, LastName FROM User WHERE FirstName = 'Peter' AND CompanyName = 'Acme Test Corp.'];
        if(usr != null){
            System.runAs(usr) {
                // Contacts
                InvestorsList__c il = [SELECT Id FROM InvestorsList__c WHERE Name = 'Test Investors List'];
                Account newAccount = new Account(Name='A.C.M.E. Corp.');
                insert newAccount;
                List<Contact> newContacts = new List<Contact>();
                Contact nC1 = new Contact(Salutation='Ms.', FirstName = 'Jean-Marie', LastName='Testè', Email='jm-teste@acmecorp.com');
                newContacts.add(nC1);
                Contact nC2 = new Contact(Salutation='Ms.', FirstName = 'Marie-Elisabeth', LastName='Teschd', Email='me-teschd@acmecorp.com');
                newContacts.add(nC2);
                insert newContacts;
                List<MyListController.ContactSearchResult> csr = new List<MyListController.ContactSearchResult>();
                csr.add(new MyListController.ContactSearchResult(nC1.Id, '', '', '', '', newAccount.Id, '', '', '', '', '', ''));
                csr.add(new MyListController.ContactSearchResult(nC2.Id, '', '', '', '', newAccount.Id, '', '', '', '', '', ''));
                csr.add(new MyListController.ContactSearchResult(nC2.Id, '', '', '', '', newAccount.Id, '', '', '', '', '', ''));
                MyListController.saveContactInvestorsListItems(csr, il.Id);
                List<InvestorsListItem__c> testIL = [SELECT Id FROM InvestorsListItem__c WHERE InvestorsList__c =: il.Id];
                System.assertEquals(3, testIL.size(), 'Number of Investors should only be 3. Current size: ' + testIL.size());
                //System.debug(testIL.size());
                // Accounts
                Account newAccount2 = new Account(Name='Dyson inc.');
                insert newAccount2;
                List<MyListController.AccountSearchResult> asr = new List<MyListController.AccountSearchResult>();
                MyListController.saveAccountInvestorsListItems(asr, il.Id);
                List<InvestorsListItem__c> testIL2 = [SELECT Id FROM InvestorsListItem__c WHERE InvestorsList__c =: il.Id];
                //System.debug(testIL2.size());
                System.assertEquals(3, testIL2.size(), 'Number of Investors should only be 3. Current size: ' + testIL.size());
            }
        }
    }

    @IsTest
    public static void testGetListItems(){
        User usr = [SELECT Id, FirstName, LastName FROM User WHERE FirstName = 'Peter' AND CompanyName = 'Acme Test Corp.'];
        if(usr != null){
            System.runAs(usr) {
                InvestorsList__c il = [SELECT Id FROM InvestorsList__c WHERE Name = 'Test Investors List'];
                Map<String, Object> listItems = MyListController.getListItems(il.Id, 100, 100);
                //System.debug(ilis.size());
                List<MyListController.investorsListItem> items =
                        (List<MyListController.investorsListItem>) listItems.get(MyListController.ListItemType.INVESTOR_LIST_ITEM_CONTACT.name());
                System.assertEquals(3, items.size());
            }
        }
    }

    @IsTest
    public static void testGetInvestorListItems(){
        User usr = [SELECT Id, FirstName, LastName FROM User WHERE FirstName = 'Peter' AND CompanyName = 'Acme Test Corp.'];
        if(usr != null){
            System.runAs(usr) {
                InvestorsList__c il = [SELECT Id FROM InvestorsList__c WHERE Name = 'Test Investors List'];
                Map<String, Object> listItems = MyListController.getInvestorListItems(il.Id, 100);
                //System.debug(ilis.size());
                List<MyListController.investorsListItem> items =
                        (List<MyListController.investorsListItem>) listItems.get(MyListController.ListItemType.INVESTOR_LIST_ITEM_CONTACT.name());
                System.assertEquals(0, items.size());
            }
        }
    }

    @IsTest
    public static void testGetInvestorContactListItems(){
        User usr = [SELECT Id, FirstName, LastName FROM User WHERE FirstName = 'Peter' AND CompanyName = 'Acme Test Corp.'];
        if(usr != null){
            System.runAs(usr) {
                InvestorsList__c il = [SELECT Id FROM InvestorsList__c WHERE Name = 'Test Investors List'];
                Map<String, Object> listItems = MyListController.getInvestorContactListItems(il.Id, 100);
                //System.debug(ilis.size());
                List<MyListController.investorsListItem> items =
                        (List<MyListController.investorsListItem>) listItems.get(MyListController.ListItemType.INVESTOR_LIST_ITEM_CONTACT.name());
                System.assertEquals(3, items.size());
            }
        }
    }

    @IsTest
    public static void testEditSharing(){
        // fetch the test Users for testing purposes
        List<User> users = [SELECT Id, FirstName, LastName FROM User WHERE CompanyName = 'Acme Test Corp.'];
        User actionUser;
        User secondUser;
        User thirdUser;
        if(users != null && users.size()>0){
            for(User usr : users){
                if(usr.FirstName == 'Peter'){
                    actionUser = usr;
                } else if(usr.FirstName == 'Petra'){
                    secondUser = usr;
                } else if(usr.FirstName == 'Ernst'){
                    thirdUser = usr;
                }
            }
        }
        // run as original user the change of sharing
        System.runAs(actionUser) {
            InvestorsList__c testIL = [SELECT Id, Name FROM InvestorsList__c WHERE Name = 'Test Investors List'];
            Map<Id, String> usersMap = new Map<Id, String>{secondUser.Id => 'Read', thirdUser.Id => 'Edit'};
            MyListController.addSharing(testIL.Id, usersMap);
        }
        // do a select as second user for test read access
        System.runAs(secondUser){
            List<InvestorsList__c> secondILs = [SELECT Id, Name FROM InvestorsList__c WHERE Name = 'Test Investors List'];
            System.assertEquals(1, secondILs.size(), 'Test for read access not successful. Should have one record in result.');
            String exceptionMessage;
            if(secondILs != null && secondILs.size() > 0){
                try{
                    update secondILs.get(0);
                } catch(DmlException dmx){
                    exceptionMessage = dmx.getMessage();
                }
                System.assertNotEquals(null, exceptionMessage, 'Test for read only failed. Read only access should not be storeable.');
            }
        }
        System.runAs(thirdUser){
            List<InvestorsList__c> thirdILs = [SELECT Id, Name FROM InvestorsList__c WHERE Name = 'Test Investors List'];
            System.assertEquals(1, thirdILs.size(), 'Test for read/write access not successful. Should have one record in result.');
            String exceptionMessage;
            if(thirdILs != null && thirdILs.size() > 0){
                try{
                    update thirdILs.get(0);
                } catch(DmlException dmx){
                    exceptionMessage = dmx.getMessage();
                }
                System.assertEquals(null, exceptionMessage, 'Test for read/write failed. Object cannot be stored.');
            }
        }
    }

    @IsTest
    public static void testsaveList(){
        List<MyListController.investorsResult> result1 = MyListController.getInvestors('Test Investors List', null);

        String clonedListId = MyListController.saveList(true, System.JSON.serialize(result1[0]), 'Copy Name', 'Copy Description', 'public', '{}');

        InvestorsList__c clonedInvestorsList = [SELECT ID, Name, Description__c, (SELECT Account__c FROM Investors_List_Items__r) FROM InvestorsList__c WHERE ID = :clonedListId LIMIT 1];

        System.assertEquals(clonedInvestorsList.Name,'Copy Name');
        System.assertEquals(clonedInvestorsList.Description__c,'Copy Description');

        List<InvestorsListItem__c> listClonedInvestorsListItem = clonedInvestorsList.Investors_List_Items__r;
        System.assertEquals(listClonedInvestorsListItem.size(), 2);

        Set<ID> setInvestorsListItemIDs = new Set<ID>();
        for(InvestorsListItem__c item : listClonedInvestorsListItem){
            setInvestorsListItemIDs.add(item.Id);
        }

        List<InvestorsListItemContact__c> listItemContacts = new List<InvestorsListItemContact__c>([SELECT ID FROM InvestorsListItemContact__c WHERE InvestorsListItem__c IN :setInvestorsListItemIDs]);
        System.assertEquals(listItemContacts.size(), 3);
    }

    @IsTest
    public static void testGetRelatedInvestorsListAccount()
    {
        Account acc = TestDataFactory.createAccount('Test Acc');
        insert acc;

        InvestorsList__c investorList = TestDataFactory.createInvestorList('Test Investor List');
        insert investorList;

        InvestorsListItem__c investorListItem = TestDataFactory.createInvestorListItem(investorList.Id, acc.Id);
        insert investorListItem;

        String resp = MyListController.getRelatedInvestorsList('Account', acc.Id);

        System.assertEquals(true, resp.contains(investorList.Id), 'Wrong list was returned');
    }

    @IsTest
    public static void testGetRelatedInvestorsListContact()
    {
        Account acc = TestDataFactory.createAccount('Test Acc');
        insert acc;

        List<Contact> contactsList = TestDataFactory.createContacts(acc.Id, 1);
        insert contactsList;

        InvestorsList__c investorList = TestDataFactory.createInvestorList('Test Investor List');
        insert investorList;

        InvestorsListItem__c investorListItem = TestDataFactory.createInvestorListItem(investorList.Id, acc.Id);
        insert investorListItem;

        InvestorsListItemContact__c investorListItemContact = TestDataFactory.createInvestorListItemContact(investorListItem.Id, contactsList[0].Id);
        insert investorListItemContact;

        String resp = MyListController.getRelatedInvestorsList('Contact', contactsList[0].Id);

        System.assertEquals(true, resp.contains(investorList.Id), 'Wrong list was returned');
    }

    @IsTest
    public static void testGetAccountRelatedAcquisitionProfiles()
    {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        List<Contact> newContactList = TestDataFactory.createContacts(newAccount.Id, 5);
        insert newContactList;

        List<AcquisitionProfile__c> acqProfilesList = TestDataFactory.createAcquisitionProfiles(newAccount.Id, newContactList[0].Id, 5);
        for (AcquisitionProfile__c acqp : acqProfilesList)
        {
            acqp.WALTFrom__c = 6;
            acqp.TotalAreaFrom__c = 9;
            acqp.TotalAreaTo__c = 20;
            acqp.InvestorRepresentative__c = newContactList[0].id;
            acqp.TypeOfUse__c = 'Office / Mixed-use';
            acqp.OccupancyRate__c = 5;
            acqp.ConversionProperty__c = true;
        }
        insert acqProfilesList;

        Test.startTest();

        System.assertEquals(acqProfilesList.size(), MyListController.getAccountRelatedAcquisitionProfiles(newAccount.Id).size(),
                    'We expect all Acquisition Profiles to be selected.');

        Test.stopTest();
    }
}