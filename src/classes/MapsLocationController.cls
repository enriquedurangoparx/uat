public class MapsLocationController
{
    @AuraEnabled
    public static List<Location> getLocations(String currentId, List<SObject> sObjectList, String sObjectName, String fields, String geolocationField)
    {
        List<Location> loc = new List<Location>();
        Set<Id> poacjIds = new Set<Id>();
        system.debug(sObjectName);
        String listType = 'List<' + sObjectName + '>';
        //Map<Id, SObject> m = new Map<Id, SObject>((List<sObject>)sObjectList);
        System.debug('fields' + fields);
        List<SObject> castRecords = (List<SObject>)Type.forName(listType).newInstance();
        castRecords.addAll(sObjectList);

        Map<Id, SObject> m = new Map<Id, SObject>(castRecords);
        //List<GG> stageList = (List<GG>)JSON.deserialize(fields, GG.class);

        String aa = String.valueOf(fields).substring(1, String.valueOf(fields).length()-1);
        System.debug('aa === ' + aa);
        List<String> ddd = new List<String>();
        List<String> aaa = aa.split('},');
        System.debug('aaa === ' + aaa);
        for (Integer i=0; i < aaa.size(); i++)
        {
            if (i == aaa.size() -1)
            {
                ddd.add(aaa.get(i));
            }
            else
            {
                ddd.add(aaa.get(i) + '}');
            }

            //Map<String, Object> stageList = (LMap<String, Object> )JSON.deserializeUntyped(aa);
        }
            //system.debug(stageList);



        //List<String> mm = (List<String>)JSON.deserializeUntyped(fields);


        String longitude = geolocationField.substring(0, geolocationField.length() -1) + 'Longitude__s';
        String latitude = geolocationField.substring(0, geolocationField.length() -1) + 'Latitude__s';
        Set<Id> g = m.keySet();
        String query = 'Where  id in : g and ' + longitude + ' != null and '+ longitude +' != 0.0 and ' + latitude + ' != null and '+ latitude +' != 0.0';

        castRecords = querySObject(sObjectName, ddd, query, g, longitude, latitude);
        for (SObject p : castRecords)
        {
            System.debug(p);
            GeoLocation geoInfo = new GeoLocation();
            geoInfo.Latitude = latitude.contains('.') ?  getLookupFieldValue(p, latitude) : String.valueOf(p.get(latitude));
            //geoInfo.Latitude = String.valueOf(p.PropertyObject__r.Geolocation__Latitude__s);
            geoInfo.Longitude = longitude.contains('.') ?  getLookupFieldValue(p, longitude) : String.valueOf(p.get(longitude));
            //geoInfo.Longitude = String.valueOf(p.PropertyObject__r.Geolocation__Longitude__s);
            Map<String, Object> fieldMap = (Map<String, Object>)JSON.deserializeUntyped(ddd.get(0));
            Location locDetail = new Location();
            locDetail.icon = 'action:map';
            locDetail.title =  String.valueOf(fieldMap.get('fieldName')).contains('.') ? getLookupFieldValue(p, (String)fieldMap.get('fieldName') ) : String.valueOf(p.get((String)fieldMap.get('fieldName') ));

            locDetail.description = getDescription(ddd, p);

            //p.PropertyObject__r.LastModifiedDate + '<br/ >' +
              //      p.PropertyObject__r.AnnualContractualRent__c + '<br/ >' + p.PropertyObject__c + '<a target="_top" href="' + System.Url.getOrgDomainUrl().toExternalForm() + '/' + p.PropertyObject__c + '" >' + p.PropertyObject__r.Name + '</a>';  // t.Account__r.Name + 'sdf';
            locDetail.location = geoInfo;
            loc.add(locDetail);
        }

        return loc ;
    }

    public static String getLookupFieldValue(Sobject record, String fieldAPIName)
    {
        List<String> fieldAPIRelNames = fieldAPIName.split('\\.');

        SObject obj = record;
        String fieldValue;
        for (Integer i = 0; i < fieldAPIRelNames.size() ; i++)
        {
            String pathName = fieldAPIRelNames[i];
            if (i == fieldAPIRelNames.size() - 1)
            {
                fieldValue = (obj == null) ? null : String.valueOf(obj.get(pathName));
            }
            else
            {
                obj = obj.getSObject(pathName);
            }
        }
        return fieldValue;
    }

    public static List<sObject> querySObject(String sObjectName, List<String> fields, String whereClause, Set<Id> g, String lo, String la) {
        String query = '';
        String fieldString = ' Id, '+ lo + ',' + la + ',' ;

        Set<String> fieldSet = new Set<String>();
        for (String field : fields)
        {
            System.debug('field === ' + field);
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(field);

            if (m.get('fieldName') != null && (String)m.get('fieldName') != 'Id')
            {
                fieldSet.add((String) m.get('fieldName'));
            }
        }

        fieldString = fieldString + String.join(new List<String>(fieldSet), ',');

        query = 'SELECT ' + fieldString + ' FROM ' + sObjectName + ' ' + whereClause + ' limit 1000000';
        System.debug(query);

        try
        {
            return Database.query(query);
        }
        catch (Exception ex)
        {
            AuraHandledException e = new AuraHandledException(ex.getMessage() + ' Executed Query: '  + query);
            throw e;
        }

    }

    public static String getDescription(List<String> fields, SObject p)
    {
        String description = '';
        for (String field : fields)
        {
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(field);
            String pValue = String.valueOf(m.get('fieldName')).contains('.') ? getLookupFieldValue(p, (String)m.get('fieldName') ) : String.valueOf(p.get((String)m.get('fieldName') ));
            if (m.get('type') == 'STRING' && pValue != null)
            {
                description = description + '<b>' + m.get('label') + ': </b> ' + pValue + '<br/>';
            }
        }
        return description;
    }
    public class Location {
        @AuraEnabled
        public String icon { get; set; }
        @AuraEnabled
        public String title { get; set; }
        @AuraEnabled
        public String description { get; set; }
        @AuraEnabled
        public GeoLocation location { get; set; }
    }

    public class GeoLocation {
        @AuraEnabled
        public String Street { get; set; }
        @AuraEnabled
        public String PostalCode { get; set; }
        @AuraEnabled
        public String City { get; set; }
        @AuraEnabled
        public String Latitude { get; set; }
        @AuraEnabled
        public String Longitude { get; set; }
        @AuraEnabled
        public String Country { get; set; }

    }
}

/*
*   Controller for lightning Maps interaction.
*
*   @author yle
*   @copyright PARX
*/
//Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(sObjectList);
//System.debug(m);
/*List< TransactionAccountContactJunction__c> tacs = [
        select Transaction__r.City__c, Transaction__r.Country__c,
                Transaction__r.PostalCode__c, Transaction__r.Street__c, Account__r.Name, TypeOfRelationship__c
        from TransactionAccountContactJunction__c
        where Account__r.Brand__r.Id = :currentId
];

List<Location> loc = new List<Location>();
for (TransactionAccountContactJunction__c t : tacs) {
    GeoLocation geoInfo = new GeoLocation();
    geoInfo.Street = t.Transaction__r.Street__c;
    geoInfo.PostalCode = t.Transaction__r.PostalCode__c;
    geoInfo.City = t.Transaction__r.City__c;
    geoInfo.Country = t.Transaction__r.Country__c;
    Location locDetail = new Location();
    locDetail.icon = 'action:map';
    locDetail.title = t.Account__r.Name + ' - ' + t.Transaction__r.City__c;
    locDetail.description = '<a target="_top" href="' + System.Url.getOrgDomainUrl().toExternalForm() + '/' + t.Account__c + '" >' + t.Account__r.Name + ' - ' + t.Transaction__r.City__c + '</a>';  // t.Account__r.Name + 'sdf';
    locDetail.location = geoInfo;
    loc.add(locDetail);
}*/

/*List<SObject> castRecords = (List<SObject>)Type.forName(listType).newInstance();
    	castRecords.addAll(sObjectList);

        Map<Id, SObject> m = new Map<Id, SObject>(castRecords);



        String longitude = geolocationField.substring(0, geolocationField.length() -1) + '__Longitude__s';
        String latitude = geolocationField.substring(0, geolocationField.length() -1) + '__Latitude__s';
        String query = 'Where id in : m.keySet() and longitude != null and latitude != null' ;

        query = 'SELECT ' + fieldString + ' FROM ' + sObjectName + ' ' + query + ' limit 1000000';
        try
        {
             //castRecords = Database.query(query);
        }
        catch (Exception ex)
        {
            AuraHandledException e = new AuraHandledException(ex.getMessage() + ' Executed Query: '  + query);
            throw e;
        }*/
/*List< PropertyObjectAccountContactJunction__c> poas = [
        select PropertyObject__r.Name,
                PropertyObject__r.Geolocation__Latitude__s, PropertyObject__r.Geolocation__Longitude__s, PropertyObject__r.LastModifiedDate, Account__r.AnnualRevenue,
                PropertyObject__r.AnnualContractualRent__c
        from PropertyObjectAccountContactJunction__c
        where id in : m.keySet() and PropertyObject__r.Geolocation__Latitude__s != null and PropertyObject__r.Geolocation__Longitude__s != null
];
for (PropertyObjectAccountContactJunction__c p : poas) {
    GeoLocation geoInfo = new GeoLocation();
    //geoInfo.Latitude = String.valueOf(p.get(latitude));
    geoInfo.Latitude = String.valueOf(p.PropertyObject__r.Geolocation__Latitude__s);
    //geoInfo.Longitude = String.valueOf(p.get(longitude));
    geoInfo.Longitude = String.valueOf(p.PropertyObject__r.Geolocation__Longitude__s);

    Location locDetail = new Location();
    locDetail.icon = 'action:map';
    locDetail.title = p.PropertyObject__r.Name;
    locDetail.description = p.PropertyObject__r.LastModifiedDate + '<br/ >' +
            p.PropertyObject__r.AnnualContractualRent__c + '<br/ >' + p.PropertyObject__c + '<a target="_top" href="' + System.Url.getOrgDomainUrl().toExternalForm() + '/' + p.PropertyObject__c + '" >' + p.PropertyObject__r.Name + '</a>';  // t.Account__r.Name + 'sdf';
    locDetail.location = geoInfo;
    loc.add(locDetail);
}*/