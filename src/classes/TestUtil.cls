/**
*	
*	@author npo
*	@copyright PARX
*/
public class TestUtil
{
	/**
	 * Create User, upsert only if doUpsert is true
	 *
	 * @param userProfile Profile for the user,
			  if the profile not exists,
			  the method uses the system administrator profile
	 *
	 * @param withPersist upsert into DB if <code>true</code>
	 */
	public static User createUser(
			String userName,
			String userProfile,
			String userRole,
			Boolean withPersist)
	{
		Profile profile = null;
		UserRole role = null;

		try
		{
			profile = [SELECT  Id
			FROM    profile
			WHERE   name = :userProfile
			LIMIT   1];
		}
		catch (Exception e)
		{
			profile = [SELECT  Id
			FROM    profile
			WHERE   name LIKE 'System%Administrator%'
			LIMIT   1];
		}

		User user = new User (alias = 'MTstTAU',
				username = userName,
				email = 'newuser@test.org.com',
				emailencodingkey = 'UTF-8',
				lastname = 'TEST_USER_LASTNAME',
				languagelocalekey = 'en_US',
				localesidkey = 'en_US',
				profileid = profile.Id,
				timezonesidkey = 'Europe/Berlin');

		//dme : PARXIMPL-3488 : Added PortalType filter
		if (userRole != null)
		{
			try
			{
				role = [SELECT  Id
				FROM    UserRole
				WHERE   DeveloperName = :userRole
				LIMIT   1];
			}
			catch (Exception e)
			{
				role = [SELECT  Id
				FROM    UserRole
				WHERE PortalType = 'None'
				LIMIT   1];
			}
			user.UserRoleId = role.Id;
		}

		if (withPersist)
		{
			insert user;
		}
		return user;
	}
}