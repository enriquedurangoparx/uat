/**
* Is used within the lwc areaMap to search for PropertyObject__c.
*
* @author:  Egor Markuzov (egor.markuzov@parx.com)
*
* @history:
* version		    | author						| changes
* ====================================================================================
* 0.1 22.10.2019	| Egor Markuzov (egor.markuzov@parx.com)	            | inital version.
*/
public class LWCAreaMapController {
    
    @AuraEnabled(cacheable=true)
    public static List<PropertyObject__c> getPropertyObjects(Id recordId)
    {
        return [SELECT Id, Name, YieldActual__c, Thumbnail__c, PropertyDescriptionDe__c, SqmInTotal__c,
                    sqmOffice__c, YearofConstruction__c, Geolocation__Latitude__s, Geolocation__Longitude__s, Street__c,
                    PostalCode__c, City__c, Country__c 
                FROM PropertyObject__c
                WHERE Area__c =:recordId];
    }
}