/**
 * @description Test class for MapsLocationController
 * @author ema
 *
 * @copyright PARX
 */
@IsTest
private class SetObjectGeolocationControllerTest
{
    @IsTest
    static void testGetLocationsPositive() 
    {
        String jsonObjectList = TestDataFactory.jsonPropertyObject();
        List<PropertyObject__c> objectListToInsert = (List<PropertyObject__c>) JSON.deserialize(jsonObjectList, List<PropertyObject__c>.class);
        insert objectListToInsert;

        System.assertEquals(true, SetObjectGeolocationController.getObjectData(objectListToInsert[0].Id).contains(objectListToInsert[0].Id), 'Wrong record was queried.');
    }

    @IsTest
    static void testGetLocationsNegative() 
    {
        String jsonObjectList = TestDataFactory.jsonPropertyObject();
        List<PropertyObject__c> objectListToInsert = (List<PropertyObject__c>) JSON.deserialize(jsonObjectList, List<PropertyObject__c>.class);
        insert objectListToInsert;
        try {
            String jsonResp = SetObjectGeolocationController.getObjectData('0011x00000xpqzkAAA');
        }
        catch (Exception e)
        {
            System.assert(String.isNotBlank(e.getMessage()));
        }
    }

    @IsTest
    static void tesSetObjectCoordinatesPositive() 
    {
        String jsonObjectList = TestDataFactory.jsonPropertyObject();
        List<PropertyObject__c> objectListToInsert = (List<PropertyObject__c>) JSON.deserialize(jsonObjectList, List<PropertyObject__c>.class);
        insert objectListToInsert;
        
        String jsonResp = SetObjectGeolocationController.setObjectCoordinates(52.0888901, 23.6802697, objectListToInsert[0].Id);

        System.assertEquals(true, jsonResp.contains('52.0888901') && jsonResp.contains('23.6802697'), 'Coordinates were not set');
    }

    @IsTest
    static void tesSetObjectCoordinatesNegative() 
    {
        String jsonObjectList = TestDataFactory.jsonPropertyObject();
        List<PropertyObject__c> objectListToInsert = (List<PropertyObject__c>) JSON.deserialize(jsonObjectList, List<PropertyObject__c>.class);
        insert objectListToInsert;
        try {
            String jsonResp = SetObjectGeolocationController.setObjectCoordinates(52.0888901, 23.6802697, null);
        }
        catch (Exception e)
        {
            System.debug('Error: ' + e.getMessage());
            System.assert(String.isNotBlank(e.getMessage()));
        }
    }
}