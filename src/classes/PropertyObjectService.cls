/**
* PropertyObject service class
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 31.01.2020    | Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
* 0.2 13.02.2020    | Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 1977] Added aggregateObjectsToPortfolio.
* 0.3 17.02.2020    | Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 1977] Added logic to set helpfield () to evaluate if Portfolio__c could be changed or not.
* 0.4 26.02.2020    | Maksim Fedorenko (maksim.fedorenko@parx.com)          | Added updateGeolocationData()
*/
public with sharing class PropertyObjectService
{
    public static final Map<SObjectField, SObjectField> sourceTargetFieldMap = new Map<SObjectField, SObjectField> {
            PropertyObject__c.WALT__c => Opportunity.WALTInYears__c,
            PropertyObject__c.GrossYield__c => Opportunity.GrossYield__c,
            PropertyObject__c.NetYield__c => Opportunity.NetYield__c,
            PropertyObject__c.FactorActual__c => Opportunity.FactorActual__c,
            PropertyObject__c.FactorTarget__c => Opportunity.FactorTarget__c,
            PropertyObject__c.IRR__c => Opportunity.IRR__c,
            PropertyObject__c.CoC__c => Opportunity.CoC__c,
            PropertyObject__c.AmountOfSpaceLet__c => Opportunity.AmountOfSpaceLet__c,
            PropertyObject__c.SqmPlot__c => Opportunity.PlotSqm__c,
            PropertyObject__c.SqmInTotal__c => Opportunity.TotalSqm__c,
            PropertyObject__c.UnitMeasuring__c => Opportunity.UnitMeasurement__c
    };

    public static final Map<SObjectField, SObjectField> sourceFieldMapForAggregation = new Map<SObjectField, SObjectField> {
        PropertyObject__c.Country__c => Portfolio__c.Countries__c,
        PropertyObject__c.Market__c => Portfolio__c.Markets__c,
        PropertyObject__c.Submarket__c => Portfolio__c.Submarkets__c,
        PropertyObject__c.FederalState__c => Portfolio__c.FederalState__c,
        PropertyObject__c.MacroLocation__c => Portfolio__c.MacroLocations__c,
        PropertyObject__c.MicroLocation__c => Portfolio__c.MicroLocations__c
    };

    /**
     * @azure US 705 Übernahme der Feldwerte aus dem verlinkten Objekt / Portfolio in die Opportunity
     * @description Copies the PropertyObject fields, described in the sourceTargetFieldMap to respective opportunity records
     * @param newRecords List of new PropertyObject records
     */
    public static void copyDataToOpportunity (List<PropertyObject__c> newRecords)
    {
        Map<Id, List<Opportunity>> propertyObjectIdOpportunityListMap = getPropertyObjectIdOpportunityListMap(new Map<Id, PropertyObject__c>(newRecords));
        OpportunityService.copyDataToOpportunity((List<SObject>) newRecords, propertyObjectIdOpportunityListMap, sourceTargetFieldMap);
    }

    /**
     * Returns a map of PropertyObject => Opportunity list for a given map of PropertyObject
     * @param propertyIdList Map of PropertyObject__c records
     * @return Map of PropertyObjectId => Opportunity list
     */
    public static Map<Id, List<Opportunity>> getPropertyObjectIdOpportunityListMap(Map<Id, PropertyObject__c> propertyObjectMap)
    {
        Map<Id, List<Opportunity>> propertyIdOpportunityListMap = new Map<Id, List<Opportunity>>();
        List<PropertyObject__c> propertyObjects = [
                SELECT Id, (
                        SELECT Id, WALTInYears__c, GrossYield__c, NetYield__c, FactorActual__c, FactorTarget__c, IRR__c, CoC__c, AmountOfSpaceLet__c,
                                PlotSqm__c, TotalSqm__c, UnitMeasurement__c, IsClosed
                        FROM Opportunities__r)
                FROM PropertyObject__c
                WHERE Id IN :propertyObjectMap.keySet()
        ];

        for (PropertyObject__c propertyObject : propertyObjects)
        {
            propertyIdOpportunityListMap.put(propertyObject.Id, propertyObject.Opportunities__r);
        }
        return propertyIdOpportunityListMap;
    }

    /**
     * Returns a map of Id => PropertyObject__c records for a given propertyIdSet
     * @param propertyIdSet Set of PropertyObject__c ids
     * @return Map of Id => PropertyObject__c records for a given propertyIdSet
     */
    public static Map<Id, PropertyObject__c> getPropertyObjectMap(Set<Id> propertyIdSet)
    {
        Map<Id, PropertyObject__c> result = new Map<Id, PropertyObject__c>();
        if (!propertyIdSet.isEmpty())
        {
            result = new Map<Id, PropertyObject__c>([
                    SELECT WALT__c, GrossYield__c, NetYield__c, FactorActual__c, FactorTarget__c, IRR__c, CoC__c, AmountOfSpaceLet__c, SqmPlot__c, SqmInTotal__c,
                            UnitMeasuring__c
                    FROM PropertyObject__c
                    WHERE Id IN :propertyIdSet
            ]);
        }
        return result;
    }

    /**
     * @azure SFINV/US 1977: Aggregation der Feldwerte aus verlinkten Objekten zum Portfolio.
     * @description Aggregate fields from all related PropertyObject__c to a portfolio.
     * @param newRecords List of new PropertyObject__c records.
     */
    public static void aggregateObjectsToPortfolio(List<PropertyObject__c> newRecords, List<PropertyObject__c> oldRecords){
        Boolean isUpdateDML = (newRecords != null && oldRecords != null ? true : false);
        Set<Id> setOfPortfolioId = new Set<Id>();
        Map<Id, List<PropertyObject__c>> mapOfPropertyByPortfolioId = new Map<Id, List<PropertyObject__c>>();//Key: Portfolio__c.Id
        Map<Id, PropertyObject__c> mapOfOldPropertyById = null;
        if(isUpdateDML){
            mapOfOldPropertyById = new Map<Id, PropertyObject__c>(oldRecords);
        }
        
        // Extract Portfolio__c-Id for every PropertyObject__c.
        for(PropertyObject__c property : newRecords){
            String newPortfolioId = property.Portfolio__c;
            String oldPortfolioId = (isUpdateDML ? mapOfOldPropertyById.get(property.Id).Id : null);

            // In case if reference to Portfolio__c changed on PropertyObject__c .
            if(isUpdateDML && (newPortfolioId != oldPortfolioId)){
                setOfPortfolioId.add(oldPortfolioId);
            }

            if(String.isNotBlank(newPortfolioId)){
                setOfPortfolioId.add(newPortfolioId);
            }
        }

        List<Portfolio__c> listPortfolio = new List<Portfolio__c>(
            [SELECT Id, Countries__c, FederalState__c, MacroLocations__c, Markets__c, MicroLocations__c, Submarkets__c, 
                (SELECT Id, Country__c, FederalState__c, MacroLocation__c, Market__c, MicroLocation__c, Submarket__c FROM Property_Objects__r)
            FROM Portfolio__c WHERE ID IN :setOfPortfolioId]
        );

        for(Portfolio__c portfolio : listPortfolio){
            Map<String, Set<String>> mapOfDistinctValuesBySourceFieldApiName = new Map<String, Set<String>>();//Key: api-name for sourceField, Value: Distinct values over all PropertyObject__c.
            Integer numberOfRelatedObjects = portfolio.Property_Objects__r.size();

            for(PropertyObject__c propertyObject : portfolio.Property_Objects__r){
                for(SObjectField sourceField : sourceFieldMapForAggregation.keySet()){
                    String sourceFieldApiName = sourceField.getDescribe().getName();
                    String value = String.valueOf(propertyObject.get(sourceField));

                    if(! mapOfDistinctValuesBySourceFieldApiName.containsKey(sourceFieldApiName)){
                        mapOfDistinctValuesBySourceFieldApiName.put(sourceFieldApiName, new Set<String> { value });
                    } else {
                        mapOfDistinctValuesBySourceFieldApiName.get(sourceFieldApiName).add(value);
                    }
                }
            }

            // Update fields on Portfolio__c.
            for(SObjectField sourceField : sourceFieldMapForAggregation.keySet()){
                String sourceFieldApiName = sourceField.getDescribe().getName();
                SObjectField targetField = sourceFieldMapForAggregation.get(sourceField);
                String targetFieldApiName = targetField.getDescribe().getName();
                String value = String.join(new List<String>(mapOfDistinctValuesBySourceFieldApiName.get(sourceFieldApiName)), ';');

                portfolio.put(targetFieldApiName, value);
            }
            
            // Prevent execution on apex validation rule on Portfolio__c.
            if(numberOfRelatedObjects > 0){
                portfolio.put('PreventPartialChanging__c', false);
            }
        }

        update listPortfolio;
    }

    /**
     * @azure SFINV/US 2254: Property - Get Geodata from Address
     * @description Updates property Geolocation data
     * @param newRecords List of new PropertyObject__c records.
     * @param oldRecords List of old PropertyObject__c records.
     */
    public static void updateGeolocationData(List<SObject> newRecords, List<SObject> oldRecords) {
        List<PropertyObject__c> propertiesForUpdate = new List<PropertyObject__c>();

        for (PropertyObject__c property : (List<PropertyObject__c>) newRecords)
        {
            if (oldRecords != null && (property.Street__c == null || property.City__c == null || property.Country__c == null))
            {
                property.Geolocation__Longitude__s = null;
                property.Geolocation__Latitude__s = null;
                continue;
            }

            Map<Id, SObject> oldRecordsMap = oldRecords != null ? new Map<Id, SObject>(oldRecords) : new Map<Id, SObject>();
            PropertyObject__c oldProperty = oldRecords != null ? (PropertyObject__c) oldRecordsMap.get(property.Id) : null;
            if (oldProperty == null || (oldProperty != null && (property.PostalCode__c != oldProperty.PostalCode__c || property.Street__c != oldProperty.Street__c
                    || property.City__c != oldProperty.City__c || property.Country__c != oldProperty.Country__c)))
            {
                propertiesForUpdate.add(property);
            }
        }

        if (!propertiesForUpdate.isEmpty())
        {
            Database.executeBatch(new PropertyGeolocationUpdaterBatch(propertiesForUpdate), 1);
        }
    }
}