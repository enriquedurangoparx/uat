/**
* Handle business logic for FeeSharingTrigger by the trigger
*
* @author: <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.9 01.04.2020	| Nadzeya Polautsava (nadzeya.polautsava@parx.com)	    | initial version - Bug1948.
*/
public inherited sharing class FeeSharingTrigger extends ATrigger
{
    public override void hookAfterInsert(List<SObject> records)
    {
        FeeSharingService.handleOpportunitySharing(records, null);
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        FeeSharingService.handleOpportunitySharing(records, oldRecords);
    }

    public override void hookAfterDelete(List<SObject> records)
    {
        FeeSharingService.deleteOpportunitySharing(records);
    }
}