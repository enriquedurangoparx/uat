/**
*  @description Wrapper class over 'Brand__c' related sobjects for related and potentially reusable logic.
*  @author ikl
*  @copyright PARX
*/
public without sharing class BrandService
{
	public static final Integer TEXT_FIELD_LENGTH_LIMIT = 255;
	public static final String COMMA = ',';
	public static final String SPACE = ' ';
	public static final String EMPTY_STRING = '';

	public static void updateKeyAccountManagerFieldOnRelatedBrandRecord(Set<Id> brandIds)
	{
		List<Brand__c> brands = [SELECT Id, KeyAccountManager__c, (SELECT Id, User__r.FirstName, User__r.LastName
																   FROM Key_Account_Managers__r ORDER BY User__r.LastName ASC)
								 FROM Brand__c
								 WHERE Id IN :brandIds];

		String currentKeyAccountManagerString;
		for (Brand__c brand : brands)
		{
			currentKeyAccountManagerString = EMPTY_STRING;
			for (KeyAccountManager__c keyAccountManager : brand.Key_Account_Managers__r)
			{
				currentKeyAccountManagerString += keyAccountManager.User__r.FirstName + SPACE + keyAccountManager.User__r.LastName + COMMA + SPACE;
			}
			if (currentKeyAccountManagerString.length() > TEXT_FIELD_LENGTH_LIMIT)
			{
				currentKeyAccountManagerString = currentKeyAccountManagerString.substring(0, TEXT_FIELD_LENGTH_LIMIT);
			}
			brand.KeyAccountManager__c = currentKeyAccountManagerString.substringBeforeLast(COMMA);
		}
		update brands;
	}
}