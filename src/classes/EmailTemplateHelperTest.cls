/**
* Handle test logic for EmailTemplateHelper
*
* @author: <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 10.04.2020	| Nadzeya Polautsava (nadzeya.polautsava@parx.com)	    | initial version - Bug1948.
*/
@IsTest
public class EmailTemplateHelperTest
{
    @IsTest
    static void testSelectEmailTemplates()
    {
        // test is just for coverage
        EmailTemplateHelper.selectEmailTemplates();
    }
}