/**
 * Created by ille on 2019-04-08.
 */

@istest
private class OpportunityProductTriggerTest
{
    private static Map<String, SObject> prepareData()
    {
        OpportunityHandlerTest.initializeProducts();// Needed to setup product, pricebook and pricebookentry.
        
        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        Product2 productA = new Product2(Name = 'A');
        Product2 productB = new Product2(Name = 'B');
        insert new List<Product2>{productA, productB};

        //PricebookEntry pricebookEntryA = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productA.Id, UnitPrice = 1, IsActive = true);
        //PricebookEntry pricebookEntryB = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productB.Id, UnitPrice = 1, IsActive = true);
        
        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.Product2Id = productA.Id;
        standardPricebookEntry.UnitPrice = 100;
        insert standardPricebookEntry;

        Pricebook2 valuationPricebook = [SELECT Id FROM Pricebook2 WHERE Name = :OpportunityHandler.VALUATION_PRICEBOOKNAME];

        PricebookEntry pricebookEntryA = standardPricebookEntry;//new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productA.Id, UnitPrice = 1, IsActive = true);
        PricebookEntry pricebookEntryB = [SELECT Id FROM PriceBookEntry WHERE Pricebook2Id = :valuationPricebook.Id];//new PricebookEntry(Pricebook2Id = valuationPricebook.Id, Product2Id = productB.Id, UnitPrice = 1, IsActive = true);

        //insert new List<PricebookEntry>{pricebookEntryA, pricebookEntryB};

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity testOpportunity = UnitTestDataTest.buildOpportunity('Test Opportunity', propertyObject);
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
        testOpportunity.ProjectStart__c = Date.today();
        testOpportunity.CloseDate = Date.today().addDays(3);
        insert testOpportunity;

        System.debug('>>>testOpportunity.Pricebook2Id: ' + [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :testOpportunity.Id]);

        System.debug('>>>COUNT: ' + [SELECT COUNT() FROM OpportunityLineItem]);
        return new Map<String, SObject>{
                'Product A' => productA,
                'Product B' => productB,
                'Price Book Entry A' => pricebookEntryA,
                'Price Book Entry B' => pricebookEntryB,
                'Opportunity' => testOpportunity
        };
    }

    @istest
    static void shouldMaintainOpportunityLineItemsAndValuation()
    {
        Map<String, SObject> dataMap = prepareData();
        Opportunity testOpportunity = (Opportunity) dataMap.get('Opportunity');
        PricebookEntry pricebookEntry = (PricebookEntry) dataMap.get('Price Book Entry A');
 
        /*CSW: Not needed anymore, because it is created a opportunity-prouct automatically during insert.
        OpportunityLineItem opportunityProduct = new OpportunityLineItem(OpportunityId = testOpportunity.Id, PricebookEntryId = pricebookEntry.Id, Quantity = 1, UnitPrice = 10);

        System.assert([select count() from OpportunityLineItem__c] == 0, 'There should be no Opportunity Line Item records!');
        insert opportunityProduct;*/

        System.assert([select count() from OpportunityLineItem__c] == 1, 'There should be 1 Opportunity Line Item record!');
        
        Product2 otherProduct = [SELECT Id FROM Product2 WHERE Name = 'Sonstiges'];
        OpportunityLineItem__c existingOpportunityLineItem = [select Product__c from OpportunityLineItem__c where Opportunity__c = :testOpportunity.Id limit 1];
        System.assertEquals(otherProduct.Id, existingOpportunityLineItem.Product__c);

        OpportunityLineItem opportunityProduct = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :testOpportunity.Id];
        delete opportunityProduct;

        System.assert([select count() from OpportunityLineItem__c] == 0, 'There should be no Opportunity Line Item record!');

        Valuation__c valuation = new Valuation__c(Opportunity__c = testOpportunity.Id);
        insert valuation;
        Pricebook2 valuationPricebook2 = [SELECT Id FROM Pricebook2 WHERE Name = 'Valuation'];
        PricebookEntry valuationPricebookEntry = [SELECT Id, Pricebook2Id, Product2Id FROM PricebookEntry WHERE Pricebook2Id = :valuationPricebook2.Id AND Product2Id = :otherProduct.Id];
        System.debug('>>>valuationPricebookEntry: ' + valuationPricebookEntry);
//        opportunityProduct = opportunityProduct.clone(false, true);//no value since opportunityProduct is being selected with nothing but id and it possibly led to Internal Server error also
        opportunityProduct = new OpportunityLineItem();
        opportunityProduct.ValuationId__c = valuation.Id;
        opportunityProduct.OpportunityId = testOpportunity.Id;//CSW
//        opportunityProduct.Quantity = 0.0;//CSW
        opportunityProduct.Quantity = 1.0;//ILE: zero-quantity lead to validation error
        opportunityProduct.TotalPrice = 0.0;//CSW
        opportunityProduct.PricebookEntryId = valuationPricebookEntry.Id;//CSW*/

        insert opportunityProduct;

        valuation = [select InvoicedProduct__c from Valuation__c where Id = :valuation.Id limit 1];
        existingOpportunityLineItem = [select Product__c from OpportunityLineItem__c where Opportunity__c = :testOpportunity.Id limit 1];

        System.assertEquals(existingOpportunityLineItem.Id, valuation.InvoicedProduct__c);
    }

    @istest
    static void shouldEnsureOpportunityProductFamilyIntegrity()
    {
        Map<String, SObject> dataMap = prepareData();
        Product2 productA = (Product2) dataMap.get('Product A');
        Product2 productB = (Product2) dataMap.get('Product B');

        productA.Family = 'Other';
        productB.Family = 'Plot';
        update new List<Product2>{productA, productB};

        List<OpportunityLineItem> opportunityProducts = new List<OpportunityLineItem>{
                new OpportunityLineItem(OpportunityId = dataMap.get('Opportunity').Id, PricebookEntryId = dataMap.get('Price Book Entry B').Id, Quantity = 1, UnitPrice = 10),
                new OpportunityLineItem(OpportunityId = dataMap.get('Opportunity').Id, PricebookEntryId = dataMap.get('Price Book Entry B').Id, Quantity = 1, UnitPrice = 10)
        };
        /*
        try
        {
            insert opportunityProducts;
            System.assert(false, 'Should throw exception regarding multiple Product2.Family values for the same Opportunity');
        }
        catch (Exception e)
        {
            System.assert(e.getMessage().contains(Label.OpportunityProductFamilyIntegrityMessage));
        }
		*/
        productB.Family = productA.Family;
        update productB;

        insert opportunityProducts;

        productB.Family = 'Plot';
        update productB;
        /*
        try
        {
            update opportunityProducts;
            System.assert(false, 'Should throw exception regarding multiple Product2.Family values for the same Opportunity');
        }
        catch (Exception e)
        {
            System.assert(e.getMessage().contains(Label.OpportunityProductFamilyIntegrityMessage));
        }
		*/
    }
}