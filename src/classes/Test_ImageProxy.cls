@isTest
public class Test_ImageProxy {
    @testSetup static void setup() {
        List<Municipality__c> mus = new List<Municipality__c>();
        
        ContentVersion cvlist = new Contentversion(); 
        cvlist.Title = 'CZDSTOU'; 
        cvlist.PathOnClient = 'test'; 
        cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
        List<ContentVersion> cvl = new List<ContentVersion>(); 
        cvl.add(cvlist); 
        insert cvl;
        
        for(Integer i=0;i<2;i++) {
            mus.add(new Municipality__c(
                Name = 'TestMuni'+i, 
                Map_Macro_Location__c = '<p>datt iset</p><p><br></p><p><img src="https://colliersde--parxdevkam--c.cs101.content.force.com/sfc/servlet.shepherd/version/download/0681X000000B0nd?asPdf=false&amp;operationContext=CHATTER" alt="image.png"></img></p>'));
        }
        insert mus;     
        
    }

    private static testMethod void doTestextractField() {
        Municipality__c muni = [
            SELECT Id, Map_Macro_Location__c FROM Municipality__c WHERE Name='TestMuni0' LIMIT 1];
    
        Test.startTest();
        List<ImageProxy.ExtractFieldName> requests = new List<ImageProxy.ExtractFieldName>();
        ImageProxy.ExtractFieldName request = new ImageProxy.ExtractFieldName();
        requests.add(request);
        request.recordId = muni.Id;
        request.richText = muni.Map_Macro_Location__c;
        request.urlFieldName = 'Map_Macro_Location_Image_URL__c';
        



        ImageProxy.extractFieldNameActionsBatch(requests);

        Test.stopTest();


    }
    private static testMethod void doTestGetImageCont() {
        ContentVersion  contVers = [
            SELECT Id FROM ContentVersion LIMIT 1];
    
        Test.startTest();        
		RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI  = '/services/apexrest/ContentDoc/' + EncodingUtil.base64Encode(Blob.valueOf('https://colliersde--parxdevkam--c.cs101.content.force.com/sfc/servlet.shepherd/version/download/0681X000000B0nd?asPdf=false&amp;operationContext=CHATTER'));

        req.httpMethod = 'GET'; 
        RestContext.request = req;
        RestContext.response = res;
        ImageProxy.getBlob();
        
        Test.stopTest();
    }
    private static testMethod void doTestGetImage() {
        ContentVersion  contVers = [
            SELECT Id FROM ContentVersion LIMIT 1];
    
        Test.startTest();        
		RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI  = '/services/apexrest/ContentDoc/' + 
            EncodingUtil.base64Encode(Blob.valueOf('https://colliersde--parxdevkam--c.cs101.content.force.com/servlet/rtaImage?eid=a1g1X000000TT1L&amp;feoid=00N1X000000LCdg&amp;refid=0EM1X0000008d78'));

        req.httpMethod = 'GET'; 
        RestContext.request = req;
        RestContext.response = res;
        ImageProxy.getBlob();
        
        Test.stopTest();
    }

    // https://colliersde--parxdevkam--c.cs101.content.force.com/servlet/rtaImage?eid=a1g1X000000TT1L&amp;feoid=00N1X000000LCdg&amp;refid=0EM1X0000008d78
    //
    // https://colliersde--parxdevkam--c.cs101.content.force.com/sfc/servlet.shepherd/version/download/0681X000000B0nd?asPdf=false&amp;operationContext=CHATTER
}