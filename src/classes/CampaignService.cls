/**
*	CampaignService.
*	@author npo
*	@copyright PARX
*/
public with sharing class CampaignService
{
    /**
     * US2522: campaign deactivation
     * @author npo
     *
     * @param c Campaign Id
     * @param withPersist if changes should be saved
     *
     * @return list of investors with contacts
     */
    public static void deactivateCampaign(Id campaignId, Boolean withPersist)
    {
        Campaign campaignToUpdate = new Campaign(
                Id = campaignId,
                isActive = false
        );
        if (withPersist)
        {
            update campaignToUpdate;
        }
    }

    public static void createCampaignMemberStatus(Id campaignId, List<String> values)
    {
        List<CampaignMemberStatus> checkExistingStatuses = [SELECT Id, Label, CampaignId FROM CampaignMemberStatus WHERE CampaignId = :campaignId];
        List<CampaignMemberStatus> newStatuses = new List<CampaignMemberStatus>();
        Set<String> existingValues = new Set<String>();
        for (CampaignMemberStatus status : checkExistingStatuses)
        {
            existingValues.add(status.Label);
        }
        System.debug('...existingValues: ' + existingValues);
        for (String v : values)
        {
            if (!existingValues.contains(v))
            {
                newStatuses.add(new CampaignMemberStatus(CampaignId = campaignId, Label = v));
            }
        }
        if (!newStatuses.isEmpty())
        {
            try
            {
                System.debug('...newStatuses: ' + newStatuses);
                Database.insert(newStatuses, false);
            }
            catch (Exception e) {}

        }
    }

}