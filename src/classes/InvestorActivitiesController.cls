/**
* Controller for InvestorActivities LWC
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 04.02.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
*/
public with sharing class InvestorActivitiesController
{
    public static final String ACCOUNT_OBJECT_API_NAME = 'Account';
    public static final String OPPORTUNITY_OBJECT_API_NAME = 'Opportunity';

    @AuraEnabled(Cacheable = true)
    public static List<Task> getActivityList(Id recordId, String objectApiName)
    {
        List<Task> activityList = new List<Task>();

        List<AccountToOpportunity__c> accountToOpportunities = new List<AccountToOpportunity__c>();
        if (objectApiName == OPPORTUNITY_OBJECT_API_NAME)
        {
            accountToOpportunities = [SELECT (SELECT Id FROM Account_to_opportunities__r) FROM Opportunity WHERE Id = :recordId ].Account_to_opportunities__r;
        }
        if (objectApiName == ACCOUNT_OBJECT_API_NAME)
        {
            accountToOpportunities = [SELECT (SELECT Id FROM Account_to_opportunities__r) FROM Account WHERE Id = :recordId].Account_to_opportunities__r;
        }

        if (!accountToOpportunities.isEmpty())
        {
            activityList = [
                    SELECT Id, Status, Subject, ActivityDate, Owner.Name, OwnerId, (SELECT Id, Relation.Name, RelationId  FROM TaskWhoRelations)
                    FROM Task
                    WHERE WhatId IN :accountToOpportunities
            ];
        }

        return activityList;
    }
}