/**
*	
*	@author npo
*	@copyright PARX
*/
@IsTest
private class Test_MailPermissionService
{
	@TestSetup
	static void setup()
	{
		TestUtil.createUser('Test_MailPermissionService.testHandleUniquenessByUser1@test.com', 'Colliers Finance', null, true);
        TestUtil.createUser('Test_MailPermissionService.testHandleUniquenessByUser2@test.com', 'Colliers Finance', null, true);

    }
	@IsTest
	static void testHandleUniquenessByUser()
	{
		TriggerTemplate.setupUnitTest();
		User testUser1 = [SELECT Id FROM User WHERE Username = 'Test_MailPermissionService.testHandleUniquenessByUser1@test.com' LIMIT 1];
        User testUser2 = [SELECT Id FROM User WHERE Username = 'Test_MailPermissionService.testHandleUniquenessByUser2@test.com' LIMIT 1];

		MailPermission__c p1 = new MailPermission__c(User__c = UserInfo.getUserId(), DelegatedUser__c = testUser1.Id);
		MailPermission__c p2 = new MailPermission__c(User__c = UserInfo.getUserId(), DelegatedUser__c = testUser1.Id);

		TriggerTemplate.startUnitTest();
		Test.startTest();

        insert p1;
		try
		{
			insert p2;
			System.assert(false, 'We expect the second insert to fail');
		}
		catch (Exception e)
		{
			System.assert(true, 'We expect Exception to be thrown');
		}

        p1.User__c = testUser2.Id;
        update p1;

        System.assertEquals(testUser2.Id, [SELECT Id, User__c FROM MailPermission__c WHERE Id = :p1.Id].User__c, 'We expect record to be updated');

		Test.stopTest();
	}

	@IsTest
	static void testFindPermissionsForCurrentUser()
	{
		TriggerTemplate.setupUnitTest();
		User testUser1 = [SELECT Id FROM User WHERE Id != :UserInfo.getUserId() LIMIT 1];
		MailPermission__c p1 = new MailPermission__c(User__c = testUser1.Id, DelegatedUser__c = UserInfo.getUserId());
		insert p1;

		TriggerTemplate.startUnitTest();
		Test.startTest();

		System.assertEquals(1, MailPermissionService.findPermissionsForCurrentUser().size(), 'We expect 1 permission to be returned');
		Test.stopTest();
	}
}