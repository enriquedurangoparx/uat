/**
* Is used for parameterized search.
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 14.11.2019	| Egor Markuzov (egor.markuzov@parx.com)    | initial version.
* 0.2 23.12.2019    | Egor Markuzov (egor.markuzov@parx.com)    | implemented logic for "add new record"
*/
public class SearchController {
    private final static Integer MAX_RESULTS = 5;
    
    @AuraEnabled(Cacheable=false)
    public static List<SearchResult> search(String searchJSON) 
    {
        System.debug('searchJSON: ' + searchJSON);
        SearchCriteriaWrapper searchCriteriaWrap = (SearchCriteriaWrapper) JSON.deserialize(searchJSON, SearchCriteriaWrapper.class);
        List<SearchResult> results = new List<SearchResult>();

        if (String.isNotBlank(searchCriteriaWrap.searchTerm))
        {
            String query = 'SELECT Id, ' + searchCriteriaWrap.fieldApiName + ' FROM ' + searchCriteriaWrap.objectApiName; 

            
            String queryWhereCondition = searchCriteriaWrap.isLikeSearch ? ' WHERE ' + searchCriteriaWrap.fieldApiName + ' LIKE \'%' + searchCriteriaWrap.searchTerm + '%\'' : ' WHERE ' + searchCriteriaWrap.fieldApiName + ' = :searchCriteriaWrap.searchTerm ';
            query += queryWhereCondition;
            query += ' ORDER BY ' + searchCriteriaWrap.orderBy + ' LIMIT :MAX_RESULTS';
            System.debug('query: ' + query);

            System.debug('searchCriteriaWrap: ' + searchCriteriaWrap);
            List<SObject> sObjectList = Database.query(query);
            
            String icon = 'standard:search';
            if (searchCriteriaWrap.objectApiName.indexOf('__c') == -1)
            {
                icon = 'standard:' + searchCriteriaWrap.objectApiName.toLowerCase();
            }

            for (SObject so : sObjectList)
            {
                String subtitle = so.getSObjectType().getDescribe().getLabel();
                results.add(new SearchResult( (Id) so.get('Id'), searchCriteriaWrap.objectApiName, icon, (String) so.get(searchCriteriaWrap.fieldApiName), subtitle, false));
            }
        } 

        if (searchCriteriaWrap.addNewRecordModeIsOn == true)
        {
            String addLabel = String.isNotBlank(searchCriteriaWrap.newRecordLabel) ? searchCriteriaWrap.newRecordLabel : 'New Record';
            results.add(new SearchResult( null, searchCriteriaWrap.objectApiName, 'utility:add', addLabel, '', true));
        }
        System.debug('results: ' + JSON.serialize(results));

        return results;
    }

    @testVisible
    public class SearchCriteriaWrapper 
    {
        @AuraEnabled
        public String objectApiName;
        @AuraEnabled
        public String fieldApiName;
        @AuraEnabled
        public String searchTerm;
        @AuraEnabled
        public Boolean isLikeSearch;
        @AuraEnabled
        public String orderBy;
        @AuraEnabled
        public Boolean addNewRecordModeIsOn;
        @AuraEnabled
        public String newRecordLabel;
    }

    public class SearchResult {
        @AuraEnabled
        public Id id;
        @AuraEnabled
        public String sObjectType;
        @AuraEnabled
        public String icon;
        @AuraEnabled
        public String title;
        @AuraEnabled
        public String subtitle;
        @AuraEnabled
        public Boolean isAddButton;

        public SearchResult(Id id, String sObjectType, String icon, String title, String subtitle, Boolean isAddButton) {
            this.id = id;
            this.sObjectType = sObjectType;
            this.icon = icon;
            this.title = title;
            this.subtitle = subtitle;
            this.isAddButton = isAddButton;
        }
    }
}