/*
*   Utils functionality.
*
*   @author Tejashree
*   @copyright PARX
*/
public class Utils {

    /**
    *  @return Map<String, ColliersCounter__c> of counter Name and Counter
    */
    public static final String ACCOUNT_TYPE_CUSTOMER = 'Customer';
    public static final String ACCOUNT_TYPE_COLLIERS = 'Intercompany Colliers';
    public static final String ACCOUNT_TYPE_SHAREHOLDER = 'Intercompany Shareholder';
    
    public static Map<String, ColliersCounter__c> getCounterRecords()
    {
        MAP<String,ColliersCounter__c> counterMap = new Map<String, ColliersCounter__c>();
        for (ColliersCounter__c counter : [SELECT Counter__c,Name FROM ColliersCounter__c limit 10])
        {
                    counterMap.put(counter.Name, counter);
        }
        return counterMap;
    }

    /**
     * Copies the fields described in the fieldsToCopy list from the source SObject to the target SObject
     * @param source Source SObject from which the fields will be copied
     * @param target Target SObject to which the fields will be copied
     * @param sourceTargetFieldMap Defines a map of sourceFieldName => targetFieldName fields which is necessary to copy
     * @return Target object with copied fields from the source object
     */
    public static SObject copySObjectFieldsByFieldMap(SObject source, SObject target, Map<SObjectField, SObjectField> sourceTargetFieldMap)
    {
        SObjectField targetField;
        for (SObjectField sourceField : sourceTargetFieldMap.keySet())
        {
            targetField = sourceTargetFieldMap.get(sourceField);
            target.put(targetField, source.get(sourceField));
        }
        return target;
    }
}