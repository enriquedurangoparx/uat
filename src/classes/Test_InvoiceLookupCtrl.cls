@IsTest
public with sharing class Test_InvoiceLookupCtrl 
{

     @IsTest
    static void test() 
    {
     
        UnitTestDataTest.createTestRecords();
        
        Account a = new Account(
            Name = 'TestAccount'
        );

        insert a;

        ONB2__Template__c template = new ONB2__Template__c(
            Name = 'default'
        );

        insert template;

        List<ONB2__Invoice__c> invoices = new List<ONB2__Invoice__c>();

        ONB2__Invoice__c invoice1 = new ONB2__Invoice__c(
            Name = 'CustInvoice1',
            ONB2__Account__c = a.Id,
            ONB2__Template__c = template.id,
            ONB2__Date__c = Date.newInstance(2018, 1, 5),
            ONB2__Status__c = 'Draft'
        );

        invoices.add(invoice1);

        ONB2__Invoice__c invoice2 = new ONB2__Invoice__c(
            Name = 'CustInvoice2',
            ONB2__Account__c = a.Id,
            ONB2__Template__c = template.id,
            ONB2__Date__c = Date.newInstance(2018, 1, 5),
            ONB2__Status__c = 'Draft'
        );

        invoices.add(invoice2);

        insert invoices;

        List<Map<String, String>> invoiceOptions = InvoiceLookupCtrl.getInvoiceOptionsByNumber('Invoice2');

        system.assertEquals(invoiceOptions.size(), 1);
        system.assertEquals(invoiceOptions[0].get('label'), invoice2.Name + ' - ' + a.Name);
        system.assertEquals(invoiceOptions[0].get('value'), invoice2.Id);

        ONB2__Invoice__c customerInvoice = InvoiceLookupCtrl.setCustomerInvoiceId(invoice1.Id, invoice2.Id);

        invoice1 = [
            SELECT Id, CustomerInvoice__c
            FROM ONB2__Invoice__c
            WHERE Id =: invoice1.Id
        ]; 

        system.assertEquals(invoice1.CustomerInvoice__c, invoice2.Id);

    }

}