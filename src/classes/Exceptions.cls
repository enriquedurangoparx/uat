/**
 * @author ile
 * @copyright PARX
 */

public with sharing class Exceptions
{
    public virtual class GenericException extends Exception {/**do nothing*/}

    public static void assert(Boolean assertion, Exception e)
    {
        if (assertion != true && e != null)
        {
            throw e;
        }
    }
}