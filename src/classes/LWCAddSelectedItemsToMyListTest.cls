/**
* Testclass for LWCAddSelectedItemsToMyList.cls and AddSelectedToInvestorsListController.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 18.11.2019	| Egor Markuzov (egor.markuzov@parx.com)                | initial version.
* 0.2 17.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Added Saluation for Contact.
*/
@isTest
private class LWCAddSelectedItemsToMyListTest {
    /**
     * test LWCAddSelectedItemsToMyList.createInvestorListItemRecords(List<Id> recordIds, Id investorListId)
     */
    @isTest
    public static void testCreateInvestorListItemRecords()
    {
        List<Account> accList = new List<Account>();

        for (Integer i = 0; i < 10; i++)
        {
            accList.add(TestDataFactory.createAccount('Test ' + i));
        } 
        insert accList;

        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        List<Id> accIdList = new List<Id>();
        for (Account acc : accList)
        {
            accIdList.add(acc.Id);
        }

        InvestorsListItem__c newInvestorListItem = TestDataFactory.createInvestorListItem(newInvestorList.Id, accIdList[0]);
        insert newInvestorListItem;

        String jsonResp = LWCAddSelectedItemsToMyList.createInvestorListItemRecords(accIdList, newInvestorList.Id);

        LWCAddSelectedItemsToMyList.InvestorListViewWrapper resp = (LWCAddSelectedItemsToMyList.InvestorListViewWrapper) JSON.deserialize(jsonResp, LWCAddSelectedItemsToMyList.InvestorListViewWrapper.class);
        
        System.assertEquals(9, resp.newInvestorsListItems.size(), 'Wrong number of created investor list items.');
        System.assertEquals(1, resp.existingInvestorsListItems.size(), 'Wrong number of existing investor list items.');
        System.assertEquals(newInvestorListItem.Id, resp.existingInvestorsListItems[0].Id, 'Wrong existing investor list item record.');
    }

    /**
     * test LWCAddSelectedItemsToMyList.createInvestorListItemRecords(List<Id> recordIds, Id investorListId)
     */
    @isTest
    public static void createInvestorListItemContactsRecords()
    {
        Account newAcc1 = TestDataFactory.createAccount('Test 1');
        Account newAcc2 = TestDataFactory.createAccount('Test 2');
        
        List<Account> accList = new List<Account>{newAcc1, newAcc2};
        insert accList;
        
        List<Contact> contList1 = TestDataFactory.createContacts(newAcc1.Id, 10);
        List<Contact> contList2 = TestDataFactory.createContacts(newAcc2.Id, 10);
        List<Contact> contList = new List<Contact>();
        contList.addAll(contList1);
        contList.addAll(contList2);
        insert contList;

        List<Id> contIdList = new List<Id>();
        for (Contact cont : contList)
        {
            contIdList.add(cont.Id);
        }

        InvestorsList__c newInvestorList = TestDataFactory.createInvestorList('Test List');
        insert newInvestorList;

        InvestorsListItem__c newInvestorListItem = TestDataFactory.createInvestorListItem(newInvestorList.Id, newAcc1.Id);
        insert newInvestorListItem;

        InvestorsListItemContact__c newInvestorListItemContact = TestDataFactory.createInvestorListItemContact(newInvestorListItem.Id, contList1[0].Id);
        insert newInvestorListItemContact;
        
        String jsonResp = LWCAddSelectedItemsToMyList.createInvestorListItemContactsRecords(contIdList, newInvestorList.Id);

        LWCAddSelectedItemsToMyList.InvestorListViewWrapper resp = (LWCAddSelectedItemsToMyList.InvestorListViewWrapper) JSON.deserialize(jsonResp, LWCAddSelectedItemsToMyList.InvestorListViewWrapper.class);
        
        System.assertEquals(1, resp.existingInvestorsListItems.size(), 'Wrong number of existing investor list items.');
        System.assertEquals(newInvestorListItem.Id, resp.existingInvestorsListItems[0].Id, 'Wrong existing investor list item record.');
        System.assertEquals(1, resp.newInvestorsListItems.size(), 'Wrong number of created investor list items.');
        System.assertEquals(1, resp.existingInvestorsListItemsContact.size(), 'Wrong number of existing investor list item contacts.');
        System.assertEquals(newInvestorListItemContact.Id, resp.existingInvestorsListItemsContact[0].Id, 'Wrong existing investor list item contact record.');
        System.assertEquals(19, resp.newInvestorsListItemsContact.size(), 'Wrong number of created investor list item contacts.');
    }

    /**
    * test AddSelectedToInvestorsListController standard controller
    */
    @isTest
    public static void testAddSelectedToInvestorsListStdController()
    {
        Account newAcc = TestDataFactory.createAccount('Test 1');
        List<Account> newAccList = new List<Account>{newAcc};
        insert newAccList;

        Test.startTest();
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(newAccList);
            stdSetController.setSelected(newAccList);

            AddSelectedToInvestorsListController ext = new AddSelectedToInvestorsListController(stdSetController);
        Test.stopTest();

        System.assertEquals(true, ext.selectedIds.contains(newAcc.Id), 'Wrong selected record Id.');
    }
}