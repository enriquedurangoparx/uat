/**
* Utility class allowing performing HTTP requests
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 25.02.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
*/

public with sharing class HttpRequestUtil
{
    /**
     * Performs a HTTP Get Query by returning HttpResponse
     * @param url Specifies the endpoint for this request
     * @param queryParamsMap query key => query value map
     * @return HttpResponse object
     */
    public static HttpResponse doGetRequest(String url, Map<String, String> queryParamsMap)
    {
        // Instantiate a new http object
        Http http = new Http();

        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url + '?' + queryParamsMapToString(queryParamsMap));
        req.setMethod('GET');

        // Send the request, and return a response
        return http.send(req);
    }

    /**
     * Returns the String representation of query params corresponding to the specified queryParamsMap
     * @param queryParamsMap query key => query value map
     * @return String representation of query params converted from queryParamsMap
     */
    public static String queryParamsMapToString(Map<String, String> queryParamsMap)
    {
        String result = '';
        for (String key : queryParamsMap.keySet())
        {
            if (!String.isEmpty(result))
            {
                result += '&';
            }
            result += key + '=' + EncodingUtil.urlEncode(queryParamsMap.get(key), 'UTF-8');
        }
        return result;
    }
}