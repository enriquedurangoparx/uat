/**
* Testclass for LWCObjectHelper.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 10.10.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)	| initial version.
* 0.2 16.12.2019    | Egor Markuzov (egor.markuzov@parx.com)                | added testGetInitialState() and testUpdateObjectsRelation() 
*/
@isTest
private class LWCObjectHelperTest {

    @isTest
    private static void testSearchRelatedObjectsBySoql(){
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Test-Portfolio';
        insert portfolio;

        PropertyObject__c objectt = new PropertyObject__c();
        objectt.Name = 'Emporio-Hochhaus';
        objectt.Portfolio__c = portfolio.Id;
        insert objectt;

        List<PropertyObject__c> listOfObjects = LWCObjectHelper.searchRelatedObjectsBySoql(portfolio.Id, 'Portfolio__c');
        System.assertEquals(1, listOfObjects.size(), 'Only one object may be referenced to the portfolio.');        
    }

    @isTest
    private static void testSearchRelatedObjectsBySosl(){
        PropertyObject__c objectt = new PropertyObject__c();
        objectt.Name = 'Emporio-Hochhaus';
        insert objectt;

        List<Id> fixedSearchResults = new List<Id>{objectt.Id};
        Test.setFixedSearchResults(fixedSearchResults);

        List<List<sObject>> listOfListOfObjects = null;
        Test.startTest();
            listOfListOfObjects = LWCObjectHelper.searchRelatedObjectsBySosl('Emporio');
        Test.stopTest();

        System.assertEquals(1, listOfListOfObjects.get(0).size(), 'Only one object should be returned.');
    }

    @isTest
    private static void testGetInitialState(){
        PropertyObject__c objectt = new PropertyObject__c();
        objectt.Name = 'Emporio-Hochhaus';
        insert objectt;

        List<String> queryFields = new List<String>{'Name', 'Street__c', 'PostalCode__c', 'City__c', 'Status__c', 'Area__c', 'Area__r.Name', 'Portfolio__c', 'Portfolio__r.Name'};
        String filteredFiled = 'Area__c';
        Area__c newArea = TestDataFactory.createArea('Test Area');
        insert newArea;
        
        List<PropertyObject__c> newObject = TestDataFactory.createPropertyObjectWithArea(newArea.Id);
        insert newObject;

        List<PropertyObject__c> objectsToCheck = LWCObjectHelper.getInitialState(queryFields, filteredFiled, newArea.Id);

        System.assertEquals(newObject.size(), objectsToCheck.size(), 'Wrong number of objects were returned.');
    }

    @isTest
    private static void testUpdateObjectsRelation(){
        PropertyObject__c obj = new PropertyObject__c();
        obj.Name = 'Emporio-Hochhaus';
        insert obj;

        Area__c newArea = TestDataFactory.createArea(' Test Area');
        insert newArea;

        System.assertEquals('success', LWCObjectHelper.updateObjectsRelation(new List<PropertyObject__c>{obj}, newArea.Id, 'Area__c'), 'Relation on the object was not updated');
    }

    
}