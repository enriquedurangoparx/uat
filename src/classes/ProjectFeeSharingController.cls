/**
* This is an apex controller for ProjectFeeSharing Lightning Component
 * @author ile
 * @copyright PARX
 */

public with sharing class ProjectFeeSharingController
{
    @AuraEnabled
    public static Opportunity selectOpportunity(Id recordId)
    {
        Opportunity opportunityRecord = new Opportunity();
        for (Opportunity each : [SELECT OSFeeShare__c, Name, WeightedBusinessLine__c, ShareBusinessLine__c FROM Opportunity WHERE Id = :recordId])
        {
            opportunityRecord = each;
        }

        return opportunityRecord;
    }

    @AuraEnabled
    public static List<FeeSharing__c> selectFeeSharing(Id opportunityId)
    {
        return [select Id, User__c, DistributionForIdentification__c, DistributionForOrderAcquisition__c,OverallPercentage__c,WeightedSharePerUser__c,SharePerUser__c,
                DistributionForExecution__c, DistributionForReporting__c, PercentageForReporting__c, PercentageForExecution__c, PercentageForOrderAcquisition__c, PercentageForIdentification__c,
                Opportunity__c, Description__c, SharePercentage__c from FeeSharing__c where Opportunity__c = :opportunityId
            order by CreatedDate asc];
    }

    @AuraEnabled
    public static FeeDistributionSettings__c selectFeeDistributionSetting()
    {
        FeeDistributionSettings__c result;
        for (FeeDistributionSettings__c feeDistributionSetting : [select Id, DistributionForIdentification__c, DistributionForOrderAcquisition__c,
                DistributionForExecution__c, DistributionForReporting__c, MaxAmountOfWorkers__c, MaxAmountOfWorkersForNonOsOwned__c from FeeDistributionSettings__c order by LastModifiedDate desc limit 1])
        {
            result = feeDistributionSetting;
        }

        return result;
    }

    @AuraEnabled
    public static SObjectDescription getSObjectDescription(String sObjectName)
    {
        return new SObjectDescription(sObjectName);
    }

    /**
     * Passing List<FeeSharing__c> as a JSON serialized String to bypass an issue with decimal delimiter and built-in
     * LEX-SF serialization mechanism when pointer delimiter in coma-delimiter locales were treated as thousands
     * delimiter and wrong number saved.
     *
     * @refactored by npo in scope of US2804
     */
    @AuraEnabled
    public static List<FeeSharing__c> commitFeeSharing(String feeSharings, Id opportunityId)
    {

        List<FeeSharing__c> feeSharing = (List<FeeSharing__c>) System.JSON.deserialize(feeSharings, List<FeeSharing__c>.class);

        if (!feeSharing.isEmpty())
        {
            prepareFeeSharing(feeSharing);
            upsert feeSharing;
        }

        if (opportunityId != null)
        {
            List<FeeSharing__c> recordsToDelete = [SELECT Id FROM FeeSharing__c WHERE Opportunity__c = :opportunityId AND Id NOT IN :feeSharing];

            if(!recordsToDelete.isEmpty())
            {
                System.debug('...recordsToDelete : ' + recordsToDelete);
                delete recordsToDelete;
            }
        }

        return feeSharing;
    }

    /**
     * Prepares FeeSharing__c records by pre-populating them with Tenant__c, ProductGroup__c, and Center__c values
     * from matching ONB2__AssignmentRulesCenter__c

        (!) this method is used in OpportunityLineItemScheduleTrigger
     *
     * @param feeSharings
     */
    public static void prepareFeeSharing(List<FeeSharing__c> feeSharings)
    {
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>(selectRelatedOpportunities(feeSharings));
        Map<Id, User> userMap = new Map<Id, User>(selectRelatedUsers(feeSharings));
        for (FeeSharing__c feeSharing : feeSharings)
        {
            Opportunity relatedOpportunity = opportunityMap.get(feeSharing.Opportunity__c);
            //if (relatedOpportunity.HasOpportunityLineItem)
            if (relatedOpportunity.OpportunityLineItems.size() > 0)
            {
                User relatedUser = userMap.get(feeSharing.User__c);

                system.debug('Retrieved User: ' + relatedUser.Name);
                system.debug('Related Family: ' + relatedOpportunity.OpportunityLineItems.get(0).Product2.Family);

                ONB2__AssignmentRulesCenter__c assignmentRulesCenterSettings = getAssignmentRulesCenterSettings(relatedUser.Name);
                if (assignmentRulesCenterSettings == null)
                {
                    assignmentRulesCenterSettings = getAssignmentRulesCenterSettings(relatedUser.Name, relatedOpportunity.OpportunityLineItems.get(0).Product2.Family);
                }

                if (assignmentRulesCenterSettings != null)
                {
                    feeSharing.Tenant__c = assignmentRulesCenterSettings.ONB2__Tenant__c;
                    feeSharing.ProductGroup__c = assignmentRulesCenterSettings.ONB2__ProductGroup__c;
                    feeSharing.Center__c = assignmentRulesCenterSettings.ONB2__Center__c;
                }
            }
        }
    }

    /**
     * Selects the list of related to FeeSharing__c records User records
     */
    private static List<User> selectRelatedUsers(List<FeeSharing__c> feeSharings)
    {
        List<Id> userIDs = new List<Id>();
        for (FeeSharing__c each : feeSharings)
        {
            userIDs.add(each.User__c);
        }

        return [select Name from User where Id in :userIDs];
    }

    /**
     * Gets a ONB2__AssignmentRulesCenter__c custom settings record where ONB2__AssignmentRulesCenter__c.BillingType__c == userName,
     * shorthand for getAssignmentRulesCenterSettings(userName, null);
     */
    @TestVisible
    private static ONB2__AssignmentRulesCenter__c getAssignmentRulesCenterSettings(String userName)
    {
        return getAssignmentRulesCenterSettings(userName, null);
    }

    /**
     * Gets a ONB2__AssignmentRulesCenter__c custom settings record where ONB2__AssignmentRulesCenter__c.BillingType__c == userName,
     * and ONB2__AssignmentRulesCenter__c.ProductGroup__c == productGroup.
     */
    private static ONB2__AssignmentRulesCenter__c getAssignmentRulesCenterSettings(String userName, String productGroup)
    {
        ONB2__AssignmentRulesCenter__c matchingCenter;

        for (ONB2__AssignmentRulesCenter__c each : ONB2__AssignmentRulesCenter__c.getAll().values())
        {
            system.debug('Scanning type ' + each.ONB2__BillingType__c);
            system.debug('Scanning group ' + each.ONB2__ProductGroup__c);

            if (each.ONB2__BillingType__c == userName
                    && (productGroup == null || each.ONB2__ProductGroup__c == productGroup))
            {
                system.debug('Match found: ' + each);
                if (matchingCenter != null && productGroup == null)
                {
                    return null;
                }
                else if (productGroup != null)
                {
                    return each;
                }
                matchingCenter = each;
            }
        }

        return matchingCenter;
    }

    /**
     * Selects related Opportunities with their Products
     */
    private static List<Opportunity> selectRelatedOpportunities(List<FeeSharing__c> feeSharings)
    {
        List<Id> opportunityIDs = new List<Id>();
        for (FeeSharing__c each : feeSharings)
        {
            opportunityIDs.add(each.Opportunity__c);
        }

        //return [select HasOpportunityLineItem, (select Product2.Family from OpportunityLineItems limit 1) from Opportunity];
        return [select HasOpportunityLineItem, (select Product2.Family from OpportunityLineItems where pricebookentry.product2.RelevantForFeeSharing__c = TRUE limit 1) from Opportunity];
    }

    public virtual class SObjectDescription
    {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public Map<String, String> labels {get; set;}
        @AuraEnabled public Map<String, List<Map<String, String>>> picklists {get; set;}

        public SObjectDescription(String sobjectName)
        {
            Schema.DescribeSObjectResult sobjectDescribe = Schema.getGlobalDescribe().get(sobjectName).getDescribe();

            this.label =  sobjectDescribe.Label;
            this.labels = new Map<String, String>();
            this.picklists = new Map<String, List<Map<String, String>>>();

            //@todo: think about iterating through a subset of a field for optimization sake
            for (Schema.SObjectField fieldToken : sobjectDescribe.fields.getMap().values())
            {
                Schema.DescribeFieldResult fieldDescription = fieldToken.getDescribe();
                this.labels.put(fieldDescription.getName(), fieldDescription.getLabel());
                this.picklists.put(fieldDescription.getName(), new List<Map<String, String>>());

                if (fieldDescription.getType() == Schema.DisplayType.PICKLIST
                        || fieldDescription.getType() == Schema.DisplayType.MULTIPICKLIST)
                {
                    for (Schema.PicklistEntry picklistEntry : fieldDescription.getPicklistValues())
                    {
                        if (picklistEntry.isActive())
                        {
                            this.picklists.get(fieldDescription.getName()).add(new Map<String, String>{'label' => picklistEntry.getLabel(), 'value' => picklistEntry.getValue()});
                        }
                    }
                }
            }
        }
    }
}