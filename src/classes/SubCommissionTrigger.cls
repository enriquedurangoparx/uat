/**
* Delegate trigger execution to SubCommissionHandler.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 01.07.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)	| initial version.
* 0.2 22.11.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)	| [SFINV/US-728] Implement rollup-helper to aggregate subcommissions.
*/
public with sharing class SubCommissionTrigger extends ATrigger
{
    private static Type rollClass = System.Type.forName('rh2', 'ParentUtil');

    public override void hookAfterInsert(List<SObject> records){
        executeRollupHelper(null, new Map<Id, SObject>(records));
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords){
        executeRollupHelper(new Map<Id, SObject>(oldRecords), new Map<Id, SObject>(records));
    }

    public override void hookBeforeDelete(List<SObject> records){
        executeRollupHelper(new Map<Id, SObject>(records), null);
    }

    public override void hookAfterDelete(List<SObject> records){
        executeRollupHelper(new Map<Id, SObject>(records), null);
    }

    public override void hookAfterUndelete(List<SObject> records){
        executeRollupHelper(new Map<Id, SObject>(records), null);
    }

    /**
    * Helpermethod to execute rollup-helper. 
    */
    private void executeRollUpHelper(Map<Id, SObject> oldMap, Map<Id, SObject> newMap){
        if(rollClass != null) {
            rh2.ParentUtil pu = (rh2.ParentUtil) rollClass.newInstance();
            pu.performTriggerRollups(oldMap, newMap, new String[]{'SubCommissions__c'}, null);
        }
    }

}