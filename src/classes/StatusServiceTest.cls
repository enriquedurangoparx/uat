/**
* StatusService class Tests
*
* @author: <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 07.04.2020	| Nadzeya Polautsava (nadzeya.polautsava@parx.com)	    | initial version - Bug1948.
*/
@IsTest
private class StatusServiceTest
{
    @testSetup
    static void setup()
    {
        TestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    @IsTest
    static void testRecalculateInvestorLastStatusDate()
    {
        Account newAccount = TestDataFactory.createAccount('Test Account 1');
        insert newAccount;

        List<Contact> newContactList = TestDataFactory.createContacts(newAccount.Id, 1);
        insert newContactList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity newOpportunity = TestDataFactory.createOpportunity(newAccount.Id, propertyObject);
        insert newOpportunity;

        AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAccount.Id, newOpportunity.Id);
        insert newAccToOpp;

        Status__c statusOld = new Status__c(InvestorStatus__c = 'Declination', StatusDate__c = Date.today(), Investor__c = newAccToOpp.Id);
        Status__c statusNew = new Status__c(InvestorStatus__c = 'Initial Contact', StatusDate__c = Date.today().addDays(-10), Investor__c = newAccToOpp.Id);

        insert new List<Status__c> {statusOld, statusNew};

        newAccToOpp.LastStatusDate__c = statusOld.StatusDate__c;
        newAccToOpp.LastStatus__c = statusOld.InvestorStatus__c;
        update newAccToOpp;

        Test.startTest();

        delete statusOld;
        System.assertEquals(statusNew.StatusDate__c, [SELECT Id, LastStatusDate__c FROM AccountToOpportunity__c WHERE Id = :newAccToOpp.Id].LastStatusDate__c,
                        'We expect LastStatusDate__c to be updated');

        Test.stopTest();
    }
}