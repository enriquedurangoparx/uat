/**
* Handle business logic for ContactTrigger by the trigger
*
* @author: <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 01.04.2020	| Nadzeya Polautsava (nadzeya.polautsava@parx.com)	    | initial version - Bug1948.
*/
public inherited sharing class ContactTrigger extends ATrigger
{
    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        populateListOfContactsAfterUpdate((List<Contact>) records, (List<Contact>) oldRecords);
    }

    /**
     * Bug1948
     *
     * @param records - list of contacts
     * @param oldRecords - list of old contacts
     */
    private void populateListOfContactsAfterUpdate(List<Contact> records, List<Contact> oldRecords)
    {
        Map<Id, Contact> oldRecordsMap = new Map<Id, Contact>(oldRecords);
        Map<Id, Contact> recordsMap = new Map<Id, Contact>(records);
        List<Contact> modifiedContacts = TriggerUtil.getModifiedObjectsList(new List<String> {'FirstName', 'LastName'}, records, oldRecordsMap);

        if(!modifiedContacts.isEmpty())
        {
            Map<Id, ContactToInvestor__c> investorContacts = new Map<Id, ContactToInvestor__c>([SELECT Id, Investor__c, Contact__c FROM ContactToInvestor__c WHERE Contact__c IN :modifiedContacts]);
            if (!investorContacts.isEmpty())
            {
                Map<Id, Set<Id>> investorContactsIdsByInvestor = new Map<Id, Set<Id>>();//contacts by investor

                for (ContactToInvestor__c cti : investorContacts.values())
                {

                    if (!investorContactsIdsByInvestor.containsKey(cti.Investor__c))
                    {
                        investorContactsIdsByInvestor.put(cti.Investor__c, new Set<Id>());
                    }

                    investorContactsIdsByInvestor.get(cti.Investor__c).add(cti.Contact__c);
                }

                Map<Id, AccountToOpportunity__c> investors = new Map<Id, AccountToOpportunity__c>([
                        SELECT Id, ListOfContacts__c, (SELECT ID FROM InvestorContacts__r)
                        FROM AccountToOpportunity__c
                        WHERE Id IN :investorContactsIdsByInvestor.keySet()
                ]);

                List<AccountToOpportunity__c> investorsToUpdate = new List<AccountToOpportunity__c>();
                for (Id investorId : investors.keySet())
                {
                    AccountToOpportunity__c investor = investors.get(investorId);
                    String listOfContacts = investor.ListOfContacts__c;
                    if (!String.isEmpty(investor.ListOfContacts__c))
                    {
                        Set<Id> contactsIds = investorContactsIdsByInvestor.get(investorId);
                        for (Id contactId : contactsIds)
                        {
                            if (listOfContacts.contains(contactId))
                            {
                                Integer indexOfId = listOfContacts.indexOf(contactId);
                                String firstPart = listOfContacts.substring(0, indexOfId);
                                String temp = listOfContacts.substring(indexOfId);
                                Integer indexOfSpecialSymbol = temp.indexOf(';');
                                String lastPart = listOfContacts.length() > indexOfId ? temp.substring(indexOfSpecialSymbol): '';
                                listOfContacts = firstPart + contactId + ' : ' + recordsMap.get(contactId).FirstName + ' ' + recordsMap.get(contactId).LastName + lastPart;
                            }
                        }

                        if (investor.ListOfContacts__c != listOfContacts)
                        {
                            investor.ListOfContacts__c = listOfContacts;
                            investorsToUpdate.add(investor);
                        }
                    }

                }

                if (!investorsToUpdate.isEmpty())
                {
                    update investorsToUpdate;
                }
            }
        }
    }
}