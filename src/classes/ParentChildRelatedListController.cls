/**
* Controller for ParentChild    RelatedList LWC.
*
* @author: <Christian Schwabe> (christian.schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 28.02.2020	| Christian Schwabe (christian.schwabe@colliers.com)    | initial version.
*/
public with sharing class ParentChildRelatedListController {
    /**
     * Prepare a flatten generic list from parent and child records.
     * I.e.: Account and optional contact.
     * 
     * Test it in Developer Console:
     * 
     * ParentChildRelatedListController.CustomMap accountName = new ParentChildRelatedListController.CustomMap();
     * accountName .key = 'Account__r.Name';
     * accountName .value = 'accountName';
     * ParentChildRelatedListController.CustomMap accountId = new ParentChildRelatedListController.CustomMap();
     * accountId.key = 'Account__c';
     * accountId.value = 'accountId';
     * 
     * ParentChildRelatedListController.CustomMap contactName = new ParentChildRelatedListController.CustomMap();
     * contactName.key = 'Contact__r.Name';
     * contactName.value = 'contactName';
     * ParentChildRelatedListController.CustomMap contactId = new ParentChildRelatedListController.CustomMap();
     * contactId.key = 'Contact__c';
     * contactId.value = 'contactId';
     * 
     * ParentChildRelatedListController.getFlatHierachy(
     *      'a2a1x000000EJfSAAW',
     *      'Contacts_To_Account_Area__r',
     *      'Area__c'
     *      new List<String>{'Account__c, Account__r.Name, Area__c, Area__r.Name'},
     *      new List<String>{'Contact__c, Contact__r.Name'},
     *      'AccountToArea__c',
     *      new List<ParentChildRelatedListController.CustomMap>{ accountName, accountId },
     *      new List<ParentChildRelatedListController.CustomMap>{ contactName, contactId }
     *  );
     */
    @AuraEnabled(cacheable=true)
    public static List<ParentAndChild> getFlatHierachy(
        Id recordId,
        String relationshipName,
        String whereApiName,
        List<String> parentFields,
        List<String> childFields,
        String parentObjectApiName,
        List<CustomMap> parentFieldMapping,
        List<CustomMap> childFieldMapping
    ){
        String query = 'SELECT Id, ' + String.join(parentFields, ', ') + ', ';
        query += '(SELECT Id, ' + String.join(childFields, ', ') + ' FROM ' + relationshipName + ') ';
        query += 'FROM ' + parentObjectApiName + ' WHERE ' + whereApiName + ' = :recordId';
        System.debug('>>>query: ' + query);

        List<sObject> listWithAccountToAreaAndContacts = new List<sObject>(Database.query(query));
        List<ParentAndChild> listOfParentAndChild = new List<ParentAndChild>();
        for(SObject parent : listWithAccountToAreaAndContacts)// I.e.: AccountToArea__c
        {
            Boolean hasChildRecords = (parent.getSObjects(relationshipName) != null ? true : false);
            Id accountId = null;

            ParentAndChild parentAndChild = new ParentAndChild();
            for(ParentChildRelatedListController.CustomMap customMap : parentFieldMapping)
            {
                String apiname = customMap.key;
                String value = customMap.value;

                switch on apiname {
                    when 'Account__c' {
                        accountId = (Id) parent.get(apiname);
                        parentAndChild.key = accountId;
                        parentAndChild.accountId = accountId;
                    }
                    when 'Account__r.Name' {
                        parentAndChild.accountName = (String) parent.getSObject('Account__r').get('Name');
                    }
                }
            }

            //Add it now because there are no child record.
            if(hasChildRecords == false)
            {
                listOfParentAndChild.add(parentAndChild);
            }
            else
            {           
                //I.e. child => ContactToAccountToArea__c
                for(SObject child : parent.getSObjects(relationshipName)){// I.e.: relationshipName = Contacts_To_Account_Area__r
                    parentAndChild = (ParentChildRelatedListController.ParentAndChild)JSON.deserialize(JSON.serialize(parentAndChild), ParentChildRelatedListController.ParentAndChild.class);
                    parentAndChild.key = (String) child.get('Id');

                    for(ParentChildRelatedListController.CustomMap customMap : childFieldMapping)
                    {
                        String apiname = customMap.key;
                        String value = customMap.value;
        
                        switch on apiname {
                            when 'Contact__c' {
                                Id contactId = (Id) child.get(apiname);
                                parentAndChild.contactId = contactId;
                            }
                            when 'Contact__r.FirstName' {
                                parentAndChild.contactFirstName = (String) child.getSObject('Contact__r').get('FirstName');
                            }
                            when 'Contact__r.LastName' {
                                parentAndChild.contactLastName = (String) child.getSObject('Contact__r').get('LastName');
                            }
                            when 'Contact__r.Phone' {
                                parentAndChild.contactPhone = (String) child.getSObject('Contact__r').get('Phone');
                            }
                            when 'Contact__r.Email' {
                                parentAndChild.contactEmail = (String) child.getSObject('Contact__r').get('Email');
                            }
                            when 'Role__c' {
                                parentAndChild.contactRole = (String) child.get('Role__c');
                            }
                            when 'IsInvestorRepresentation__c' {
                                parentAndChild.isInvestorRepresentative = (Boolean) child.get('IsInvestorRepresentation__c');
                            }
                        }
                    }
                    
                    listOfParentAndChild.add(parentAndChild);
                }
            }
        }

        return listOfParentAndChild;
    }

    public class ParentAndChild
    {
        @AuraEnabled
        public String key;
        @AuraEnabled
        public String accountName;
        @AuraEnabled
        public Id accountId;
        @AuraEnabled
        public Id contactId;
        @AuraEnabled
        public String contactFirstName;
        @AuraEnabled
        public String contactLastName;
        @AuraEnabled
        public String contactPhone;
        @AuraEnabled
        public String contactEmail;
        @AuraEnabled
        public String contactRole;
        @AuraEnabled
        public Boolean isInvestorRepresentative;
    }

    public class CustomMap
    {
        @AuraEnabled
        public String key {get;set;}
        @AuraEnabled
        public String value {get;set;}
    }
}