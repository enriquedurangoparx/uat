/**
 * Created by ille on 2019-05-10.
 */

@istest
private class ExpenseAccountTriggerTest
{
    @istest
    static void shouldValidateNonOpenExpenseAccountDeletion()
    {
        ExpenseAccount__c expenseAccount = UnitTestDataTest.buildExpenseAccount();
        expenseAccount.Status__c = 'Ready for Approval';
        insert expenseAccount;

        try
        {
            delete expenseAccount;
            System.assert(false, 'Should throw exception above!');
        }
        catch (Exception e)
        {
            System.assert(true);
        }

        expenseAccount.Status__c = 'Open';
        update expenseAccount;
        delete expenseAccount;
    }

    @istest
    static void shouldPopulateExpenseAccountName()
    {
        ExpenseAccount__c expenseAccount = UnitTestDataTest.buildExpenseAccount();
        insert expenseAccount;

        User expenseKeeper = [select Name from User where Id = :expenseAccount.ExpenseKeeper__c];
        ExpenseAccount__c updatedExpenseAccount = [select Name from ExpenseAccount__c where Id = :expenseAccount.Id limit 1];

        System.assertEquals(expenseAccount.Year__c + '-' + ((Datetime) expenseAccount.DateRefund__c).format('MM') + ' ' + expenseKeeper.Name, updatedExpenseAccount.Name);
    }
}