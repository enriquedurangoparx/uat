/**
 * Created by thomas.schnocklake on 15.05.18.
 */

@IsTest
private class FilteredListController_Test {

    @isTest static void test_getParent() {

        Account a = new Account(
                Name='account test1',
                ShippingCountry = 'Switzerland');
        insert a;


        Test.startTest();

        FilteredListController.getParent(a.Id);
        Test.stopTest();
    }
    @isTest static void test_querySObject() {

        Account a = new Account(
                Name='account test1',
                ShippingCountry = 'Switzerland');
        insert a;


        Test.startTest();
        String sObjectName = 'Account';
        List<String> fields = new List<String>{'{"fieldName":"Name"}', '{"soqlStmt":"AccountType__c"}', '{"soqlStmt":"AccountType__c"}'};
        String whereClause = ' AccountType__c != null';
        FilteredListController.querySObject(sObjectName, fields, whereClause);
        Test.stopTest();
    }
    @isTest static void test_getSObjectFieldList() {



        Test.startTest();

        FilteredListController.getSObjectFieldList('Account');
        Test.stopTest();
    }
    @isTest static void test_getTranslations() {



        Test.startTest();

        FilteredListController.getTranslations();
        Test.stopTest();
    }
}