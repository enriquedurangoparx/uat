/**
 * Created by ille on 2019-05-29.
 */

@istest
private class ColliersContractTriggerTest
{
    @istest
    static void shouldMaintainAmountOfFrameContracts()
    {
        Account accountA = UnitTestDataTest.buildAccount('Account A');
        Account accountB = UnitTestDataTest.buildAccount('Account B');
        insert new List<Account>{accountA, accountB};

        Account updatedAccountA = [select AmountOfFrameContracts__c from Account where Id = :accountA.Id limit 1];
        System.assert(updatedAccountA.AmountOfFrameContracts__c == null || updatedAccountA.AmountOfFrameContracts__c == 0,
            'AmountOfFrameContracts__c should be 0');

        ColliersContract__c colliersContract = (ColliersContract__c) ColliersContract__c.SObjectType.newSObject(null, true);
        colliersContract.FrameworkContractType__c = 'Preferred in Panel';
        colliersContract.Account__c = accountA.Id;
        colliersContract.Status__c = 'Activated';
        insert colliersContract;

        updatedAccountA = [select AmountOfFrameContracts__c from Account where Id = :accountA.Id limit 1];
        System.assertEquals(1, updatedAccountA.AmountOfFrameContracts__c, 'AmountOfFrameContracts__c should be 1');

        colliersContract.Account__c = accountB.Id;
        update colliersContract;

        updatedAccountA = [select AmountOfFrameContracts__c, (select Id from Contracts__r) from Account where Id = :accountA.Id limit 1];
        System.assertEquals(0, updatedAccountA.AmountOfFrameContracts__c, 'AmountOfFrameContracts__c should be 0: ' + updatedAccountA.Contracts__r.size());

        Account updatedAccountB = [select AmountOfFrameContracts__c from Account where Id = :accountB.Id limit 1];
        System.assertEquals(1, updatedAccountB.AmountOfFrameContracts__c, 'AmountOfFrameContracts__c should be 1');

        ColliersContract__c colliersContract2 = (ColliersContract__c) ColliersContract__c.SObjectType.newSObject(null, true);
        colliersContract2.FrameworkContractType__c = 'Preferred in Panel';
        colliersContract2.Account__c = accountB.Id;
        colliersContract2.Status__c = 'Draft';
        insert colliersContract2;
        
        updatedAccountB = [select AmountOfFrameContracts__c from Account where Id = :accountB.Id limit 1];
        System.assertEquals(1, updatedAccountB.AmountOfFrameContracts__c, 'AmountOfFrameContracts__c should be 1');
        
        colliersContract2.Status__c = 'Activated';
        update colliersContract2;
        
        updatedAccountB = [select AmountOfFrameContracts__c from Account where Id = :accountB.Id limit 1];
        System.assertEquals(2, updatedAccountB.AmountOfFrameContracts__c, 'AmountOfFrameContracts__c should be 2');
        
        colliersContract2.Status__c = 'Expired';
        update colliersContract2;
        
        updatedAccountB = [select AmountOfFrameContracts__c from Account where Id = :accountB.Id limit 1];
        System.assertEquals(1, updatedAccountB.AmountOfFrameContracts__c, 'AmountOfFrameContracts__c should be 1');
        
        delete colliersContract;
        updatedAccountB = [select AmountOfFrameContracts__c from Account where Id = :accountB.Id limit 1];
        System.assertEquals(0, updatedAccountB.AmountOfFrameContracts__c, 'AmountOfFrameContracts__c should be 0');
    }
}