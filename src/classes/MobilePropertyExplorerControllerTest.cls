@isTest
public class MobilePropertyExplorerControllerTest {

    @isTest
    public static void getPropertiesTest(){

        PropertyObject__c propertyObject1 = new PropertyObject__c(Name = 'TestTower1', Geolocation__latitude__s = 50.116135, Geolocation__longitude__s = 8.681198, TypeOfUse__c = 'Office / Mixed-use');
        insert propertyObject1;

        PropertyObject__c propertyObject2 = new PropertyObject__c(Name = 'TestTower2', Geolocation__latitude__s = 50.117443, Geolocation__longitude__s = 8.681104, TypeOfUse__c = 'Office / Mixed-use');
        insert propertyObject2;

        Test.startTest();

        List<sObject> listProperties = MobilePropertyExplorerController.getProperties(50.117200, 8.681114, 0, 10, 0, 100, '\'Office / Mixed-use\'');

        System.assertEquals(listProperties.size(), 2);


        Test.stopTest();
    }

    @isTest
    public static void urlConstructionTest(){
        Test.setCurrentPageReference(new PageReference('Page.mobilePropertyExplorerMap')); 
        System.currentPageReference().getParameters().put('latitude', '52.094633099999996');
        System.currentPageReference().getParameters().put('longitude', '23.691689399999998');
        System.currentPageReference().getParameters().put('distanceMin', '0');
        System.currentPageReference().getParameters().put('distanceMax', '400');
        System.currentPageReference().getParameters().put('rentedMin', '0');
        System.currentPageReference().getParameters().put('rentedMax', '100');
        String jsonObjectList = TestDataFactory.jsonListPropertyObject();
        List<PropertyObject__c> objectListToInsert = (List<PropertyObject__c>) JSON.deserialize(jsonObjectList, List<PropertyObject__c>.class);
        insert objectListToInsert;

        Test.startTest();
            MobilePropertyExplorerController controller = new MobilePropertyExplorerController();
            System.assertEquals(2, controller.propertiesList.size(), 'Wrong number of Property Objects.');
        Test.stopTest();
    }

}