/**
*	
*	@author npo
*	@copyright PARX
*/
@IsTest
private class NewMailingControllerTest
{
	@testSetup
	static void setup()
	{
		TestDataFactory.initializeProducts();

		Embargo__c embargo = new Embargo__c();
		embargo.Name = 'Embargo';
		embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
		insert embargo;

		Account newAcc = TestDataFactory.createAccount('Test 1');
		insert newAcc;

		List<Contact> contacts = TestDataFactory.createContacts(newAcc.Id, 1);
		insert contacts;

		PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
		Opportunity newOpportunity = TestDataFactory.createOpportunity(newAcc.Id, propertyObject);
		insert newOpportunity;

		AccountToOpportunity__c newAccToOpp = TestDataFactory.createAccountToOpportunity(newAcc.Id, newOpportunity.Id);
		insert newAccToOpp;

		List<ContactToInvestor__c> contactToInvestors = TestDataFactory.createContactToInvestor(newAccToOpp.Id, contacts);
		insert contactToInvestors;

		Campaign activeCampaign = new Campaign(IsActive = true, Name = 'Active', Opportunity__c = newOpportunity.Id, Sender__c = UserInfo.getUserId());
		Campaign inactiveCampaign = new Campaign(IsActive = false, Name = 'Inactive', Opportunity__c = newOpportunity.Id);

		insert new List<Campaign> {activeCampaign, inactiveCampaign};

		InvestorMailingConfiguration__c mailingConfiguration = new InvestorMailingConfiguration__c(Campaign__c = activeCampaign.Id,
				Investor__c = newAccToOpp.Id);
        CampaignMember member =
                new CampaignMember(CampaignId = activeCampaign.Id,
                        InvestorContact__c = contactToInvestors.get(0).Id,
                        ContactId = contacts.get(0).Id);
        insert member;

		insert mailingConfiguration;
	}

	@IsTest
	static void testGetInvestorsWithContactsAndLoadInvestorsForCampaign()
	{
		AccountToOpportunity__c newAccToOpp = [SELECT Id FROM AccountToOpportunity__c LIMIT 1];
		Campaign activeCampaign = [SELECT Id FROM Campaign WHERE IsActive = true LIMIT 1];

		Test.startTest();
		System.assertEquals(1, NewMailingController.getInvestorsWithContacts(new List<Id> { newAccToOpp.Id }).size(),
				'We expect one record to be returned');
		System.assertEquals(1, NewMailingController.loadInvestorsForCampaign(activeCampaign.Id).size(),
				'We expect one record to be returned');
		Test.stopTest();
	}

	@IsTest
	static void testLoadCampaigns()
	{
		Opportunity newOpportunity = [SELECT Id FROM Opportunity LIMIT 1];

		Test.startTest();
		System.assertEquals(1, NewMailingController.loadCampaigns(newOpportunity.Id, true).size(),
				'We expect only active campaign to be returned');
		System.assertEquals(2, NewMailingController.loadCampaigns(newOpportunity.Id, false).size(),
				'We expect both active and not active campaigns to be returned');
		Test.stopTest();
	}

	@IsTest
	static void testLoadCampaignById()
	{
		Campaign activeCampaign = [SELECT Id FROM Campaign WHERE IsActive = true LIMIT 1];

		Test.startTest();
		// test is mostly for coverage only
		System.assertNotEquals(null, NewMailingController.loadCampaignById(activeCampaign.Id),
				'We expect json to be returned');
		Test.stopTest();
	}

	@IsTest
	static void testLoadInvestorsWrappersForCampaignAndLoadInvestorsWrappersByIdsAndCampaign()
	{
		Campaign activeCampaign = [SELECT Id FROM Campaign WHERE IsActive = true LIMIT 1];
		AccountToOpportunity__c investor = [SELECT Id FROM AccountToOpportunity__c LIMIT 1];

		Test.startTest();
		// test is mostly for coverage only
		System.assertEquals(1, NewMailingController.loadInvestorsWrappersForCampaign(activeCampaign.Id).size(),
				'We expect one wrapper to be returned, as we have only one investor in campaign');
		System.assertNotEquals(null,
				NewMailingController.loadInvestorsWrappersJsonByIdsAndCampaign(new List<Id>{investor.Id}, activeCampaign.Id),
				'We expect json to be returned');
		Test.stopTest();
	}

	@IsTest
	static void testSaveMailingTemplate()
	{
        Contact c = [SELECT Id, Status__c FROM Contact LIMIT 1];
		Campaign activeCampaign = [SELECT Id, Opportunity__c, Sender__c, EnglishEmailTemplateId__c, GermanEmailTemplateId__c FROM Campaign WHERE IsActive = true LIMIT 1];
		AccountToOpportunity__c investor = [SELECT Id FROM AccountToOpportunity__c LIMIT 1];
		NewMailingController.MailingWrapper mainWrapper = new NewMailingController.MailingWrapper();
		List<NewMailingController.InvestorsWrapper> investorsList = NewMailingController.loadInvestorsWrappersForCampaign(activeCampaign.Id);
		mainWrapper.investorsList = investorsList;
		NewMailingController.MailInfoWrapper mailInfoWrapper = new NewMailingController.MailInfoWrapper();
		mailInfoWrapper.sender = new NewMailingController.SenderWrapper();
		mailInfoWrapper.sender.id = System.UserInfo.getUserId();
		mailInfoWrapper.correspondenceName = 'Test';
        mailInfoWrapper.selectedEnTemplateId = activeCampaign.EnglishEmailTemplateId__c;
        mailInfoWrapper.selectedDeTemplateId = activeCampaign.GermanEmailTemplateId__c;
		mainWrapper.mailInfo  = mailInfoWrapper;

		NewMailingController.CampaignWrapper campInfo = new NewMailingController.CampaignWrapper();
		campInfo.campaignId = activeCampaign.Id;
		campInfo.campaignOpportunityId = activeCampaign.Opportunity__c;
		campInfo.campaignSender = activeCampaign.Sender__c;
		mainWrapper.campaignInfo = campInfo;

		Test.startTest();
		String result = NewMailingController.saveMailingTemplate(JSON.serialize(mainWrapper));
		System.assertNotEquals(null, result, 'We expect result of save not to be empty');

        // validation should fail
//        c.Status__c = 'Inactive';
//        update c;

//        result = NewMailingController.saveMailingTemplate(JSON.serialize(mainWrapper));
//        System.assertNotEquals(null, result, 'We expect result of save not to be empty');

		Test.stopTest();
	}

    @IsTest
    static void testRemoveInvestorFromCampaign()
    {
        Campaign activeCampaign = [SELECT Id, Opportunity__c, Sender__c FROM Campaign WHERE IsActive = true LIMIT 1];
        AccountToOpportunity__c investor = [SELECT Id FROM AccountToOpportunity__c LIMIT 1];

        Test.startTest();
        String result = NewMailingController.removeInvestorFromCampaign(activeCampaign.Id, investor.Id);

        System.assertEquals(0,
                [SELECT Id FROM InvestorMailingConfiguration__c WHERE Investor__c = :investor.Id AND Campaign__c = :activeCampaign.Id].size(),
                'We expect InvestorMailingConfiguration__c to be deleted');
        System.assertEquals(0,
                [SELECT Id FROM CampaignMember WHERE InvestorContact__r.Investor__c = :investor.Id AND CampaignId = :activeCampaign.Id].size(),
                'We expect CampaignMember to be deleted');

        Test.stopTest();
    }

    @IsTest
    static void testStartEmailsSendingProcess()
    {
        Campaign activeCampaign = [SELECT Id, Name, Opportunity__c, Sender__c, Bcc__c, InvestorStatus__c, EnglishEmailTemplateId__c, GermanEmailTemplateId__c, IsActive FROM Campaign WHERE IsActive = true LIMIT 1];

        NewMailingController.MailingWrapper mw = new NewMailingController.MailingWrapper();
        NewMailingController.populateWrapperWithCampaign(mw, activeCampaign);
        List<NewMailingController.InvestorsWrapper> investorsWrappers = NewMailingController.loadInvestorsWrappersForCampaign(activeCampaign.Id);

        List<EmailTemplate> emailTemplates = [SELECT Id FROM EmailTemplate LIMIT 1];
        if (!emailTemplates.isEmpty())
        {
            mw.mailInfo.selectedEnTemplateId = emailTemplates.get(0).Id;
            mw.mailInfo.selectedDeTemplateId = emailTemplates.get(0).Id;
        }
        mw.mailInfo.sender = new NewMailingController.SenderWrapper();
        mw.mailInfo.sender.id = System.UserInfo.getUserId();
        mw.investorsList = investorsWrappers;

        if (!emailTemplates.isEmpty())
        {
            activeCampaign.EnglishEmailTemplateId__c = emailTemplates.get(0).Id;
            activeCampaign.GermanEmailTemplateId__c = emailTemplates.get(0).Id;
        }
        update activeCampaign;

        Test.startTest();
        NewMailingController.startEmailsSendingProcess(JSON.serialize(mw));
        Test.stopTest();
    }

    @IsTest
    static void testValidateEmail()
    {
        System.assert(NewMailingController.validateEmail('test@test.com'));
        System.assert(!NewMailingController.validateEmail('test@testcom'));
        System.assert(!NewMailingController.validateEmail('test.test.com'));
    }



	
	@IsTest
    static void testInvestorsStatusUpdate()
    {
		Campaign activeCampaign = [SELECT Id, Opportunity__c, Sender__c, EnglishEmailTemplateId__c, GermanEmailTemplateId__c FROM Campaign WHERE IsActive = true LIMIT 1];
        List<EmailTemplate> emailTemplates = [SELECT Id FROM EmailTemplate LIMIT 1];
        if (!emailTemplates.isEmpty())
        {
            activeCampaign.EnglishEmailTemplateId__c = emailTemplates.get(0).Id;
            activeCampaign.GermanEmailTemplateId__c = emailTemplates.get(0).Id;
            update activeCampaign;
        }

        AccountToOpportunity__c investor = [SELECT Id FROM AccountToOpportunity__c LIMIT 1];
        CampaignMember campaignMember = [SELECT Id, SendingType__c, Language__c FROM CampaignMember WHERE CampaignId = :activeCampaign.Id LIMIT 1];
        campaignMember.SendingType__c = 'To';
        campaignMember.Language__c = 'English';
        update campaignMember;

        NewMailingController.MailingWrapper mainWrapper = new NewMailingController.MailingWrapper();
        List<NewMailingController.InvestorsWrapper> investorsList = NewMailingController.loadInvestorsWrappersForCampaign(activeCampaign.Id);
        mainWrapper.investorsList = investorsList;
        NewMailingController.MailInfoWrapper mailInfoWrapper = new NewMailingController.MailInfoWrapper();
        mailInfoWrapper.sender = new NewMailingController.SenderWrapper();
        mailInfoWrapper.sender.id = System.UserInfo.getUserId();
        mailInfoWrapper.correspondenceName = 'Test';
        mailInfoWrapper.selectedEnTemplateId = activeCampaign.EnglishEmailTemplateId__c;
		mailInfoWrapper.selectedDeTemplateId = activeCampaign.GermanEmailTemplateId__c;
		mailInfoWrapper.investorStatus = 'Teaser sent';
        mainWrapper.mailInfo  = mailInfoWrapper;

        NewMailingController.CampaignWrapper campInfo = new NewMailingController.CampaignWrapper();
        campInfo.campaignId = activeCampaign.Id;
        campInfo.campaignOpportunityId = activeCampaign.Opportunity__c;
        campInfo.campaignSender = activeCampaign.Sender__c;
		mainWrapper.campaignInfo = campInfo;

		Test.startTest();

        mainWrapper = NewMailingController.investorsStatusUpdate(mainWrapper);

		Test.stopTest();

		System.assertEquals(true, !mainWrapper.statusList.isEmpty(), 'Statuses were not updated');
    }
    
    @IsTest
    static void testValidations()
    {
        String jsonStr = '{"opportunityId":"0061x00000Bez1pAAB","campaignInfo":{},"investorsList":[{"mailingConfigSalutationEn":"","mailingConfigSalutationDe":"","mailingConfigRec":{"attributes":{"type":"InvestorMailingConfiguration__c"}},"mailingConfigId":null,"linkToInvestor":null,"investorId":"a1u1x000000FpWCAA0","investorContactsList":[{"mailingStatus":null,"linkToCampaignMember":null,"language":"German","investorContactSendingType":"Cc","investorContactId":"a2S1x000000EBXgEAO","contactUrl":"/lightning/r/ContactToInvestor__c/a2S1x000000EBXgEAO/view","contactToInvestor":null,"contactStatus":"Inactive","contactSalutationEn":"Dear Mr. TEST","contactSalutationDe":"Sehr geehrter Herr TEST","contactName":"TEST","contactId":"0031x00000fRR8nAAG","contactEmail":"test@test.com","campaignMemberRec":null,"campaignMemberId":null},{"mailingStatus":null,"linkToCampaignMember":null,"language":"","investorContactSendingType":"To","investorContactId":"a2S1x000000EBXpEAO","contactUrl":"/lightning/r/ContactToInvestor__c/a2S1x000000EBXpEAO/view","contactToInvestor":null,"contactStatus":"Active","contactSalutationEn":"Dear Mr. Hossainzadeh","contactSalutationDe":"Sehr geehrter Herr Hossainzadeh","contactName":"Baktash Hossainzadeh","contactId":"0031x00000bWm1LAAS","contactEmail":null,"campaignMemberRec":null,"campaignMemberId":null}],"investor":{"attributes":{"type":"AccountToOpportunity__c"}},"accountStatus":"Active","accountName":"CSW GmbH","accountLink":null,"accountId":"0011x00000iAkqBAAS"},{"mailingConfigSalutationEn":"Dear Mr. Sending 01;","mailingConfigSalutationDe":"","mailingConfigRec":{"attributes":{"type":"InvestorMailingConfiguration__c"}},"mailingConfigId":null,"linkToInvestor":null,"investorId":"a1u1x000000GI1EAAW","investorContactsList":[{"mailingStatus":null,"linkToCampaignMember":null,"language":"English","investorContactSendingType":"To","investorContactId":"a2S1x000000EHyqEAG","contactUrl":"/lightning/r/ContactToInvestor__c/a2S1x000000EHyqEAG/view","contactToInvestor":null,"contactStatus":"Active","contactSalutationEn":"Dear Mr. Sending 01","contactSalutationDe":"Sehr geehrter Herr Sending 01","contactName":"Test Sending 01","contactId":"0031x00000twnuhAAA","contactEmail":"egor.markuzov@parx.com","campaignMemberRec":null,"campaignMemberId":null},{"mailingStatus":null,"linkToCampaignMember":null,"language":"","investorContactSendingType":"Cc","investorContactId":"a2S1x000000EHyrEAG","contactUrl":"/lightning/r/ContactToInvestor__c/a2S1x000000EHyrEAG/view","contactToInvestor":null,"contactStatus":"Active","contactSalutationEn":"Dear Mr. Sending 02","contactSalutationDe":"Sehr geehrter Herr Sending 02","contactName":"Test Sending 02","contactId":"0031x00000twnumAAA","contactEmail":"egor.markuzov@gmail.com","campaignMemberRec":null,"campaignMemberId":null}],"investor":{"attributes":{"type":"AccountToOpportunity__c"}},"accountStatus":null,"accountName":"Test sending","accountLink":null,"accountId":"0011x000011p6CYAAY"}],"mailInfo":{"correspondenceName":"Test","sender":{"id":"0051x000002aScjAAE","sObjectType":"User","icon":"standard:user","title":"Egor Markuzov"},"bcc":"test","selectedEnTemplateId":"00X1x00000168TEEAY","opportunityId":"0061x00000Bez1pAAB"}}';
        NewMailingController.MailingWrapper mainWrapper = (NewMailingController.MailingWrapper) JSON.deserialize(jsonStr, NewMailingController.MailingWrapper.class);

        Test.startTest();

        mainWrapper = NewMailingController.validateMailingTemplate(mainWrapper, true);

		Test.stopTest();
    }
    /**
     * US2488: Test for loadReportId
     * @author npo
     */
    @IsTest
    static void testLoadReportId()
    {
        // test is for coverage only
        Test.startTest();
        Id reportId = NewMailingController.loadReportId();
        Test.stopTest();
    }
}