/**
* Handle business logic for AccountToOpportunity__c executed by the trigger
*
* @author: <Egor Markuzov> (egor.markuzov@colliers.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 20.02.2020	| Egor Markuzov (egor.markuzov@colliers.com)	        | initial version.
*/
public inherited sharing class AccountToOpportunityTrigger extends ATrigger 
{
    public override void hookBeforeDelete(List<SObject> records)
    {
        System.debug('AccountToOpportunityTrigger hookBeforeDelete');
        preventDeletionIfHasRelatedActivity(records);
    }

    private void preventDeletionIfHasRelatedActivity(List<AccountToOpportunity__c> newList)
    {
        Map<Id, Boolean> preventToDeleteMap = new Map<Id, Boolean>();
        for (AccountToOpportunity__c investor : newList)
        {
            preventToDeleteMap.put(investor.Id, false);
        }

        for (Task tsk : [SELECT Id, WhatId FROM Task WHERE WhatId IN :preventToDeleteMap.keySet()])
        {
            if (preventToDeleteMap.containsKey(tsk.WhatId))
            {
                preventToDeleteMap.put(tsk.WhatId, true);
            }
        }

        for (Status__c status : [SELECT Id, Investor__c FROM Status__c WHERE Investor__c IN :preventToDeleteMap.keySet()])
        {
            if (preventToDeleteMap.containsKey(status.Investor__c))
            {
                preventToDeleteMap.put(status.Investor__c, true);
            }
        }

        for (AccountToOpportunity__c investor : newList)
        {
            if (preventToDeleteMap.containsKey(investor.Id) && preventToDeleteMap.get(investor.Id))
            {
                investor.addError(System.Label.GeneralLabelErrorBecauseHasActivityOrStatus);
            }
        }
    }
}