/**
* Helperclass to aggregate SubCommissions__c.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 02.07.2019    | Christian Schwabe (Christian.Schwabe@colliers.com)    | initial version.
*/
public without sharing class SubCommissionHelper
{
	/**
	 * Extract recordId from listOfSubCommission by object.
	 * 
	 * @param  listOfSubCommission		source (lookup-relationship) to extract parent recordId (Opportunity, ONB2__Invoice__c).
	 * @return Map<String, Set<Id>>    Key: apiname of object, Value: Set of ids for object
	 */
	public static Map<String, Set<Id>> extractParentRecordId(List<SubCommissions__c> listOfSubCommission){
		Map<String, Set<Id>> mapOfIdsByObjectName = new Map<String, Set<Id>>();

    	Set<Id> setOfOpportunityId = new Set<Id>();
    	Set<Id> setOfInvoiceId = new Set<Id>();
    	for(SubCommissions__c subCommission : listOfSubCommission){
    		Id opportunityId = subCommission.Opportunity__c;
    		Id invoiceId = subCommission.Invoice__c;

    		if(String.isNotBlank(opportunityId)){
    			setOfOpportunityId.add(opportunityId);
    		} 
    		if(String.isNotBlank(invoiceId)){
    			setOfInvoiceId.add(invoiceId);
    		}
    	}

    	mapOfIdsByObjectName.put('Opportunity', setOfOpportunityId);
    	mapOfIdsByObjectName.put('ONB2__Invoice__c', setOfInvoiceId);

    	return mapOfIdsByObjectName;
	}

	/**
	 * Check if subcommission is related to 'Opportunity' or 'ONB2__Invoice__c' or 'both'.
	 * 
	 * @param  listOfSubCommission	list of subcommission to check.
	 * @return                     'Opportunity' or 'ONB2__Invoice__c' or 'both'
	 */
	public static String checkRelation(SubCommissions__c subCommission){
		String relatedTo = null;
		Id opportunityId = subcommission.Opportunity__c;
		Id invoiceId = subcommission.Invoice__c;

		if(String.isNotBlank(opportunityId) && String.isNotBlank(invoiceId)){
			relatedTo = 'both';
		} else if(String.isNotBlank(opportunityId)){
			relatedTo = 'Opportunity';
		} else if(String.isNotBlank(invoiceId)){
			relatedTo = 'ONB2__Invoice__c';
		}

		return relatedTo;
	}
}