/**
* Test class for PropertyGeolocationUpdaterBatch
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
* @Resources: https://developers.google.com/maps/documentation/geocoding/start
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 26.02.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
*/

@IsTest
private class PropertyGeolocationUpdaterBatchTest
{
    @IsTest
    static void batchTest()
    {
        Test.setMock(HttpCalloutMock.class, new GeolocationCalloutMock());
        PropertyObject__c property = new PropertyObject__c(
                PostalCode__c = '1600',
                Street__c = 'Amphitheatre Parkway, Mountain View',
                City__c = 'CA',
                Country__c = 'United States'
        );

        insert property;
        System.debug(JSON.serializePretty(property));

        Test.startTest();
        PropertyGeolocationUpdaterBatch batch = new PropertyGeolocationUpdaterBatch(new List<PropertyObject__c>{ property });
        Database.executeBatch(batch, 1);
        Test.stopTest();

        property = [
                SELECT Id, Geolocation__Latitude__s, Geolocation__Longitude__s
                FROM PropertyObject__c
        ];

        System.assertEquals(55.1793713, property.Geolocation__Latitude__s, 'Geolocation data should be updated');
        System.assertEquals(30.241673, property.Geolocation__Longitude__s, 'Geolocation data should be updated');
    }
}