/**
* Controller for InvestorSelectionObjectMatching Lightning Web Component
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 09.12.2019    | Egor Markuzov (egor.markuzov@parx.com)                | initial version.
* 0.2 13.12.2019    | Maksim Fedorenko (maksim.fedorenko@parx.com)          | Added getOpportunity method
*/
public class LWCObjectMatchingController {
    // PropertyObject -> AcquisitionProfile field map
    private static final Map<String, String> objToProfileFieldsMatching = new Map<String, String>
    {
        'RiskCategory__c' => 'RiskCategory__c', // picklist
        'Category__c' => 'CategoryHotel__c', // multi-picklist
        'LocationType__c' => 'LocationType__c', // multi-picklist
        'FactorActual__c' => 'Factor__c',
        'WALT__c' => 'WALTFrom__c',
        'Certification__c' => 'Certification__c', // picklist
        'AmountOfSpaceLet__c' => 'OccupancyRate__c',
        //'BrandHotels__c' => 'BrandHotels__c',// Deprecated (US 2886)
        'Submarket__c' => 'Submarket__c',
        'TypeOfContract__c' => 'TypeOfContract__c',
        //'BrandHotels__c' => 'BrandHotels__c',
        'PlanningRegulations__c' => 'AccountingForPlanningAndControl__c',
        'PlotWithBuildingPermission__c' => 'PlotWithBuildingPermission__c',
        'Status__c' => 'Status__c',
        'TypeOfUse__c' => 'TypeOfUse__c'
        // 'SqmInTotal__c' => 'Volume__c',
        // 'PurchasingPriceOffer__c' => 'InvestmentVolumeFrom__c',
        // 'SqmPlot__c' => 'PlotFrom__c',
        // 'VacancySqm__c' => 'LettingAreaFrom__c',
        // 'AccomodationUnit__c' => 'ResidentialFrom__c',
        // 'NumberOfRooms__c' => 'NumberOfRoomsFrom__c',
        // 'OccupancyRate__c' => 'OccupancyRate__c',
    };

    private static final Map<String, List<String>> compoundFieldsMatching = new Map<String, List<String>>
    {
        'PurchasingPriceOffer__c' => new List<String>{'InvestmentVolumeFrom__c', 'InvestmentVolumeTo__c'},
        'SqmInTotal__c' => new List<String>{'LettingAreaFrom__c', 'LettingAreaTo__c'},
        'SqmPlot__c' => new List<String>{'PlotAreaFrom__c', 'PlotAreaTo__c'},
        //'VacancySqm__c' => new List<String>{'LettingAreaFrom__c', 'LettingAreaTo__c'},
        'AccomodationUnit__c' => new List<String>{'ResidentialFrom__c', 'ResidentialTo__c'},
//        'YearofConstruction__c' => new List<String>{'YearOfConstructionFrom__c', 'YearOfConstructionTo__c'},
        'NumberOfRooms__c' => new List<String>{'NumberOfRoomsFrom__c', 'NumberOfRoomsTo__c'},
        'SqmOffice__c' => new List<String>{'OfficeAreaFrom__c', 'OfficeAreaTo__c'},
        'RealizableBGF__c' => new List<String>{'RealizableBGFSqmFrom__c', 'RealizableBGFSqmTo__c'},
        'SqmLogistics__c' => new List<String>{'LogisticsAreaFrom__c', 'LogisticsAreaTo__c'},
        'SqmRetail__c' => new List<String>{'RetailAreaFrom__c', 'RetailAreaTo__c'},
        'AccomodationUnit__c' => new List<String>{'ResidentialFrom__c', 'ResidentialTo__c'},
        'IRR__c' => new List<String>{'IRRFrom__c', 'IRRTo__c'},
        'CoC__c' => new List<String>{'CoCFrom__c', 'CoCTo__c'},
        'EquityMultiplier__c' => new List<String>{'EquityMultiplierFrom__c', 'EquityMultiplierTo__c'}
    };

    private static final Map<String, String> multiPicklistFieldsMatching = new Map<String, String>
    {
        'Country__c' => 'InvestmentAreaStates__c', // picklist
        'TypeOfRealEstate__c' => 'TypeOfRealEstate__c', // multi-picklist
        'MacroLocation__c' => 'MacroLocation__c',
        'MicroLocation__c' => 'MicroLocation__c'
    };

    private static final Map<String, String> checkboxMatching = new Map<String, String>
    {
        'ConversionProperty__c' => 'ConversionProperty__c'
    };

    @AuraEnabled
    public static String getMatchingContacts(String searchTerm, Id opportunityId, Integer limitSize)
    {
        System.debug('searchTerm: ' + searchTerm);
        List<FilterStructure> fsList = (List<FilterStructure>) JSON.deserialize(searchTerm, List<FilterStructure>.class);
        System.debug('>>>: ' + fsList);

        String query = '' +
                'SELECT Id, Name, InvestorRepresentative__c, InvestorRepresentative__r.FirstName, InvestorRepresentative__r.LastName, ' +
                    'InvestorRepresentative__r.Account.Name, InvestorRepresentative__r.Account.BillingStreet, InvestorRepresentative__r.Account.BillingCity, ' +
                    'InvestorRepresentative__r.Email, InvestorRepresentative__r.Brand__c, InvestorRepresentative__r.Brand__r.Name, ' +
                    'TypeOfUse__c, TypeOfRealEstate__c, RiskCategory__c, ' +
                    'MacroLocation__c, MicroLocation__c, CategoryHotel__c, Certification__c ' +
                'FROM AcquisitionProfile__c';
        List<String> andFilter = new List<String>();

        String objectName = 'AcquisitionProfile__c';
        SObjectType r = ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType();
        DescribeSObjectResult d = r.getDescribe();
        Set<String> objectTypes = new Set<String>{'CURRENCY', 'DOUBLE', 'PERCENT'};

        for (FilterStructure fltstr : fsList)
        {
            if (fltstr.isActive)
            {
                for (Filter flt : fltstr.filters)
                {
                    List<String> orFilter = new List<String>();

                    // Adjustments to https://dev.azure.com/ColliersDeutschland/SFINV/_workitems/edit/1959/
                    // Because it is very special case, we just hardcode the values
                    // Section for US 1959 STARTS
                    if (flt.isActive && flt.values != null && !flt.values.isEmpty() && flt.values[0] != null)
                    {
                        String tempFilterString = '';
                        if (flt.apiName == 'KindOfDevelopment__c' && flt.values[0] == 'Brown-Field')
                        {
                            tempFilterString = 'BrownFields__c = true';
                            andFilter.add(tempFilterString);
                            continue;
                        }
                        else if (flt.apiName == 'KindOfDevelopment__c'
                                 && (flt.values[0] == 'Project Development' || flt.values[0] == 'Green-Field'))
                        {
                            tempFilterString = 'ProjectDevelopmentGreenFields__c = true';
                            andFilter.add(tempFilterString);
                            continue;
                        }
                    }
                    // Section for US 1959 ENDS

                    if (flt.isActive
                        && (objToProfileFieldsMatching.containsKey(flt.apiName)
                            || compoundFieldsMatching.containsKey(flt.apiName)
                            || multiPicklistFieldsMatching.containsKey(flt.apiName)
                            || checkboxMatching.containsKey(flt.apiName)
                            )
                        )
                    {
                        // Second validation just a workaround that will be fixed on frontend in future
                        if (flt.values != null
                            && !flt.values.isEmpty()
                            && flt.values[0] != null
                            && (objToProfileFieldsMatching.containsKey(flt.apiName)
                                || multiPicklistFieldsMatching.containsKey(flt.apiName)
                                || checkboxMatching.containsKey(flt.apiName)
                                )
                            )
                        {
                            if (objToProfileFieldsMatching.containsKey(flt.apiName)
                                && objectTypes.contains(String.valueOf(d.fields.getMap().get(objToProfileFieldsMatching.get(flt.apiName)).getDescribe().getType())))
                            {
                                // For currency, double, percent fields
                                andFilter.add(objToProfileFieldsMatching.get(flt.apiName) + ' IN ' + flt.values);
                            }
                            else if (multiPicklistFieldsMatching.containsKey(flt.apiName))
                            {
                                // For multipicklist fields
                                List<String> tempValues = new List<String>();

                                for (String val : flt.values)
                                {
                                    tempValues.add('\'' + val + '\'');
                                }
                                andFilter.add(multiPicklistFieldsMatching.get(flt.apiName) + ' INCLUDES (' + String.join(tempValues, ', ') + ')');
                            }
                            else if (checkboxMatching.containsKey(flt.apiName))
                            {
                                // For checkbox fields
                                if (!flt.values.isEmpty())
                                {
                                    andFilter.add(checkboxMatching.get(flt.apiName) + ' = ' + Boolean.valueOf(flt.values.get(0)));
                                }
                            }
                            else
                            {
                                // For picklists fields
                                List<String> tempValues = new List<String>();

                                for (String val : flt.values)
                                {
                                    tempValues.add('\'' + val + '\'');
                                }
                                andFilter.add(objToProfileFieldsMatching.get(flt.apiName) + ' IN ' + tempValues);
                            }
                            continue;
                        }
                        String tempFilterString = '';
                        if (String.isNotBlank(flt.minValue) && String.isNotBlank(flt.maxValue))
                        {
                            tempFilterString = objToProfileFieldsMatching.containsKey(flt.apiName) ? objToProfileFieldsMatching.get(flt.apiName) + ' >= ' + Decimal.valueOf(flt.minValue)  + ' AND ' + objToProfileFieldsMatching.get(flt.apiName) + ' <= ' + Decimal.valueOf(flt.maxValue) :
                                               compoundFieldsMatching.containsKey(flt.apiName) ? compoundFieldsMatching.get(flt.apiName)[0] + ' >= ' + Decimal.valueOf(flt.minValue)  + ' AND ' + compoundFieldsMatching.get(flt.apiName)[1] + ' <= ' + Decimal.valueOf(flt.maxValue) : '';
                            andFilter.add(tempFilterString);
                            continue;
                        }
                        if (String.isNotBlank(flt.minValue) && String.isBlank(flt.maxValue))
                        {
                            tempFilterString = objToProfileFieldsMatching.containsKey(flt.apiName) ? objToProfileFieldsMatching.get(flt.apiName) + ' >= ' + Decimal.valueOf(flt.minValue) :
                                               compoundFieldsMatching.containsKey(flt.apiName) ? compoundFieldsMatching.get(flt.apiName)[0] + ' >= ' + Decimal.valueOf(flt.minValue) : '';
                            andFilter.add(tempFilterString);
                            continue;
                        }
                        if (String.isBlank(flt.minValue) && String.isNotBlank(flt.maxValue))
                        {
                            tempFilterString = objToProfileFieldsMatching.containsKey(flt.apiName) ? objToProfileFieldsMatching.get(flt.apiName) + ' <= ' + Decimal.valueOf(flt.maxValue) :
                                               compoundFieldsMatching.containsKey(flt.apiName) ? compoundFieldsMatching.get(flt.apiName)[1] + ' <= ' + Decimal.valueOf(flt.maxValue) : '';
                            andFilter.add(tempFilterString);
                            continue;
                        }
                    }
                }
            }
        }

        String filterQuery = '';
        if (!andFilter.isEmpty())
        {
            query += ' WHERE ';
            System.debug('>>>andFilter: ' + andFilter);
            filterQuery = String.join(andFilter, ' AND ');
        }

        query += filterQuery;
        query += ' LIMIT :limitSize';
        System.debug('filterQuery: ' + query);
        Map<Id, ContactMatching> contIdToContactMatchingMap = new Map<Id, ContactMatching>();
        Map<Id, AcquisitionProfile__c> IdToacqProfilesMap = new Map<Id, AcquisitionProfile__c>();
        // Collect matching AcquisitionProfile__c
        if (String.isNotBlank(filterQuery))
        {
            IdToacqProfilesMap = new Map<Id, AcquisitionProfile__c>((List<AcquisitionProfile__c>) Database.query(query));

            for (AcquisitionProfile__c acqProfile : IdToacqProfilesMap.values())
            {
                if (String.isNotBlank(acqProfile.InvestorRepresentative__c))
                {
                    if (!contIdToContactMatchingMap.containsKey(acqProfile.InvestorRepresentative__c))
                    {
                        contIdToContactMatchingMap.put(acqProfile.InvestorRepresentative__c, new ContactMatching(acqProfile));
                    }

                    contIdToContactMatchingMap.get(acqProfile.InvestorRepresentative__c).acquisitionProfiles += acqProfile.Name + '; ';
                }
            }

            for (AcquisitionProfileContact__c acqProfile : [SELECT Id, Contact__c,toLabel(Role__c),
                        Contact__r.LastName, Contact__r.FirstName, Contact__r.AccountId,
                        Contact__r.Title, Contact__r.Position__c, Contact__r.Email,
                        PurchaseProfile__c, PurchaseProfile__r.Name
                FROM AcquisitionProfileContact__c
                WHERE PurchaseProfile__c IN :IdToacqProfilesMap.keySet()])
            {
                if (!contIdToContactMatchingMap.containsKey(acqProfile.Contact__c))
                {
                    contIdToContactMatchingMap.put(acqProfile.Contact__c, new ContactMatching());
                }

                contIdToContactMatchingMap.get(acqProfile.Contact__c).acquisitionProfiles += acqProfile.PurchaseProfile__r.Name + '; ';

                if(acqProfile.Role__c != null){
                    contIdToContactMatchingMap.get(acqProfile.Contact__c).investorRole += acqProfile.Role__c + '; ';
                }
            }

            for (Contact cont : [SELECT Id, FirstName, LastName, Account.Name, Account.BillingStreet, Status__c, Account.Status__c,
                                        Account.BillingCity, Email, Brand__c, Brand__r.Name
                                FROM Contact
                                WHERE Id IN :contIdToContactMatchingMap.keySet()])
            {
                contIdToContactMatchingMap.get(cont.Id).record = cont;
                if (cont.Status__c == 'Active' || cont.Account.Status__c == 'Active')
                {
                    contIdToContactMatchingMap.get(cont.Id).isActive = true;
                }
                else
                {
                    contIdToContactMatchingMap.get(cont.Id).isActive = false;
                }
            }

            for (ContactToInvestor__c coi : [SELECT Contact__c
                                            FROM ContactToInvestor__c
                                            WHERE Investor__r.Opportunity__c = :opportunityId]){
                if (contIdToContactMatchingMap.containsKey(coi.Contact__c))
                {
                    contIdToContactMatchingMap.get(coi.Contact__c).bAlreadyExists = true;
                }
            }
        }
        Map<String, Object> result = new Map<String, Object>();
        result.put('matchingRecords', contIdToContactMatchingMap.values());
        result.put('acquisitionProfiles', IdToacqProfilesMap.values());
        System.debug('result: ' + JSON.serialize(result));

        return JSON.serialize(result);
    }

    public static Opportunity getOpportunity(Id opportunityId)
    {
        return [
                SELECT PrimaryProperty__r.TypeOfUse__c, PrimaryProperty__r.TypeOfRealEstate__c, PrimaryProperty__r.RiskCategory__c,
                        PrimaryProperty__r.MacroLocation__c, PrimaryProperty__r.Submarket__c, PrimaryProperty__r.PurchasingPriceOffer__c,
                        PrimaryProperty__r.SqmInTotal__c, PrimaryProperty__r.OccupancyRate__c, PrimaryProperty__r.WALT__c,
                        PrimaryProperty__r.Certification__c, PrimaryProperty__r.YearofConstruction__c, PrimaryProperty__r.NumberOfRooms__c,
                        PrimaryProperty__r.Category__c, PrimaryProperty__r.LocationType__c,
                        PrimaryProperty__r.BrandHotels__c, PrimaryProperty__r.SqmOffice__c, PrimaryProperty__r.RealizableBGF__c,
                        PrimaryProperty__r.SqmPlot__c, PrimaryProperty__r.PlanningRegulations__c, PrimaryProperty__r.PlotWithBuildingPermission__c,
                        PrimaryProperty__r.ConversionProperty__c, PrimaryProperty__r.SqmRetail__c, PrimaryProperty__r.AccomodationUnit__c,
                        PrimaryProperty__r.IRR__c, PrimaryProperty__r.CoC__c, PrimaryProperty__r.EquityMultiplier__c, PrimaryProperty__r.FactorActual__c,
                        PrimaryProperty__r.KindOfDevelopment__c
                FROM Opportunity
                WHERE Id =: opportunityId
        ];
    }

    @AuraEnabled(Cacheable=true)
    public static PropertyObjectWrapper getPropertyObjectWrapper(Id opportunityId)
    {
        return new PropertyObjectWrapper(opportunityId);
    }

    public class PropertyObjectWrapper
    {
        @AuraEnabled
        public Opportunity opportunity { get; set; }

        @AuraEnabled
        public Map<String, String> objToProfileFieldsMatching { get; set; }

        public PropertyObjectWrapper(Id opportunityId){
            opportunity = getOpportunity(opportunityId);
            objToProfileFieldsMatching = LWCObjectMatchingController.objToProfileFieldsMatching;
            objToProfileFieldsMatching.putAll(LWCObjectMatchingController.multiPicklistFieldsMatching);
        }
    }

    public class ContactMatching
    {
        @AuraEnabled public Contact record { get; set; }
        @AuraEnabled public AcquisitionProfile__c acquisitionProfile { get; set; }
        @AuraEnabled public String acquisitionProfiles { get; set; }
        @AuraEnabled public String investorRole { get; set; }
        @AuraEnabled public Boolean bAlreadyExists { get; set; }
        @AuraEnabled public Boolean isActive { get; set; }

        public ContactMatching(){
            this.record = new Contact();
            this.acquisitionProfile = new AcquisitionProfile__c();
            this.acquisitionProfiles = '';
            this.investorRole = '';
            this.bAlreadyExists = false;
            this.isActive = false;
        }

        public ContactMatching(AcquisitionProfile__c acquisitionProfile){
            this.record = new Contact();
            this.acquisitionProfile = acquisitionProfile;
            this.acquisitionProfiles = '';
            this.investorRole = '';
            this.bAlreadyExists = false;
            this.isActive = false;
        }
    }

    public class FilterStructure
    {
        // String accordionName { get; set; }
        List<Filter> filters { get; set; }
        Boolean isActive { get; set; }

        public FilterStructure()
        {
            // this.accordionName = '';
            this.filters = new List<Filter>();
            this.isActive = false;
        }
    }

    public class Filter
    {
        String apiName { get; set; }
        String label { get; set; }
        Boolean isActive { get; set; }
        List<String> values { get; set; }
        String minValue { get; set; }
        String maxValue { get; set; }

        public Filter()
        {
            // this.apiName = '';
            // this.label = '';
            // this.isActive = false;
            this.values = new List<String>();
            // this.minValue = '';
            // this.maxValue = '';
        }
    }
}