/**
* Handles the trigger logic of Account.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 18.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | initial version.
*/

public with sharing class AccountTrigger extends ATrigger
{
    public override void hookBeforeDelete(List<SObject> records)
    {
        AccountService.setAccountHierachy(records);
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        AccountService.setContactToInactive(records, oldRecords);
    }
}