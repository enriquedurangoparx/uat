/**
* Is used within the lwc PropertyTileList to search for PropertyObject__c.
*
* @author:  Sebastian Meier (s.meier@artemis-innovations.de)
*
* @history:
* version		    | author						| changes
* ====================================================================================
* 0.1 12.09.2019	| Sebastian Meier (s.meier@artemis-innovations.de)	    | inital version.
* 0.2 13.09.2019	| Christian Schwabe (christian.schwabe@colliers.com)	| change method definition for getPagedPropertyList to search with a date range.
* 0.3 22.11.2019	| Egor Markuzov (egor.markuzov@parx.com)                | added getPropertyForMap() to show pins on Map
*/

public with sharing class PropertyController {
   /* @AuraEnabled(cacheable=true)
    public static PropertyObject__c[] getPropertyList(String searchKey, Decimal yearOfConstruction) {//Integer numberOfRooms, Integer minBathrooms
        String key = '%' + searchKey + '%';
        //address__c, city__c, state__c, price__c, baths__c, beds__c, , location__latitude__s, location__longitude__s
        return [SELECT Id, Name, YieldActual__c, Thumbnail__c, SubmarketDescription__c, SqmInTotal__c, sqmOffice__c, YearofConstruction__c FROM PropertyObject__c
                      WHERE (Name LIKE :key)//OR city__c LIKE :key OR tags__c LIKE :key
					  AND YearOfConstructionNumber__c >= :yearOfConstruction
                      //AND beds__c >= :numberOfRooms
                      //AND baths__c >= :minBathrooms
                      ORDER BY YearOfConstructionNumber__c LIMIT 100];
    }*/

    @AuraEnabled(cacheable=true)
    public static PagedResult getPagedPropertyList(String string_searchKey, String string_minYearOfConstruction, String string_maxYearOfConstruction, /*Integer numberOfRooms, Integer minBathrooms,*/ Integer pageSize, Integer pageNumber) {
        System.debug('>>>getPagedPropertyList called.');
        System.debug('>>>searchKey: ' + string_searchKey);
        System.debug('>>>string_minYearOfConstruction: ' + string_minYearOfConstruction);
        System.debug('>>>string_maxYearOfConstruction: ' + string_maxYearOfConstruction);

        Integer integer_minYearOfConstruction = (String.isBlank(string_minYearOfConstruction) ? 0 : Integer.valueOf(string_minYearOfConstruction));
        Integer integer_maxYearOfConstruction = (String.isBlank(string_maxYearOfConstruction) ? 9999 : Integer.valueOf(string_maxYearOfConstruction));
        System.debug('>>>integer_minYearOfConstruction: ' + integer_minYearOfConstruction);
        System.debug('>>>integer_maxYearOfConstruction: ' + integer_maxYearOfConstruction);

        pageSize = Integer.valueOf(pageSize + '');
        pageNumber = Integer.valueOf(pageNumber + '');

        Integer pSize = (Integer)pageSize;
        String key = (String.isBlank(string_searchKey) ? '%' : '%' + string_searchKey + '%');// https://salesforce.stackexchange.com/questions/16377/how-to-include-null-values-for-a-field-filtered-using-a-like-expression-in-a-sta/16379
        Integer offset = ((Integer)pageNumber - 1) * pSize;
        PagedResult result =  new PagedResult();
        result.pageSize = pSize;
        result.pageNumber = (Integer) pageNumber;
        result.totalItemCount = [SELECT count() FROM PropertyObject__c
                        WHERE (Name LIKE :key)/*OR city__c LIKE :key OR tags__c LIKE :key*/
                  	    AND YearOfConstructionNumber__c >= :integer_minYearOfConstruction AND YearOfConstructionNumber__c <= :integer_maxYearOfConstruction
                        /*AND beds__c >= :numberOfRooms
                        AND baths__c >= :minBathrooms*/];
        result.records = [SELECT
                                Id,
                                Name,
                                YieldActual__c,
                                Thumbnail__c,
                                PropertyDescriptionDe__c,
                                SqmInTotal__c,
                                sqmOffice__c,
                                YearofConstruction__c,
                                Geolocation__Latitude__s,
                                Geolocation__Longitude__s,
                                Street__c,
                                PostalCode__c,
                                City__c,
                                Country__c
                        FROM PropertyObject__c
                        WHERE (Name LIKE :key)/*OR city__c LIKE :key OR tags__c LIKE :key*/
                            AND YearOfConstructionNumber__c >= :integer_minYearOfConstruction AND YearOfConstructionNumber__c <= :integer_maxYearOfConstruction
                            //AND YearOfConstructionNumber__c <= :yearOfConstruction
                            //AND beds__c >= :numberOfRooms
                            //AND baths__c >= :minBathrooms
                        ORDER BY
                            YearOfConstructionNumber__c
                        LIMIT :pSize
                        OFFSET :offset];
        System.debug('>>>result: ' + result);
        return result;
    }

    @AuraEnabled(cacheable=true)
    public static PagedResult getPropertyForMap(String string_searchKey, String string_minYearOfConstruction, String string_maxYearOfConstruction)
    {
        Integer integer_minYearOfConstruction = (String.isBlank(string_minYearOfConstruction) ? null : Integer.valueOf(string_minYearOfConstruction));
        Integer integer_maxYearOfConstruction = (String.isBlank(string_maxYearOfConstruction) ? null : Integer.valueOf(string_maxYearOfConstruction));
        String key = (String.isBlank(string_searchKey) ? '%' : '%' + string_searchKey + '%');
        PagedResult result =  new PagedResult();
        result.records = [SELECT Id, Name, YieldActual__c, Thumbnail__c, PropertyDescriptionDe__c, SqmInTotal__c,
                                 sqmOffice__c, YearofConstruction__c, Geolocation__Latitude__s, Geolocation__Longitude__s, Street__c,
                                 PostalCode__c, City__c, Country__c 
                          FROM PropertyObject__c
                          WHERE (Name LIKE :key)
                          AND YearOfConstructionNumber__c >= :integer_minYearOfConstruction AND YearOfConstructionNumber__c <= :integer_maxYearOfConstruction
                          ORDER BY YearOfConstructionNumber__c];
        result.totalItemCount = result.records.size();
        return result;
    }

    @AuraEnabled(cacheable=true)
    public static List<ContentVersion> getPictures (Id propertyId) {

        List<ContentDocumentLink> links = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink 
                                           WHERE LinkedEntityId=:propertyId AND ContentDocument.FileType in ('PNG','JPG','GIF')];

        if (links.isEmpty()) {
            return null;
        }

        Set<Id> contentIds = new Set<Id>();

        for (ContentDocumentLink link :links) {
            contentIds.add(link.ContentDocumentId);
        }

        return [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :contentIds AND IsLatest=true ORDER BY CreatedDate];
    }

}