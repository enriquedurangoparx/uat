/**
* This class captures, and handles all trigger events for OpportunityLineItem object.
*
 * @author ile
 * @copyright PARX
 */
public with sharing class OpportunityProductTrigger extends ATrigger
{
    /**
     * Using w/o sharing helper to bypass security and sharing rules
     *
     * @return
     */
    private OpportunityProductHelperWoSharing getHelper()
    {
        return new OpportunityProductHelperWoSharing();
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        getHelper()
            //.ensureProductFamilyIntegrity((List<OpportunityLineItem>) records)
            .synchronizeOpportunityProducts((List<OpportunityLineItem>) records, (List<OpportunityLineItem>) oldRecords);
    }

    public override void hookAfterInsert(List<SObject> records)
    {
        getHelper()
            //.ensureProductFamilyIntegrity((List<OpportunityLineItem>) records)
            .synchronizeOpportunityProducts((List<OpportunityLineItem>) records, true);
    }

    public override void hookAfterDelete(List<SObject> records)
    {
        getHelper().deleteOpportunityLineItems((List<OpportunityLineItem>) records);
    }

    public without sharing class OpportunityProductHelperWoSharing
    {
        /**
         * Deletes related OpportunityLineItem__c whenever related OpportunityLineItem records have been deleted,
         * as long as there are no TimeCard__c records associated with them!
         *
         * @param opportunityProducts
         */
        private void deleteOpportunityLineItems(List<OpportunityLineItem> opportunityProducts)
        {
            List<OpportunityLineItem__c> opportunityLineItems = [select Id from OpportunityLineItem__c where OpportunityProductId__c in :new Map<Id, OpportunityLineItem>(opportunityProducts).keySet()];

            Exceptions.assert([select count() from TimeCard__c where Product__c in :opportunityLineItems limit 1] == 0,
                new Exceptions.GenericException(Label.OpportunityLineHasTimeCardsError));

            delete opportunityLineItems;
        }

        /**
         * Upserts corresponding OpportunityLineItem__c records whenever OpportunityLineItem.Product__c has changed.
         *
         * @param opportunityProducts
         * @param oldOpportunityProducts
         */
        private void synchronizeOpportunityProducts(List<OpportunityLineItem> opportunityProducts, List<OpportunityLineItem> oldOpportunityProducts)
        {
            List<OpportunityLineItem> changedOpportunityProducts = new List<OpportunityLineItem>();
            for (Integer i = 0, n = opportunityProducts.size(); i < n; i++)
            {
                OpportunityLineItem opportunityProduct = opportunityProducts.get(i);
                OpportunityLineItem oldOpportunityProduct = oldOpportunityProducts.get(i);
                if (opportunityProduct.Product2Id != oldOpportunityProduct.Product2Id)
                {
                    changedOpportunityProducts.add(opportunityProduct);
                }
            }

            synchronizeOpportunityProducts(changedOpportunityProducts, false);
        }

        /**
         * Upserts corresponding OpportunityLineItem__c records for given OpportunityLineItem records.
         *
         * @param opportunityProducts
         * @param oldOpportunityProducts
         */
        private OpportunityProductHelperWoSharing synchronizeOpportunityProducts(List<OpportunityLineItem> opportunityProducts, Boolean isInsert)
        {
            Map<Id, Product2> productsMap = new Map<Id, Product2>(selectRelatedProducts(opportunityProducts));
            Map<Id, OpportunityLineItem__c> opportunityLineItemsByOpportunityProductId = new Map<Id, OpportunityLineItem__c>();
            for (OpportunityLineItem__c each : selectRelatedOpportunityLineItems(opportunityProducts))
            {
                opportunityLineItemsByOpportunityProductId.put(each.OpportunityProductId__c, each);
            }

            List<OpportunityLineItem__c> opportunityLineItems = new List<OpportunityLineItem__c>();
            Map<Id, Valuation__c> valuationMap = new Map<Id, Valuation__c>();
            for (OpportunityLineItem opportunityProduct : opportunityProducts)
            {
                Product2 relatedProduct = productsMap.get(opportunityProduct.Product2Id);
                OpportunityLineItem__c opportunityLineItem = toOpportunityLineItem(opportunityProduct);
                opportunityLineItem.Name = relatedProduct.Name;
                if (opportunityLineItemsByOpportunityProductId.get(opportunityProduct.Id) != null)
                {
                    opportunityLineItem.Id = opportunityLineItemsByOpportunityProductId.get(opportunityProduct.Id).Id;
                }

                opportunityLineItems.add(opportunityLineItem);

                if (isInsert && opportunityProduct.ValuationId__c != null)
                {
                    valuationMap.put(opportunityProduct.ValuationId__c, new Valuation__c(Id = opportunityProduct.ValuationId__c, InvoicedProduct__r = opportunityLineItem));
                }
            }

            upsert opportunityLineItems;

            for (Valuation__c eachValuation : valuationMap.values())
            {
                eachValuation.InvoicedProduct__c = eachValuation.InvoicedProduct__r.Id;
            }

            update valuationMap.values();

            return this;
        }

        /**
         * Selects child OpportunityLineItem__c records
         *
         * @param opportunityProducts
         *
         * @return
         */
        private List<OpportunityLineItem__c> selectRelatedOpportunityLineItems(List<OpportunityLineItem> opportunityProducts)
        {
            return [select OpportunityProductId__c, Product__c from OpportunityLineItem__c where Id in :new Map<Id, OpportunityLineItem>(opportunityProducts).keySet()];
        }

        private OpportunityLineItem__c toOpportunityLineItem(OpportunityLineItem opportunityProduct)
        {
            return new OpportunityLineItem__c(
                    Opportunity__c = opportunityProduct.OpportunityId,
                    Product__c = opportunityProduct.Product2Id,
                    OpportunityProductId__c = opportunityProduct.Id
            );
        }

        /**
         * Selects related (OpportunityLineItem.Product2Id) Product2 records.
         *
         * @param opportunityProducts
         *
         * @return
         */
        private List<Product2> selectRelatedProducts(List<OpportunityLineItem> opportunityProducts)
        {
            List<Id> productIds = new List<Id>();
            for (OpportunityLineItem opportunityProduct : opportunityProducts)
            {
                productIds.add(opportunityProduct.Product2Id);
            }

            return [select Name from Product2 where Id in :productIds];
        }

        /**
         * Ensures that all OpportunityLineItem records on the same Opportunity have the same value in "Product2.Family"
         *
         * @param opportunityProducts   newly inserted OpportunityLineItem records
         */
       	/*
        private OpportunityProductHelperWoSharing ensureProductFamilyIntegrity(List<OpportunityLineItem> opportunityProducts)
        {
            //OpportunityProductFamilyIntegrityMessage
            List<Id> opportunityIDs = new List<Id>();
            for (OpportunityLineItem opportunityProduct : opportunityProducts)
            {
                opportunityIDs.add(opportunityProduct.OpportunityId);
            }

            Set<Id> uniqueOpportunityIDs = new Set<Id>();
            for (AggregateResult eachAggregateResult : [select count(Id), OpportunityId, Product2.Family from OpportunityLineItem where OpportunityId in : opportunityIDs and ProductCode != 'Other' group by OpportunityId, Product2.Family])
            {
                Exceptions.assert(uniqueOpportunityIDs.add((Id) eachAggregateResult.get('OpportunityId')),
                    new Exceptions.GenericException(Label.OpportunityProductFamilyIntegrityMessage));
            }

            return this;
        }
		*/
    }
}