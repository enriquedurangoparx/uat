/**
* Create a task for mailing.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 13.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Make it possible to add a task.
*/
public with sharing class MailingCreateTask {
    @AuraEnabled
    public static Id create(Task task){
        Id taskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Task').getRecordTypeId();
        task.RecordTypeId = taskRecordTypeId;

        Database.SaveResult saveResult = Database.insert(task);

        return task.Id;
    }
}