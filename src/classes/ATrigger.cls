/**
* @author ile
* @copyright PARX
*/

global abstract class ATrigger
{
    @testvisible
    private static Boolean ALLOW_EXECUTION = false;
    public static final String CAN_SKIP_TRIGGERS_PERMISSION_NAME = 'CanSkipTriggers';
    private Boolean executionDisabled = false;
    public List<SObject> contextRecords = new List<SObject>();

    /**
    * @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
    * @azure: US 2380 Custom Permission to bypass triggers, validations and processes
    * Allows to skip the execution of the trigger for the user who has the specified custom permission
    * @param permissionName Name of custom permission which allows to skip the trigger execution
    */
    global void disableForUserWithPermission(String permissionName)
    {
        if (FeatureManagement.checkPermission(permissionName))
        {
            executionDisabled = true;
			System.debug('...executionDisabled: ' + executionDisabled);
        }
    }

    global void execute()
    {
        try
        {
            if (!executionDisabled && (Trigger.isExecuting == true || ALLOW_EXECUTION == true))
            {
                if (Trigger.isExecuting)
                {
                    contextRecords = (Trigger.isDelete ? Trigger.old : Trigger.new);
                }

                hookInitialize();

                if (Trigger.isBefore == true)
                {
                    onBefore();
                }
                else if (Trigger.isAfter == true)
                {
                    onAfter();
                }

                hookFinalize();
            }
        }
        catch (Exception e)
        {
            hookException(e);
        }
    }

    @testvisible protected void onBefore()
    {
        if (Trigger.isDelete == true)
        {
            onBeforeDelete(Trigger.old);
        }
        else if (Trigger.isUpdate == true)
        {
            onBeforeUpdate(Trigger.new, Trigger.old);
        }
        else if (Trigger.isInsert == true)
        {
            onBeforeInsert(Trigger.new);
        }
    }

    @testvisible protected void onAfter()
    {
        if (Trigger.isDelete == true)
        {
            onAfterDelete(Trigger.old);
        }
        else if (Trigger.isUpdate == true)
        {
            onAfterUpdate(Trigger.new, Trigger.old);
        }
        else if (Trigger.isInsert == true)
        {
            onAfterInsert(Trigger.new);
        }
        else if (Trigger.isUndelete == true)
        {
            onAfterUndelete(Trigger.new);
        }
    }

    global virtual void hookInitialize() {/** override if necessary */}
    global virtual void hookFinalize() {/** override if necessary */}

    global virtual void hookException(Exception e)
    {
        for (SObject each : contextRecords)
        {
            each.addError(e.getMessage());
        }

        //re-throwing initial exception if there were no context records to attach error via addError method.
        Exceptions.assert(contextRecords != null && !contextRecords.isEmpty(), e);
    }

    global virtual void hookBeforeInsert(List<SObject> records) {/** override if necessary */}
    global virtual void hookBeforeInsert(SObject record) {/** override if necessary */}

    @testvisible protected void onBeforeInsert(List<SObject> records)
    {
        hookBeforeInsert(records);
        for (SObject record : records)
        {
            hookBeforeInsert(record);
        }
    }


    global virtual void hookBeforeUpdate(List<SObject> records, List<SObject> oldRecords) {/** override if necessary */}
    global virtual void hookBeforeUpdate(SObject record, SObject oldRecord) {/** override if necessary */}

    @testvisible protected void onBeforeUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        hookBeforeUpdate(records, oldRecords);
        for (Integer i = 0; i < records.size(); i++)
        {
            hookBeforeUpdate(records.get(i), oldRecords.get(i));
        }
    }

    global virtual void hookBeforeDelete(List<SObject> records) {/** override if necessary */}
    global virtual void hookBeforeDelete(SObject record) {/** override if necessary */}

    @testvisible protected virtual void onBeforeDelete(List<SObject> records)
    {
        hookBeforeDelete(records);
        for (SObject record : records)
        {
            hookBeforeDelete(record);
        }
    }

    global virtual void hookAfterInsert(List<SObject> records) {/** override if necessary */}
    global virtual void hookAfterInsert(SObject record) {/** override if necessary */}

    @testvisible protected void onAfterInsert(List<SObject> records)
    {
        hookAfterInsert(records);
        for (SObject record : records)
        {
            hookAfterInsert(record);
        }
    }

    global virtual void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords) {/** override if necessary */}
    global virtual void hookAfterUpdate(SObject record, SObject oldRecord) {/** override if necessary */}

    @testvisible protected void onAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        hookAfterUpdate(records, oldRecords);
        for (Integer i = 0; i < records.size(); i++)
        {
            hookAfterUpdate(records.get(i), oldRecords.get(i));
        }
    }

    global virtual void hookAfterDelete(List<SObject> records) {/** override if necessary */}
    global virtual void hookAfterDelete(SObject record) {/** override if necessary */}

    @testvisible protected void onAfterDelete(List<SObject> records)
    {
        hookAfterDelete(records);
        for (SObject record : records)
        {
            hookAfterDelete(record);
        }
    }

    global virtual void hookAfterUndelete(List<SObject> records) {/** override if necessary */}
    global virtual void hookAfterUndelete(SObject record) {/** override if necessary */}

    @testvisible protected void onAfterUndelete(List<SObject> records)
    {
        hookAfterUndelete(records);
        for (SObject record : records)
        {
            hookAfterUndelete(record);
        }
    }
}