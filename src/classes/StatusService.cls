/**
* Handle business logic which is related to Status__c
*
* @author: <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 07.04.2020	| Nadzeya Polautsava (nadzeya.polautsava@parx.com)	    | initial version - Bug1948.
*/
public with sharing class StatusService
{
    public static void recalculateInvestorLastStatusDate(List<Status__c> statuses)
    {
        Set<Id> investorsIds = new Set<Id>();

        for (Status__c s : statuses)
        {
            investorsIds.add(s.Investor__c);
        }

        if (!investorsIds.isEmpty())
        {
            List<AccountToOpportunity__c> investors =
            [
                    SELECT Id, LastStatus__c, LastStatusDate__c,
                        (SELECT Id, CreatedDate, StatusDate__c, InvestorStatus__c
                         FROM Status__r
                         ORDER BY StatusDate__c DESC NULLS LAST)
                    FROM AccountToOpportunity__c
                    WHERE Id IN :investorsIds
            ];

            List<AccountToOpportunity__c> recordsToUpdate = new List<AccountToOpportunity__c>();
            for (AccountToOpportunity__c investor : investors)
            {
                List<Status__c> investorsStatuses = investor.Status__r;
                System.debug('...investorsStatuses: ' + investorsStatuses);
                if (investorsStatuses != null && !investorsStatuses.isEmpty())
                {
                    Status__c lastStatus = investorsStatuses.get(0);
                    System.debug('...investor.LastStatus__c: ' + investor.LastStatus__c);
                    System.debug('...investor.LastStatusDate__c: ' + investor.LastStatusDate__c);
                    if (investor.LastStatus__c != lastStatus.InvestorStatus__c || investor.LastStatusDate__c != lastStatus.StatusDate__c)
                    {
                        investor.LastStatus__c = lastStatus.InvestorStatus__c;
                        investor.LastStatusDate__c = lastStatus.StatusDate__c;
                        investor.Status__c = lastStatus.InvestorStatus__c;
                        recordsToUpdate.add(investor);
                    }
                }
                else
                {
                    investor.LastStatusDate__c = null;
                    investor.LastStatus__c = null;
                    investor.Status__c = null;
                    recordsToUpdate.add(investor);
                }
            }

            if(!recordsToUpdate.isEmpty())
            {
                update recordsToUpdate;
            }
        }
    }

}