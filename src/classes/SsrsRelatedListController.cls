/**
* Convert all retrieved records into a list of wrapper.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 20.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2426] query ssrs reports on several objects.
*/
public with sharing class SsrsRelatedListController
{
    /**
     * Query all ssrs-report records which are corresponding to objectApiName and flatten the structure to a list.
     * 
     * @param String objectApiName: i.e. 'Opportunity' or 'AccountToOpportunity__c'.
     * @return List<SsrsRelatedListController.SsrsObjectWrapper>: A list of the given structure.
     */
    @AuraEnabled(cacheable=true)
    public static List<SsrsRelatedListController.SsrsObjectWrapper> getSsrsReportsForObject(String objectApiName)
    {
        List<SsrsRelatedListController.SsrsObjectWrapper> listOfWrapper = new List<SsrsRelatedListController.SsrsObjectWrapper>();
        for(SSRSReport__c ssrsReport : Database.query('SELECT Id, Name, BaseUrl__c, Description__c, ExternalId__c, (SELECT Id, AdditionalParameter__c FROM Reports_To_Object__r WHERE ObjectApiName__c = :objectApiName) FROM SSRSReport__c WHERE Active__c = true'))
        {
            for(SSRSReportToObject__c reportToObject : ssrsReport.Reports_To_Object__r)
            {
                SsrsRelatedListController.SsrsObjectWrapper wrapper = new SsrsRelatedListController.SsrsObjectWrapper();
                wrapper.key = reportToObject.Id;
                wrapper.reportName = ssrsReport.Name;
                wrapper.baseUrl = ssrsReport.BaseUrl__c;
                wrapper.description = ssrsReport.Description__c;
                wrapper.externalId = ssrsReport.ExternalId__c;
                wrapper.additionalParameter = reportToObject.AdditionalParameter__c;
                listOfWrapper.add(wrapper);
            }
        }
        
        return listOfWrapper;
    }

    public class SsrsObjectWrapper
    {
        @AuraEnabled
        public String key;
        @AuraEnabled
        public String reportName;
        @AuraEnabled
        public String externalId;
        @AuraEnabled
        public String baseUrl;
        @AuraEnabled
        public String description;
        @AuraEnabled
        public String additionalParameter;
    }

}