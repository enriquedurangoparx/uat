/*
 * @author jens.siffermann@youperience.com
 * @date 2019-10-29
 * @version 1.0.0
 *
 * @description InvestorsListController unit test
 */
@isTest
public with sharing class InvestorsListControllerTest {
    @testSetup
    static void setup(){
        InvestorsListControllerTestDataFactory.initializeProducts();

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    /**
     * test InvestorsListController.findInvestors()
     */
    @isTest
    public static void testFindInvestors() {
        AccountToOpportunity__c investor = InvestorsListControllerTestDataFactory.createInvestor();
        Id opportunityId = investor.Opportunity__c;

        Test.startTest();
        List<AccountToOpportunity__c> investors = InvestorsListController.findInvestors(opportunityId, '', 100);
        Test.stopTest();

        System.assertEquals(1, investors.size(), 'Unexpected number of AccountToOpportunity__c records');
    }

    @IsTest
    public static void saveUnderbiddersTest() {
        AccountToOpportunity__c investor = InvestorsListControllerTestDataFactory.createInvestor();
        investor.Underbidder__c = 'Yes';
        List<AccountToOpportunity__c> investors = new List<AccountToOpportunity__c>{investor};
        InvestorsListController.saveUnderbidders(JSON.serialize(investors));

        System.assertEquals('Yes', [SELECT Underbidder__c FROM AccountToOpportunity__c].Underbidder__c, 'The underbidder should be updated');
    }

    @IsTest
    public static void getUnderbidderPicklistEntriesTest() {
        String entries = InvestorsListController.getUnderbidderPicklistEntries();
        System.assertNotEquals(entries, '', 'Serialized string should contain the Underbidder__c picklist entries');
    }

    /**
     * test InvestorsListController.getRelatedData()
     */
    @isTest
    public static void testGetRelatedData() {
        Id investorId = InvestorsListControllerTestDataFactory.createInvestorRelatedRecords();

        Test.startTest();
        Map<String, List<Object>> related = InvestorsListController.getRelatedData(investorId);
        Test.stopTest();

        List<Object> tasks = related.get('Task');
        // We don't use events anymore
        // List<Object> events = related.get('Event');
        List<Object> contacts = related.get('Contact');
        List<Object> status = related.get('Status__c');

        System.assertEquals(4, related.size(), 'Unexpected size of related data map');
        System.assertEquals(1, tasks.size(), 'Unexpected number of related tasks');
        System.assertEquals(1, contacts.size(), 'Unexpected number of related ContactToInvestor__c records');
        System.assertEquals(1, status.size(), 'Unexpected number of related Status__c records');
    }

    /**
     * test InvestorsListController.setPinnedOnOpportunity()
     */
    @isTest
    public static void testSetPinnedOnOpportunity() {
        AccountToOpportunity__c investor = InvestorsListControllerTestDataFactory.createInvestor();
        Id opportunityId = investor.Opportunity__c;
        Id accountId = investor.Account__c;

        Test.startTest();
        Boolean pinned = InvestorsListController.setPinnedOnOpportunity(opportunityId, accountId, true);
        Test.stopTest();

        Opportunity pinnedOpportunity = [SELECT Id, Pinned_Investors__c FROM Opportunity WHERE Id = :opportunityId];
        List<String> pinnedIds = pinnedOpportunity.Pinned_Investors__c.split(';');

        System.assertEquals(true, pinned, 'Unexpected pin result');
        System.assertEquals(true, pinnedIds.contains(accountId), 'Account not pinned');
    }

    /**
     * test InvestorsListController.setPinnedOnOpportunity()
     */
    @isTest
    public static void testSetPinnedOnOpportunityUnpin() {
        AccountToOpportunity__c investor = InvestorsListControllerTestDataFactory.createInvestor();
        Id opportunityId = investor.Opportunity__c;
        Id accountId = investor.Account__c;

        InvestorsListController.setPinnedOnOpportunity(opportunityId, accountId, true);

        Test.startTest();
        Boolean pinned = InvestorsListController.setPinnedOnOpportunity(opportunityId, accountId, false);
        Test.stopTest();

        Opportunity pinnedOpportunity = [SELECT Id, Pinned_Investors__c FROM Opportunity WHERE Id = :opportunityId];

        System.assertEquals(true, pinned, 'Unexpected pin result');
        System.assertEquals(true, pinnedOpportunity.Pinned_Investors__c == null, 'Unexpected pin result');
    }

    /**
     * test InvestorsListController.deleteInvestors()
     */
    @isTest
    public static void testDeleteInvestors() {
        AccountToOpportunity__c investor = InvestorsListControllerTestDataFactory.createInvestor();

        Test.startTest();
        String jsonResp = InvestorsListController.deleteInvestors(new List<id> {investor.Id});
        InvestorsListController.ResponseMessage resp = (InvestorsListController.ResponseMessage) JSON.deserialize(jsonResp, InvestorsListController.ResponseMessage.class);
        Test.stopTest();

        System.assertEquals(true, resp.isSuccess, 'Not able to delete AccountToOpportunity__c');
    }

    /**
     * test InvestorsListController.deleteInvestors()
     */
    @isTest
    public static void testDeleteInvestorsWithTask() {
        AccountToOpportunity__c investor = InvestorsListControllerTestDataFactory.createInvestor();

        Task newTask = TestDataFactory.createTask(investor.Id);
        newTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Offer').getRecordTypeId();
        newTask.InvestorStatus__c = 'Offer';
        insert newTask;

        Test.startTest();
        String jsonResp = InvestorsListController.deleteInvestors(new List<id> {investor.Id});
        InvestorsListController.ResponseMessage resp = (InvestorsListController.ResponseMessage) JSON.deserialize(jsonResp, InvestorsListController.ResponseMessage.class);
        Test.stopTest();

        System.assertEquals(false, resp.isSuccess, 'Investor was deleted, but it has related task. So it should not be deleted.');
    }

    /**
     * test InvestorsListController.updateInvestors()
     */
    @isTest
    public static void testUpdateInvestors() {
        AccountToOpportunity__c investor = InvestorsListControllerTestDataFactory.createInvestor();
        investor.Status__c = 'Offer';

        Test.startTest();
        Boolean updated = InvestorsListController.updateInvestors(new List<AccountToOpportunity__c> {investor});
        Test.stopTest();

        System.assertEquals(true, updated, 'Not able to update AccountToOpportunity__c');
    }

    /**
    * test InvestorsListController.updateInvestors()
    */
    @isTest
    public static void testUpdateStatusDateInvestors() {
        AccountToOpportunity__c investor = InvestorsListControllerTestDataFactory.createInvestor();
        investor.Status__c = 'Teaser sent';
        Date statusDate = Date.today();

        Test.startTest();
        Boolean updated = InvestorsListController.updateInvestors(new List<AccountToOpportunity__c> {investor}, statusDate);
        Test.stopTest();

        System.assertEquals(true, updated, 'Not able to update AccountToOpportunity__c');
        System.assertEquals(statusDate, [SELECT Id, LastStatusDate__c FROM AccountToOpportunity__c WHERE Id = :investor.id].LastStatusDate__c, 'Wrong Date after Status__c insertion');
    }

    /**
     * test InvestorsListController.createTask()
     */
    // @isTest
    // public static void testCreateTask() {

    //     Task task = new Task();
    //     task.Subject = 'Test Task';
    //     task.Status = 'New';
    //     task.Priority = 'Normal';

    //     Test.startTest();
    //     InvestorsListController.createTask(task);
    //     Test.stopTest();

    //     System.assert(task.Id != null, 'Not able to create Task');
    // }

    /**
     * test InvestorsListController.createEvent()
     */
    @isTest
    public static void testCreateEvent() {

        Account account = InvestorsListControllerTestDataFactory.createAccount();

        Event event = new Event();
        event.Subject = 'Test Event';
        event.Type = 'Email';
        event.WhatId = account.Id;
        event.StartDateTime = System.now();
        event.EndDateTime = System.now().addHours(1);

        Test.startTest();
        InvestorsListController.createEvent(event);
        Test.stopTest();

        System.assert(event.Id != null, 'Not able to create Event');
    }

    /**
     * test InvestorsListController.searchContacts()
     */
    @isTest
    public static void testSearchContacts() {
        Account account = InvestorsListControllerTestDataFactory.createAccountWithContacts(10);

        List<Contact> contacts = [SELECT Id, Name, FirstName, LastName FROM Contact WHERE AccountId = :account.Id];

        Id [] fixedSearchResults = new Id[]{contacts.get(0).Id};
        Test.setFixedSearchResults(fixedSearchResults);

        List<Id> exclude = new List<Id>();

        Test.startTest();
        List<Contact> searchResult = InvestorsListController.searchContacts('Doe-0', exclude, account.Id, false, 2000);
        Test.stopTest();

        System.assertEquals(1, searchResult.size(), 'Unexpected number of search result');
    }

    /**
     * test InvestorsListController.searchContacts()
     */
    @isTest
    public static void testSearchContactsGlobal() {
        Account account = InvestorsListControllerTestDataFactory.createAccountWithContacts(10);

        List<Contact> contacts = [SELECT Id, Name, FirstName, LastName FROM Contact WHERE AccountId = :account.Id];

        Id [] fixedSearchResults = new Id[]{contacts.get(0).Id};
        Test.setFixedSearchResults(fixedSearchResults);

        List<Id> exclude = new List<Id>();

        Test.startTest();
        List<Contact> searchResult = InvestorsListController.searchContacts('Doe-0', exclude, account.Id, true, 2000);
        Test.stopTest();

        System.assertEquals(1, searchResult.size(), 'Unexpected number of search result');
    }

    /**
     * test InvestorsListController.searchContacts()
     */
    @isTest
    public static void testSearchContactsExclude() {
        Account account = InvestorsListControllerTestDataFactory.createAccountWithContacts(10);

        List<Contact> contacts = [SELECT Id, Name, FirstName, LastName FROM Contact WHERE AccountId = :account.Id];

        Id [] fixedSearchResults = new Id[]{contacts.get(0).Id};
        Test.setFixedSearchResults(fixedSearchResults);

        List<Id> exclude = new List<Id>();
        exclude.add(contacts.get(0).Id);

        Test.startTest();
        List<Contact> searchResult = InvestorsListController.searchContacts('Doe-0', exclude, account.Id, true, 2000);
        Test.stopTest();

        System.assertEquals(0, searchResult.size(), 'Unexpected number of search result');
    }

    /**
     * test InvestorsListController.searchContacts()
     */
    @isTest
    public static void testSearchAccount() {
        Account account = InvestorsListControllerTestDataFactory.createAccountWithContacts(10);

        Id [] fixedSearchResults = new Id[]{};
        fixedSearchResults.add(account.Id);
        Test.setFixedSearchResults(fixedSearchResults);

        List<Id> exclude = new List<Id>();

        Test.startTest();
        List<Contact> searchResult = InvestorsListController.searchContacts(account.Name, exclude, account.Id, true, 2000);
        Test.stopTest();

        System.assertEquals(10, searchResult.size(), 'Unexpected number of search result');
    }

    /**
     * test InvestorsListController.getBidsByRecordId()
     */
    @isTest
    public static void testGetBidsByRecordId() {
        AccountToOpportunity__c investor = InvestorsListControllerTestDataFactory.createInvestor();

        Task newTask = InvestorsListControllerTestDataFactory.createTask(investor.Id, 'New', 'Offer', Date.today(), 100, 'Offer');
        insert newTask;

        Test.startTest();
        List<Task> taskList = InvestorsListController.getBidsByRecordId(investor.Id);
        Test.stopTest();

        System.assertEquals(true, taskList.size() > 0, 'Wrong number of tasks');
    }
}