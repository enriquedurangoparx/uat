/**
* Testclass for SsrsRelatedListController.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 25.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2426] query ssrs reports on several objects.
*/
@isTest
private class SsrsRelatedListControllerTest {
    
    @isTest
    public static void testGetSsrsReportsForObject()
    {
        List<SSRSReport__c> listOfReport = new List<SSRSReport__c>();
        SSRSReport__c investorReport = new SSRSReport__c();
        investorReport.Name = 'InvestorReport';
        investorReport.Description__c = 'Eine andere Beschreibung';
        investorReport.ExternalId__c = 'InvestorReport';
        investorReport.Active__c = true;
        listOfReport.add(investorReport);

        SSRSReport__c opportunityDetailsReport = new SSRSReport__c();
        opportunityDetailsReport.Name = 'OpportunityDetails';
        opportunityDetailsReport.Description__c = 'Dies ist eine Beschreibung des Berichts, der erläutert wozu dieser Bericht inhaltlich konkret dient und zu welchen Anwendungszwecken er geeignet ist.';
        opportunityDetailsReport.ExternalId__c = 'OpportunityDetails';
        opportunityDetailsReport.Active__c = true;
        listOfReport.add(opportunityDetailsReport);
        insert listOfReport;

        List<SSRSReportToObject__c> listOfSSRSReportToObject = new List<SSRSReportToObject__c>();
        SSRSReportToObject__c investorReportDetail = new SSRSReportToObject__c();
        investorReportDetail.Report__c = investorReport.Id;
        investorReportDetail.ObjectApiName__c = 'Opportunity';
        investorReportDetail.AdditionalParameter__c = 'OpportunityId={!recordId}';
        listOfSSRSReportToObject.add(investorReportDetail);

        SSRSReportToObject__c investorOpportunityDetail = new SSRSReportToObject__c();
        investorOpportunityDetail.Report__c = opportunityDetailsReport.Id;
        investorOpportunityDetail.ObjectApiName__c = 'Opportunity';
        investorOpportunityDetail.AdditionalParameter__c = 'OpportunityId={!recordId}';
        listOfSSRSReportToObject.add(investorOpportunityDetail);
        insert listOfSSRSReportToObject;

        List<SsrsRelatedListController.SsrsObjectWrapper> listOfWrapper = null;
        Test.startTest();
            listOfWrapper = SsrsRelatedListController.getSsrsReportsForObject('Opportunity');
        Test.stopTest();

        System.assertEquals(2, listOfWrapper.size());
    }
}