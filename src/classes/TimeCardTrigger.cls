/**
 * @author ile
 * @copyright PARX
 */

public with sharing class TimeCardTrigger extends ATrigger
{
    public static final String TIME_CARD_DELETION_DE_TEMPLATE_DEVELOPER_NAME = 'TimeCardDeletionNotificationDE';
    public static final String TIME_CARD_DELETION_EN_TEMPLATE_DEVELOPER_NAME = 'TimeCardDeletionNotificationEN';

    public static final Integer LINE_ITEM_SCHEDULES_HOURS_PER_DAY = 8;

    private List<TimeCard__c> contextRecords = new List<TimeCard__c>();

    public static Map<String, EmailTemplate> TIME_CARD_DELETION_TEMPLATE_BY_DEVELOPER_NAME = new Map<String, EmailTemplate>{
        TIME_CARD_DELETION_DE_TEMPLATE_DEVELOPER_NAME => new EmailTemplate(),
        TIME_CARD_DELETION_EN_TEMPLATE_DEVELOPER_NAME => new EmailTemplate()
    };

    static
    {
        for (EmailTemplate template : [select DeveloperName from EmailTemplate where DeveloperName in :TIME_CARD_DELETION_TEMPLATE_BY_DEVELOPER_NAME.keySet()])
        {
            TIME_CARD_DELETION_TEMPLATE_BY_DEVELOPER_NAME.put(template.DeveloperName, template);
        }
    }

    public override void hookBeforeInsert(List<SObject> records)
    {
        populateHoursWorked((List<TimeCard__c>) records);
    }

    public override void hookAfterInsert(List<SObject> records)
    {
        updateLineItemSchedules((List<TimeCard__c>) records);
    }

    public override void hookBeforeUpdate(List<SObject> records, List<SObject> oldRecords)
    {
        populateHoursWorked((List<TimeCard__c>) records, (List<TimeCard__c>) oldRecords);
        decreaseScheduleRevenue((List<TimeCard__c>) records, (List<TimeCard__c>) oldRecords);
    }

    public override void hookAfterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
	    increaseScheduleRevenue((List<TimeCard__c>) records, (List<TimeCard__c>) oldRecords);
	    updateLineItemSchedules((List<TimeCard__c>) records, (List<TimeCard__c>) oldRecords);
    }

    public override void hookBeforeDelete(List<SObject> records)
    {
        ensureDeletionRights((List<TimeCard__c>) records);
        decreaseScheduleRevenue((List<TimeCard__c>) records);
        sendDeletionEmails((List<TimeCard__c>) records);
    }

    private void ensureDeletionRights(List<TimeCard__c> records)
    {
        Boolean hasSubmitted = false;
        Set<Id> opportunityIds = new Set<Id>();
        for (TimeCard__c timeCard : records)
        {
            hasSubmitted |= timeCard.Status__c == 'Billed';
            opportunityIds.add(timeCard.Project__c);
        }
		
        system.debug('Amount of Opportunity Ids: ' + opportunityIds.size());
        
        Boolean isCurrentUserAdmin = (![select Id from Profile where Name = :System.Label.SystemAdministratorProfilename and Id = :UserInfo.getProfileId()].isEmpty());
        Boolean isCurrentUserOwner = ([select Id from Opportunity where Id IN :opportunityIds and OwnerId = :UserInfo.getUserId()].size() == opportunityIds.size());
        Boolean isCurrentUserManager = FeatureManagement.checkPermission('ManageTimeCards');
        
        system.debug('\nCurrent User :' + UserInfo.getProfileId() + 
                     '\nIs Current User an Admin? ' + isCurrentUserAdmin + 
                     '\nIs Current User the Owner? ' + isCurrentUserOwner +
                     '\nIs Current User a Manager? ' + isCurrentUserManager);
        
        Exceptions.assert(!hasSubmitted || isCurrentUserAdmin,
            new Exceptions.GenericException(Label.BilledTimeCardsDeletionError));
		
        Exceptions.assert(isCurrentUserAdmin || isCurrentUserOwner || isCurrentUserManager, 
			new Exceptions.GenericException(Label.NotOwnTimeCardDeletion));
    }

    /**
     * Prepares and sends Email notification based on Time Card Deletion Email Templates, when a records are being deleted.
     *
     * @param timeCards
     */
    private void sendDeletionEmails(List<TimeCard__c> timeCards)
    {
        //We're using Messaging.renderEmailTemplate method which consumes a single SOQL statement per invocation,
        //thus we're trying to make sure there are enough SOQL Queries left.
        Exceptions.assert(Limits.getLimitSoslQueries() > Limits.getSoslQueries() + timeCards.size(),
            new Exceptions.GenericException(Label.TooManyTimeCardsForDeletionError));

        List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
        for (TimeCard__c extendedTimeCard : selectExtendedTimeCards(timeCards))
        {
            emailMessages.add(prepareDeletionEmail(extendedTimeCard));
        }

        List<Messaging.SendEmailResult> sendResults = (!Test.isRunningTest() ? Messaging.sendEmail(emailMessages) : new List<Messaging.SendEmailResult>());
        for (Messaging.SendEmailResult sendResult : sendResults)
        {
            if (!sendResult.isSuccess() && !sendResult.getErrors().isEmpty())
            {
                throw new Exceptions.GenericException(sendResult.getErrors().get(0).getMessage());
            }
        }
    }

    /**
     * Selects "extended" versions of TimeCard__c records with related data which will be required for further methods.
     *
     * @param timeCards
     *
     * @return
     */
    private List<TimeCard__c> selectExtendedTimeCards(List<TimeCard__c> timeCards)
    {
        return [select Id, Date__c, BillingType__c, User__c, User__r.LanguageLocaleKey, User__r.HoursPerDay__c, Product__r.OpportunityProductId__c,
            HoursWorked__c, Role__r.DailyRate__c, OpportunityLineItemScheduleId__c from TimeCard__c where Id in :timeCards];
    }

    private Messaging.SingleEmailMessage prepareDeletionEmail(TimeCard__c extendedTimeCard)
    {
        Id templateId = chooseDeletionTemplateId(extendedTimeCard.User__r);
        Messaging.SingleEmailMessage deletionEmail =
            Messaging.renderStoredEmailTemplate(templateId, extendedTimeCard.User__c, extendedTimeCard.Id);

        deletionEmail.setSaveAsActivity(false);

        return deletionEmail;
    }

    /**
     * Chooses which Email Template should be used for Time Card deletion notification for the User, based on it's Locale.
     *
     * @param recipient
     *
     * @return
     */
    private Id chooseDeletionTemplateId(User recipient)
    {
        if (recipient != null && recipient.LanguageLocaleKey.startsWithIgnoreCase('de'))
        {
            return TIME_CARD_DELETION_TEMPLATE_BY_DEVELOPER_NAME.get(TIME_CARD_DELETION_DE_TEMPLATE_DEVELOPER_NAME).Id;
        }

        return TIME_CARD_DELETION_TEMPLATE_BY_DEVELOPER_NAME.get(TIME_CARD_DELETION_EN_TEMPLATE_DEVELOPER_NAME).Id;
    }

    /**
     * Filters out TimeCard__c records where HoursWorked__c or PercentageWorked__c has been changed and
     * invokes populateHoursWorked(List<TimeCard__c>) method.
     *
     * @param timeCards
     * @param oldTimeCards
     */
    private void populateHoursWorked(List<TimeCard__c> timeCards, List<TimeCard__c> oldTimeCards)
    {
        List<TimeCard__c> modifiedTimeCards = new List<TimeCard__c>();
        for (Integer i = 0, n = timeCards.size(); i < n; i++)
        {
            TimeCard__c timeCard = timeCards.get(i);
            TimeCard__c oldTimeCard = oldTimeCards.get(i);

            if (timeCard.HoursWorked__c != oldTimeCard.HoursWorked__c
                    || timeCard.PercentageWorked__c != oldTimeCard.PercentageWorked__c)
            {
                modifiedTimeCards.add(timeCard);
            }
        }

        populateHoursWorked(modifiedTimeCards);
    }

    /**
     * Maintains TimeCard__c.HoursWorked__c or TimeCard__c.PercentageWorked__c depending on Time Tracking Type,
     * and User Hours Per Day configuration.
     * @param timeCards
     */
    private void populateHoursWorked(List<TimeCard__c> timeCards)
    {
        Map<Id, User> relatedUsers = new Map<Id, User>(selectRelatedUsers(timeCards));
        Map<Id, Opportunity> relatedProjects = new Map<Id, Opportunity> (selectRelatedProjects(timeCards));

        for (TimeCard__c timeCard : timeCards)
        {
            User relatedUser = relatedUsers.get(timeCard.User__c);
            Opportunity relatedProject = relatedProjects.get(timeCard.Project__c);

            if (relatedProject != null && relatedProject.TimeTrackingType__c == 'Percentage' && timeCard.PercentageWorked__c != null)
            {
                validateUserHoursPerDay(relatedProject, relatedUser);
                timeCard.HoursWorked__c = (timeCard.PercentageWorked__c / 100) * relatedUser.HoursPerDay__c;

                validateTotalHours(timeCard, relatedUser, timeCards);
            }
            else if (relatedProject != null && relatedProject.TimeTrackingType__c == 'Hours')
            {
                timeCard.PercentageWorked__c = (relatedUser != null && relatedUser.HoursPerDay__c != null && timeCard.HoursWorked__c != null)
                    ? (100 * timeCard.HoursWorked__c)/relatedUser.HoursPerDay__c
                    : null;

                normalizedHoursWorked(timeCard);
            }
        }
    }

    /**
     * Normalizes TimeCard__c.HoursWorked__c value.
     *
     * @param timeCard
     */
    private void normalizedHoursWorked(TimeCard__c timeCard)
    {
        if (timeCard.HoursWorked__c == null)
        {
            return;
        }

        Decimal decimalPart = timeCard.HoursWorked__c - Math.floor(timeCard.HoursWorked__c);
        timeCard.HoursWorked__c = Math.floor(timeCard.HoursWorked__c);
        if (decimalPart >= 0.75)
        {
            timeCard.HoursWorked__c += 1;
        }
        else if (decimalPart >= 0.25)
        {
            timeCard.HoursWorked__c += 0.5;
        }
    }

    /**
     * Validates that SUM of TimeCard__c
     *
     * @param timeCard
     * @param relatedUser
     * @param timeCards
     */
    private void validateTotalHours(TimeCard__c timeCard, User relatedUser, List<TimeCard__c> timeCards)
    {
        Decimal totalHours = 0;
        List<TimeCard__c> combinedTimeCards = timeCards.deepClone();
        combinedTimeCards.addAll(relatedUser.TimeCards__r);

        for (TimeCard__c each : combinedTimeCards)
        {
            if (each.User__c == timeCard.User__c && each.Date__c == timeCard.Date__c && each.HoursWorked__c != null)
            {
                totalHours += each.HoursWorked__c;
            }
        }

        Exceptions.assert(relatedUser.HoursPerDay__c >= totalHours,
                new Exceptions.GenericException(Label.TotalHoursExceededHoursPerDayError));
    }

    /**
     * Validates that if Project has Time Tracking Type "Percentage" corresponding User.HoursPerDay__c must be set.
     *
     * @param relatedProject
     * @param relatedUser
     */
    private void validateUserHoursPerDay(Opportunity relatedProject, User relatedUser)
    {
        Exceptions.assert(relatedProject.TimeTrackingType__c != 'Percentage' || (relatedUser != null && relatedUser.HoursPerDay__c != null),
            new Exceptions.GenericException(Label.UserHasNoHoursPerDayError));
    }


    /**
     * Selects list of User records related to timeCards passed via TimeCard__c.User__c lookup field.
     *
     * @param timeCards
     *
     * @return
     */
    private List<User> selectRelatedUsers(List<TimeCard__c> timeCards)
    {
        List<Id> userIds = new List<Id>();
        List<Date> projectDates = new List<Date>();
        for (TimeCard__c timeCard : timeCards)
        {
            userIds.add(timeCard.User__c);
            projectDates.add(timeCard.Date__c);
        }

        return [select HoursPerDay__c, (select HoursWorked__c, Date__c, BillingType__c, User__c from TimeCards__r where Date__c in :projectDates and Id not in :timeCards) from User where Id in :userIds];
    }

    /**
     * Selects list of Opportunity records related to timeCards passed via TimeCard__c.Project__c lookup field.
     *
     * @param timeCards
     *
     * @return
     */
    private List<Opportunity> selectRelatedProjects(List<TimeCard__c> timeCards)
    {
        List<Id> projectIds = new List<Id>();
        for (TimeCard__c timeCard : timeCards)
        {
            projectIds.add(timeCard.Project__c);
        }

        return [select TimeTrackingType__c from Opportunity where Id in :projectIds];
    }

    private void updateLineItemSchedules(List<TimeCard__c> timeCards, List<TimeCard__c> oldTimeCards)
    {
        List<TimeCard__c> modifiedTimeCards = new List<TimeCard__c>();
        for (Integer i = 0, n = timeCards.size(); i < n; i++)
        {
            TimeCard__c timeCard = timeCards.get(i);
            TimeCard__c oldTimeCard = oldTimeCards.get(i);

            if (timeCard.Product__c != oldTimeCard.Product__c || timeCard.BillingType__c != oldTimeCard.BillingType__c)
            {
                modifiedTimeCards.add(timeCard);
            }
        }

        updateLineItemSchedules(modifiedTimeCards);
    }

    /**
     * @author dme
     * @description Set correct Schedule for timecards
     */
    private void updateLineItemSchedules(List<TimeCard__c> timeCards)
    {
        Set<String> opportunityProductIds = new Set<String>();

        List<TimeCard__c> timeCardsToUpdate = new List<TimeCard__c>();
        List<TimeCard__c> extendedTimeCards = selectExtendedTimeCards(timeCards);

        Map<String, Id> oldestSchedulesByLineItemIdMap = new Map<String, Id>();
        Map<String, List<OpportunityLineItemSchedule>> schedulesByLineItemIdMap = new Map<String, List<OpportunityLineItemSchedule>>();

        for (TimeCard__c timeCard : extendedTimeCards)
        {
            opportunityProductIds.add(timeCard.Product__r.OpportunityProductId__c);
        }

        List<OpportunityLineItemSchedule> schedules = [
            SELECT Id, ScheduleDate, OpportunityLineItemId FROM OpportunityLineItemSchedule
            WHERE OpportunityLineItemId IN :opportunityProductIds
                AND ON_Invoice__c = NULL AND OpportunityLineItem.Opportunity.BillingType__c INCLUDES('Time and Material')
            ORDER BY ScheduleDate DESC
        ];

        for (OpportunityLineItemSchedule record : schedules)
        {
            oldestSchedulesByLineItemIdMap.put(record.OpportunityLineItemId, record.Id);
            if (!schedulesByLineItemIdMap.containsKey(record.OpportunityLineItemId))
            {
                schedulesByLineItemIdMap.put(record.OpportunityLineItemId, new List<OpportunityLineItemSchedule>());
            }
            schedulesByLineItemIdMap.get(record.OpportunityLineItemId).add(record);
        }

	    timeCardsToUpdate = TimecardService.setTimecardSchedules(extendedTimeCards, schedulesByLineItemIdMap, oldestSchedulesByLineItemIdMap);

        if (!timeCardsToUpdate.isEmpty())
        {
            update timeCardsToUpdate;
        }
    }

    /**
     * @author dme
     * @description Update Revenue for Schedules
     */
    private List<OpportunityLineItemSchedule> modifyLineItemSchedules(List<TimeCard__c> extendedTimeCards, Map<Id, OpportunityLineItemSchedule> lineItemScheduleMap)
    {
        List<OpportunityLineItemSchedule> schedulesToUpdate = new List<OpportunityLineItemSchedule>();
        Set<Id> scheduleToUpdateIds = new Set<Id>();

        for (TimeCard__c timeCard : extendedTimeCards)
        {
            OpportunityLineItemSchedule lineItemSchedule = lineItemScheduleMap.get(timeCard.OpportunityLineItemScheduleId__c);
            if (lineItemSchedule != null)
            {
                lineItemSchedule.Revenue = (lineItemSchedule.IsAutoRecalculated__c ? lineItemSchedule.Revenue : 0)
                    + (timeCard.HoursWorked__c *
	                (timeCard.BillingType__c == TimecardService.TIME_CARD_BILLING_TYPE_BILLABLE ? timeCard.Role__r.DailyRate__c : 0) /
	                LINE_ITEM_SCHEDULES_HOURS_PER_DAY);

                lineItemSchedule.IsAutoRecalculated__c = true;
                OpportunityLineItemScheduleTrigger.allowQuantityRevenueChange(lineItemSchedule.Id);
                scheduleToUpdateIds.add(lineItemSchedule.Id);
            }
        }

        for (Id scheduleId : scheduleToUpdateIds)
        {
            schedulesToUpdate.add(lineItemScheduleMap.get(scheduleId));
        }

        return schedulesToUpdate;
    }

    private List<OpportunityLineItemSchedule> selectLineItemSchedules(List<TimeCard__c> extendedTimeCards)
    {
        List<String> scheduleIds = new List<String>();
        for (TimeCard__c extendedTimeCard : extendedTimeCards)
        {
            scheduleIds.add(extendedTimeCard.OpportunityLineItemScheduleId__c);
        }

        return [select Id,
                	OpportunityLineItemId, 
                	IsAutoRecalculated__c, 
                	Revenue 
                from 
                	OpportunityLineItemSchedule 
                where 
                	ON_Invoice__c = null 
                and 
                	Id IN :scheduleIds
                and
                	OpportunityLineItem.Opportunity.BillingType__c INCLUDES('Time and Material') 
                order by 
                	ScheduleDate asc];
    }

    /**
    * Decreases corresponding OpportunityLineItemSchedule.Revenue - should be called before update/delete
    */
    private void decreaseScheduleRevenue(List<TimeCard__c> timeCards, List<TimeCard__c> oldTimeCards)
    {
        List<TimeCard__c> modifiedTimeCards = new List<TimeCard__c>();
        for (Integer i = 0, n = timeCards.size(); i < n; i++)
        {
            TimeCard__c timeCard = timeCards.get(i);
            TimeCard__c oldTimeCard = oldTimeCards.get(i);

            if (timeCard.HoursWorked__c != oldTimeCard.HoursWorked__c || timeCard.Role__c != oldTimeCard.Role__c || timeCard.BillingType__c != oldTimeCard.BillingType__c)
            {
	            modifiedTimeCards.add(timeCard);
            }

            if (timeCard.OpportunityLineItemScheduleId__c != oldTimeCard.OpportunityLineItemScheduleId__c)
            {
                if (String.isNotBlank(oldTimeCard.OpportunityLineItemScheduleId__c))
                {
                    modifiedTimeCards.add(oldTimeCard);
                }
            }
        }

        decreaseScheduleRevenue(modifiedTimeCards);
    }

    /**
     * Decreases OpportunityLineItemSchedule.Revenue for OpportunityLineItemSchedule records associated with passed TimeCard__c
     * by TimeCard__c.OpportunityLineItemScheduleId__c = OpportunityLineItemSchedule.Id
     */
    private void decreaseScheduleRevenue(List<TimeCard__c> timeCards)
    {
        List<Id> lineItemScheduleIDs = new List<Id>();
        for (TimeCard__c timeCard : timeCards)
        {
            lineItemScheduleIDs.add(timeCard.OpportunityLineItemScheduleId__c);
        }

        List<TimeCard__c> extendedTimeCards = selectExtendedTimeCards(timeCards);
        Map<Id, OpportunityLineItemSchedule> lineItemScheduleMap = new Map<Id, OpportunityLineItemSchedule>([SELECT Revenue FROM OpportunityLineItemSchedule WHERE Id IN :lineItemScheduleIDs AND ON_Invoice__c = NULL]);
        for (TimeCard__c extendedTimeCard : extendedTimeCards)
        {
            OpportunityLineItemSchedule lineItemSchedule = lineItemScheduleMap.get(extendedTimeCard.OpportunityLineItemScheduleId__c);
            if (lineItemSchedule != null)
            {
                lineItemSchedule.Revenue = (lineItemSchedule.Revenue != null ? lineItemSchedule.Revenue : 0)
                    - extendedTimeCard.HoursWorked__c *
	                (extendedTimeCard.BillingType__c == TimecardService.TIME_CARD_BILLING_TYPE_BILLABLE ? extendedTimeCard.Role__r.DailyRate__c : 0) /
	                LINE_ITEM_SCHEDULES_HOURS_PER_DAY;

                if (lineItemSchedule.Revenue == 0)
                {
                    lineItemSchedule.IsAutoRecalculated__c = false;
                }

                OpportunityLineItemScheduleTrigger.allowQuantityRevenueChange(lineItemSchedule.Id);
            }
        }

        update lineItemScheduleMap.values();
    }

    /**
    * @author dme
    * @description Increases corresponding OpportunityLineItemSchedule.Revenue - should be called after update
    */
    private void increaseScheduleRevenue(List<TimeCard__c> timeCards, List<TimeCard__c> oldTimeCards)
    {
        List<TimeCard__c> modifiedTimeCards = new List<TimeCard__c>();
        for (Integer i = 0, n = timeCards.size(); i < n; i++)
        {
            TimeCard__c timeCard = timeCards.get(i);
            TimeCard__c oldTimeCard = oldTimeCards.get(i);

            if (timeCard.HoursWorked__c != oldTimeCard.HoursWorked__c || timeCard.Role__c != oldTimeCard.Role__c || timeCard.BillingType__c != oldTimeCard.BillingType__c)
            {
                modifiedTimeCards.add(timeCard);
            }

            if (timeCard.OpportunityLineItemScheduleId__c != oldTimeCard.OpportunityLineItemScheduleId__c)
            {
                if (String.isNotBlank(timeCard.OpportunityLineItemScheduleId__c))
                {
                    modifiedTimeCards.add(timeCard);
                }
            }
        }

        increaseScheduleRevenue(modifiedTimeCards);
    }

    private void increaseScheduleRevenue(List<TimeCard__c> timeCards)
    {
        List<TimeCard__c> extendedTimeCards = selectExtendedTimeCards(timeCards);
        Map<Id, OpportunityLineItemSchedule> lineItemScheduleMap = new Map<Id, OpportunityLineItemSchedule>(selectLineItemSchedules(extendedTimeCards));

        //Modify (increase) OpportunityLineItemSchedule Revenue values
        List<OpportunityLineItemSchedule> schedulesToUpdate = modifyLineItemSchedules(extendedTimeCards, lineItemScheduleMap);
        if (!schedulesToUpdate.isEmpty())
        {
            update schedulesToUpdate;
        }
    }

}