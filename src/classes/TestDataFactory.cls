/**
* Testclass for InvestorSelectionHelper.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 13.11.2019	| Egor Markuzov (egor.markuzov@parx.com)                | initial version.
* 0.2 19.12.2019	| Maksim Fedorenko (maksim.fedorneko@parx.com)          | updated createOpportunity() method
* 0.3 17.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Added Saluation for Contact.
*/
@isTest
public class TestDataFactory 
{
    /**
     * create simple account
     * @return   Account
     */
    public static Account createAccount(String name)
    {
        Account newAccount = new Account(
            Name = name
        );
        return newAccount;
    }

    /**
     * create simple list of accounts
     * @return   List<Account>
     */
    public static List<Account> createAccount(Integer amountOfAccs)
    {
        List<Account> newAccList = new List<Account>();
        for (Integer i = 0; i < amountOfAccs; i ++)
        {
             Account newAccount = new Account(
                Name = 'Test Account' + i
            );
            newAccList.add(newAccount);
        }
       
        return newAccList;
    }

    /**
     * create simple list of contacts
     * @return   List<Contact>
     */
    public static List<Contact> createContacts(Id accId, Integer amountOfContacts)
    {
        List<Contact> newContactList = new List<Contact>();

        for (Integer i = 0; i < amountOfContacts; i ++)
        {
            Contact newContact = new Contact(
                FirstName = 'Test',
                LastName = 'Contact' + i,
                Salutation = 'Mr.',
                AccountId = accId
            );
            newContactList.add(newContact);
        }
       
        return newContactList;
    }

    /**
    * create simple opportunity
    * @return   Opportunity
    */
    public static Opportunity createOpportunity(Id accId)
    {
        return new Opportunity(
            Name = 'TestOpp1',
            Region__c = 'Global',
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('InvestmentInternal').getRecordTypeId(),
            StageName = SObjectType.Opportunity.fields.StageName.getPicklistValues().get(0).getValue(),
            AccountId = accId,
            ProjectStart__c = Date.today(),
            CloseDate = Date.today().addDays(1)
        );
    }

    /**
    * create simple opportunity
    * @return   Opportunity
    */
    public static Opportunity createOpportunity(Id accId, PropertyObject__c propertyObject)
    {
        Opportunity opp = createOpportunity(accId);
        opp.PrimaryProperty__c = propertyObject.Id;
        return opp;
    }

    /**
    * create simple list of ContactToInvestor
    * @return   List<ContactToInvestor__c>
    */
    public static List<ContactToInvestor__c> createContactToInvestor(Id investorId, List<Contact> contactList)
    {
        List<ContactToInvestor__c> newContactToInvestorList = new List<ContactToInvestor__c>();
        for (Contact cont : contactList)
        {
            ContactToInvestor__c newContactToInvestor = new ContactToInvestor__c(
                Investor__c = investorId,
                Contact__c = cont.Id
            );
            newContactToInvestorList.add(newContactToInvestor);
        }
        return newContactToInvestorList;
    } 

    /**
    * create simple AccountToOpportunity
    * @return   AccountToOpportunity__c
    */
    public static AccountToOpportunity__c createAccountToOpportunity(Id accId, Id oppId)
    {
        AccountToOpportunity__c newAccountToOpportunity = new AccountToOpportunity__c(
            Account__c = accId,
            Opportunity__c = oppId
        );
        return newAccountToOpportunity;
    }

    /**
    * create simple InvestorList record
    * @return   InvestorsList__c
    */
    public static InvestorsList__c createInvestorList(String name)
    {
        InvestorsList__c newInvestorList = new InvestorsList__c(
            Name = name
        );
        return newInvestorList;
    }

    /**
    * create simple InvestorListItem record
    * @return   InvestorsListItem__c
    */
    public static InvestorsListItem__c createInvestorListItem(Id investorListId, Id accId)
    {
        InvestorsListItem__c newInvestorListItem = new InvestorsListItem__c(
            InvestorsList__c = investorListId,
            Account__c = accId
        );
        
        return newInvestorListItem;
    }

    /**
    * create simple InvestorListItemContact record
    * @return   InvestorsListItemContact__c
    */
    public static InvestorsListItemContact__c createInvestorListItemContact(Id investorListItemId, Id contactId)
    {
        InvestorsListItemContact__c newInvestorListItemContact = new InvestorsListItemContact__c(
            InvestorsListItem__c = investorListItemId,
            Contact__c = contactId
        );
        
        return newInvestorListItemContact;
    }

    /**
     * create list of AcquisitionProfile__c
     * @return   List<AcquisitionProfile__c>
     */
    public static List<AcquisitionProfile__c> createAcquisitionProfiles(Id accId, Id contId, Integer amountOfRecords)
    {
        List<AcquisitionProfile__c> acqProfileList = new List<AcquisitionProfile__c>();
       
        for (Integer i = 0; i < amountOfRecords; i ++)
        {
            AcquisitionProfile__c newAcqProfile = new AcquisitionProfile__c(
                Name = 'Test Profile' + i,
                Account__c = accId,
                ContactFunding__c = contId
            );
            acqProfileList.add(newAcqProfile);
        }
        return acqProfileList;
    }

    /**
     * create simple Task
     * @return   Task
     */
    public static Task createTask(Id recordId)
    {
        Task newTask = new Task(
            Subject = 'Test Task',
            Status = 'New',
            Priority = 'Normal',
            WhatId = recordId,
            ActivityDate = Date.today()
        );
        return newTask;
    }

    /**
     * create simple Event
     * @return   Event
     */
    public static Event createEvent(Id recordId)
    {
        Event newEvent = new Event(
            Subject = 'Test Event',
            Type = 'Email',
            WhatId = recordId,
            StartDateTime = System.now(),
            EndDateTime = System.now().addHours(1),
            ActivityDate = Date.today()
        );
        
        return newEvent;
    }

    /**
     * initialize Products for opportunity creation
     */
    public static void initializeProducts(){
        Product2 product = new Product2();
        product.Name = 'Sonstiges';
        product.IsActive = true;
        product.Family = 'Cost Transfer';
        product.ProductCode = 'Other';
        product.CanUseRevenueSchedule = true;
        product.RevenueScheduleType = 'Repeat';
        product.NumberOfRevenueInstallments = 1;
        product.RevenueInstallmentPeriod = 'Daily';
        insert product;

        Pricebook2 valuationPricebook = new Pricebook2();
        valuationPricebook.Name = 'Valuation';
        valuationPricebook.IsActive = true;
        insert valuationPricebook;

        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.Product2Id = product.Id;
        standardPricebookEntry.UnitPrice = 100.00;
        insert standardPricebookEntry;

        PricebookEntry valuationPricebookEntry = new PricebookEntry();
        valuationPricebookEntry.IsActive = true;
        valuationPricebookEntry.Pricebook2Id = valuationPricebook.Id;
        valuationPricebookEntry.Product2Id = product.Id;
        valuationPricebookEntry.UnitPrice = 100.00;
        valuationPricebookEntry.UseStandardPrice = true;
        insert new List<PricebookEntry>{ valuationPricebookEntry };
    }

    /**
     * create simple Area__c record
     * @return   Area__c
     */
    public static Area__c createArea(String name)
    {
        Area__c newArea = new Area__c(
            Name = name
        );
        
        return newArea;
    }

    /**
     * create simple Area__c record
     * @return   Area__c
     */
    public static List<PropertyObject__c> createPropertyObjectWithArea(Id areaId)
    {
        String jsonStr = '[{"Name":"Europahaus","Thumbnail__c":"https://upload.wikimedia.org/wikipedia/commons/a/a0/Europahaus_Leipzig.jpg","YearofConstruction__c":"1929","Geolocation__Latitude__s":51.3461250,"Geolocation__Longitude__s":12.3910220,"Street__c":"Dohnanyistra?e 28","PostalCode__c":"04103","City__c":"Leipzig","Country__c":"Germany"},{"Name":"Dreischeibenhaus (for Area)","Thumbnail__c":"https://upload.wikimedia.org/wikipedia/commons/e/ea/Thyssen_Krupp_Hochhaus_in_D?sseldorf_%28vom_Gr?ndgens-Platz_aus%29.jpg","SqmInTotal__c":30000.00,"YearofConstruction__c":"1960","Geolocation__Latitude__s":51.2280000,"Geolocation__Longitude__s":6.7826700,"Street__c":"August-Thyssen-Stra?e 1","PostalCode__c":"40211","City__c":"D?sseldorf","Country__c":"Germany"},{"Name":"University Medical Center","Street__c":"Martinistraße 52","PostalCode__c":"20251","City__c":"Hamburg","Country__c":"Germany"}]';
        
        List<PropertyObject__c> newProperties = (List<PropertyObject__c>) JSON.deserialize(jsonStr, List<PropertyObject__c>.class);

        for (PropertyObject__c po : newProperties)
        {
            po.Area__c = areaId;
        }
        return newProperties;
    }

    /**
     * create simple ContentVersion record
     * @return   ContentVersion
     */
    public static Contentversion createContentversion()
    {
        ContentVersion cv = new Contentversion(
            Title = 'Test Title',
            PathOnClient = 'Test' + '.' + 'PNG',
            ContentLocation = 'S',
            Versiondata = EncodingUtil.base64Decode('Unit Test Attachment Body')
        );
        return cv;
    }

    /**
     * create simple ContentDocumentLink record
     * @return   ContentDocumentLink
     */
    public static ContentDocumentLink createContentDocumentLink(Id contVerId, Id recId)
    {
        ContentVersion cd = [SELECT Id, ContentDocumentId  FROM ContentVersion WHERE Id = :contVerId LIMIT 1];
        ContentDocumentLink cdl = new ContentDocumentLink(
            ContentDocumentId = cd.ContentDocumentId,
            LinkedEntityId = recId,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        return cdl;
    }

    /**
    * @description Allows to create a PropertyObject__c record for unit testing
    * @return PropertyObject__c record
    */
    public static PropertyObject__c buildPropertyObject(String name, Boolean withInsertion)
    {
        PropertyObject__c propertyObject = new PropertyObject__c(
                Name = name
        );

        if (withInsertion)
        {
            insert propertyObject;
        }

        return propertyObject;
    }

    /**
    * @description provide JSON string of Property Object
    * @return JSON string
    */
    public static String jsonPropertyObject()
    {
        return '[{"Name":"Test Egor 2","City__c":"Test City 2","Street__c":"Test street 2","PostalCode__c":"Test postal Code 2","Thumbnail__c":"https://upload.wikimedia.org/wikipedia/commons/a/ab/Stuttgart-Vaihingen_STEP_6.jpg","Geolocation__Latitude__s":52.09463409999999,"Geolocation__Longitude__s":23.691688399999997,"RecordTypeId":"0120O000000dAY1QAM","CurrencyIsoCode":"EUR"}]';
    }

    /**
    * @description provide JSON string of List Property Object
    * @return JSON string
    */
    public static String jsonListPropertyObject()
    {
        return '[{"attributes":{"type":"PropertyObject__c","url":"/services/data/v47.0/sobjects/PropertyObject__c/a001x000004UV9FAAW"},"Name":"Test Egor 2","City__c":"Test City 2","Street__c":"Test street 2","PostalCode__c":"Test postal Code 2","Thumbnail__c":"https://upload.wikimedia.org/wikipedia/commons/a/ab/Stuttgart-Vaihingen_STEP_6.jpg","Geolocation__Latitude__s":52.094634099999996,"Geolocation__Longitude__s":23.691688399999998,"RecordTypeId":"0120O000000dAY1QAM","CurrencyIsoCode":"EUR"},{"attributes":{"type":"PropertyObject__c","url":"/services/data/v47.0/sobjects/PropertyObject__c/a001x000004UV9KAAW"},"Name":"Test Egor 3","City__c":"Test City 3","Street__c":"Test street 3","PostalCode__c":"Test postal Code 3","Thumbnail__c":"https://upload.wikimedia.org/wikipedia/commons/a/ab/Stuttgart-Vaihingen_STEP_6.jpg","Geolocation__Latitude__s":52.094633098217656,"Geolocation__Longitude__s":23.69166308872822,"RecordTypeId":"0120O000000dAY1QAM","CurrencyIsoCode":"EUR"}]';
    }

    public static User createUser(String profileName, Boolean withInsertion)
    {
        String uniqueUserName = 'usr' + Datetime.now().getTime() + '@test.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name = :profileName];
        User user = new User(
                Alias = 'standt', Email = 'standarduser@testorg.com', EmailEncodingKey = 'UTF-8', LastName = 'Testing',
                LanguageLocaleKey = 'ru', ProfileId = p.Id, TimeZoneSidKey = 'Europe/Minsk', LocaleSidKey = 'ru_RU',
                Username = uniqueUserName
        );

        if (withInsertion)
        {
            insert user;
        }
        return user;
    }

    public static PermissionSetAssignment createPermissionSetAssignment(User user, String permissionSetName)
    {
        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name = :permissionSetName];
        PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = permissionSet.Id, AssigneeId = user.Id);
        insert psa;
        return psa;
    }
}