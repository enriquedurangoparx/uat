/**
* Updates Geolocation of the given PropertyObject__c records using it's address data
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
* @Resources: https://developers.google.com/maps/documentation/geocoding/start
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 25.02.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
*/
public with sharing class PropertyGeolocationUpdaterBatch implements Database.Batchable<SObject>, Database.AllowsCallouts
{
    public final static String GOOGLE_API_KEY_DEVELOPER_NAME = 'GeolocationAPIKey';
    public final static String GOOGLE_GEOLOCATION_ENDPOINT = 'https://maps.googleapis.com/maps/api/geocode/json';

    private List<PropertyObject__c> propertyList;
    private final String GOOGLE_API_KEY;

    public PropertyGeolocationUpdaterBatch(List<PropertyObject__c> properties)
    {
        this.propertyList = properties;
        this.GOOGLE_API_KEY = [SELECT Key__c FROM GoogleMapsAPI__mdt WHERE DeveloperName = :GOOGLE_API_KEY_DEVELOPER_NAME].Key__c;
    }

    public Iterable<SObject> start(Database.BatchableContext bc)
    {
        return this.propertyList;
    }

    public void execute(Database.BatchableContext bc, List<PropertyObject__c> properties)
    {
        List<PropertyObject__c> propertyObjectsForUpdate = new List<PropertyObject__c>();
        for (PropertyObject__c property : properties)
        {
            String address = (property.PostalCode__c != null ? property.PostalCode__c + ', ' : '') + property.Street__c + ', '
                    + property.City__c + ', ' + property.Country__c;
            HttpResponse response = HttpRequestUtil.doGetRequest(GOOGLE_GEOLOCATION_ENDPOINT, new Map<String, String>{
                    'address' => address,
                    'key' => this.GOOGLE_API_KEY
            });

            if (response.getStatusCode() == 200)
            {
                GoogleGeocodeWrapper geocode = (GoogleGeocodeWrapper) JSON.deserialize(response.getBody(), GoogleGeocodeWrapper.class);

                if (geocode.status == 'OK' && !geocode.results.isEmpty())
                {
                    GoogleGeocodeLocationItemWrapper locationWrapper = geocode.results.get(0).geometry.location;
                    Location location = locationWrapper.getLocation();

                    propertyObjectsForUpdate.add(new PropertyObject__c(
                            Id = property.Id,
                            Geolocation__Latitude__s = location.getLatitude(),
                            Geolocation__Longitude__s = location.getLongitude()
                    ));
                }
            }
        }

        update propertyObjectsForUpdate;
    }

    public void finish(Database.BatchableContext bc) { }

    public class GoogleGeocodeWrapper
    {
        List<GoogleGeocodeGeometryWrapper> results;
        String status;
    }

    public class GoogleGeocodeGeometryWrapper
    {
        GoogleGeocodeLocationWrapper geometry;
    }

    public class GoogleGeocodeLocationWrapper
    {
        GoogleGeocodeLocationItemWrapper location;
    }

    public class GoogleGeocodeLocationItemWrapper
    {
        Decimal lat;
        Decimal lng;

        public Location getLocation()
        {
            return Location.newInstance(this.lat, this.lng);
        }
    }
}