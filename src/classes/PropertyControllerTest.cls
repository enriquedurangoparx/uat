/**
* Test class for PropertyController.cls
*
* @author: <Egor Markuzov> (egor.markuzov@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 26.11.2019	| Egor Markuzov (egor.markuzov@parx.com)                | initial version.
*/
@isTest
private class PropertyControllerTest {
    /**
    * test getPagedPropertyList()
    */
    @isTest
    public static void testGetPagedPropertyList() 
    {
        List<PropertyObject__c> newObjectList = TestDataFactory.createPropertyObjectWithArea(null);
        insert newObjectList;

        String searchKey = 'Europahaus';

        PagedResult recsToCheck = PropertyController.getPagedPropertyList(searchKey, '', '', 9, 1);

        System.assertEquals(1, recsToCheck.totalItemCount, 'Wrong number of property object records.');
    }

    /**
    * test getPropertyForMap()
    */
    @isTest
    public static void testGetPropertyForMapAllRecords() 
    {
        List<PropertyObject__c> newObjectList = TestDataFactory.createPropertyObjectWithArea(null);
        insert newObjectList;

        PagedResult recsToCheck = PropertyController.getPropertyForMap('', '', '');

        System.assertEquals(newObjectList.size(), recsToCheck.totalItemCount, 'Wrong number of property object records.');
    }

    /**
    * test getPropertyForMap()
    */
    @isTest
    public static void testGetPropertyForMapFiltered() 
    {
        List<PropertyObject__c> newObjectList = TestDataFactory.createPropertyObjectWithArea(null);
        insert newObjectList;
        System.debug('newObjectList:' + newObjectList.size());

        PagedResult recsToCheck = PropertyController.getPropertyForMap('', '1929', '1929');

        System.assertEquals(1, recsToCheck.totalItemCount, 'Wrong number of property object records.');
    }

    /**
    * test getPictures()
    */
    @isTest
    public static void testGetPictures() 
    {
        List<PropertyObject__c> newObjectList = TestDataFactory.createPropertyObjectWithArea(null);
        insert newObjectList;
        System.assertEquals(3, newObjectList.size(), 'Property objects where not created');

        Contentversion cv = TestDataFactory.createContentversion();
        insert cv;

        ContentDocumentLink cdl = TestDataFactory.createContentDocumentLink(cv.Id, newObjectList[0].Id);
        insert cdl;

        List<ContentVersion> result = PropertyController.getPictures(newObjectList[0].Id);

        System.assertEquals(1, result.size(), 'Wrong number of pictures was selected.');
        System.assertEquals(cv.Id, result[0].Id, 'Wrong picture was selected.');
    }
}