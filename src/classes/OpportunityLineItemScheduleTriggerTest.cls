/**
 * Created by ille on 2019-05-30.
 */

@istest
private class OpportunityLineItemScheduleTriggerTest
{
    @TestSetup
    private static void setup()
    {
        insert new List<ONB2__TriggerSettings__c>{
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceBeforeInsert'
            ),
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceBeforeUpdate'
            ),
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceLineItemBeforeInsert'
            ),
            new ONB2__TriggerSettings__c(
                Name = 'InvoiceLineItemBeforeUpdate'
            )
        };

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
    }

    private static Map<String, SObject> prepareData()
    {
        OpportunityHandlerTest.initializeProducts();// Needed to setup product, pricebook and pricebookentry.

        Product2 productA = new Product2(Name = 'A', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        Product2 productB = new Product2(Name = 'B', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        insert new List<Product2>{productA, productB};
       
        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.Product2Id = productA.Id;
        standardPricebookEntry.UnitPrice = 100;
        insert standardPricebookEntry;

        Pricebook2 valuationPricebook = [SELECT Id FROM Pricebook2 WHERE Name = :OpportunityHandler.VALUATION_PRICEBOOKNAME];

        PricebookEntry pricebookEntryA = standardPricebookEntry;//new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productA.Id, UnitPrice = 1, IsActive = true);
        //PricebookEntry pricebookEntryB = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productB.Id, UnitPrice = 1, IsActive = true);
        PricebookEntry pricebookEntryB = [SELECT Id FROM PriceBookEntry WHERE Pricebook2Id = :valuationPricebook.Id];//new PricebookEntry(Pricebook2Id = valuationPricebook.Id, Product2Id = productB.Id, UnitPrice = 1, IsActive = true);

        //insert new List<PricebookEntry>{pricebookEntryA, pricebookEntryB};


        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity testOpportunity = UnitTestDataTest.buildOpportunity('Test Opportunity', propertyObject);
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
        testOpportunity.CloseDate = Date.today().addDays(3);
        testOpportunity.ProjectStart__c = Date.today();
        insert testOpportunity;

        Account accountA = UnitTestDataTest.buildAccount('Account A');
        insert accountA;

        OpportunityLineItem opportunityProduct = new OpportunityLineItem(PricebookEntryId = pricebookEntryB.Id, OpportunityId = testOpportunity.Id, Quantity = 1, UnitPrice = 10);
        insert opportunityProduct;

        ONB2__Template__c templateA = new ONB2__Template__c();
        insert templateA;

        ONB2__Invoice__c invoiceA = new ONB2__Invoice__c(Name = 'A', ONB2__Account__c = accountA.Id, ONB2__Template__c = templateA.Id);
        ONB2__Invoice__c invoiceB = new ONB2__Invoice__c(Name = 'A', ONB2__Account__c = accountA.Id, ONB2__Template__c = templateA.Id);
        insert new List<ONB2__Invoice__c>{invoiceA, invoiceB};

        return new Map<String, SObject>{
            'Product A' => productA,
            'Product B' => productB,
            'Price Book Entry A' => pricebookEntryA,
            'Price Book Entry B' => pricebookEntryB,
            'Opportunity' => testOpportunity,
            'Opportunity Product' => opportunityProduct,
            'Invoice A' => invoiceA,
            'Invoice B' => invoiceB
        };
    }

    @istest
    static void shouldUpdateSubCommissions()
    {
        Map<String, SObject> objectMap = prepareData();
        Test.startTest();
        Opportunity testOpportunity = (Opportunity) objectMap.get('Opportunity');
//
//        Account accountA = UnitTestDataTest.buildAccount('Account A');
//        insert accountA;
//
        OpportunityLineItem opportunityProduct = (OpportunityLineItem) objectMap.get('Opportunity Product');
//        OpportunityLineItem opportunityProduct = new OpportunityLineItem(PricebookEntryId = objectMap.get('Price Book Entry A').Id, OpportunityId = testOpportunity.Id, Quantity = 1, UnitPrice = 10);
//        insert opportunityProduct;
//
//        ONB2__Template__c templateA = new ONB2__Template__c();
//        insert templateA;
//
//        ONB2__Invoice__c invoiceA = new ONB2__Invoice__c(Name = 'A', ONB2__Account__c = accountA.Id, ONB2__Template__c = templateA.Id);
//        ONB2__Invoice__c invoiceB = new ONB2__Invoice__c(Name = 'A', ONB2__Account__c = accountA.Id, ONB2__Template__c = templateA.Id);
        ONB2__Invoice__c invoiceA = (ONB2__Invoice__c) objectMap.get('Invoice A');
        ONB2__Invoice__c invoiceB = (ONB2__Invoice__c) objectMap.get('Invoice B');
//        insert new List<ONB2__Invoice__c>{invoiceA, invoiceB};

        SubCommissions__c subCommission = new SubCommissions__c(Opportunity__c = testOpportunity.Id,
                                                                Classification__c = 'Colliers International',
                                                                AmountNet__c = 10.00);
        insert subCommission;
        SubCommissions__c updatedSubCommission = [select Invoice__c, ProductScheduleId__c from SubCommissions__c where Id = :subCommission.Id limit 1];
        System.assertEquals(null, updatedSubCommission.Invoice__c);
        System.assertEquals(null, updatedSubCommission.ProductScheduleId__c);

        OpportunityLineItemSchedule lineItemSchedule = new OpportunityLineItemSchedule(OpportunityLineItemId = opportunityProduct.Id, Revenue = 100, ScheduleDate = System.today().addDays(10), Type = 'Revenue', ON_Invoice__c = invoiceA.Id);
        insert lineItemSchedule;
        updatedSubCommission = [select Invoice__c, ProductScheduleId__c from SubCommissions__c where Id = :subCommission.Id limit 1];
        System.assertEquals(invoiceA.Id, updatedSubCommission.Invoice__c);
        System.assertEquals(lineItemSchedule.Id, updatedSubCommission.ProductScheduleId__c);


        lineItemSchedule.ON_Invoice__c = invoiceB.Id;
        update lineItemSchedule;
        updatedSubCommission = [select Invoice__c, ProductScheduleId__c from SubCommissions__c where Id = :subCommission.Id limit 1];
        System.assertEquals(invoiceB.Id, updatedSubCommission.Invoice__c);
    }

    @istest
    static void shouldMaintainPredefinedCenterSplit()
    {
        Map<String, SObject> objectMap = prepareData();

        Test.startTest();

        OpportunityLineItemSchedule lineItemSchedule = new OpportunityLineItemSchedule(OpportunityLineItemId = objectMap.get('Opportunity Product').Id, Revenue = 100, ScheduleDate = System.today().addDays(10), Type = 'Revenue', Invoice__c = objectMap.get('Invoice A').Id, FeeSharingMapPt1__c ='AB', FeeSharingMapPt2__c ='CD',FeeSharingMapPt3__c ='X');
        insert lineItemSchedule;

        lineItemSchedule.ReadyForInvoicing__c = true;
        update lineItemSchedule;

        OpportunityLineItemSchedule updatedLineItemSchedule = [select ON_PredefinedCenterSplit__c from OpportunityLineItemSchedule where Id = :lineItemSchedule.Id limit 1];
        System.debug(updatedLineItemSchedule.ON_PredefinedCenterSplit__c);
        System.assert(updatedLineItemSchedule.ON_PredefinedCenterSplit__c.contains('ABCDX'));
    }

    /**
     * @author dme
     * @description Validate, that it is not possible to Update/Delete Schedule with Invoice set
     */
    @istest
    static void validateUpdateDeleteActionsOnScheduleWithInvoice()
    {
        Map<String, SObject> objectMap = prepareData();
        Test.startTest();
        OpportunityLineItem opportunityProduct = (OpportunityLineItem) objectMap.get('Opportunity Product');

        ONB2__Invoice__c invoiceA = (ONB2__Invoice__c) objectMap.get('Invoice A');
        ONB2__Invoice__c invoiceB = (ONB2__Invoice__c) objectMap.get('Invoice B');

        OpportunityLineItemSchedule lineItemSchedule = new OpportunityLineItemSchedule(OpportunityLineItemId = opportunityProduct.Id, Revenue = 100, ScheduleDate = System.today().addDays(10), Type = 'Revenue', ON_Invoice__c = invoiceA.Id);
        insert lineItemSchedule;

        //Update test
        Boolean isErrorHappened = false;
        String errorMessage = '';
        try
        {
            lineItemSchedule.Revenue = 101;
            update lineItemSchedule;
        }
        catch (Exception e)
        {
            isErrorHappened = true;
            errorMessage = e.getMessage();
        }
        System.assert(isErrorHappened, 'Should not be possible to update Schedule with Invoice set');
        System.assert(errorMessage.containsIgnoreCase(Label.OpportunityLineItemScheduleEditDeleteError), 'Should not be possible to update Schedule with Invoice set');

        lineItemSchedule.Revenue = 100;
        lineItemSchedule.ON_Invoice__c = invoiceB.Id;
        update lineItemSchedule;

        //Delete test
        isErrorHappened = false;
        errorMessage = '';
        try
        {
            delete lineItemSchedule;
        }
        catch (Exception e)
        {
            isErrorHappened = true;
            errorMessage = e.getMessage();
        }
        System.assert(isErrorHappened, 'Should not be possible to delete Schedule with Invoice set');
        System.assert(errorMessage.containsIgnoreCase(Label.OpportunityLineItemScheduleEditDeleteError), 'Should not be possible to delete Schedule with Invoice set');

        lineItemSchedule.ON_Invoice__c = null;
        update lineItemSchedule;

        delete lineItemSchedule;
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM OpportunityLineItemSchedule], 'Schedule should be deleted');
    }
}