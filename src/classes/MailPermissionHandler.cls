/**
*	@description MailPermissionHandler
*	@author npo
*	@copyright PARX
*/
public class MailPermissionHandler extends ATrigger
{
	public override void hookBeforeInsert(List<SObject> records)
	{
		MailPermissionService.handleUniquenessByUser(records, null);
	}

	public override void hookBeforeUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		MailPermissionService.handleUniquenessByUser(records, oldRecords);
	}
}