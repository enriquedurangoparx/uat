/**
* Handle business logic for ContactToInvestor__c by the trigger
*
* @author: <Egor Markuzov> (egor.markuzov@colliers.com)
*
* @history:
* version           | author                                                | changes
* ====================================================================================
* 0.1 31.12.2019	| Egor Markuzov (egor.markuzov@parx.com)	            | initial version.
*/
public inherited sharing class ContactToInvestorTrigger extends ATrigger
{
    public override void hookAfterInsert(List<SObject> records)
    {
        populateListOfContactsAfterInsert((List<ContactToInvestor__c>) records);
    }

    public override void hookBeforeDelete(List<SObject> records)
    {
        preventDeletionIfHasRelatedActivity((List<ContactToInvestor__c>) records);
    }

    public override void hookAfterDelete(List<SObject> records)
    {
        populateListOfContactsAfterDelete((List<ContactToInvestor__c>) records);
    }

    private void populateListOfContactsAfterInsert(List<ContactToInvestor__c> newList)
    {
        System.debug('populateListOfContactsAfterInsert start');
        Map<Id, Set<Id>> investorIdToContactId = new Map<Id, Set<Id>>();
        Set<Id> contactIdSet = new Set<Id>();

        for (ContactToInvestor__c cti : newList)
        {
            contactIdSet.add(cti.Contact__c);

            if (!investorIdToContactId.containsKey(cti.Investor__c))
            {
                investorIdToContactId.put(cti.Investor__c, new Set<Id>());
            }

            investorIdToContactId.get(cti.Investor__c).add(cti.Contact__c);
        }

        Map<Id, Contact> contactIdToContactMap = new Map<Id, Contact>([SELECT Id, Name
                                                                       FROM Contact
                                                                       WHERE Id IN :contactIdSet]);

        Map<Id, AccountToOpportunity__c> investorIdToInvestorMap = new Map<Id, AccountToOpportunity__c>([SELECT Id, ListOfContacts__c, (SELECT ID FROM InvestorContacts__r)
                                                                                                        FROM AccountToOpportunity__c
                                                                                                        WHERE Id IN :investorIdToContactId.keySet()]);

        List<AccountToOpportunity__c> investorToUpdateList = new List<AccountToOpportunity__c>();

        for (Id investorId : investorIdToContactId.keySet())
        {
            AccountToOpportunity__c investorToUpdate = investorIdToInvestorMap.get(investorId);

            String listOfContacts = '';

            for (Id contactId : investorIdToContactId.get(investorId))
            {
                if (String.isNotBlank(investorToUpdate.ListOfContacts__c) && !investorToUpdate.ListOfContacts__c.contains(contactId))
                {
                    listOfContacts += contactId + ' : ' + contactIdToContactMap.get(contactId).Name + ';';
                }

                if (String.isBlank(investorToUpdate.ListOfContacts__c))
                {
                    listOfContacts += contactId + ' : ' + contactIdToContactMap.get(contactId).Name + ';';
                }
            }

            if (String.isNotBlank(investorToUpdate.ListOfContacts__c))
            {
                investorToUpdate.ListOfContacts__c = listOfContacts + investorToUpdate.ListOfContacts__c;
            }
            else
            {
                investorToUpdate.ListOfContacts__c = listOfContacts;
            }

            List<String> contactRow = investorToUpdate.ListOfContacts__c.split(';');

            if (contactRow.size() > 5)
            {
                investorToUpdate.ListOfContacts__c = '';

                for (Integer i = 0; i < 5; i ++)
                {
                    investorToUpdate.ListOfContacts__c += contactRow[i] + ';';
                }
                if(investorToUpdate.InvestorContacts__r.size() > 5){
                    investorToUpdate.ListOfContacts__c += '000000000000000000 : (+' + String.valueOf(investorToUpdate.InvestorContacts__r.size() - 5) + ');';
                }
            }

            investorToUpdateList.add(investorToUpdate);
        }

        update investorToUpdateList;
        System.debug('populateListOfContactsAfterInsert end');
    }

    private void populateListOfContactsAfterDelete(List<ContactToInvestor__c> newList)
    {
        Map<Id, Set<Id>> investorIdToContactId = new Map<Id, Set<Id>>();
        // Set<Id> deletedContactIdSet = new Set<Id>();

        for (ContactToInvestor__c cti : newList)
        {
            if (!investorIdToContactId.containsKey(cti.Investor__c))
            {
                investorIdToContactId.put(cti.Investor__c, new Set<Id>());
            }

            investorIdToContactId.get(cti.Investor__c).add(cti.Contact__c);
        }

        Map<Id, AccountToOpportunity__c> investorIdToInvestorMap = new Map<Id, AccountToOpportunity__c>([SELECT Id, ListOfContacts__c, (SELECT ID, Contact__c, Contact__r.Name FROM InvestorContacts__r)
                                                                                                        FROM AccountToOpportunity__c
                                                                                                        WHERE Id IN :investorIdToContactId.keySet()]);
        for (Id investorId : investorIdToContactId.keySet())
        {
            List<ContactToInvestor__c> listContacts = investorIdToInvestorMap.get(investorId).InvestorContacts__r;
            String newListOfContacts = '';
            Integer i = 0;
            for (ContactToInvestor__c coi : listContacts)
            {
                newListOfContacts += coi.Contact__c + ' : ' + coi.Contact__r.Name + ';';
                i++;
                if(i == 5){
                    break;
                }
            }

            if(investorIdToInvestorMap.get(investorId).InvestorContacts__r.size() > 5){
                newListOfContacts += '000000000000000000 : (+' + String.valueOf(investorIdToInvestorMap.get(investorId).InvestorContacts__r.size() - 5) + ');';
            }

            investorIdToInvestorMap.get(investorId).ListOfContacts__c = newListOfContacts;
        }

        update investorIdToInvestorMap.values();
    }

    private void preventDeletionIfHasRelatedActivity(List<ContactToInvestor__c> newList)
    {
        Map<Id, Boolean> preventToDeleteMap = new Map<Id, Boolean>();
        for (ContactToInvestor__c cti : newList)
        {
            if (String.isNotBlank(cti.Investor__c))
            {
                preventToDeleteMap.put(cti.Investor__c, false);
            }
        }

        for (Task tsk : [SELECT Id, WhatId FROM Task WHERE WhatId IN :preventToDeleteMap.keySet()])
        {
            if (preventToDeleteMap.containsKey(tsk.WhatId))
            {
                preventToDeleteMap.put(tsk.WhatId, true);
            }
        }

        for (Status__c status : [SELECT Id, Investor__c FROM Status__c WHERE Investor__c IN :preventToDeleteMap.keySet()])
        {
            if (preventToDeleteMap.containsKey(status.Investor__c))
            {
                preventToDeleteMap.put(status.Investor__c, true);
            }
        }

        for (ContactToInvestor__c cti : newList)
        {
            if (String.isNotBlank(cti.Investor__c) && preventToDeleteMap.containsKey(cti.Investor__c) && preventToDeleteMap.get(cti.Investor__c))
            {
                cti.addError(System.Label.GeneralLabelErrorBecauseHasActivityOrStatus);
            }
        }
    }
}