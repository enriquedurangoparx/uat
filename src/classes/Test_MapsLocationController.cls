/**
 * @description Test class for MapsLocationController
 * @author dvo
 *
 * @copyright PARX
 */

@IsTest
private class Test_MapsLocationController {
    @IsTest
    static void testGetLocations() {

        PropertyObject__c testRecord = new PropertyObject__c(
                Name = 'Minsk',
                PropertyDescriptionEn__c = 'Minks Team Office',
                Geolocation__Longitude__s = 27.516388879157606,
                Geolocation__Latitude__s = 53.90834369283391
        );
        insert testRecord;

        List<MapsLocationController.Location> testLocations = MapsLocationController.getLocations('', new List<PropertyObject__c>{testRecord}, 'PropertyObject__c', '[{"fieldName": "Name"}]', 'Geolocation__c');
        System.assertEquals(1, testLocations.size(), 'Wrong number of locations are selected');
    }

    @IsTest
    static void testGetLookupFieldValue()
    {
        Account testAccount = new Account(Name = 'Test Account');
        String accName = MapsLocationController.getLookupFieldValue(testAccount, 'Name');
        System.assertEquals('Test Account', accName, 'Wrong field value was returned');
    }

    @IsTest
    static void testQuerySObject()
    {
        PropertyObject__c testRecord = new PropertyObject__c(
                Name = 'test'
        );
        insert testRecord;

        String whereClause = 'WHERE Id = \'' + testRecord.Id + '\'';

        List<SObject> testRecords = MapsLocationController.querySObject('PropertyObject__c', new List<String>{'{"fieldName": "Name"}'}, whereClause, null, 'Geolocation__Longitude__s', 'Geolocation__Latitude__s');
        System.assertEquals(1, testRecords.size(), 'Wrong number of records are selected');
    }
}