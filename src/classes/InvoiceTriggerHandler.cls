/*
*   @description InvoiceTriggerHandler functionality.
*   @copyright PARX
*/
public class InvoiceTriggerHandler extends TriggerTemplate.TriggerHandlerAdapter
{  
    
    public override void onAfterInsert(List<sObject> newList, Map<Id, sObject> newMap)
    {
        /** 
        Function:Creating debtor number and collier counter for invoice creation
        
        **/
        MAP<String,ColliersCounter__c> counterDataMap = new Map<String, ColliersCounter__c>();
        MAP<Id,Account> accMap;
        counterDataMap = Utils.getCounterRecords();
        
        system.debug('***counterDataMap::'+counterDataMap);
        
        List<ONB2__Invoice__c> invoiceNewList = (List<ONB2__Invoice__c>)newList;
        List<Id> acclist = new List<Id>();
        
        for(ONB2__Invoice__c invoice:invoiceNewList){
            acclist.add(invoice.ONB2__Account__c);
        }
        
        if(acclist.size()>0)
        {
        accMap = new MAP<Id,Account>([select id,DebtorNo__c,AccountType__c from Account where id in :acclist ]);
        }
        
        for(ONB2__Invoice__c invoice:invoiceNewList )
        {
               Account accObj = accMap.get(invoice.ONB2__Account__c);
                if(accObj.DebtorNo__c ==null && accObj.AccountType__c !=null)
                    {
                        
                        system.debug('**accObj::'+accObj);
                            
                            if(counterDataMap.containsKey(accObj.AccountType__c)){
                                ColliersCounter__c counterObj = counterDataMap.get(accObj.AccountType__c);
                                system.debug('**counterObj::'+counterObj);
                            
                                Integer debtorNumber = Integer.valueOf(counterObj.Counter__c);
                                counterObj.Counter__c=debtorNumber + 1;
                                accObj.DebtorNo__c = String.ValueOf(debtorNumber);
                                
                                
                            }else
                            { 
                                 //create new record in collier counter object
                                 
                                Integer debtorNo =  (accObj.AccountType__c == Utils.ACCOUNT_TYPE_CUSTOMER?1000000:
                                                        (accObj.AccountType__c ==Utils.ACCOUNT_TYPE_COLLIERS?6000000:
                                                        (accObj.AccountType__c ==Utils.ACCOUNT_TYPE_SHAREHOLDER?2000000:null)));
                                
                                accObj.DebtorNo__c = ''+debtorNo;
                                counterDataMap.put(accObj.AccountType__c, new ColliersCounter__c(Name=accObj.AccountType__c,Counter__c=debtorNo+1));

                            } 
                    
                    }
        }//End of for loop
         
          system.debug('**accMap::'+accMap+'**counterDataMap*'+counterDataMap);
        if(counterDataMap.size()>0)
        {
                upsert counterDataMap.values();
        }
        if(accMap.size()>0)
        {
                update accMap.values();
        }
        
    }
    /** INSERT ACTIONS END */
    
    public override void onAfterUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap)
    {
        /** 
        Function:Creating debtor number and collier counter for invoice creation
        
        **/
        MAP<String,ColliersCounter__c> counterDataMap = new Map<String, ColliersCounter__c>();
        MAP<Id,Account> accMap;
        counterDataMap = Utils.getCounterRecords();
        
        system.debug('***counterDataMap::'+counterDataMap);
        
        List<ONB2__Invoice__c> invoiceNewList = (List<ONB2__Invoice__c>)newList;
        List<Id> acclist = new List<Id>();
        Map<Id, ONB2__Invoice__c> invoiceOldMap = (Map<Id, ONB2__Invoice__c>)oldMap;

        processCanceledInvoices(invoiceNewList, invoiceOldMap);
        
        for(ONB2__Invoice__c invoice:invoiceNewList){
            acclist.add(invoice.ONB2__Account__c);
        }
        
        if(acclist.size()>0)
        {
        accMap = new MAP<Id,Account>([select id,DebtorNo__c,AccountType__c from Account where id in :acclist ]);
        }
        
        for(ONB2__Invoice__c invoice:invoiceNewList )
        {
               Account accObj = accMap.get(invoice.ONB2__Account__c);
                if(accObj.DebtorNo__c ==null && accObj.AccountType__c !=null)
                    {
                        
                        system.debug('**accObj::'+accObj);
                            
                            if(counterDataMap.containsKey(accObj.AccountType__c)){
                                ColliersCounter__c counterObj = counterDataMap.get(accObj.AccountType__c);
                                system.debug('**counterObj::'+counterObj);
                            
                                Integer debtorNumber = Integer.valueOf(counterObj.Counter__c);
                                counterObj.Counter__c=debtorNumber + 1;
                                accObj.DebtorNo__c = String.ValueOf(debtorNumber);
                                
                                
                            }else
                            { 
                                 //create new record in collier counter object
                                 
                                Integer debtorNo =  (accObj.AccountType__c == Utils.ACCOUNT_TYPE_CUSTOMER?1000000:
                                                        (accObj.AccountType__c ==Utils.ACCOUNT_TYPE_COLLIERS?6000000:
                                                        (accObj.AccountType__c ==Utils.ACCOUNT_TYPE_SHAREHOLDER?2000000:null)));
                                
                                accObj.DebtorNo__c = ''+debtorNo;
                                counterDataMap.put(accObj.AccountType__c, new ColliersCounter__c(Name=accObj.AccountType__c,Counter__c=debtorNo+1));

                            } 
                    
                    }
        }//End of for loop
         
          system.debug('**accMap::'+accMap+'**counterDataMap*'+counterDataMap);
        if(counterDataMap.size()>0)
        {
                upsert counterDataMap.values();
        }
        if(accMap.size()>0)
        {
                update accMap.values();
        }
        
    }
    /** INSERT UPDATE END */

    public static void processCanceledInvoices(List<ONB2__Invoice__c> newInvoices, Map<Id, ONB2__Invoice__c> oldInvoicesMap) 
    {
        
        List<ONB2__Invoice__c> canceledInvoices = new List<ONB2__Invoice__c>();

        for (ONB2__Invoice__c invoice : newInvoices)
        {
            
            if (invoice.ONB2__Status__c == 'Canceled' && 
                invoice.ONB2__Status__c != oldInvoicesMap.get(invoice.Id).ONB2__Status__c &&
                invoice.ONB2__CanceledWith__c != null) 
            {
                canceledInvoices.add(invoice);
            }

        }
        
        if (!canceledInvoices.isEmpty()) 
        {

            WithoutSharingActions actions = new WithoutSharingActions();
            actions.clearSubCommissionRelations(canceledInvoices);

        }

    }

    public without sharing class WithoutSharingActions 
    {

        public void clearSubCommissionRelations(List<ONB2__Invoice__c> invoices) 
        {

            List<SubCommissions__c> subCommissions = [
                SELECT Id, Invoice__c
                FROM SubCommissions__c
                WHERE Invoice__c IN: invoices
                AND Opportunity__c != null
            ];

            if (!subCommissions.isEmpty()) 
            {

                for (SubCommissions__c sc : subCommissions) 
                {
                    sc.Invoice__c = null;
                }

                update subCommissions;

            }

        }

    }
    
}