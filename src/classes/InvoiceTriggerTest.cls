@isTest
public class InvoiceTriggerTest {
    
    public static testMethod void testInvoiceForNoCounter() {
        UnitTestDataTest.createTestRecords();
        MAP<String,Account> accMap = new MAP<String,Account>();
        accMap = UnitTestDataTest.createTestAccountCustomer();
        ONB2__Template__c template = new ONB2__Template__c(Name='default');
        insert template;
        Test.startTest();
        TriggerTemplate.setupUnitTest();
        TriggerTemplate.startUnitTest(); 
        ONB2__Invoice__c custInvoice = new ONB2__Invoice__c(Name='CustInvoice',ONB2__Account__c=accMap.get('Customer').id,ONB2__Template__c=template.id,ONB2__Date__c=Date.newInstance(2018, 1, 5),
        ONB2__Status__c='Draft');
        insert custInvoice;
        System.assertEquals('Draft', custInvoice.ONB2__Status__c);
        
        ONB2__Invoice__c collierInvoice = new ONB2__Invoice__c(Name='collierInvoice',ONB2__Account__c=accMap.get('Intercompany Colliers').id,ONB2__Template__c=template.id,ONB2__Date__c=Date.newInstance(2018, 1, 5),
        ONB2__Status__c='Draft');
        insert collierInvoice;
        
        ONB2__Invoice__c shareInvoice = new ONB2__Invoice__c(Name='shareInvoice',ONB2__Account__c=accMap.get('Intercompany Shareholder').id,ONB2__Template__c=template.id,ONB2__Date__c=Date.newInstance(2018, 1, 5),
        ONB2__Status__c='Draft');
        insert shareInvoice;
        System.assertEquals(shareInvoice.ONB2__Account__c!=null,true);
        
        Test.stopTest();
    }
    public static testMethod void testInvoiceForCounter() {
        UnitTestDataTest.createTestRecords();
        MAP<String,Account> accMap = new MAP<String,Account>();
        accMap = UnitTestDataTest.createTestAccountCustomer();
        UnitTestDataTest.createTestCounter();
        ONB2__Template__c template = new ONB2__Template__c(Name='default');
        insert template;
        
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true; 
        Account custAccount = new Account(Name='My number customer 123',AccountType__c='Customer');
        Database.SaveResult sr = Database.insert(custAccount, dml);
        //insert custAccount;

        ONB2__Invoice__c custInvoice = new ONB2__Invoice__c(Name='CustInvoice',ONB2__Account__c=accMap.get(Utils.ACCOUNT_TYPE_CUSTOMER).id,ONB2__Template__c=template.id,ONB2__Date__c=Date.newInstance(2018, 1, 5),
        ONB2__Status__c='Draft');
        insert custInvoice;
        System.assertEquals('Customer', accMap.get(Utils.ACCOUNT_TYPE_CUSTOMER).AccountType__c);

        Test.startTest();
        custInvoice.ONB2__Account__c = custAccount.Id;
        update custInvoice;
        System.assertEquals('Customer', accMap.get(Utils.ACCOUNT_TYPE_CUSTOMER).AccountType__c);
       
        ONB2__Invoice__c collierInvoice = new ONB2__Invoice__c(Name='collierInvoice',ONB2__Account__c=accMap.get(Utils.ACCOUNT_TYPE_COLLIERS).id,ONB2__Template__c=template.id,ONB2__Date__c=Date.newInstance(2018, 1, 5),
        ONB2__Status__c='Draft');
        insert collierInvoice;
        System.assertEquals('Intercompany Colliers', accMap.get(Utils.ACCOUNT_TYPE_COLLIERS).AccountType__c);
       
        ONB2__Invoice__c shareInvoice = new ONB2__Invoice__c(Name='shareInvoice',ONB2__Account__c=accMap.get(Utils.ACCOUNT_TYPE_SHAREHOLDER).id,ONB2__Template__c=template.id,ONB2__Date__c=Date.newInstance(2018, 1, 5),
        ONB2__Status__c='Draft');
        insert shareInvoice;
        Test.stopTest();
    }
    public static testMethod void testClearSubCommissionRelations() 
    {
        UnitTestDataTest.createTestRecords();
        MAP<String,Account> accMap = new MAP<String,Account>();
        accMap = UnitTestDataTest.createTestAccountCustomer();
        ONB2__Template__c template = new ONB2__Template__c(Name='default');
        insert template;
        
        OpportunityHandlerTest.initializeProducts();
        
        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;
        
        Test.startTest();
        TriggerTemplate.setupUnitTest();
        TriggerTemplate.startUnitTest();

        List<ONB2__Invoice__c> ONB2InvoiceList = new List<ONB2__Invoice__c>();

        ONB2__Invoice__c custInvoice1 = new ONB2__Invoice__c(
            Name = 'CustInvoice',
            ONB2__Account__c = accMap.get('Customer').id,
            ONB2__Template__c = template.id,
            ONB2__Date__c = Date.newInstance(2018, 1, 5),
            ONB2__Status__c = 'Draft'
        );
        ONB2InvoiceList.add(custInvoice1);

        ONB2__Invoice__c custInvoice2 = new ONB2__Invoice__c(
            Name = 'CustInvoice',
            ONB2__Account__c = accMap.get('Customer').id,
            ONB2__Template__c = template.id,
            ONB2__Date__c = Date.newInstance(2018, 1, 5),
            ONB2__Status__c = 'Draft'
        );
        ONB2InvoiceList.add(custInvoice2);
        insert ONB2InvoiceList;

        PropertyObject__c propertyObject = TestDataFactory.buildPropertyObject('property', true);
        Opportunity opp = TestDataFactory.createOpportunity(null, propertyObject);
        insert opp;

        SubCommissions__c subCommission = new SubCommissions__c(
            Invoice__c = custInvoice2.Id,
            Opportunity__c = opp.Id,
            Classification__c = 'Colliers International',
            AmountNet__c = 10.00
        );
        insert subCommission;

        ONB2__InvoiceLineItem__c invoiceLineItem = new ONB2__InvoiceLineItem__c(
            ONB2__Invoice__c = custInvoice2.Id,
            ONB2__UnitPrice__c = 100,
            ONB2__TaxRate__c = 100, 
            ONB2__Title__c = 'Test'
        );
        insert invoiceLineItem;

        custInvoice2.ONB2__Status__c = 'Canceled';
        custInvoice2.ONB2__CanceledWith__c = custInvoice1.Id;

        ONB2__Invoice__c oldInvoice = new ONB2__Invoice__c(
            Name = 'CustInvoice',
            ONB2__Account__c = accMap.get('Customer').id,
            ONB2__Template__c = template.id,
            ONB2__Date__c = Date.newInstance(2018, 1, 5),
            ONB2__Status__c = 'Draft'
        );

        Map<Id, ONB2__Invoice__c> oldInvoicesMap = new Map<Id, ONB2__Invoice__c>{
            custInvoice2.Id => oldInvoice
        }; 
            
        InvoiceTriggerHandler.processCanceledInvoices(new List<ONB2__Invoice__c>{custInvoice2}, oldInvoicesMap);

        List<SubCommissions__c> subCommissions = [
            SELECT Id, Invoice__c
            FROM SubCommissions__c
            WHERE Invoice__c =: custInvoice2.Id
        ];
        System.assertEquals(0, subCommissions.size());
        delete subCommission;
        Test.stopTest();
    }

    public static testMethod void testSyncStatuses()
    {
        UnitTestDataTest.createTestRecords();
        MAP<String,Account> accMap = new MAP<String,Account>();
        accMap = UnitTestDataTest.createTestAccountCustomer();
        ONB2__Template__c template = new ONB2__Template__c(Name = 'default');
        insert template;

        TriggerTemplate.setupUnitTest();
        TriggerTemplate.startUnitTest();
        ONB2__Invoice__c custInvoice = new ONB2__Invoice__c(
                Name = 'CustInvoice',
                ONB2__Account__c = accMap.get('Customer').id,
                ONB2__Template__c = template.id,
                ONB2__Date__c = Date.newInstance(2018, 1, 5),
                ONB2__Status__c='Draft'
        );
        insert custInvoice;
        System.assertEquals('Draft', custInvoice.ONB2__Status__c);

        ONB2__InvoiceLineItem__c invoiceLineItem = new ONB2__InvoiceLineItem__c(
                ONB2__Invoice__c = custInvoice.Id,
                ONB2__UnitPrice__c = 100,
                ONB2__TaxRate__c = 100,
                ONB2__Title__c = 'Test',
                ONB2__Center__c = 'Test',
                ONB2__GLAccount__c = 'Test'
        );
        insert invoiceLineItem;

        Test.startTest();

        initTimCards();

        List<TimeCard__c> timeCards = [SELECT Id FROM TimeCard__c];
        System.assertEquals(3, timeCards.size());

        for (TimeCard__c tc: timeCards)
        {
            tc.ON_Invoice__c = custInvoice.Id;
        }
        update timeCards;

        custInvoice.ONB2__Status__c = 'Open';
        update custInvoice;

        timeCards = [SELECT Id FROM TimeCard__c WHERE Status__c = 'Billed'];
        System.assertEquals(3, timeCards.size());
    }


    private static void initTimCards()
    {
        OpportunityHandlerTest.initializeProducts();// Needed to setup product, pricebook and pricebookentry.

        Embargo__c embargo = new Embargo__c();
        embargo.Name = 'Embargo';
        embargo.Countries__c = 'Egypt;Afghanistan;Belarus;Burundi;Iraq;Yemen';
        insert embargo;

        Opportunity project = UnitTestDataTest.buildOpportunity('Project Opportunity');
        project.BillingType__c = 'Time and Material';
        project.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('ValuationAcquisition').getRecordTypeId();
        project.ProjectStart__c = Date.today();
        project.CloseDate = Date.today().addDays(3);
        insert project;

        ProjectRole__c projectRole = new ProjectRole__c(Project__c = project.Id, DailyRate__c = 10, TargetAmountHours__c = 8, StartDate__c = System.today().addDays(-1), EndDate__c = System.today().addDays(10));
        insert projectRole;

        Product2 productA = new Product2(Name = 'A', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        Product2 productB = new Product2(Name = 'B', CanUseRevenueSchedule = true, RelevantForFeeSharing__c = true);
        insert new List<Product2>{productA, productB};

        PricebookEntry standardPricebookEntry = new PricebookEntry();
        standardPricebookEntry.IsActive = true;
        standardPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        standardPricebookEntry.Product2Id = productA.Id;
        standardPricebookEntry.UnitPrice = 100;
        insert standardPricebookEntry;

        Pricebook2 valuationPricebook = [SELECT Id FROM Pricebook2 WHERE Name = :OpportunityHandler.VALUATION_PRICEBOOKNAME];

        PricebookEntry pricebookEntryA = standardPricebookEntry;//new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = productA.Id, UnitPrice = 1, IsActive = true);
        PricebookEntry pricebookEntryB = [SELECT Id FROM PriceBookEntry WHERE Pricebook2Id = :valuationPricebook.Id];//new PricebookEntry(Pricebook2Id = valuationPricebook.Id, Product2Id = productB.Id, UnitPrice = 1, IsActive = true);

        Opportunity testOpportunity = project;

        Account accountA = UnitTestDataTest.buildAccount('Account A');
        insert accountA;

        OpportunityLineItem opportunityProduct = new OpportunityLineItem(Product2Id = productA.Id, PricebookEntryId = pricebookEntryB.Id, OpportunityId = testOpportunity.Id, Quantity = 1, UnitPrice = 10);
        insert opportunityProduct;

        List<OpportunityLineItem__c> list_customOpportunityProduct = [SELECT Id FROM OpportunityLineItem__c WHERE OpportunityProductId__c = :opportunityProduct.Id];
        system.assertEquals(1, list_customOpportunityProduct.size());

        TimeCard__c timeCard1 = new TimeCard__c(Product__c = list_customOpportunityProduct.get(0).Id, User__c = UserInfo.getUserId(), Project__c = testOpportunity.Id, HoursWorked__c = 1, Description__c = 'Testing 1', Role__c = projectRole.Id);
        TimeCard__c timeCard2 = new TimeCard__c(Product__c = list_customOpportunityProduct.get(0).Id, User__c = UserInfo.getUserId(), Project__c = testOpportunity.Id, HoursWorked__c = 1, Description__c = 'Testing 2', Role__c = projectRole.Id);
        TimeCard__c timeCard3 = new TimeCard__c(Product__c = list_customOpportunityProduct.get(0).Id, User__c = UserInfo.getUserId(), Project__c = testOpportunity.Id, HoursWorked__c = 1, Description__c = 'Testing 3', Role__c = projectRole.Id);
        insert new List<TimeCard__c> { timeCard1, timeCard2, timeCard3 };
    }
}