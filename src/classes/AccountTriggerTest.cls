/**
* Test class for AccountTrigger.cls
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 19.02.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | initial version.
* 0.2 17.03.2020	| Christian Schwabe (Christian.Schwabe@colliers.com)    | [SFINV/US 2407] Added Saluation for Contact.
*/
@isTest
private class AccountTriggerTest {

    @isTest
    public static void testAccountDeletion() {
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true; 
        Account custAccount = new Account(Name='My number customer 123',AccountType__c='Customer');
        Database.SaveResult sr = Database.insert(custAccount, dml);
        
        Account custAccountChild = new Account(Name='My number customer 456',AccountType__c='Customer',ParentId=custAccount.Id);
        Database.SaveResult sr2 = Database.insert(custAccountChild, dml);
        
        Test.startTest();
            delete custAccount;
        Test.stopTest();
    }

    /**
     * [SFINV/US 1386]
     * Test if all related contacts set to Contact.Status__c = 'Inactive' if Account.Status = 'Inactive'.
     */
    @isTest
    public static void testSetContactToInactive(){
        Account account = new Account();
        account.Name = 'Test GmbH';
        account.Status__c = 'Active';
        insert account;

        Contact contact = new Contact();
        contact.AccountId = account.Id;
        contact.LastName = 'Mustermann';
        contact.Salutation = 'Mr.';
        insert contact;

        Contact contact2 = new Contact();
        contact2.AccountId = account.Id;
        contact2.LastName = 'Maier-Müller';
        contact2.Salutation = 'Mr.';
        insert contact2;

        Test.startTest();
            account.Status__c = 'Inactive';
            update account;
        Test.stopTest();

        // All related contacts to inactive account should also be inactive.
        for(Contact inactiveContact : [SELECT Id, Name, Status__c FROM Contact WHERE AccountId = :account.Id]){
            System.assertEquals('Inactive', inactiveContact.Status__c);
        }
    }

    /**
     * DataImport permission set allows to skip the trigger execution, therefore no trigger logic should be applied after the execution
     */
    @IsTest
    public static void triggerDisablingTest(){
        User user = TestDataFactory.createUser('System Administrator', true);
        TestDataFactory.createPermissionSetAssignment(user, 'DataImport');

        System.runAs(user) {
            Account account = new Account();
            account.Name = 'Test GmbH';
            account.Status__c = 'Active';
            insert account;

            Contact contact = new Contact();
            contact.AccountId = account.Id;
            contact.LastName = 'Mustermann';
            contact.Salutation = 'Mr.';
            insert contact;

            Test.startTest();
            account.Status__c = 'Inactive';
            update account;
            Test.stopTest();

            contact = [SELECT Id, Name, Status__c FROM Contact WHERE AccountId = :account.Id];
            System.assertEquals(contact.Status__c, 'Active', 'Trigger logic should not be fired');
        }
    }
}