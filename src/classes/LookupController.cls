/* 
* @author jens.siffermann@youperience.com
* @date 2019-03-20
* @version 1.0.0
*
* @group Lookup
*
* @description c-lookup lwc apex controller
*/
public with sharing class LookupController {
    private final static Integer MAX_RESULTS = 5;

    /**
     * Search related records
     * @param  objectApiName        SObject Api name
     * @param  lookupApiName        Lookup field Api name
     * @param  searchTerm           Search term
     * @param  limitToObjectTypes   Limit search to object types
     * @return                 List<LookupSearchResult> list of records
     */
    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> search(String parentObjectApiName, String lookupApiName, String searchTerm, List<String> limitToObjectTypes) {
        List<LookupSearchResult> results = search(parentObjectApiName, lookupApiName, searchTerm, limitToObjectTypes, null, null);
        return results;
    }
    
    /**
     * Search related records
     * @param  objectApiName        SObject Api name
     * @param  lookupApiName        Lookup field Api name
     * @param  searchTerm           Search term
     * @param  limitToObjectTypes   Limit search to object types
     * @param  selectedIds          excluded selected Ids 
     * @return                 List<LookupSearchResult> list of records
     */
    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> search(String parentObjectApiName, String lookupApiName, String searchTerm, List<String> limitToObjectTypes,  List<String> selectedIds) {
        List<LookupSearchResult> results = search(parentObjectApiName, lookupApiName, searchTerm, limitToObjectTypes, selectedIds, null);
        return results;
    }

    /**
     * Search related records
     * @param  objectApiName        SObject Api name
     * @param  lookupApiName        Lookup field Api name
     * @param  searchTerm           Search term
     * @param  limitToObjectTypes   Limit search to object types
     * @param  selectedIds          excluded selected Ids
     * @param  lookupFilters        lookup filter
     * @return                 List<LookupSearchResult> list of records
     */
    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> search(String parentObjectApiName, String lookupApiName, String searchTerm, List<String> limitToObjectTypes,  List<String> selectedIds, List<LookupFilter> lookupFilters) {
        // get lookup field meta data
        List<String> referenceObjectTypes = MetadataUtil.getLookupReferenceObjectTypes(parentObjectApiName, lookupApiName, true);

        // remove unnecessary object types
        if(limitToObjectTypes != null && limitToObjectTypes.size() > 0) {
            for(Integer i = (referenceObjectTypes.size() -1); i >= 0; i--) {
                String objectType = referenceObjectTypes.get(i);

                if(limitToObjectTypes.contains(objectType) == false) {
                    referenceObjectTypes.remove(i);
                }
            }
        }
        return searchWithReferenceObjectTypes(referenceObjectTypes, searchTerm, limitToObjectTypes, selectedIds, lookupFilters);
    }

    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> searchWithReferenceObjectTypes(List<String> referenceObjectTypes, String searchTerm, List<String> limitToObjectTypes,  List<String> selectedIds, List<LookupFilter> lookupFilters)
    {
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Prepare query paramters
        searchTerm += '*';

        // build sosl query
        String sosl = 'FIND \'' + searchTerm + '\' IN ALL FIELDS RETURNING ';

        Map<String, String> objectNameFieldMap = new Map<String, String>();

        // build sosl for each reference object
        String[] objectSosls = new String[]{};
        for(String reference : referenceObjectTypes) {
            String objectSosl = reference + '(Id';

            // name field of object type
            String nameField = MetadataUtil.getObjectNameFieldApiName(reference);

            objectNameFieldMap.put(reference, nameField);

            if(nameField != null) {
                objectSosl += ', ' + nameField;
            }

            if(selectedIds != null && selectedIds.size() > 0) {
                objectSosl += ' WHERE Id NOT IN :selectedIds';
            }

            if(lookupFilters != null && lookupFilters.size() > 0) {

                Boolean addAnd = true;
                if(selectedIds == null || selectedIds.size() == 0) {
                    addAnd = false;
                    objectSosl += ' WHERE';
                }

                for(LookupFilter lookupFilter : lookupFilters) {
                    if(lookupFilter.objectType == reference) {
                        if(addAnd == true) {
                            objectSosl += ' AND';
                        }
                        else {
                            addAnd = true;
                        }

                        objectSosl += ' ' + lookupFilter.filter;
                    }
                }
            }

            objectSosl += ')';

            objectSosls.add(objectSosl);
        }

        sosl += String.join(objectSosls, ', ') + ' LIMIT ' + MAX_RESULTS;
        System.debug('...sosl:' + sosl);

        // execute search
        Search.SearchResults searchResults = Search.find(sosl);

        // get search result for each object type
        for(String reference : referenceObjectTypes) {
            List<Search.SearchResult> objectResult = searchResults.get(reference);

            // Prepare results

            String icon = 'standard:search';
            if(reference.indexOf('__c') == -1) {
                // in case of standard object use object standard icon
                icon = 'standard:' + reference.toLowerCase();
            }

            for (Search.SearchResult searchResult : objectResult) {
                SObject sobj = searchResult.getSObject();

                String nameField = objectNameFieldMap.get(reference);
                String title = (String) sobj.get(nameField);
                String subtitle = sobj.getSObjectType().getDescribe().getLabel();

                results.add(new LookupSearchResult(sobj.Id, reference, icon, title, subtitle));
            }
        }

        return results;
    }

    /**
     * Query lookup record
     * @param  lookupApiName   Lookup field API name
     * @param  recordId        Record Id of related record
     * @return                 LookupRecordResult
     */
    @AuraEnabled(Cacheable=true)
    public static LookupController.LookupRecordResult lookupDetail(String lookupApiName, Id recordId) {
        
        if(recordId == null) {
            throw new AuraHandledException('MISSING_RECORD_ID');
        }

        // target object type bases on recordId
        Schema.DescribeSObjectResult targetDescribe = recordId.getSobjectType().getDescribe();
        String targetObjectType = targetDescribe.getName();

        LookupController.LookupRecordResult result = new LookupController.LookupRecordResult();
        result.objectType = targetObjectType;

        if(!String.isBlank(targetObjectType) && recordId != null) {
            String queryFields = 'Id';

            String nameField = MetadataUtil.getObjectNameFieldApiName(targetDescribe);
            if(nameField != null) {
                queryFields += ', ' + nameField;
            }

            // get related record details
            String query = 'SELECT ' + queryFields + ' FROM ' + targetObjectType + ' WHERE Id =\'' + recordId + '\'';
            SObject obj = Database.query(query);

            result.record = obj;
        }
        
        return result;
    }

    /**
     * check if a field exist in SObject
     * @param  objectApiName SObject API name
     * @param  fieldApiName  Field API name
     * @return               true if field exists and is accessible
     */
    /*private static boolean fieldExists(String objectApiName, String fieldApiName) {
        SObject so = Schema.getGlobalDescribe().get(objectApiName).newSObject();
        return so.getSObjectType().getDescribe().fields.getMap().containsKey(fieldApiName);
    }*/

    /**
     * inner class LookupRecordResult
     */
    @testVisible
    public class LookupRecordResult {
        @AuraEnabled
        public String objectType;
        @AuraEnabled
        public SObject record;
    }

    @testVisible
    public class LookupFilter {
        @AuraEnabled
        public String objectType {get; set;}
        @AuraEnabled
        public String filter {get; set;} 
    }
}