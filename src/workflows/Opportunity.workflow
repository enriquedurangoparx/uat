<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Message_for_reminding_of_expiration_of_the_current_contract_DE</fullName>
        <description>Message for reminding of expiration of the current contract - DE</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ReminderForContractExpirationDE</template>
    </alerts>
    <alerts>
        <fullName>Message_for_reminding_of_expiration_of_the_current_contract_EN</fullName>
        <description>Message for reminding of expiration of the current contract - EN</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ReminderForContractExpirationEN</template>
    </alerts>
    <alerts>
        <fullName>Notification_Alert_about_Owner_Change</fullName>
        <description>Notification Alert about Owner Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>PreviousOwner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity/NotificationChangeOpportunityOwnershipDE</template>
    </alerts>
    <alerts>
        <fullName>OpportunityContractExpirationAlertDE</fullName>
        <description>Message for reminding of expiration of the current contract - DE</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ReminderForContractExpirationDE</template>
    </alerts>
    <alerts>
        <fullName>OpportunityContractExpirationAlertEN</fullName>
        <description>Message for reminding of expiration of the current contract - EN</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ReminderForContractExpirationEN</template>
    </alerts>
    <alerts>
        <fullName>OpportunityProjectDatesUpdateDE</fullName>
        <description>Opportunity Project Date Update DE</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ProjectDateUpdateNotificationDE</template>
    </alerts>
    <alerts>
        <fullName>OpportunityProjectDatesUpdateEN</fullName>
        <description>Opportunity Project Date Update EN</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ProjectDateUpdateNotificationEN</template>
    </alerts>
    <alerts>
        <fullName>SendProjectStatusReminder</fullName>
        <description>Send notification about updating Project Status (DE)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>DelegatedOwner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ProjectStatusReminder</template>
    </alerts>
    <fieldUpdates>
        <fullName>SetLastReminderDate</fullName>
        <field>LastReminderDate__c</field>
        <formula>TODAY()</formula>
        <name>SetLastReminderDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetProbabilityToZero</fullName>
        <description>[INV/US 1677] Set probability to 0 % if the record type is internal or watchlist.</description>
        <field>Probability</field>
        <formula>0</formula>
        <name>Set Probability To Zero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto-Reminder for ProjectStatus after 1 day of last reminder - DE</fullName>
        <active>true</active>
        <formula>AND( 				NOT($Permission.CanSkipTriggers), 				LastReminderDate__c ==  TODAY(), 				RecordType.Name == &apos;WPC Project&apos;, 				(TODAY() - LastReportDate__c) &gt; 7 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SendProjectStatusReminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SetLastReminderDate</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.LastReminderDate__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Auto-Reminder for ProjectStatus after 7 days - DE</fullName>
        <active>true</active>
        <formula>AND(     NOT($Permission.CanSkipTriggers),     LastReportDate__c == TODAY(),     RecordType.Name == &apos;WPC Project&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SendProjectStatusReminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SetLastReminderDate</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.LastReportDate__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Auto-Reminder for closing contract - DE</fullName>
        <active>true</active>
        <formula>AND( 				NOT($Permission.CanSkipTriggers), 				NOT(ISBLANK(  Reminderdate__c   )) &amp;&amp; (Owner.Language__c = &apos;de_DE&apos; || Owner.Language__c = &apos;de&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Message_for_reminding_of_expiration_of_the_current_contract_DE</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Reminderdate__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Auto-Reminder for closing contract - EN</fullName>
        <active>true</active>
        <formula>AND( 				NOT($Permission.CanSkipTriggers), 				NOT(ISBLANK( Reminderdate__c )) &amp;&amp; NOT(Owner.Language__c = &apos;de_DE&apos; || Owner.Language__c = &apos;de&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Message_for_reminding_of_expiration_of_the_current_contract_EN</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Reminderdate__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SetProbabilityToZero</fullName>
        <actions>
            <name>SetProbabilityToZero</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Investment External,Investment Watchlist</value>
        </criteriaItems>
        <description>[INV/US 1677] Set probability to 0 % if the record type is internal or watchlist.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
