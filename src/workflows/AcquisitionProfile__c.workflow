<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetHotel</fullName>
        <field>TypeOfUse__c</field>
        <literalValue>Hotel</literalValue>
        <name>Set Hotel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetIndustrialLogistics</fullName>
        <field>TypeOfUse__c</field>
        <literalValue>Industrial &amp; Logistics</literalValue>
        <name>Set Industrial &amp; Logistics</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetLand</fullName>
        <field>TypeOfUse__c</field>
        <literalValue>Land</literalValue>
        <name>Set Land</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetOffice</fullName>
        <field>TypeOfUse__c</field>
        <literalValue>Office / Mixed-use</literalValue>
        <name>Set Office</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetOther</fullName>
        <field>TypeOfUse__c</field>
        <literalValue>Other</literalValue>
        <name>Set Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetResidential</fullName>
        <field>TypeOfUse__c</field>
        <literalValue>Residential</literalValue>
        <name>Set Residential</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRetail</fullName>
        <field>TypeOfUse__c</field>
        <literalValue>Retail</literalValue>
        <name>Set Retail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Hotel</fullName>
        <actions>
            <name>SetHotel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AcquisitionProfile__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hotel</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Industrial %26 Logistics</fullName>
        <actions>
            <name>SetIndustrialLogistics</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AcquisitionProfile__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Industrial &amp; Logistics</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Land</fullName>
        <actions>
            <name>SetLand</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AcquisitionProfile__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Land</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Office</fullName>
        <actions>
            <name>SetOffice</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AcquisitionProfile__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Office / Mixed-Use</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Other</fullName>
        <actions>
            <name>SetOther</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AcquisitionProfile__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Other</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Residential</fullName>
        <actions>
            <name>SetResidential</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AcquisitionProfile__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Residential</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Retail</fullName>
        <actions>
            <name>SetRetail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AcquisitionProfile__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
