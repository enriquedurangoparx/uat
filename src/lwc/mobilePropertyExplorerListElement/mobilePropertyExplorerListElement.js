import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import rentedSpace from '@salesforce/label/c.mobilePropertyExplorerRentedSpace';

export default class MobilePropertyExplorerListElement extends NavigationMixin(LightningElement) {

    @api property;
    @api key;

    label = {
        rentedSpace
       };


    handlePropertySelected() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": this.property.Id,
                "objectApiName": "Property__c",
                "actionName": "view"
            },
        });

    }

}