import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

import loadInvestorsListItems from '@salesforce/apex/InvestorSelectionHelper.loadInvestorsListItems';

// import custom labels:
import investorsSelectionItemAmountLabel from '@salesforce/label/c.InvestorsSelectionItemAmount';
import investorsSelectionListLastEditedOnLabel from '@salesforce/label/c.InvestorsSelectionListLastEditedOn';
import investorsSelectionListLastEditedByLabel from '@salesforce/label/c.InvestorsSelectionListLastEditedBy';
import investorsSelectionListCreatedOnLabel from '@salesforce/label/c.InvestorsSelectionListCreatedOn';
import investorsSelectionListOwnerLabel from '@salesforce/label/c.InvestorsSelectionListOwner';
import brandLabel from '@salesforce/label/c.InvestorsSelectionBrand';
import representativeLabel from '@salesforce/label/c.InvestorsSelectionRepresentative';
import streetLabel from '@salesforce/label/c.InvestorsSelectionStreet';
import cityLabel from '@salesforce/label/c.InvestorsSelectionCity';
import accountNameLabel from '@salesforce/label/c.InvestorsSelectionAccountName';
import firstnameLabel from '@salesforce/label/c.InvestorsSelectionFirstname';
import lastnameLabel from '@salesforce/label/c.InvestorsSelectionLastname';
import emailLabel from '@salesforce/label/c.InvestorsSelectionEmail';

const KEY_FIELD = 'Id';
const SORTED_BY = 'investorListName';

const INVESTOR_LIST_ITEMS_TABLE_COLUMNS = [
    { label: ' ', fieldName: 'Id', type: 'checkboxContainer', cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: {disabledandchecked: {fieldName : 'disabledandchecked'}, tableName: 'listTabInvestorContact'}, fixedWidth: 45, sortable: true },
    { label: accountNameLabel, fieldName: 'linkToAccount',  type: 'url', typeAttributes: { label: {fieldName: 'InvestorName'}, target:'_blank'}, sortable: true },
    { label: firstnameLabel, fieldName: 'linkToContactForName',  type: 'url',typeAttributes: { label: {fieldName: 'FirstName'}, target:'_blank'}, sortable: true },
    { label: lastnameLabel, fieldName: 'linkToContactForSurname',  type: 'url',typeAttributes: { label: {fieldName: 'LastName'}, target:'_blank'}, sortable: true },
    { label: representativeLabel, fieldName: 'isRepresentative',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
    { label: streetLabel, fieldName: 'accountStreet',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
    { label: cityLabel, fieldName: 'accountCity',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
    { label: emailLabel, fieldName: 'Email',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
    { label: brandLabel, fieldName: 'linkToBrand',  type: 'url',typeAttributes: { label: {fieldName: 'accountBrandName'}, target:'_blank'}, sortable: true },
    { label: '', type: 'actionButtonRow', fieldName: 'Id', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
];

const INVESTOR_LIST_ITEMS_WITHOUT_CONTACTS_TABLE_COLUMNS = [
    { label: ' ', fieldName: 'accountId', type: 'checkboxContainer', cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: {disabledandchecked: {fieldName : 'disabledandchecked'}, tableName: 'listTabInvestor'}, fixedWidth: 45, sortable: true },
    { label: accountNameLabel, fieldName: 'linkToAccount',  type: 'url', typeAttributes: { label: {fieldName: 'accountName'}, target:'_blank'}, sortable: true },
    { label: streetLabel, fieldName: 'accountStreet',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
    { label: cityLabel, fieldName: 'accountCity',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
    { label: brandLabel, fieldName: 'accountBrandLink',  type: 'url',typeAttributes: { label: {fieldName: 'accountBrandName'}, target:'_blank'}, sortable: true },
    { label: '', type: 'actionButtonRow', fieldName: 'accountId', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
];


const ACCOUNT_FORM_FIELDS = ['Name', 'Brand__c', 'BillingStreet', 'BillingCity'];
const CONTACT_FORM_FIELDS = ['AccountId', 'Title', 'FirstName', 'LastName', 'Position__c', 'Email'];

export default class investorSelectionInvestorList extends LightningElement
{
    @api investorList;
    @api investorListItems;
    @api investorListItemsWithoutContacts;
    opportunityId = new URL(window.location.href).searchParams.get('c__recordId');

    @track popoverShown;

    accountFormFields = ACCOUNT_FORM_FIELDS;
    contactFormFields = CONTACT_FORM_FIELDS;

    label = {
        investorsSelectionItemAmountLabel,
        investorsSelectionListLastEditedOnLabel,
        investorsSelectionListLastEditedByLabel,
        investorsSelectionListCreatedOnLabel,
        investorsSelectionListOwnerLabel
    };

    columnsInvestorListItems = INVESTOR_LIST_ITEMS_TABLE_COLUMNS;
    columnsInvestorListItemsWithout = INVESTOR_LIST_ITEMS_WITHOUT_CONTACTS_TABLE_COLUMNS;
    keyField = KEY_FIELD;
    sortedBy = SORTED_BY;

    @wire(CurrentPageReference) pageRef;

    @track
    top;

    @track
    left;
    handleOver(event)
    {
        this.popoverShown = !this.popoverShown;

        let top = event.clientY;     // Get the horizontal coordinate
        let left = event.clientX;     // Get the vertical coordinate
        this.top = top;
        this.left = left + 25;
    }

    get positionStyle() {
        return `top: ${this.top}px; left:${this.left}px`;
    }

    investorsListItemLimitSize = 25;

    loadInvestorListItems()
    {
        const object = {
            tableMetadata: {
                columnsInvestorListItems: this.columnsInvestorListItems,
                columnsInvestorListItemsWithout: this.columnsInvestorListItemsWithout,
                keyFieldInvestorList: this.keyField,
                sortedByInvestorList: this.sortedBy,

            },
            formMetadata: {
                fields: {
                    account: this.accountFormFields,
                    contact: this.contactFormFields
                },
                columns: 2
            },
            investorListItems: [],
            investorListItemsWithoutContacts: [],
            investorListName: this.investorList.name,
            investorListId: this.investorList.investorListId,
            isLoading: true
        }

        //Notify investorSelectionDatatable.js to search for records.
        fireEvent(this.pageRef, 'InvestorSelection__showInvestorListItem', object);

        loadInvestorsListItems({
            opportunityId: this.opportunityId,
            investorListId: this.investorList.investorListId,
            limitSize: this.investorsListItemLimitSize
        })
        .then( result => {
            console.log(result);
            let tempResult = JSON.parse(result);
            object.investorListItems = JSON.stringify(tempResult.investorListItemsWithContacts);
            object.investorListItemsWithoutContacts = JSON.stringify(tempResult.investorListItemsWithoutContacts);
            object.isLoading = false;

            fireEvent(this.pageRef, 'InvestorSelection__showInvestorListItem', object);
        }).catch(error => {
            console.log('error: ', error);
            object.isLoading = false;
        });
    }
}