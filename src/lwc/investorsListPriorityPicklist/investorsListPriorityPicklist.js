import { LightningElement, track, api, wire } from 'lwc';
import { registerListener, fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';

export default class InvestorsListPriorityPicklist extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @api rowId;
    @api priority;
    @track selectedA;
    @track selectedB;
    @track selectedC;
    // @track selectedZ;

    connectedCallback(){
        if(this.priority === 'A') this.selectedA = 'selected';
        if(this.priority === 'B') this.selectedB = 'selected';
        if(this.priority === 'C') this.selectedC = 'selected';
        registerListener('investorsListPriorityPicklist_refresh', this.refreshSelectedValue, this);
    }


    handlePriorityChange(event){
        let data = {}
        data.id = this.rowId;
        data.priority = event.target.value;
        fireEvent(this.pageRef, 'investorsListPriorityPicklist_priorityChange', data);
    }

    refreshSelectedValue(data){
        for(var i = 0; i < data.length; i++){
            if(this.rowId === data[i].Id){
                this.selectedA = '';
                this.selectedB = '';
                this.selectedC = '';

                if(data[i].Priority__c === 'A') this.selectedA = 'selected';
                if(data[i].Priority__c === 'B') this.selectedB = 'selected';
                if(data[i].Priority__c === 'C') this.selectedC = 'selected';
            }
        }
    }
}