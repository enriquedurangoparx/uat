import { LightningElement, api, track } from 'lwc';

export default class FilterElementTreeView extends LightningElement {
    @api sectionLabel;
    @api picklistValues;
    @track items = [];

    treeIsRendered = false;

    renderedCallback() {
        if (this.picklistValues && this.picklistValues.data && !this.treeIsRendered) {
            let picklistItems = [];
            for (const picklistValue of this.picklistValues.data.values) {
                picklistItems.push({
                    label: picklistValue.label,
                    name: picklistValue.value,
                    disabled: false,
                    expanded: true
                });
            }
            this.items = picklistItems;
            this.treeIsRendered = true;
        }
    }

    handleOnselect(event) {
        console.log("FilterElementTreeView.handleOnselect", event.detail.name);
    }
}