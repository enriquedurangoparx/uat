/**
 * Created by npo.
 * US1910
 */
import { LightningElement, api, track } from 'lwc';
import getInvestorContacts from '@salesforce/apex/InvestorsListController.getInvestorContacts';
import { NavigationMixin } from 'lightning/navigation';
import confirmLabel from '@salesforce/label/c.GeneralLabelConfirm';
import cancelLabel from '@salesforce/label/c.investorsListCancelLabel';
import investorsWillBeExcludedLabel from '@salesforce/label/c.InvestorsWillBeExcluded';
import investorsListNewMailingModalProceed from '@salesforce/label/c.InvestorsListNewMailingModalProceed';
import newMailing from '@salesforce/label/c.InvestorsListNewMailing';
import close from '@salesforce/label/c.InvestorsListCloseLabel';




const CSS_CLASS = 'modal-hidden';

export default class InvestorsListNewMailingModal extends NavigationMixin(LightningElement)  {

	label = {
		confirmLabel,
		cancelLabel,
		investorsWillBeExcludedLabel,
		investorsListNewMailingModalProceed,
		newMailing,
		close
	}
	@track showModal = false;
	@api
	newMailingModalContent;
	@api
	newMailingModalHeader;
	@api
	newMailingModalWarning;

	@api
	selectedInvestors;
	@api
	records;

	@api
	opportunityId;

	@api
	campaignId;

	@track
	confirmButtonDisabled;

	@track
	investorsWithContacts;

	@api show() {
		this.newMailingModalContent = this.label.investorsListNewMailingModalProceed;
		getInvestorContacts({ recordsIds: this.selectedInvestors })
		.then(result => {
			let investorsContacts = result;
			let investorsWithAndWithoutContacts = this.calculateInvestorsWithoutContacts(this.selectedInvestors, investorsContacts);
			let investorsWithNoContacts = investorsWithAndWithoutContacts.investorsWithNoContacts;
			console.log('...investorsWithNoContacts ' + investorsWithNoContacts);
			this.investorsWithContacts = investorsWithAndWithoutContacts.investorsWithContacts;
			this.confirmButtonDisabled = this.investorsWithContacts.length < 1;
			this.constructWarningMessage(investorsWithNoContacts, this.records);
			this.showModal = true;
		})
		.catch(error => {
			console.log('handleNewMailingClick: failed.' + error);
		});
	}

	@api hide() {
		this.showModal = false;
	}

	handleDialogClose() {
		//Let parent know that dialog is closed (mainly by that cross button) so it can set proper variables if needed
		const closedialog = new CustomEvent('closedialog');
		this.dispatchEvent(closedialog);
		this.hide();
	}

	handleCancelNewMailingModal()
	{
		this.hide();
	}

	handleConfirmNewMailingModal()
	{
		let state = {
			c__investors: this.investorsWithContacts,
			c__opportunityId: this.opportunityId
		};
	    if (this.campaignId != undefined) {
	        state.c__campaignId = this.campaignId;
     	}
		this[NavigationMixin.Navigate]({
			type: 'standard__navItemPage',
			attributes: {
				apiName: 'NewMailing'
			},
			state: state
		});
	}

	constructWarningMessage(investorsWithNoContacts, records)
	{
		if (investorsWithNoContacts.length > 0)
		{
			let newMailingModalWarning = this.label.investorsWillBeExcludedLabel;
			let names = '';
			investorsWithNoContacts.forEach(function(item){
				let foundRecord = records.find(function(record){
					return record.Id === item;
				});
				if (foundRecord !== undefined)
				{
					if (names.length > 0)
					{
						names += ', ';
					}
					names += foundRecord.Name;
				}
				console.log('... names:' + names);
			});
			this.newMailingModalWarning = newMailingModalWarning + ' ' + names;
		}
		else
		{
			this.newMailingModalWarning = undefined;
		}
	}

	calculateInvestorsWithoutContacts(investors, investorsContacts)
	{
		let investorsContactsMap = new Map();
		investorsContacts.forEach(function(cont){
			let listOfContacts = investorsContactsMap.has(cont.Investor__c) ? investorsContactsMap.get(cont.Investor__c) : [];
			listOfContacts.push(cont);
			investorsContactsMap.set(cont.Investor__c, listOfContacts);
		});

		let investorsWithNoContacts = [];
		let investorsWithContacts = [];
		investors.forEach(function(investorId){
			if (!investorsContactsMap.has(investorId) || investorsContactsMap.get(investorId).length < 1)
			{
				investorsWithNoContacts.push(investorId);
			}
			else
			{
				investorsWithContacts.push(investorId);
			}
		});
		return {investorsWithNoContacts : investorsWithNoContacts, investorsWithContacts : investorsWithContacts};
	}
}