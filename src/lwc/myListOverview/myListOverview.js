import { LightningElement, track, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import getInvestors from '@salesforce/apex/MyListController.getInvestors';
import getUsersAndGroups from '@salesforce/apex/MyListController.searchForGroupOrUser';
import { deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';

// custom labels
import title from '@salesforce/label/c.MyListOverviewTitle';
import newButtonLabel from '@salesforce/label/c.MyListOverviewNewButtonLabel';
import myOwnLists from '@salesforce/label/c.MyListOverviewMyOwnLists';
import allLists from '@salesforce/label/c.MyListOverviewAllLists';
import description from '@salesforce/label/c.MyListOverviewDescription';
import searchPlaceholder from '@salesforce/label/c.MyListOverviewSearchPlaceholder';
import listName from '@salesforce/label/c.MyListOverviewListName';
import numContacts from '@salesforce/label/c.MyListOverviewNumContacts';
import lastEdited from '@salesforce/label/c.MyListOverviewLastEdited';
import lastEditedBy from '@salesforce/label/c.MyListOverviewLastEditedBy';
import createdDate from '@salesforce/label/c.MyListOverviewCreatedDate';
import owner from '@salesforce/label/c.MyListOverviewOwner';
import editAction from '@salesforce/label/c.MyListOverviewEditAction';
import cloneAction from '@salesforce/label/c.MyListOverviewCloneAction';
import deleteAction from '@salesforce/label/c.MyListOverviewDeleteAction';
import closeLabel from '@salesforce/label/c.MyListOverviewCloseLabel';
import deleteModalHeader from '@salesforce/label/c.MyListOverviewDeleteModalHeader';
import deleteModalText from '@salesforce/label/c.MyListOverviewDeleteModalText';
import cancelLabel from '@salesforce/label/c.MyListOverviewCancelLabel';
import deleteLabel from '@salesforce/label/c.MyListOverviewDeleteLabel';
import deletedToastMessage from '@salesforce/label/c.MyListOverviewDeletedToastMessage';
import editToastMessage from '@salesforce/label/c.MyListOverviewEditToastMessage';

/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 300;


const columns = [
    {label: listName,fieldName: 'url',type: 'url',typeAttributes: { label: { fieldName: 'name' } }},
    { label: numContacts, fieldName: 'numberOfContacts', type: 'number' },
    { label: description, fieldName: 'description', type: 'text' },
    {
        label: lastEdited,
        fieldName: 'lastModifiedDate',
        type: 'date',
        typeAttributes: {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit'
        }
    },
    { label: lastEditedBy, fieldName: 'lastModifiedBy', type: 'text' },
    {
        label: createdDate,
        fieldName: 'createdDate',
        type: 'date',
        typeAttributes: {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit'
        }
    },
    { label: owner, fieldName: 'owner', type: 'text' },
    {
        label: '',
        type: 'button-icon',
        initialWidth: 16,
        typeAttributes: {
            iconName: 'utility:edit',
            title: editAction,
            variant: 'bare',
            name: 'edit',
            disabled: { fieldName: 'allowEdit' }
        }
    },
    {
        label: '',
        type: 'button-icon',
        initialWidth: 16,
        typeAttributes: {
            iconName: 'utility:copy',
            title: cloneAction,
            name: 'clone',
            variant: 'bare'
        }
    },
    {
        label: '',
        type: 'button-icon',
        initialWidth: 16,
        typeAttributes: {
            iconName: 'utility:delete',
            title: deleteAction,
            name: 'delete',
            variant: 'bare',
            disabled: { fieldName: 'allowDelete' }
        }
    }
];

export default class MyListOverview extends LightningElement {
    wiredInvestors;
    @track showListSelection = true;
    @track selectedOption = 'myOwnList';
    @track options;
    @track searchTerm = '';

    @track data;
    @track columns = columns;

    // edit / create list
    @track showEditModal = false;
    @track showDeleteModal = false;
    
    componentInfo = {
        isEditMode : false
    };


    currentList;
    usersOrGroups = [];

    connectedCallback() {
        this.options = [
            { label: this.label.myOwnLists, value: 'myOwnList' },
            { label: this.label.allLists, value: 'allLists' }
        ];

        this.initUsersAndGroups();
    }

    label = {
        title: title,
        newButtonLabel: newButtonLabel,
        myOwnLists: myOwnLists,
        allLists: allLists,
        searchPlaceholder: searchPlaceholder,
        close: closeLabel,
        deleteModalHeader: deleteModalHeader,
        deleteModalText: deleteModalText,
        cancel: cancelLabel,
        delete: deleteLabel,
        deletedToastMessage: deletedToastMessage,
        editToastMessage: editToastMessage,
        description : description
    };

    @track objUser = {};
    // Get current User
    @wire(getRecord, { recordId: USER_ID, fields: ['User.Id', 'User.ProfileId', 'User.Profile.Name'] })
    userData({error, data}) {
        if(data) {
            let objCurrentData = data.fields;
            this.objUser = {
                Id : objCurrentData.Id.value,
                ProfileId : objCurrentData.ProfileId.value,
                ProfileName : objCurrentData.Profile.displayValue
            }
        }
        else if(error) {
            window.console.log('error ====> '+JSON.stringify(error))
        }
    }


    initUsersAndGroups(){
        getUsersAndGroups()
        .then(result => {
            for(var i = 0; i < result.length; i++){
                this.usersOrGroups.push({ key: result[i].Id, value: result[i].Name });
            }
        });

    }


    @wire(getInvestors, {
        searchTerm: '$searchTerm',
        filter: '$selectedOption'
    })
    getInvestors(wiredInvestors) {
        this.wiredInvestors = wiredInvestors;
        const { data, error } = wiredInvestors;

        if (data) {
            this.data = data;
        } else if (error) {
            // TODO: handle error
        }
    }

    /**
     *
     * @param {*} event
     *
     * @description
     */
    handleKeyChange(event) {
        // Debouncing this method: Do not update the reactive property as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
        window.clearTimeout(this.delayTimeout);
        const searchTerm = event.target.value;

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.searchTerm = searchTerm;
        }, DELAY);
    }

    newButtonHandler() {
        this.isClone = false;
        this.componentInfo.isEditMode = false;
        this.showEditModal = true;
    }

    handleSelectionChange(event) {
        this.selectedOption = event.detail.value;
    }

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        if (actionName === 'edit') {
            this.editRow(row);
        } else if (actionName === 'clone') {
            this.cloneRow(row);
        } else if (actionName === 'delete') {
            this.deleteRow(row);
        }
    }

    editRow(row) {
        this.isClone = false;
        this.componentInfo.isEditMode = true;
        this.currentList = row;
        this.showEditModal = true;

    }

    cloneRow(row) {
        this.isClone = true;
        this.currentList = row;
        this.showEditModal = true;
    }

    deleteRow(row) {
        this.idSelectedForDeletion = row.id;
        this.showDeleteModal = true;
    }

    idSelectedForDeletion;

    closeDeleteModal(){
        this.showDeleteModal = false;
        this.idSelectedForDeletion = undefined;
    }

    deleteItem(){
        deleteRecord(this.idSelectedForDeletion)
        .then(() => {
            refreshApex(this.wiredInvestors);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: this.label.deletedToastMessage,
                    variant: 'success'
                })
            );
            this.closeDeleteModal();
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error deleting record',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });
    }

    refreshList() {
        refreshApex(this.wiredInvestors);
        this.closeEditModal();
    }

    closeEditModal() {
        this.showEditModal = false;
        this.currentList = null;
    }
}