import { LightningElement, track, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import { CurrentPageReference } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import OPPORTUNITY_NAME_FIELD from '@salesforce/schema/Opportunity.Name';
import OPPORTUNITY_ACCOUNT_ID_FIELD from '@salesforce/schema/Opportunity.AccountId';
// controller methods
import findInvestors from '@salesforce/apex/InvestorsListController.findInvestors';
import setPinned from '@salesforce/apex/InvestorsListController.setPinnedOnOpportunity';

// import custom labels:

// constants for sorting by default
const DEFAULT_SORT_FIELD = 'Name';
const DEFAULT_SORT_DIRECTION = 'asc';

/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 300;

// Datatable column definition
// main datatable
const columns = [
    {
        label: '',
        type: 'pinButtonRow',
        fieldName: 'Id',
        fixedWidth: 36,
        typeAttributes: {
            isPinned: { fieldName: 'isPinned', accountId: 'accountId' }
        }
    },
    {
        label: 'Name',
        fieldName: 'accountLink',
        type: 'url',
        sortable: 'true',
        typeAttributes: { label: { fieldName: 'Name' } }
    },
    { label: 'Status', fieldName: 'Stage', type: 'text', sortable: 'true' },
    {
        label: 'Status seit',
        fieldName: 'statusSince',
        type: 'date',
        sortable: 'true'
    },
    {
        label: 'Letzte Aktivität',
        fieldName: 'lastActivityDate',
        type: 'date',
        sortable: 'true'
    },
    {
        label: 'Status seit',
        fieldName: 'lastBidDate',
        type: 'date',
        sortable: 'true'
    },
    {
        label: 'Absage',
        fieldName: 'declination',
        type: 'text',
        sortable: 'false'
    },
    {
        label: 'Action',
        type: 'actionButtonRow',
        fieldName: 'Id',
        fixedWidth: 100
    }
];

/**
 * Component for handling Investors on an overview
 *
 * @version 1.0.0
 * @author andreas.reichert@youperience.com
 */
export default class InvestorsListTestComponent extends LightningElement {
    // opportunity Id
    @track recordId;
    // data object for datatable
    @track columns = columns;
    // raw result of Investors data - do not use for datatable!
    @track data;
    @track error;
    // Table sorting
    @track sortBy = DEFAULT_SORT_FIELD;
    @track sortDirection = DEFAULT_SORT_DIRECTION;

    @track disableAllMassEditButtons = true;

    _records;
    // table data
    @track records;
    // Account search key
    @track searchKey = '';

    @track selectedRecordId;

    // opportunity Name for Header
    get name() {
        let returnValue;
        if (this.opportunity && this.opportunity.data) {
            returnValue = this.opportunity.data.fields.Name.value;
        } else {
            returnValue = null;
        }
        return returnValue;
    }

    get accountId() {
        let returnValue;
        if (this.opportunity && this.opportunity.data) {
            returnValue = this.opportunity.data.fields.AccountId.value;
        } else {
            returnValue = null;
        }
        return returnValue;
    }

    // array for semicolon separated investor ids
    pinnedInvestorIds;

    @wire(CurrentPageReference) pageRef;
    connectedCallback() {
        if (this.pageRef && this.pageRef.state.c__recordId) {
            this.recordId = this.pageRef.state.c__recordId;
        }
    }

    @wire(getRecord, {
        recordId: '$recordId',
        fields: [OPPORTUNITY_NAME_FIELD, OPPORTUNITY_ACCOUNT_ID_FIELD]
    })
    opportunity;

    // UI-Labels, lateron import from custom labels
    label = {
        searchPlaceholder: 'Suchen',
        investorsReport: 'Investorenbericht',
        statusUpdate: 'Status Update',
        rejectionUpdate: 'Absage Update',
        removeFromSelection: 'Aus Selektion Entfernen',
        pinToastTitle: 'Datensatz wurde fixiert',
        unpinToastTitle: 'Datensatzfixierung wurde gelöst',
        pinToastMessage: '',
        backToOpporunity: 'Zurück'
    };

    /**
     *
     * @param {*} result
     *
     * @description
     */
    @wire(findInvestors, { searchKey: '$searchKey', recordId: '$recordId' })
    findContacts(result) {
        if (result.data) {
            let resultArray = this.prepareData(result);
            this._records = resultArray;
            this.records = resultArray;
            // presort data
            this.sortData(this.sortBy, this.sortDirection);
            this.data = result;
            this.error = undefined;
        } else if (result.error) {
            this.error = result.error;
            this.records = undefined;
        }
    }

    /**
     *
     * @param {*} event
     *
     * @description
     */
    handleKeyChange(event) {
        // Debouncing this method: Do not update the reactive property as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.records = this._records.filter(function(el) {
                let sk = searchKey.toLowerCase();
                let accountNameMatch = el.Name.toLowerCase().indexOf(sk) > -1;
                if (accountNameMatch) {
                    return true;
                }
                for (let i = 0; i < el.contacts.length; i++) {
                    if (
                        el.contacts[i].firstName.toLowerCase().indexOf(sk) >
                            -1 ||
                        el.contacts[i].lastName.toLowerCase().indexOf(sk) > -1
                    ) {
                        return true;
                    }
                }
                return false;
            });
            this.sortData(this.sortBy, this.sortDirection);
        }, DELAY);
    }

    /**
     *
     * @param {*} result Investors data fetchied from InvestorsListController.getInvestorsList or InvestorsListController.findInvestors
     *
     * @description creates data object for datatable
     */
    prepareData(result) {
        let pinnedInvestorIds;
        let resultArray = [];
        // store data for refreshApex method
        this.data = result;
        for (let i = 0; i < result.data.length; i++) {
            // create data object for table row
            let dataObject = {};
            dataObject.Id = result.data[i].Id;
            dataObject.Name = result.data[i].Account__r.Name;
            dataObject.Stage = result.data[i].Stage__c;
            dataObject.lastStatus = result.data[i].LastStatus__c;
            dataObject.statusSince = result.data[i].LastStatusDate__c;
            dataObject.lastActivityDate = result.data[i].LastActivityDate__c;
            dataObject.lastBidDate = result.data[i].LastBidDate__c;
            dataObject.declination = result.data[i].Declination__c
                ? result.data[i].Declination__c
                : '-';
            // set the id's array
            if (!pinnedInvestorIds) {
                if (result.data[i].Opportunity__r.Pinned_Investors__c) {
                    pinnedInvestorIds =
                        result.data[i].Opportunity__r.Pinned_Investors__c;
                    this.pinnedInvestorIds = pinnedInvestorIds.split(';');
                } else {
                    this.pinnedInvestorIds = [];
                }
            }
            dataObject.contacts = [];
            if (result.data[i].InvestorContacts__r) {
                for (
                    let j = 0;
                    j < result.data[i].InvestorContacts__r.length;
                    j++
                ) {
                    let contact = {};
                    contact.firstName =
                        result.data[i].InvestorContacts__r[
                            j
                        ].Contact__r.FirstName;
                    contact.lastName =
                        result.data[i].InvestorContacts__r[
                            j
                        ].Contact__r.LastName;
                    contact.accountName =
                        result.data[i].InvestorContacts__r[
                            j
                        ].Contact__r.Account.Name;
                    dataObject.contacts.push(contact);
                }
            }
            // build hyperlink to Account record
            dataObject.accountLink =
                '/lightning/r/Account/' + result.data[i].Account__c + '/view';
            dataObject.accountId = result.data[i].Account__c;
            // create a "pinned" field if Id is contained in the pinned list
            dataObject.isPinned =
                this.pinnedInvestorIds.indexOf(result.data[i].Id) > -1;
            resultArray.push(dataObject);
        }
        return resultArray;
    }
    /**
     *
     * @param {*} event Datatable Sort Event
     *
     * @description event handler for sorting the header fields
     */
    handleSortData(event) {
        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    /**
     *
     * @param {*} fieldname Name of the field to be sorted
     * @param {*} direction "asc" or "desc"
     *
     * @description function to sort the array
     */
    sortData(fieldname, direction) {
        // field name
        this.sortBy = fieldname;

        // sort direction
        this.sortDirection = direction;
        // we need to remap fieldname because of that columns data is a hyperlink
        if (fieldname === 'accountLink') {
            fieldname = 'Name';
        }
        window.console.log('sortData', fieldname, direction);

        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.records));

        // Return the value stored in the field
        let keyValue = (a, b) => {
            // add a prefix depending on Sort Direction to keep pinned records on top
            if (a && a.Id && this.pinnedInvestorIds.indexOf(a.Id) > -1) {
                return b === 1
                    ? '0000000000' + a[fieldname]
                    : 'XXXXXXXXXX' + a[fieldname];
            }
            return a[fieldname];
        };

        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1 : -1;

        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x, isReverse) ? keyValue(x, isReverse) : ''; // handling null values
            y = keyValue(y, isReverse) ? keyValue(y, isReverse) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.records = parseData;
    }

    /**
     *
     * @param {*} event Custom event dispatched by the pin button (investorsListPinButton)
     *
     * @description stores the "pinned" value in a comma separated list
     */
    async handlePinRow(event) {
        const { rowId, isPinned } = event.detail;
        window.console.log(rowId, isPinned, event);
        const setPinnedResult = await setPinned({
            opportunityId: this.recordId,
            accountId: rowId,
            pin: !isPinned
        });
        let pinToastMsg = isPinned
            ? this.label.unpinToastTitle
            : this.label.pinToastTitle;
        this.dispatchToast(pinToastMsg, this.label.pinToastMessage, 'success');
        window.console.log('setPinnedResult:', setPinnedResult);
        // refresh data
        refreshApex(this.data);
    }

    /**
     *
     * @param {*} event Custom event dispatched by the activity button (investorsListActivityButton)
     *
     * @description opens the Activity Modal Window
     */
    handleRowAction(event) {
        const { rowId } = event.detail;
        const child = this.template.querySelector(
            'c-investors-list-edit-modal'
        );
        window.console.log(rowId, event);
        child.openModal(rowId);
    }

    /**
     *
     * @param {*} event Event thrown by datatable component if checkbox is selected or deselected
     *
     * @description Handles the selection of a single row (no selection or multiple selections are skipped)
     */
    handleRowSelection(event) {
        if (event.detail.selectedRows.length === 1) {
            let selectedId = event.detail.selectedRows[0].Id;
            this.selectedRecordId = selectedId;
            this.disableAllMassEditButtons = false;
        } else {
            this.selectedRecordId = undefined;
            this.disableAllMassEditButtons =
                event.detail.selectedRows.length === 0;
        }
    }

    /**
     * @description handles click on updateStatus button. Calls investorsListStateModal component
     */
    updateStatus() {
        //const pl = this.template.querySelector('select');
        const sm = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = sm.getSelectedRows();
        if (selectedRows && selectedRows.length > 0) {
            const child = this.template.querySelector(
                'c-investors-list-state-modal'
            );
            // removed preselection for presentation
            //child.openModal(selectedRows, pl.options[pl.selectedIndex].value, pl.options[pl.selectedIndex].label);
            child.openModal(selectedRows, '', '');
        }
    }

    /**
     * @description handles click on the updateRejection Button. Calls the investorsListDeclinationModal component
     */
    updateRejection() {
        const sm = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = sm.getSelectedRows();
        if (selectedRows && selectedRows.length > 0) {
            const child = this.template.querySelector(
                'c-investors-list-declination-modal'
            );

            child.openModal(selectedRows);
        }
    }

    /**
     * @description handles click on the deleteInvestor Button. Calls the investorsListDeleteModal component
     */
    removeFromSelection() {
        const dt = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = dt.getSelectedRows();
        if (selectedRows && selectedRows.length > 0) {
            const child = this.template.querySelector(
                'c-investors-list-delete-modal'
            );

            child.openModal(selectedRows);
        }
    }

    /**
     *
     * @param {*} event Customevent called by modal windows
     *
     * @description callback function for the modal windows to refresh the data in the datatable and show a toast message.
     */
    handleRefreshInvestors(event) {
        const { toastTitle, toastMessage, toastVariant } = event.detail;
        this.dispatchToast(toastTitle, toastMessage, toastVariant);
        refreshApex(this.data);
        // refresh sidepanel data in case of an update on status (this creates a new record on the Status History section)
        if (event.type === 'investorsupdated') {
            const sp = this.template.querySelector(
                'c-investors-list-side-panel'
            );
            sp.refresh();
        }
    }

    /**
     *
     * @param {*} title Title of the toast displayed
     * @param {*} message Message displayed in the toast
     * @param {*} variant Variant of the toast (info (default), success, warning, and error)
     */
    dispatchToast(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant
            })
        );
    }
}