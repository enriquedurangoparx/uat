import { LightningElement, api, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import getActivityList from '@salesforce/apex/InvestorActivitiesController.getActivityList';
import FORM_FACTOR from '@salesforce/client/formFactor';

import INVESTOR_ACTIVITIES from '@salesforce/label/c.InvestorActivities';
import LOADING_ERROR from '@salesforce/label/c.InvestorActivitiesLoadingError';
import ACTIVITIES_NOT_FOUND from '@salesforce/label/c.InvestorActivitiesNotFound';
import STATUS_LABEL from '@salesforce/label/c.InvestorsListStatusLabel';
import SUBJECT_LABEL from '@salesforce/label/c.InvestorsListTableRowSubject';
import ACTIVITY_LABEL from '@salesforce/label/c.InvestorsListTableRowActivity';
import DATE_LABEL from '@salesforce/label/c.InvestorsListActionFormDate';
import CONTACTS_LABEL from '@salesforce/label/c.InvestorsListActionFormContacts';
import VIEW_ALL_LABEL from '@salesforce/label/c.RelatedListViewAll';

const RECORDS_TO_SHOW = 5;

export default class InvestorActivities extends LightningElement {
    @api recordId;
    @api objectApiName;
    @track activities;
    @track error;
    @track hasNoActivities = false;
    @track isFullPage;
    @track showViewAll = false;
    @wire(CurrentPageReference) pageRef;

    labels = {
        investorActivities: INVESTOR_ACTIVITIES,
        loadingError: LOADING_ERROR,
        noActivitiesFound: ACTIVITIES_NOT_FOUND,
        status: STATUS_LABEL,
        subject: SUBJECT_LABEL,
        activity: ACTIVITY_LABEL,
        date: DATE_LABEL,
        contacts: CONTACTS_LABEL,
        viewAll: VIEW_ALL_LABEL
    };
    
    connectedCallback() {
        this.isFullPage = this.recordId === undefined;
        this.recordId = this.recordId ? this.recordId : new URL(window.location.href).searchParams.get('c__recordId');
        this.objectApiName = this.objectApiName ? this.objectApiName : new URL(window.location.href).searchParams.get('c__objectApiName');
    }

    get isDesktopDevice() {
        return FORM_FACTOR === 'Large';
    }

    get viewAllLink() {
        return '/lightning/n/InvestorActivities?c__recordId=' + this.recordId + '&c__objectApiName=' + this.objectApiName;
    }

    @wire(getActivityList, { recordId: '$recordId', objectApiName: '$objectApiName' })
    getActivityList({ error, data }) {
        if (data) {
            let activityUrl;
            this.activities = data.map(row => {
                let contactList = [];
                if (row['TaskWhoRelations']) {
                    for (let i = 0; i < row['TaskWhoRelations'].length; i++) {
                        try {
                            contactList.push({
                                'name': row['TaskWhoRelations'][i]['Relation']['Name'],
                                'url': '/' + row['TaskWhoRelations'][i]['Relation']['Id']
                            });
                        } catch (e) {
                            console.error("InvestorActivities:error", e);
                        }
                    }
                }
                activityUrl = '/' + row.Id;
                return { ...row, activityUrl, contactList };
            });

            this.showViewAll = !this.isFullPage && this.activities.length > RECORDS_TO_SHOW;
            this.activities = this.isFullPage ? this.activities : this.activities.slice(0, RECORDS_TO_SHOW);
            this.hasNoActivities = this.activities.length === 0;
            this.error = null;
        } else if (error) {
            this.error = error;
            this.activities = null;
        }
    };

    get columns() {
        return [
            { label: this.labels.subject, fieldName: 'activityUrl', type: 'url', typeAttributes: { label: {fieldName: 'Subject'}, tooltip: { fieldName: 'Subject' }}, target:'_blank'},
            { label: this.labels.date, fieldName: 'ActivityDate', type: 'date', typeAttributes:{ year: "numeric", month: "2-digit", day: "2-digit" }},
            { label: this.labels.contacts, fieldName: 'contactList', type: 'investorActivityContacts'},
        ];
    }
}