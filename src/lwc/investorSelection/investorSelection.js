import { LightningElement, track, wire } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import ACQUISITION_OBJECT from '@salesforce/schema/AcquisitionProfile__c';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';
import searchMethod from '@salesforce/apex/GenericSearch.search';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import NAME_FIELD from '@salesforce/schema/Opportunity.Name';

// import custom labels:
import back from '@salesforce/label/c.InvestorsListBackLinkLabel';
import addSelection from '@salesforce/label/c.AddSelectionLabel';
import toInvestorsOverview from '@salesforce/label/c.ToInvestorsOverviewLabel';
import centerHelpMessage from '@salesforce/label/c.InvestorSelectionCenterHelpMessage';
import rightHelpMessage from '@salesforce/label/c.InvestorSelectionRightHelpMessage';
import opportunity from '@salesforce/label/c.InvestorsSelectionOpportunity';
import contact from '@salesforce/label/c.InvestorsSelectionContact';
import account from '@salesforce/label/c.InvestorsSelectionAccount';
import search from '@salesforce/label/c.InvestorsSelecetionSearch';
import matching from '@salesforce/label/c.InvestorsSelectionMatching';
import underbidder from '@salesforce/label/c.InvestorsSelectionUnderbidder';
import mylists from '@salesforce/label/c.InvestorsSelectionMyLists';
import myListItemHelpMessageLabel from '@salesforce/label/c.InvestorsSelectionMyListItemHelpMessage';
import myListItemNoRecordsLabel from '@salesforce/label/c.InvestorsSelectionMyListItemNoRecords';
import pleaseChooseListLabel from '@salesforce/label/c.PleaseChooseList';
import acquisitionProfileName from '@salesforce/label/c.investorSelectionAcquisitionProfileName';
import acquisitionProfileTypeOfRealEstate from '@salesforce/label/c.investorSelectionAcquisitionProfileTypeOfRealEstate';
import acquisitionProfileInvestorRepresentative from '@salesforce/label/c.investorSelectionAcquisitionProfileInvestorRepresentative';
import generalLabelNoRecordsFound from '@salesforce/label/c.GeneralLabelNoRecordsFound';



// constants for sorting by default
const DEFAULT_SORT_FIELD = 'Name';
const DEFAULT_SORT_DIRECTION = 'asc';

export default class InvestorSelection extends NavigationMixin(LightningElement) {
    @wire(CurrentPageReference) pageRef;

    @wire(getRecord, { recordId: '$recordId', fields: [NAME_FIELD] })
    opportunity;

    @track aquisitionProfileColumns = [
        {label: acquisitionProfileName, fieldName: 'acquisitionProfileId',  type: 'url', typeAttributes: { label: {fieldName: 'acquisitionProfileName'}, target:'_blank'}, sortable: true },
        {label: acquisitionProfileTypeOfRealEstate, fieldName: 'TypeOfRealEstate__c', type: 'text', sortable: true },
        {label: acquisitionProfileInvestorRepresentative, fieldName: 'investorRepresentativeId',  type: 'url', typeAttributes: { label: {fieldName: 'investorRepresentativeName'}, target:'_blank'}, sortable: true }
    ];

    @track acquisitionProfileObjectInfo;
    @track acquisitionProfilePluralNameLabel;
    @wire(getObjectInfo, { objectApiName: ACQUISITION_OBJECT })
    wiredObjectInfo({ error, data }) {
        if (data) {
            this.acquisitionProfileObjectInfo = data;
            this.acquisitionProfilePluralNameLabel = data.labelPlural;
        }
    }

    @track recordId;
    @track test;
    @track accountId;
    @track contactId;
    @track apObjectInfo;

    // tracking visibility variables
    @track buttonAddSelectionIsDisabled = true;
    @track centerHelpMessageIsShown = true;
    @track rightHelpMessageIsShown = true;
    @track buttonAddSelectionIsShown;

    @track showInformationForSearchTab = true;
    @track showInformationForMatchingTab = false;
    @track showInformationForMyListsTab = false;
    @track showInformationForBidderTab = false;
    @track myListsCenterSectionHasRecords = false;
    @track bidderListHasRecords = false;
    @track dataTableClass = "slds-hide";
    @track showAcquisitionProfilesRelatedList = false;

    @track tableColumns = [];
    @track investorSelectionListItems;
    @track investorSelectionListItemsWithoutContacts;

    // help / error message for center section
    @track centerHelpMessage;

    // Table sorting
    @track sortedBy = DEFAULT_SORT_FIELD;
    @track sortedDirection = DEFAULT_SORT_DIRECTION;

    // UI-Labels, lateron import from custom labels
    label = {
        opportunity,
        back,
        addSelection,
        toInvestorsOverview,
        centerHelpMessage,
        rightHelpMessage,
        contact,
        account,
        search,
        matching,
        underbidder,
        mylists,
        myListItemHelpMessageLabel,
        myListItemNoRecordsLabel,
        pleaseChooseListLabel,
        generalLabelNoRecordsFound
    }

    connectedCallback(){
        this.recordId = this.pageRef.state.c__recordId;
        registerListener('InvestorSelection__toggleTabInfo', this.toggleTabInformation, this);
        registerListener('InvestorSelection__showInvestorListItem', this.showInvestorListItemTable, this);//Is fired from "investorSelectionTab.js" if search has changed.
        registerListener('InvestorSelection__handleNoRowIsSelectedHandler', this.handleNoRowIsSelectedHandler, this);//Is fired from "investorSelectionTab.js" if search is resetted.
        registerListener('InvestorSelection__toggleRightSideHelpMessage', this.toggleRightSideHelpMessage, this);
        // registerListener('InvestorSelection__bidderSectionControll', this.handleBidderSectionDisplay, this);

        registerListener('payloadrender', this.handlePayloadRender, this);
    }

    /**
     * Is used to show / hide additional information for tabs
     */
    toggleTabInformation(event){
        this.showAcquisitionProfilesRelatedList = false;

        this.showInformationForSearchTab = event === this.label.search ? true : false;
        this.showInformationForMatchingTab = event === this.label.matching ? true : false;
        this.showInformationForMyListsTab = event === this.label.mylists ? true : false;
        this.showInformationForBidderTab = event === this.label.underbidder ? true : false;

        // if (this.showInformationForMyListsTab){
        //     this.centerHelpMessage = this.label.myListItemHelpMessageLabel;
        // }

        try {
            if (!this.showInformationForBidderTab)
            {
                this.template.querySelector('[data-name=accountRecordForm]').handleReset();
                this.template.querySelector('[data-name=contactRecordForm]').handleReset();
            }
        } catch (e) {}
    }

    get showInformationForRightSidePanel(){
        return this.showInformationForSearchTab || this.showInformationForMatchingTab || this.showInformationForMyListsTab || this.showInformationForBidderTab ? true : false;
    }

    @track
    showRightSideHelpMessage = false;

    toggleRightSideHelpMessage(obj)
    {
        if (obj && !obj.displayMessage)
        {
            this.showRightSideHelpMessage = true;
        }
    }
    /**
     * Is used to show / hide center section for My List Tab
     */
    showInvestorListItemTable(event){
        this.tableColumns = event.tableMetadata.columns;
        this.investorSelectionListItems = JSON.parse(event.investorListItems);
        this.investorSelectionListItemsWithoutContacts = JSON.parse(event.investorListItemsWithoutContacts);
        this.myListsCenterSectionHasRecords = this.investorList && this.investorList.length > 0 && this.investorSelectionListItemsWithoutContacts && this.investorSelectionListItemsWithoutContacts.length > 0 ? true : false;
        this.dataTableClass = this.myListsCenterSectionHasRecords ? "" : "slds-hide";
        if (!this.myListsCenterSectionHasRecords)
        {
            this.centerHelpMessage = this.label.myListItemNoRecordsLabel;
        }
        this.centerHelpMessageIsShown = false;
    }

    handleDisplayOfBidderSection(event)
    {
        console.log('=== investor Selection showBidderSection starts');
        console.log(event);
        this.showInformationForBidderTab = true;
        if (event.hasRecordsAfterSearch || event.hasSelectedRecords)
        {
            this.centerHelpMessageIsShown = false;
        }
        else if (!event.hasRecordsAfterSearch && !event.hasSelectedRecords)
        {
            this.centerHelpMessageIsShown = true;
            // this.centerHelpMessage = this.label.generalLabelNoRecordsFound;
        }

        // this.tableColumns = event.tableMetadata.columns;
        // let bidderRecords = JSON.parse(event.bidderRecords);
        // this.bidderListHasRecords = bidderRecords && bidderRecords.length > 0 ? true : false;
        // console.log('this.bidderListHasRecords: ', this.bidderListHasRecords);
        // if (!this.bidderListHasRecords)
        // {
        //     this.centerHelpMessage = this.label.generalLabelNoRecordsFound;
        // }
        // this.centerHelpMessageIsShown = false;
        console.log('=== investor Selection showBidderSection ends');
    }

    disconnectedCallback() {
		unregisterAllListeners(this);
    }

    get name() {
        return getFieldValue(this.opportunity.data, NAME_FIELD);
    }

    /**
     * If no row is selected, both record-forms should be reseted.
     */
    handleNoRowIsSelectedHandler(){
        this.toggleAddSelectionButtonAvailability(true);
        if (!this.showInformationForBidderTab) {
            this.template.querySelector('[data-name=accountRecordForm]').handleReset();
            this.template.querySelector('[data-name=contactRecordForm]').handleReset();
        }
        this.showAcquisitionProfilesRelatedList = false;
    }

    handleShowAccount(event){
        this.accountId =  event.detail.accountId;
    }

    handleShowContact(event){
        this.contactId =  event.detail.contactId;
    }

    addSelectionOnClickHandler(event){
        fireEvent(this.pageRef, 'InvestorSelection__addSelection', null);
    }

    handleOneRecordSelected(event){
        this.toggleAddSelectionButtonAvailability(false);
        this.showRightSideHelpMessage = false;

        fireEvent(this.pageRef, 'InvestorSelection__showAccount', event.detail.account);
        fireEvent(this.pageRef, 'InvestorSelection__showContact', event.detail.contact);
        this.accountId = event.detail.account.detail.recordId;
        this.showAcquisitionProfilesRelatedList = true;
        this.getAcquisitionProfiles(event.detail.account.detail.recordId);
    }

    /**
     * Is triggered from child-lwc investorSelectionDatatable.js if datatable has records to show.
     */
    handleRecordsAvailable(event){
        this.toggleCenterHelpMessageShow(!event.detail.showInformation);
        this.toggleButtonAddSelectionShow(event.detail.showInformation);
    }

    toggleCenterHelpMessageShow(isShown){
        this.centerHelpMessageIsShown = isShown;
    }

    toggleAddSelectionButtonAvailability(isAvailable){
        this.buttonAddSelectionIsDisabled = isAvailable;
        this.rightHelpMessageIsShown = isAvailable;
    }

    toggleButtonAddSelectionShow(isShown){
        this.buttonAddSelectionIsShown = isShown;
    }

    navigateBackToOpportunityHandler(){
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: 'Opportunity',
                actionName: 'view'
            }
        });
    }

    /**
     * Is triggered if recordForm.js is rerendered to receive recordId to handle navigation to record page.
     */
    handleRecordFormChangeHandler(event){
        // let recordId = event.detail.recordId;
        // switch (event.detail.objectApiName) {
        //     case 'Account':
        //         this.accountId = recordId;
        //         break;
        //     case 'Contact':
        //         this.contactId = recordId;
        //         break;
        //     default:
        //         break;
        // }
    }

    handleNavigateToAccountRecord() {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.accountId,
                objectApiName: 'Account',
                actionName: 'view'
            }
        }).then(url => {
            window.open(url);
        });
    }

    handleNavigateToContactRecord() {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.contactId,
                objectApiName: 'Contact',
                actionName: 'view'
            }
        }).then(url => {
            window.open(url);
        });
    }

    @track acquisitionProfileData;
    getAcquisitionProfiles(selectedRecordId){
        searchMethod({
            list_fieldApiName: ['Id', 'Name', 'toLabel(TypeOfRealEstate__c)', 'InvestorRepresentative__c', 'InvestorRepresentative__r.Name', 'toLabel(TypeOfUse__c)', 'InvestmentVolumeFrom__c', 'InvestmentVolumeTo__c', 'OccupancyRate__c', 'MacroLocation__c', 'MicroLocation__c', 'RiskCategory__c', 'WALTFrom__c', 'IRRFrom__c', 'CoCFrom__c', 'Factor__c', 'Comments__c'],
            string_objectApiName: 'AcquisitionProfile__c',
            string_searchTerm: selectedRecordId,
            string_whereApiName: 'Account__c',
            string_filter : '',
            string_soqlOperator: '=',
            string_orderBy: 'Name',
            integer_limit: 100,
            integer_offset: 0
        })
        .then(result => {
            const tempResult = JSON.parse(JSON.stringify(result));
            let _acquisitionProfileyData = this._preprocessAcquisitionProfileData(tempResult);
            this.acquisitionProfileData = JSON.parse(JSON.stringify(_acquisitionProfileyData));

        }).catch(error => {
            console.error(JSON.stringify(error, null, '\t'));
        })
    }

    _preprocessAcquisitionProfileData(result){
        let preprocessData = [];
        result.forEach(element => {
            element.acquisitionProfileId = '/' + element.Id;
            element.acquisitionProfileName = element.Name;

            if(typeof element.InvestorRepresentative__c !== 'undefined'){
                element.investorRepresentativeId = '/' + element.InvestorRepresentative__c;
                element.investorRepresentativeName = element.InvestorRepresentative__r.Name;
            }

            preprocessData.push(element);
        });

       return preprocessData;
    }

    /**
     * @param {*} event Datatable Sort Event
     * @description event handler for sorting the header fields
     */
    updateColumnSorting(event) {
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;

        this.sortData(this.sortedBy, this.sortedDirection);
    }

    /**
     *
     * @param {*} fieldname Name of the field to be sorted
     * @param {*} direction "asc" or "desc"
     * @param {*} isContactInvestorTable if sorting comes from contact table then true of false in other case
     * @description function to sort the array
     */
    sortData(fieldname, direction){
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.investorSelectionListItems));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };
        let isReverse = direction === 'asc' ? 1 : -1;

        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            return isReverse * ((x > y) - (y > x));
        });
        this.investorSelectionListItems = parseData;
    }

    @track currentPayloadRequestId;
    @track currentRecord;
    handlePayloadRender(event) {

        var recordId = event.recordId;
        var recordNumber = recordId.split('-')[1];
        var shouldOpen = event.open;
        var top = event.top;
        var left = event.left;



        //render Request JSON data as table
        if ( shouldOpen && !this.currentPayloadRequestId && recordId)
        {
            this.currentPayloadRequestId = recordId;
            this.currentPayloadTop = top-200;
            this.currentPayloadLeft = left + 20;
            this.currentRecord = this.acquisitionProfileData[recordNumber];
        }
        else if ( !shouldOpen && this.currentPayloadRequestId && recordId)
        {
            this.currentPayloadRequestId = undefined;
            this.currentPayloadTop = 0;
            this.currentPayloadLeft = 0;
        }
    }
}