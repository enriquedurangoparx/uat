import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import acquisitionProfiles from '@salesforce/label/c.MyListRecordDetailComponentAcquisitionProfiles';
import acquisitionProfile from '@salesforce/label/c.MyListRecordDetailComponentAcquisitionProfiles';
import propertyType from '@salesforce/label/c.MyListRecordDetailComponentPropertyType';
import brand from '@salesforce/label/c.MyListRecordDetailComponentBrand';
import street from '@salesforce/label/c.MyListRecordDetailComponentStreet';
import city from '@salesforce/label/c.MyListRecordDetailComponentCity';
import email from '@salesforce/label/c.MyListRecordDetailComponentEmail';
import position from '@salesforce/label/c.MyListRecordDetailComponentPosition';

import contact from '@salesforce/label/c.InvestorsSelectionContact';
import account from '@salesforce/label/c.InvestorsSelectionAccount';

const AP_COLUMNS = [
    { label: acquisitionProfile, fieldName: 'name', type: 'text' },
    { label: propertyType, fieldName: 'type', type: 'text' },
];

export default class MyListDetailRecorddetailComponent extends NavigationMixin(LightningElement) {
    @track
    detailsObject;

    @track apColumns = AP_COLUMNS;

    @track
    dataReceived = false;

    @track
    acquisitionProfiles;
    
    accountFormFields = ['Name', 'Brand__c', 'BillingStreet', 'BillingCity'];
    contactFormFields = ['Title', 'FirstName', 'LastName', 'Position__c', 'Email'];
    formColumns = 2;

    label = {
        acquisitionProfiles,
        brand: brand,
        street: street,
        city: city,
        email: email,
        position: position,
        contact,
        account
    }

    @api
    setDetailsObject(value)
    {
        if (value)
        {
            setTimeout(() => {
                // do that to prevent the error: Cannot read property 'apiName' of undefined
                this.dataReceived = true;
                this.detailsObject = value;
            }, 100);
            
        }
        this.dataReceived = false;
    }

    @api
    setAcquisitionProfiles(value)
    {
        if (value)
        {
            this.acquisitionProfiles = value;
        }
    }

    get showDetails()
    {
        return this.detailsObject ? true : false;
    }

    get showAccountDetails()
    {
        return this.detailsObject && this.detailsObject.accountId ? true : false;
    }

    get showContactDetails()
    {
        return this.detailsObject && this.detailsObject.hasContact ? true : false;
    }

    handleNavigateToRecord(event) {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: event.target.dataset.name === 'Account' ? this.detailsObject.accountId : this.detailsObject.contactId, 
                objectApiName: event.target.dataset.name,
                actionName: 'view'
            }
        }).then(url => {
            window.open(url);
        });
    }
}