import { LightningElement, api, track, wire } from 'lwc';
import { fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import updateInvestorsCall from '@salesforce/apex/InvestorsListController.updateInvestorsPriority';
import PRIORITY_FIELD from '@salesforce/schema/AccountToOpportunity__c.Priority__c';

import priorityLabel from '@salesforce/label/c.InvestorsListPriorityLabel';
import prioUpdate from '@salesforce/label/c.InvestorsListPriorityUpdate';
import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';
import update from '@salesforce/label/c.investorsListUpdateLabel';
import toastTitle from '@salesforce/label/c.InvestorsListPriorityModalToastTitle';

export default class InvestorsListPriorityModal extends LightningElement {
    @wire(CurrentPageReference) pageRef;

    label = {
        close,
        cancel,
        update,
        prioUpdate,
        toastTitle,
        toastMessage: "",
        priorityLabel
    };

    @track options;
    @track value = '';

    @track showModal = false;
    @api recordsToUpdate;
    @track saveButtonDisabled = true;

    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: PRIORITY_FIELD })
    picklistValues;

    handleChange(event){
        this.value = event.target.value;
    }

    @api
    openModal(recordsToUpdate) {
        this.recordsToUpdate = recordsToUpdate;
        this.showModal = true;
    }

    closeModal() {
        this.showModal = false;
    }

    async updateInvestors(){
        let investors = [];
        for(let i=0; i<this.recordsToUpdate.length; i++){
            investors.push({Id: this.recordsToUpdate[i].Id, Priority__c: this.value});
        }

        // call update function
        const updateInvestorsResult = await updateInvestorsCall({
            investors : investors
        });

        fireEvent(this.pageRef, 'investorsListPriorityPicklist_refresh', investors);

        const event = CustomEvent('investorsupdated', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                toastTitle: this.label.toastTitle,
                toastMessage: this.label.toastMessage,
                toastVariant: 'success'
            },
        });
        this.dispatchEvent(event);
        this.closeModal();
    }
}