import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo, getPicklistValues  } from 'lightning/uiObjectInfoApi';
import PROPERTY_OBJECT from '@salesforce/schema/PropertyObject__c';
import STATUS_FIELD from '@salesforce/schema/PropertyObject__c.Status__c';

import searchRelatedObjectsBySosl from '@salesforce/apex/LWCObjectHelper.searchRelatedObjectsBySosl';

import addToArea from '@salesforce/label/c.AddToArea';
import searchPlaceholder from '@salesforce/label/c.ArealSearch';
import searchDescription from '@salesforce/label/c.InvestorsSelectionSearchDescription';

export default class AreaRelatePropertyObjects extends LightningElement {
    @api recordId;
    
    @track objectInfo;

    @track data = [];
    @track searchTerm = '';
    @track statusPicklist;
    @track selectedRows = [];

    @track columns = [
        { label: 'Name', fieldName: 'Name' },
        { label: 'Street', fieldName: 'Street__c' },
        { label: 'Postalcode', fieldName: 'PostalCode__c' },
        { label: 'City', fieldName: 'City	__c' },
        { label: 'Status', fieldName: 'Status__c' },
    ];

    label = {
        addToArea,
        searchPlaceholder,
        searchDescription
    };
    
    @wire(getObjectInfo, { objectApiName: PROPERTY_OBJECT })
    wiredObjectInfo({ error, data }) {

        if (data) {
            this.objectInfo = data;
            this.error = undefined;
        } else if (error) {
            this.error = error;
        }
    }

    @wire(getPicklistValues, { recordTypeId: '0120O000000dAY1QAM', fieldApiName: STATUS_FIELD })
    wiredPicklistValues({ error, data }) {

        if(data) {
            this.statusPicklist = data;
            this.setDatableColumns();
            this.error = undefined;
        } else if(error) {
            this.error = error;
        }
    }

    /*
    * Set the columns for datatable, based on the object metadata to prevent custom labels. 
    */
    setDatableColumns(){
        if(this.objectInfo){
            let tempColumns = [];

            this.columns.forEach(column => {
                column.label = this.objectInfo.fields[column.fieldName].label;
                tempColumns.push(column);
            });
            this.columns = tempColumns;
        }
        
    }

    /**
     * This is triggered when the user hits a key and is filtered by enter key.
     */
    keyUpHandler(event){
        this.searchTerm = this.template.querySelector('lightning-input').value;
        const isEnterKey = event.keyCode === 13;

        if (isEnterKey) {
            this.loadData();
        }
    }

    rowSelectionHandler(event){
        const selectedRows = event.detail.selectedRows;
        this.selectedRows = [];

        selectedRows.forEach(row => {
            this.selectedRows.push(row.Id);
        })
    }

    /*
    * Load records, based on the searchTermn with a sosl.
    */
    loadData(){
        searchRelatedObjectsBySosl({searchTerm: this.searchTerm})
            .then(result => {
                this.data = JSON.parse(JSON.stringify(result[0]));

                this.convertPicklistValue();
            })
            .catch(error => {
                this.error = error;
            });
    }

    /**
     * Handle translation for picklist value.
     */
    convertPicklistValue(){
        let tempData = [];
        this.data.forEach(record => {

            if(typeof record.Status__c !== 'undefined'){
                const picklistApiName = record.Status__c;

                this.statusPicklist.values.forEach(picklist => {
                    if(picklistApiName === picklist.value){
                        record.Status__c = picklist.label;
                    }
                })
            }

            tempData.push(record);
        });

        this.data = tempData;
    }
}