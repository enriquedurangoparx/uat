import { LightningElement, api, wire, track } from 'lwc';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import getPropertyObjectWrapper from '@salesforce/apex/LWCObjectMatchingController.getPropertyObjectWrapper';


export default class InvestorSelectionTabMatching extends LightningElement {
    recordId = new URL(window.location.href).searchParams.get('c__recordId');
    hasRendered = false;
    @track isPropertyObjectLoaded = false;
    @track propertyObject = {};
    @track propertyToAcquisitionProfileMatchingMap;

    @wire(getPropertyObjectWrapper, {opportunityId: '$recordId'})
    getPropertyObjectWrapper({ error, data }) {
        if (data) {
            this.propertyObject = data["opportunity"]["PrimaryProperty__r"];
            this.propertyToAcquisitionProfileMatchingMap = data["objToProfileFieldsMatching"];
            this.isPropertyObjectLoaded = true;
        } else if (error) {
            console.error("InvestorSelectionTabMatching:getPropertyObjectWrapper error", JSON.stringify(error));
        }
    };

    renderedCallback() {
        if (!this.hasRendered) {
            this.hasRendered = true;
        }
    }

    handleReset() {
        this.isPropertyObjectLoaded = false;
        setTimeout(() => {
            this.isPropertyObjectLoaded = true;
        }, 50);
    }
}