import { LightningElement, wire, track, api } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveContactInvestorsListItems from '@salesforce/apex/MyListController.saveContactInvestorsListItems';
import saveAccountInvestorsListItems from '@salesforce/apex/MyListController.saveAccountInvestorsListItems';
import deleteInvestorsListItems from '@salesforce/apex/MyListController.deleteInvestorsListItems';

import { SORTING_DIRECTION, sort } from 'c/utils';

// for lazy loading
import getInvestorListItems from '@salesforce/apex/MyListController.getInvestorListItems';
import getInvestorContactListItems from '@salesforce/apex/MyListController.getInvestorContactListItems';

// Custom Labels
import account from '@salesforce/label/c.MyListDetailDataComponentAccount';
import addToChoice from '@salesforce/label/c.MyListDetailDataComponentAddToChoice';
import brand from '@salesforce/label/c.MyListDetailDataComponentBrand';
import city from '@salesforce/label/c.MyListDetailDataComponentCity';
import email from '@salesforce/label/c.MyListDetailDataComponentEmail';
import firstName from '@salesforce/label/c.MyListDetailDataComponentFirstName';
import lastName from '@salesforce/label/c.MyListDetailDataComponentLastName';
import status from '@salesforce/label/c.InvestorsListStatusLabel';
import contractEntries from '@salesforce/label/c.MyListDetailDataComponentContactEntries';
import accountEntries from '@salesforce/label/c.MyListDetailDataComponentAccountEntries';
import searchResult from '@salesforce/label/c.MyListDetailDataComponentSearchResult';
import street from '@salesforce/label/c.MyListDetailDataComponentStreet';
import deletedToastMessage from '@salesforce/label/c.MyListDataComponentDeleteToastMessage';
import deleteLabel from '@salesforce/label/c.MyListDetailComponentDeleteLabel';
import cancelLabel from '@salesforce/label/c.MyListDetailDataComponentCancelLabel';
import deleteModalHeader from '@salesforce/label/c.MyListDetailDataComponentDeleteModalHeader';
import deleteModalText from '@salesforce/label/c.MyListDetailDataComponentDeleteModalText';
import addToastMessage from '@salesforce/label/c.MyListDetailDataComponentAddToastMessage';
import successMessage from '@salesforce/label/c.InvestorsListContactDeleteSuccessTitle';
import noRecordsFound from '@salesforce/label/c.No_records_found';

/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 300;

const columnsContact = [
    { label: firstName, fieldName: 'firstName', type: 'text', sortable: true },
    { label: lastName, fieldName: 'lastName', type: 'text', sortable: true },
    { label: account, fieldName: 'accountName', type: 'text', sortable: true },
    { label: street, fieldName: 'street', type: 'text', sortable: true },
    { label: city, fieldName: 'city', type: 'text', sortable: true },
    { label: email, fieldName: 'email', type: 'text' },
    { label: brand, fieldName: 'brandName', type: 'text', sortable: true },
    { label: status, fieldName: 'status', type: 'text', sortable: true },
    { label: '', type: 'actionButtonRow', fieldName: 'id', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
];

const columnsAccount = [
    { label: ' ', fieldName: 'id', type: 'checkboxContainer',typeAttributes: {disabledandchecked: {fieldName : 'investorListItemCreated'}}, fixedWidth: 36 },
    { label: account, fieldName: 'name', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: street, fieldName: 'street', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: city, fieldName: 'city', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: brand, fieldName: 'brandName', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: status, fieldName: 'status', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: '', type: 'actionButtonRow', fieldName: 'id', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
];

const columnsContactListItems = [
    { label: account, fieldName: 'accountName', type: 'text', sortable: true },
    { label: firstName, fieldName: 'firstName', type: 'text', sortable: true },
    { label: lastName, fieldName: 'lastName', type: 'text', sortable: true },
    { label: street, fieldName: 'street', type: 'text', sortable: true },
    { label: city, fieldName: 'city', type: 'text', sortable: true },
    { label: email, fieldName: 'email', type: 'text', sortable: true },
    { label: brand, fieldName: 'brandName', type: 'text', sortable: true },
    { label: status, fieldName: 'status', type: 'text', sortable: true },
    { label: '', type: 'actionButtonRow', fieldName: 'id', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
];

const columnsAccountListItems = [
    { label: account, fieldName: 'name', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: street, fieldName: 'street', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: city, fieldName: 'city', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: brand, fieldName: 'brandName', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: status, fieldName: 'status', type: 'text', cellAttributes: { class: { fieldName: 'styleClass' }}, sortable: true },
    { label: '', type: 'actionButtonRow', fieldName: 'Id', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
];

const ListTypeItem = {contact: 'INVESTOR_LIST_ITEM_CONTACT', account: 'INVESTOR_LIST_ITEM_ACCOUNT', contactSearch: 'CONTACT_SEARCH', accountSearch: 'ACCOUNT_SEARCH' };

export default class MyListDetailDataComponent extends NavigationMixin(LightningElement) {
    @wire(CurrentPageReference) pageRef;

    @track columns;
    @track addButtonDisabled=true;
    @track deleteContactButtonDisabled = true;
    @track deleteAccountButtonDisabled = true;
    @track showSearchList = false;
    @track showItemsList = true;
    @track showDeleteModal = false;
    /**
     Structure for _filteredSearchResult for Accounts:
        {
            "brandName": "Deka",
            "city": "Frankfurt",
            "id": "0011x00000UV9okAAD",
            "investorListItemCreated": false,
            "name": "LWC Sebastian Test",
            "numberOfAcquisitionProfiles": "0",
            "status": "Active",
            "street": "Test Street 1"
        }

        Structure for _filteredSearchResult for Contacts:
        {
            "accountId": "0011x00000T4IyTAAV",
            "accountName": "Test GmbH 1111",
            "brandName": "Deka",
            "firstName": "matching 02",
            "id": "0031x00000iwFHoAAM",
            "lastName": "01",
            "status": "Active",
            "street": "Test Straße 9"
        },
     */
    @track _filteredSearchResult;
    /**
     Structure for _filteredContactListItems:
        {
            "accountId": "0011x00000T4IyTAAV",
            "accountName": "Test GmbH 1111",
            "brandName": "Deka",
            "contactId": "0031x00000iwFHoAAM",
            "firstName": "matching 02",
            "id": "a2Y1x000000RZFsEAO",
            "investorsListItemId": "a2X1x000000EZjIEAW",
            "lastName": "01",
            "status": "Active",
            "street": "Test Straße 9"
        }
     */
    @track _filteredContactListItems;
     /**
     Structure for _filteredAccountListItems:
        {
            "Id": "a2X1x000000EWfMEAW",
            "Account__c": "0011x00000xpqzkAAA",
            "Account__r": {
                "Name": "Allianz",
                "BillingStreet": "Taunusanlage 12E",
                "BillingCity": "Frankfurt am Main",
                "NumberOfAcquisitionProfiles__c": 0,
                "Status__c": "Active",
                "Id": "0011x00000xpqzkAAA"
            },
            "name": "Allianz",
            "street": "Taunusanlage 12E",
            "city": "Frankfurt am Main",
            "brandName": "",
            "status": "Active"
        },
     */
    @track _filteredAccountListItems;
    @track isContactsSearch;
    @track isLoading;
    @track sortedBy = {
        INVESTOR_LIST_ITEM_CONTACT: 'lastName',
        INVESTOR_LIST_ITEM_ACCOUNT: 'name',
        CONTACT_SEARCH: 'lastName',
        ACCOUNT_SEARCH: 'name'
    };
    @track sortDirection = {
        INVESTOR_LIST_ITEM_CONTACT: 'asc',
        INVESTOR_LIST_ITEM_ACCOUNT: 'asc',
        CONTACT_SEARCH: 'asc',
        ACCOUNT_SEARCH: 'asc'
    };

    @api selectedRow;
    @api recordId;
    @api columnsContactListItems = columnsContactListItems;
    @api columnsAccountListItems = columnsAccountListItems;

    _contactListItems;
    _accountListItems;
    selectedRecords = [];
    currentListItemType = '';
    ListTypeItem = ListTypeItem;

    @api set listItems(value){
        this.initLazyLoadState();
        this._contactListItems = value ? value[ListTypeItem.contact] : value;
        this._filteredContactListItems = this._contactListItems;
        this.investorContactsLastLoaded = this._filteredContactListItems ? this._filteredContactListItems.length : undefined;

        if (value) {
            this._accountListItems = value[ListTypeItem.account].map(row => {
                const name = row['Account__r']['Name'];
                const street = row['Account__r']['BillingStreet'];
                const city = row['Account__r']['BillingCity'];
                const brandName = row['Account__r']['Brand__r'] ? row['Account__r']['Brand__r']['Name'] : '';
                const status = row['Account__r']['Status__c'] ? row['Account__r']['Status__c'] : '';
                return { ...row, name, street, city, brandName, status };
            });
            this._filteredAccountListItems = this._accountListItems;
            this.investorsLastLoaded = this._filteredAccountListItems ? this._filteredAccountListItems.length : undefined;
        }
    }

    @track
    investorContactsIsLoading = false;
    @track
    investorsIsLoading = false;

    investorsLoadingFinished = false;
    investorContactsLoadingFinished = false;
    investorsLimitSize = 25;
    investorsLastLoaded = 0;
    investorContactsLimitSize = 25;
    investorContactsLastLoaded = 0;
    limitSizeStep = 25;


    initLazyLoadState()
    {
        this.investorContactsIsLoading = false;
        this.investorsIsLoading = false;
        this.investorsLoadingFinished = false;
        this.investorContactsLoadingFinished = false;
        this.investorsLimitSize = 25;
        this.investorsLastLoaded = 0;
        this.investorContactsLimitSize = 25;
        this.investorContactsLastLoaded = 0;
    }

    loadMoreInvestors()
    {

        if (this.filterValue)
        {
            return;
        }

        if (this.investorsLoadingFinished)
        {
            this.investorsIsLoading = false;
            return;
        }
        
        this.investorsLimitSize = this.investorsLimitSize + this.limitSizeStep;
        this.investorsIsLoading = true;

        getInvestorListItems({
            recordId : this.recordId,
            limitSize : this.investorsLimitSize,
        })
        .then(result => {

            this._accountListItems = result[ListTypeItem.account].map(row => {
                const name = row['Account__r']['Name'];
                const street = row['Account__r']['BillingStreet'];
                const city = row['Account__r']['BillingCity'];
                const brandName = row['Account__r']['Brand__r'] ? row['Account__r']['Brand__r']['Name'] : '';
                const status = row['Account__r']['Status__c'] ? row['Account__r']['Status__c'] : '';
                return { ...row, name, street, city, brandName, status };
            });

            if (this.investorsLastLoaded < this._accountListItems.length)
            {
                this._filteredAccountListItems = this._accountListItems;
                this.investorsLastLoaded = this._accountListItems.length;
                this.investorsIsLoading = false;
            }
            else
            {
                this.investorsIsLoading = false;
                this.investorsLoadingFinished = true;
            }
        }).catch(error => {
        })
    }

    loadMoreInvestorContacts()
    {
        if (this.filterValue)
        {
            return;
        }

        if (this.investorContactsLoadingFinished)
        {
            this.investorContactsIsLoading = false;
            return;
        }
        
        this.investorContactsLimitSize = this.investorContactsLimitSize + this.limitSizeStep;
        this.investorContactsIsLoading = true;

        getInvestorContactListItems({
            recordId : this.recordId,
            limitSize : this.investorContactsLimitSize,
        })
        .then(result => {

            this._contactListItems = result[ListTypeItem.contact] ? result[ListTypeItem.contact] : [];
            if (this.investorContactsLastLoaded < this._contactListItems.length)
            {
                this._filteredContactListItems = this._contactListItems;
                this.investorContactsLastLoaded = this._contactListItems.length;
                this.investorContactsIsLoading = false;
            }
            else
            {
                this.investorContactsIsLoading = false;
                this.investorContactsLoadingFinished = true;
            }
        }).catch(error => {
        })
    }

    get listItems(){
        return this._contactListItems;
    }

    _searchResult;

    @api set searchResult(value){
        this.searchDataTableIsLoading = false;
        this.addButtonDisabled = true;
        this._searchResult = value;
        this._filteredSearchResult = value;
    }

    @track
    searchDataTableIsLoading = false;

    loadMoreSearchData()
    {
        if (this.filterValue)
        {
            return;
        }

        this.searchDataTableIsLoading = true;
        
        fireEvent(this.pageRef, 'MyListDetailSearchComponent__loadMoreData', null);
    }

    tableIsLoading()
    {
        this.searchDataTableIsLoading = false;
    }



    get searchResult(){
        return this._searchResult;
    }

    filterValue;

    _mode = 'list';
    // mode values: list or search
    @api set mode(value){
        if(value==='list'){
            this.showSearchList=false;
            this.showItemsList=true;
        } else if(value==='search'){
            this.showSearchList=true;
            this.showItemsList=false;
        }
        this._mode = value;
    }

    get mode(){
        return this._mode;
    }

    label = {
        addToChoice: addToChoice,
        searchResult: searchResult,
        contractEntries: contractEntries,
        accountEntries: accountEntries,
        delete: deleteLabel,
        cancel: cancelLabel,
        deleteModalHeader: deleteModalHeader,
        deleteModalText: deleteModalText,
        deletedToastMessage: deletedToastMessage,
        addToastMessage: addToastMessage,
        noRecordsFound: noRecordsFound,
        success: successMessage,
    };

    _objectType;

    @api set objectType(value){
        this._objectType = value;
        if(value === 'Contact'){
            this.isContactsSearch = true;
            this.columns = columnsContact;
        } else {
            this.isContactsSearch = false;
            this.columns = columnsAccount;
        }
    }

    @api disableAddButton() {
        this.addButtonDisabled = false;
    }

    get tableHeight() {
        const screenHeight = screen.height;
        const viewHeight = screenHeight * 0.44;
        const accountTableHeight = !this.hasContactRecords ? screenHeight * 0.53 : (viewHeight * 0.3);
        const contactTableHeight = !this.hasAccountRecords ? screenHeight * 0.53 : (viewHeight * 0.7);

        return {
            account: 'height: ' + accountTableHeight + 'px',
            contact: 'height: ' + contactTableHeight+ 'px'
        }
    }

    get hasAccountRecords() {
        return this._filteredAccountListItems && this._filteredAccountListItems.length > 0;
    }

    get hasContactRecords() {
        return this._filteredContactListItems && this._filteredContactListItems.length > 0 || this._filteredSearchResult && this._filteredSearchResult.length > 0;
    }

    get objectType(){
        return this._objectType;
    }

    connectedCallback(){
        this.columns = columnsContact;
        registerListener('InvestorSelectionTab__checkboxChecked', this.handleRowSelection, this);
        registerListener('MyListDetailDataComponent__tableIsLoading', this.tableIsLoading, this);
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    async handleAddButtonClick(){
        let saveInvestorsListItemsResult;
        if (this.isContactsSearch) {
            const dt = this.template.querySelector('c-investor-selection-datatable-color');
            saveInvestorsListItemsResult = await saveContactInvestorsListItems({
                records: dt.getSelectedRows(),
                investorsListId: this.recordId
            });
        } else {
            saveInvestorsListItemsResult = await saveAccountInvestorsListItems({
                records: this.selectedRecords,
                investorsListId: this.recordId
            });
            this.selectedRecords = [];
        }

        this.mode = 'list';
        if(saveInvestorsListItemsResult){
            const event = CustomEvent('refreshdata', {
                composed: true,
                bubbles: true,
                cancelable: true,
            });
            this.dispatchEvent(event);
            const evt = new ShowToastEvent({
                "title": this.label.success,
                "message": this.label.addToastMessage,
            });
            this.dispatchEvent(evt);
        }
    }

    handleDeleteButtonClick(event){
        // show confirmation modal
        this.currentListItemType = event.target.className;
        this.showDeleteModal=true;
    }

    closeDeleteModal(){
        this.showDeleteModal=false;
    }


    async deleteInvestorsListItemsHandler(){
        this.showDeleteModal = false;
        const dt = this.template.querySelector('c-investor-selection-datatable-color.' + this.currentListItemType);
        const selectedRows = dt.getSelectedRows();
        let selectedIds = [];

        for(let i=0; i < selectedRows.length; i++){
            selectedIds.push(selectedRows[i].id || selectedRows[i].Id);
        }

        const deleteInvestorsListItemsResult = await deleteInvestorsListItems({
            itemIds: selectedIds,
            strListItemType: this.currentListItemType
        });
        if(deleteInvestorsListItemsResult){
            const event = CustomEvent('refreshdata', {
                composed: true,
                bubbles: true,
                cancelable: true
            });
            this.dispatchEvent(event);
            const evt = new ShowToastEvent({
                "title": this.label.success,
                "message": this.label.deletedToastMessage,
                "messageData": [
                    deleteInvestorsListItemsResult,
                ]
            });
            this.dispatchEvent(evt);
        }
    }

    handleRowSelection(event){
        if (event.detail) {
            const selectedRows = event.detail.selectedRows;
            if(selectedRows && selectedRows.length > 0){
                this.addButtonDisabled = false;
                if (event.target.className === ListTypeItem.contact) {
                    this.deleteContactButtonDisabled = false;
                } else if (event.target.className === ListTypeItem.account) {
                    this.deleteAccountButtonDisabled = false;
                }
                this.selectedRow = selectedRows[selectedRows.length-1];
            } else {
                this.addButtonDisabled = true;
                this.deleteContactButtonDisabled = true;
                this.deleteAccountButtonDisabled = true;
                this.selectedRow = undefined;
            }
            this.selectedRecords = selectedRows;
        } else {
            // data from InvestorSelectionTab__checkboxChecked event
            let value = event.split('-')[0];
            let rowId = event.split('-')[1];

            for (let i = 0; i < this._filteredSearchResult.length; i++) {
                if(this._filteredSearchResult[i].id === rowId){
                    if (value === 'true'){
                        this.selectedRow = this._filteredSearchResult[i];
                        this.selectedRecords.push(this.selectedRow);
                    } else {
                        for(let j = 0; j < this.selectedRecords.length; j++){
                            if (this.selectedRecords[j].id === rowId) {
                                this.selectedRecords.splice(j, 1);
                            }
                        }
                    }
                }
            }

            this.addButtonDisabled = this.selectedRecords.length === 0;
            this.selectedRow = this.selectedRecords.length === 0 ? undefined : this.selectedRow;
        }

        // const evt = CustomEvent('mylistselected', {
        //     composed: true,
        //     bubbles: true,
        //     cancelable: true,
        //     detail: {
        //         selectedRow: this.selectedRow
        //     },
        // });
        // this.dispatchEvent(evt);
    }

    /**
     *
     * @param {*} event
     *
     * @description
     */
    handleKeyChange(event) {
        // Debouncing this method: Do not update the reactive property as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
        window.clearTimeout(this.delayTimeout);
        this.filterValue = event.target.value;
        const targetName = event.target.name;

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            if(this._mode === 'list'){
                this.filterListData(targetName);
            } else {
                this.filterSearchData();
            }

        }, DELAY);
    }

    /**
     * filter list data
     */
    filterListData(listItemType) {
        const filterValue = this.filterValue ? this.filterValue.toLowerCase() : null;
        // write the originaldata back to the filtered data if there is no filter defined
        if (!filterValue) {
            this._filteredContactListItems = this._contactListItems;
            this._filteredAccountListItems = this._accountListItems;
            return;
        }

        let currentListItem = listItemType === ListTypeItem.contact ? this._filteredContactListItems : this._filteredAccountListItems;
        const filteredData = currentListItem.filter(function(el) {
            // filtering for all possible values
            switch(true){
                case el.name && el.name.toLowerCase().includes(filterValue) : return true;
                case el.firstName && el.firstName.toLowerCase().includes(filterValue) : return true;
                case el.lastName && el.lastName.toLowerCase().includes(filterValue): return true;
                case el.accountName && el.accountName.toLowerCase().includes(filterValue): return true;
                case el.city && el.city.toLowerCase().includes(filterValue): return true;
                case el.street && el.street.toLowerCase().includes(filterValue): return true;
                case el.brandName && el.brandName.toLowerCase().includes(filterValue): return true;
                case el.email && el.email.toLowerCase().includes(filterValue): return true;
                default: return false;
            }
        });

        this._filteredContactListItems = listItemType === ListTypeItem.contact ? filteredData : this._filteredContactListItems;
        this._filteredAccountListItems = listItemType === ListTypeItem.account ? filteredData : this._filteredAccountListItems;
    }

    /**
     * filter search data
     */
    filterSearchData() {
        const filterValue = this.filterValue
            ? this.filterValue.toLowerCase()
            : null;
        // write the originaldata back to the filtered data if there is no filter defined
        if (!filterValue) {
            this._filteredSearchResult = this._searchResult;
            return;
        }
        // filtering for all possible values
        this._filteredSearchResult = this._filteredSearchResult.filter(function(el) {
            switch(true){
                case el.name && el.name.toLowerCase().includes(filterValue) : return true;
                case el.firstName && el.firstName.toLowerCase().includes(filterValue) : return true;
                case el.lastName && el.lastName.toLowerCase().includes(filterValue): return true;
                case el.accountName && el.accountName.toLowerCase().includes(filterValue): return true;
                case el.city && el.city.toLowerCase().includes(filterValue): return true;
                case el.street && el.street.toLowerCase().includes(filterValue): return true;
                case el.brandName && el.brandName.toLowerCase().includes(filterValue): return true;
                case el.email && el.email.toLowerCase().includes(filterValue): return true;
                default: return false;
            }
        });
    }

    handleSorting(event) {
        const className = event.target.className;
        const sortedBy = event.detail.fieldName;
        const direction = event.detail.sortDirection;

        this['sortedBy'][className] = sortedBy;
        this['sortDirection'][className] = direction;

        switch (className) {
            case ListTypeItem.contact:
                this._filteredContactListItems = sort(this._filteredContactListItems, sortedBy, direction);
                break;
            case ListTypeItem.account:
                this._filteredAccountListItems = sort(this._filteredAccountListItems, sortedBy, direction);
                break;
            case ListTypeItem.contactSearch:
            case ListTypeItem.accountSearch:
                this._filteredSearchResult = sort(this._filteredSearchResult, sortedBy, direction);
                break;
        }
    }

    // The old logic for displaying right side panel with information of Account / Contact was not working correct
    // because it was not take into account that we use 4 tables with different data and data sctructure
    showRecordDetailsFilteredAccounts(event)
    {
        let detailsObject = {};
        if (event.detail) {
            this._filteredAccountListItems.forEach(function (item, index) {
                if (item.Id === event.detail.rowId)
                {
                    detailsObject = {
                        accountId : item.Account__c,
                        accountName : item.name,
                        Name : item.name,
                        brandName : item.brandName,
                        Brand__c : item.brandName,
                        street : item.street,
                        BillingStreet : item.street,
                        city : item.city,
                        BillingCity : item.city,
                        hasContact : false
                    };
                }
            });

            this.showRecordDetails(detailsObject);
        }
    }

    showRecordDetailsFilteredContacts(event)
    {
        let detailsObject;
        if (event.detail) {
            this._filteredContactListItems.forEach(function (item, index) {
                if (item.id === event.detail.rowId)
                {
                    detailsObject = {
                        accountId : item.accountId,
                        accountName : item.accountName,
                        Name : item.accountName,
                        brandName : item.brandName,
                        Brand__c : item.brandName,
                        street : item.street,
                        BillingStreet : item.street,
                        city : item.city,
                        BillingCity : item.city,
                        hasContact : true,
                        contactId : item.contactId,
                        contactName : item.firstName && item.lastName ? item.firstName + ' ' + item.lastName : item.firstName ? item.firstName : item.lastName ? item.lastName : '',
                        Title : item.title,
                        FirstName : item.firstName ? item.firstName : '',
                        LastName : item.lastName ? item.lastName : '',
                        email : item.email,
                        Email : item.email,
                        position : item.position,
                        Position__c : item.position
                    };
                }
            });
            
            this.showRecordDetails(detailsObject);
        }
    }

    showRecordDetailsSearchedAccounts(event)
    {
        let detailsObject;
        if (event.detail) {
            this._filteredSearchResult.forEach(function (item, index) {
                if (item.id === event.detail.rowId)
                {
                    detailsObject = {
                        accountId : item.id,
                        accountName : item.name,
                        Name : item.name,
                        brandName : item.brandName,
                        Brand__c : item.brandName,
                        street : item.street,
                        BillingStreet : item.street,
                        city : item.city,
                        BillingCity : item.city,
                        hasContact : false
                    };
                }
            });

            this.showRecordDetails(detailsObject);
        }
    }

    showRecordDetailsSearchedContacts(event)
    {
        let detailsObject;
        if (event.detail) {
            this._filteredSearchResult.forEach(function (item, index) {
                if (item.id === event.detail.rowId)
                {
                    detailsObject = {
                        accountId : item.accountId,
                        accountName : item.accountName,
                        Name : item.accountName,
                        brandName : item.brandName,
                        Brand__c : item.brandName,
                        street : item.street,
                        BillingStreet : item.street,
                        city : item.city,
                        BillingCity : item.city,
                        hasContact : true,
                        contactId : item.id,
                        contactName : item.firstName && item.lastName ? item.firstName + ' ' + item.lastName : item.firstName ? item.firstName : item.lastName ? item.lastName : '',
                        Title : item.title,
                        FirstName : item.firstName ? item.firstName : '',
                        LastName : item.lastName ? item.lastName : '',
                        email : item.email,
                        Email : item.email,
                        position : item.position,
                        Position__c : item.position
                    };
                }
            });
            this.showRecordDetails(detailsObject);
        }
    }

    /**
     *
     * @param {*} detailsObject - the information about record
     * 
     * @description 
     */
    showRecordDetails(detailsObject)
    {
        const evt = CustomEvent('mylistselected', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                selectedRow: detailsObject,
            },
        });
        this.dispatchEvent(evt);
    }

    
}