import { LightningElement, api, track } from 'lwc';

export default class UlListItem extends LightningElement {
    @api item; //Required. The api-name for picklist row.
    @api label; //Required. The text label that displays in the pill.
    @api searchTerm; //Holds value for the given input value from parent compnent

   /**
    * Track internal states.
    */
   @track _isActive = false; //Holds the internal state about if li has an checkmark or not.

   /********************
    * HANDLE EVENTS
    ********************/
    /**
    * Handle on click for row in picklist.
    */
    handleOnClick(event){
        this._isActive = ! this._isActive;

        // Creates the event with the item.
        const selectedEvent = new CustomEvent(
            'listitemclicked',
            {
                detail: {
                    item: this.item,
                    label: this.label,
                    isActive: this.isActive
                }
            }
        );

        // Dispatches the event.
        this.dispatchEvent(selectedEvent);        
    }

    /********************
    * TEMPLATE
    ********************/
   /**
    * Indicate the state about if li has an checkmark or not.
    */
    @api
    get isActive(){
        return this._isActive;
    }

    set isActive(value){
        this._isActive = value;
    }

    get showCheckmark(){
        return (this._isActive || this.isActive ? true : false);
    }

    /**
     * BEGIN -- Hightlight searchTearm and append and prepend the rest of the word.
     * 
     * If there is no searchTerm available, show whole unfiltered label.
     */
    get prefixSearchTerm(){
        let returnValue = '';
        const searchTermLowercase = this._getSearchTermToLowerCase();
        const labelToLowerCase = this._getLabelToLowerCase();
        const positionForSearchTermTo = labelToLowerCase.indexOf(searchTermLowercase);

        if(this._getSearchTermIsAvailable()){
            //Only if there are leading character for marked string.
            if(positionForSearchTermTo > 0){
                let extractedPrefix = this.label.substring(0, positionForSearchTermTo);
                returnValue = extractedPrefix;
            }
        } else {
            return this.label;
        }
        
        return returnValue;
    }
    
    /**
     * Extract the part which matches witth the given searchTerm in label -- case insensitive.
     * 
     * Does NOT highlight anything if there is no searchTerm given.
     */
    get markedSearchTerm(){
        let returnValue = '';

        if(this._getSearchTermIsAvailable()){
            const searchTermLowercase = this._getSearchTermToLowerCase();
            const labelToLowerCase = this._getLabelToLowerCase();
            const lengthOfSearchTerm = this._getLengthOfSearchTerm();

            const positionForSearchTermFrom = labelToLowerCase.indexOf(searchTermLowercase);
            const positionForSearchTermTo = positionForSearchTermFrom + lengthOfSearchTerm;
    
            returnValue = this.label.substring(positionForSearchTermFrom, positionForSearchTermTo);
        }
        
        return returnValue;
    }

    /**
     * Extract the part after the searchTerm - if available, which should not be highlighted.
     * 
     * If there is no searchTerm available, it shows nothing.
     */
    get suffixSearchTerm(){
        let returnValue = '';

        if(this._getSearchTermIsAvailable()){
            const searchTermLowercase = this._getSearchTermToLowerCase();
            const labelToLowerCase = this._getLabelToLowerCase();   
            const positionForSearchTermFrom = labelToLowerCase.indexOf(searchTermLowercase);
            const positionForSearchTermTo = positionForSearchTermFrom + this._getLengthOfSearchTerm();

            let extractedSuffix = this.label.substring(positionForSearchTermTo);
            returnValue = extractedSuffix;
            }

        return returnValue;
    }
    /**
     * END -- Hightlight searchTearm and append and prepend the rest of the word.
     */

    /********************
     * HELPER FUNCTIONS
     ********************/
    _getSearchTermToLowerCase(){
        return (typeof this.searchTerm !== 'undefined' ? this.searchTerm.toLowerCase() : '');
    }

    _getLabelToLowerCase(){
        return (typeof this.label !== 'undefined' ? this.label.toLowerCase() : '');
    }

    _getLengthOfSearchTerm(){
        return (this._getSearchTermIsAvailable() ? this.searchTerm.length : 0);
    }

    _getSearchTermIsAvailable(){
        return (typeof this.searchTerm !== 'undefined' ? true : false);
    }
}