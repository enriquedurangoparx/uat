import { LightningElement, api, track } from 'lwc';
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

// import custom labels
import generalLabelClose from '@salesforce/label/c.GeneralLabelClose';
import generalLabelCancel from '@salesforce/label/c.GeneralLabelCancel';
import generalLabelLatitude from '@salesforce/label/c.GeneralLabelLatitude';
import generalLabelLongitude from '@salesforce/label/c.GeneralLabelLongitude';
import generalLabelSuccess from '@salesforce/label/c.GeneralLabelSuccess';
import generalLabelError from '@salesforce/label/c.GeneralLabelError';
import generalLabelSuccessUpdateMessage from '@salesforce/label/c.GeneralLabelSuccessUpdateMessage';
import setObjectGeodataSaveLocation from '@salesforce/label/c.SetObjectGeodataSaveLocation';
import setObjectGeodataConfirmModalHeader from '@salesforce/label/c.SetObjectGeodataConfirmModalHeader';
import setObjectGeodataConfirmModalText from '@salesforce/label/c.SetObjectGeodataConfirmModalText';

export default class setObjectGeodataConfirmModal extends LightningElement 
{
    @api objectInfo;
    @track showModal = false;
    @track isLoading = false;

    label = {
        generalLabelClose,
        generalLabelCancel,
        generalLabelLatitude,
        generalLabelLongitude,
        generalLabelSuccess,
        generalLabelError,
        generalLabelSuccessUpdateMessage,
        setObjectGeodataSaveLocation,
        setObjectGeodataConfirmModalHeader,
        setObjectGeodataConfirmModalText
    }

    /********************
     * EXPOSED FUNCTIONS
     ********************/
    /**
     * open objective selection modal
     */
    @api
    openModal(obj) {
        this.objectInfo = obj;
        this.showModal = true;
    }

    /**
     * close objective selection modal
     */
    closeModal() {
        this.showModal = false;
    }

    /**
     * Update data
     */
    handleSave(){
        this.isLoading = true;
        const fieldsToUpdate = {};
        fieldsToUpdate['Id'] = this.objectInfo.Id;
        fieldsToUpdate['Geolocation__Latitude__s'] = this.objectInfo.Geolocation__Latitude__s;
        fieldsToUpdate['Geolocation__Longitude__s'] = this.objectInfo.Geolocation__Longitude__s;
        const recordToUpdate = { fields: fieldsToUpdate};

        updateRecord(recordToUpdate)
            .then(() => {
                this.dispatchEvent(new Event('recordupdate'));
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: this.label.generalLabelSuccess,
                        message: this.label.generalLabelSuccessUpdateMessage,
                        variant: 'success',
                    }),
                );
                this.isLoading = false;
                this.closeModal();
            })
            .catch(error => {
                this.isLoading = false;
                let message = 'Unknown error';
                if (Array.isArray(error.body)) 
                {
                    message = error.body.map(e => e.message).join(', ');
                } 
                else if (typeof error.body.message === 'string') 
                {
                    message = error.body.message;
                }
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: this.label.generalLabelError,
                        message,
                        variant: 'error',
                    }),
                );
            });
    }
}