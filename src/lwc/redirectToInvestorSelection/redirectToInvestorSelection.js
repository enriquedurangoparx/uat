import { LightningElement, api } from 'lwc';

export default class RedirectToInvestorSelection extends LightningElement {
    @api recordId;
}