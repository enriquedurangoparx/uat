/**
 * Created by npo on 3/11/20.
 */

import LightningDatatable from 'lightning/datatable';
import SENDING_TYPE_FIELD from '@salesforce/schema/CampaignMember.SendingType__c';
import CAMPAIGN_MEMBER_OBJECT from '@salesforce/schema/CampaignMember';

import { track, wire, api } from 'lwc';

import sendingTypePicklistTemplate from './sendingTypePicklistTemplate.html';


export default class NewMailingInvestorContactsTable extends LightningDatatable {

	static customTypes = {
		sendingTypePicklist: {
			template: sendingTypePicklistTemplate,
			typeAttributes: ['customValue', 'fieldName', 'recordId', 'isDisabled'],
		}
	};
}