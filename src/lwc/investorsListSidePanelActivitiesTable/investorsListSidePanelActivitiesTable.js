import LightningDatatable from 'lightning/datatable';
import urlList from './urlList.html'

export default class InvestorsListSidePanelActivitiesTable extends LightningDatatable {
   static customTypes = {
       contactsList: {
           template: urlList,
           standardCellLayout: true,
           // Provide template data here if needed
           typeAttributes: ['relations'],
       }
	}
}