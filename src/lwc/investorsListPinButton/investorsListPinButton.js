import { LightningElement, api, track } from 'lwc';
// Accessibility module
//import { baseNavigation } from 'lightning/datatableKeyboardMixins';
// For the render() method
import template from './investorsListPinButton.html';

// export default class DatatableDeleteRowBtn extends baseNavigation(LightningElement) {
export default class investorsListPinButton extends LightningElement {
    @api rowId;
    @api accountId;
    @track _isPinned;
    @api set isPinned(value){
        this._isPinned=value;
        if(value)
            this.pinnedClass='utility:pinned';
        else
            this.pinnedClass = 'utility:pin';
    }

    get isPinned(){
        return this._isPinned;
    }
    @track pinnedClass;

    // Required for mixins
    render() {
        return template;
    }

    firePinRow() {
      ////window.console.log(baseNavigation);
        const event = CustomEvent('pinrow', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                rowId: this.rowId,
                accountId: this.accountId,
                isPinned: this._isPinned
            },
        });
        this.dispatchEvent(event);
    }
}