import { LightningElement, api } from 'lwc';

export default class InvestorsContactListAction extends LightningElement {
    @api contactId;
    @api action;

    get details() {
        return this.action === 'details';
    }

    get deleteContact() {
        return this.action === 'deleteContact';
    }

    get addActivity() {
        return this.action === 'addActivity';
    }

    handleShowDetails(event) {
        const showEvent = CustomEvent('showdetails', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                contactId: this.contactId,
                top: event.offsetY,
                left: event.offsetX
            }
        });
        this.dispatchEvent(showEvent);
    }

    handleHideDetails() {
        this.dispatchEvent(
            CustomEvent('hidedetails', {
                composed: true,
                bubbles: true,
                cancelable: true
            })
        );
    }

    handleDeleteContact() {
        const deleteEvent = CustomEvent('deletecontact', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                contactId: this.contactId
            }
        });
        this.dispatchEvent(deleteEvent);
    }

    handleAddActivity() {
        this.dispatchEvent(
            CustomEvent('addactivity', {
                composed: true,
                bubbles: true,
                cancelable: true,
                detail: {
                    contactId: this.contactId
                }
            })
        );
    }
}