import LightningDatatable from 'lightning/datatable';
import deleteRelationRow from './relatedObjectsDeleteRelationRow.html';
import checkboxContainer from './relatedObjectsCheckboxContainer.html';

export default class RelatedObjectsDataTable extends LightningDatatable
{
    static customTypes = {
        deleteRelationRow: {
            template: deleteRelationRow,
            typeAttributes: ['relateToId']
        },
        checkboxContainer: {
            template: checkboxContainer,
            typeAttributes: ['disabledandchecked'],
        }
    };
}