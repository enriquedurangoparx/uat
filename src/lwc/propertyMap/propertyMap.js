import { LightningElement, api, track } from 'lwc';

import noRecordsFoundLabel from '@salesforce/label/c.GeneralLabelNoRecordsFound';

export default class PropertyMap extends LightningElement {
    @api markers = [];

    label = {
        noRecordsFoundLabel
    }

    @track zoomLevel = 11;
    @track error;

    get showMap()
    {
        let showFlag = true;
        if (this.markers && this.markers.length === 0)
        {
            this.error = this.label.noRecordsFoundLabel;
            showFlag = false;
        }
        return showFlag;
    }
}