/**
*
*	@author npo
*	@copyright PARX
*/
import { LightningElement, track, api } from 'lwc';
import cancelLabel from '@salesforce/label/c.investorsListCancelLabel';
import newMailingInvestorConfirmationTitleLabel from '@salesforce/label/c.NewMailingInvestorConfirmationTitle';
import newMailingInvestorConfirmationMessageLabel from '@salesforce/label/c.NewMailingInvestorConfirmationMessage';
import close from '@salesforce/label/c.MyListCloseEditModal';
import newMailingRemoveLabel from '@salesforce/label/c.NewMailingRemove';


export default class NewMailingInvestorRemoveModal extends LightningElement {
	label = {
		cancelLabel,
		newMailingInvestorConfirmationTitleLabel,
		newMailingInvestorConfirmationMessageLabel,
		close,
		newMailingRemoveLabel
	};

	@api investorId;

	@track showModal = false;
	@api show() {
	    this.showModal = true;
	}

	@api hide() {
		this.showModal = false;
	}

	handleCancel()
	{
		this.hide();
	}

	handleConfirm(event)
	{
	    const e = new CustomEvent(
		  'removeinvestor',
		  {
			  detail: {
				  investorId: this.investorId
			  }
		  });
		  this.dispatchEvent(e);
		  this.hide();
	}

}