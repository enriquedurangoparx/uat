import LightningDatatable from 'lightning/datatable';
import actionButtonRow from './FileListAllRecordsDatatableAction.html';

export default class FileListAllRecordsDatatable extends LightningDatatable {
    static customTypes = {
        actionButtonRow: {
            template: actionButtonRow,
            typeAttributes: ['action']
        }
    };
}