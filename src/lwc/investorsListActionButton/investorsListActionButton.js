import { LightningElement, api } from 'lwc';
// For the render() method
import template from './investorsListActionButton.html';

import activity from '@salesforce/label/c.InvestorsListActivityButtonLabel';

// export default class DatatableDeleteRowBtn extends baseNavigation(LightningElement) {
export default class investorsListActionButton extends LightningElement {
    @api rowId;
    @api rowStatus;
    @api showActionButton;

    label = {
        activity
    }

    get _showActionButton() {
        return this.showActionButton ? (this.showActionButton === 'true') : true;
    }
    // Required for mixins
    render() {
        return template;
    }

    fireActionButton() {
        const event = CustomEvent('actionbutton', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                rowId: this.rowId,
                rowStatus: this.rowStatus
            },
        });
        this.dispatchEvent(event);
    }

    fireDetailButton(){
        const event = CustomEvent('detailsbutton', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                rowId: this.rowId
            },
        });
        this.dispatchEvent(event);
    }
}