import { LightningElement, api, track, wire } from 'lwc';
import { reduceErrors } from 'c/lwcUtil';

import LANG from '@salesforce/i18n/lang';// Import internationalization properties.

import getRecords from '@salesforce/apex/SsrsRelatedListController.getSsrsReportsForObject';

/********************
 * CUSTOM LABELS
 ********************/
import noRecordsFound from '@salesforce/label/c.GeneralLabelNoRecordsFound';
import relatedListTitle from '@salesforce/label/c.SSRSRelatedListTitle';


const GERMAN_DATA_COLUMNS = [
    { label: 'Berichtsname', fieldName: 'reportUrl', type: 'url', typeAttributes: { tooltip: { fieldName: 'reportName' }, label: { fieldName: 'reportName' } } },
    { label: 'Beschreibung', fieldName: 'description' },
];

const ENGLISH_DATA_COLUMNS = [
    { label: 'Report Name', fieldName: 'reportUrl', type: 'url', typeAttributes: { tooltip: { fieldName: 'reportName' }, label: { fieldName: 'reportName' } } },
    { label: 'Description', fieldName: 'description' },
];

export default class SsrsRelatedList extends LightningElement {
    @api objectApiName;// The page sets the property to the API name of the current object.
    @api recordId;// The page sets the property to the ID of the current record.

    @track data;
    @track columns;
    @track hasRecords;// Indicates if there are records to display.

    label = {
        relatedListTitle,
        noRecordsFound
    }

    /**
     * Holds internal state.
     */
    @track _showSpinner;// Indicates if spinner is shown.
    @track _userLanguage = LANG;// Current language of user.
    @track error;

    @track iconName = 'custom:custom8';

    connectedCallback()
    {
        this.columns = (this._isGermanUserLanguage() ? GERMAN_DATA_COLUMNS : ENGLISH_DATA_COLUMNS);
        this._showSpinner = true;// Start loading the spinner.
    }

    /********************
     * WIRE METHODS
     ********************/
    @wire(getRecords, {
        objectApiName: '$objectApiName'
    })
    wiredRecords({ error, data})
    {
        if (data) {
            this.data = JSON.parse(JSON.stringify(data));// Copy data, because data is immutable.
            console.log('>>>this.data: ' + JSON.stringify(this.data, null, '\t'));

            this.error = undefined;
            this.preProcessData();
            this._showSpinner = false;// Disable spinner.
            this.hasRecords = (typeof this.data !== undefined && this.data.length > 0 ? true : false);
        } else if (error) {
            this.error = error;
            this.data = undefined;
            this._showSpinner = false;// Disable spinner.
            this.hasRecords = false;

            this.notifyUser(
                'Error',
                'An error occured while retrieving records.\n' +
                    reduceErrors(error).join(', '),
                'error'
            );
        }
    }

    /********************
     * TEMPLATE
     ********************/
    get showSpinner()
    {
        return this._showSpinner;
    }

    /**
     * Define the title of related list, depending on user language.
     */
    get relatedListTitle()
    {
        return (this._isGermanUserLanguage() ? 'this.relatedListLabelGerman' : this.relatedListLabelEnglish);
    }


    /********************
     * HELPER METHODS
     ********************/
    _isGermanUserLanguage(){
        return (this._userLanguage === 'de' ? true : false);
    }

    preProcessData()
    {
        let preprocessData = [];
        this.data.forEach(element => {
            let baseUrl = element.baseUrl; 
            let externalId = element.externalId;
            let reportUrl = baseUrl + '/' + externalId;
            reportUrl += '&' + element.additionalParameter.replace('{!recordId}', this.recordId);

            element.reportUrl = reportUrl;

            preprocessData.push(element);
        });

        this.data = preprocessData;
    }

    /**
     * notify user
     * @param {string} title
     * @param {string} message
     * @param {string} variant
     */
    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }
}