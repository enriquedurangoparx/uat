import { LightningElement, api, track, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import { NavigationMixin } from 'lightning/navigation';
import { deleteRecord } from 'lightning/uiRecordApi';
import { createRecord } from 'lightning/uiRecordApi';
import { getRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getRelatedData from '@salesforce/apex/InvestorsListController.getRelatedData';
import searchContacts from '@salesforce/apex/InvestorsListController.searchContacts';
import search from '@salesforce/apex/GenericSearch.search';

import searchAccount from '@salesforce/label/c.InvestorsListSearchAccountContacts';
import searchAll from '@salesforce/label/c.InvestorsListSearchAllContacts';
import investorDetails from '@salesforce/label/c.InvestorsListInvestorDetails';
import investorDetailsAddedOn from '@salesforce/label/c.InvestorsListInvestorDetailsAddedOn';
import investorDetailsAddedBy from '@salesforce/label/c.InvestorsListInvestorDetailsAddedBy';
import investorDetailsAddedThrough from '@salesforce/label/c.InvestorsListInvestorDetailsAddedThrough';
import tabActivities from '@salesforce/label/c.InvestorsListTabActivities';
import tabContacts from '@salesforce/label/c.InvestorsListTabContacts';
import tabHistory from '@salesforce/label/c.InvestorsListTabHistory';
import cancelDeleteContact from '@salesforce/label/c.InvestorsListCancelDeleteContact';
import deleteContact from '@salesforce/label/c.InvestorsListDeleteContact';
import confirmDeleteHeadline from '@salesforce/label/c.InvestorsListConfirmDeleteHeadline';
import confirmDeleteText from '@salesforce/label/c.InvestorsListConfirmDeleteText';
import deleteError from '@salesforce/label/c.InvestorsListContactDeleteError';
import deleteSuccess from '@salesforce/label/c.InvestorsListContactDeleteSuccess';
import deleteSuccessTitle from '@salesforce/label/c.InvestorsListContactDeleteSuccessTitle';
import addContact from '@salesforce/label/c.InvestorsListAddContact';
import contactSearchPlaceholder from '@salesforce/label/c.InvestorsListContactSearchPlaceholder';
import rowLabelContact from '@salesforce/label/c.InvestorsListTableRowContact';
import rowLabelSubject from '@salesforce/label/c.InvestorsListTableRowSubject';
import rowLabelDate from '@salesforce/label/c.InvestorsListTableRowDate';
import rowLabelName from '@salesforce/label/c.InvestorsListTableRowName';
import rowLabelEmail from '@salesforce/label/c.InvestorsListTableRowEmail';
import rowLabelAccount from '@salesforce/label/c.InvestorsListTableRowAccount';
import rowLabelLastModifiedBy from '@salesforce/label/c.InvestorsListTableRowLastModifiedBy';
import rowLabelStatusSince from '@salesforce/label/c.InvestorsListTableRowStatusSince';
import rowLabelStatus from '@salesforce/label/c.InvestorsListStatusLabel';
import sectionOpenOpps from '@salesforce/label/c.InvestorsListSectionOpenOpportunities';
import sectionOpenBrandOpps from '@salesforce/label/c.InvestorsListSectionOpenBrandOpportunities';
import sectionStatusHistory from '@salesforce/label/c.InvestorsListSectionStatusHistory';
import sectionActivities from '@salesforce/label/c.InvestorsListSectionActivity';
import sectionContact from '@salesforce/label/c.InvestorsListSectionContact';
import viewAll from '@salesforce/label/c.RelatedListViewAll';
import contactSearchMessageLabel from '@salesforce/label/c.ContactSearchMessage';


// search delay
const DELAY = 300;

const CONTACT_SEARCH_LIMIT = 200;

// contacts table
const ctColumns = [{label: '', type: 'contactListAction', fieldName: 'investorRelation', fixedWidth: 36, typeAttributes: { action: 'deleteContact' }},
                   {label: '', type: 'contactListAction', fieldName: 'id', fixedWidth: 36, typeAttributes: { action: 'details'}},
                   {label: rowLabelName, fieldName: 'contactUrl', type: 'url', typeAttributes: { label: {fieldName: 'name'}, target:'_blank'}, sortable: 'false' },
                   {label: rowLabelEmail, fieldName: 'email', type: 'email', sortable: 'false'},
                   {label: rowLabelAccount, fieldName: 'accountUrl', type: 'url', typeAttributes: { label: {fieldName: 'accountName'}, target:'_blank'}, sortable: 'false'},
                   {label: '', type: 'contactListAction', fieldName: 'id', fixedWidth: 36, typeAttributes: { action: 'addActivity' }}
];

// activities table
const srColumns = [{label: rowLabelSubject, fieldName: 'activityid',  type: 'url', typeAttributes: { label: {fieldName: 'subject'}, target:'_blank'}, sortable: true },
                   {label: rowLabelDate, fieldName: 'date', type: 'date', sortable: 'false' },
                   {label: rowLabelContact, fieldName: 'whoid',  type: 'contactsList', sortable: true }
];

// status history object
const hiColumns = [{label: rowLabelName, fieldName: 'statusId',  type: 'url', typeAttributes: { label: {fieldName: 'statusName'}, target:'_blank'}, sortable: true },
                   {label: rowLabelDate, fieldName: 'StatusDate__c', type: 'date', sortable: 'false' },
                   {label: rowLabelLastModifiedBy, fieldName: 'lastModifiedBy', type: 'text', sortable: 'false' }
];


const oppColumns = [{label: rowLabelName, fieldName: 'opportunityId',  type: 'url', typeAttributes: { label: {fieldName: 'opportunityName'}, target:'_blank'}, sortable: true },
                    {label: rowLabelStatus, fieldName: 'Status__c', type: 'text', sortable: 'false'},
                    {label: rowLabelStatusSince, fieldName: 'LastStatusDate__c', type: 'date', sortable: 'false' }
];

const oppBrandColumns = [{label: rowLabelName, fieldName: 'opportunityId',  type: 'url', typeAttributes: { label: {fieldName: 'opportunityName'}, target:'_blank'}, sortable: true },
                        {label: rowLabelAccount, fieldName: 'accountName', type: 'text', sortable: 'false'},
                        {label: rowLabelStatus, fieldName: 'Status__c', type: 'text', sortable: 'false'},
                        {label: rowLabelStatusSince, fieldName: 'LastStatusDate__c', type: 'date', sortable: 'false' }
];


// search result columns
const searchColumns = [{type: 'button-icon',initialWidth: 36,typeAttributes: {iconName: 'utility:add',title: 'add',variant: 'bare',disabled: { fieldName: 'disabled' }}},
                        { label: rowLabelName, fieldName: 'Name', type: 'text', sortable: 'false' },
                        {label: rowLabelEmail, fieldName: 'Email', type: 'email',sortable: 'false'},
                        {label: rowLabelAccount, fieldName: 'accountName', type: 'text', sortable: 'false'}
];

const INVESTOR_FIELDS = ['AccountToOpportunity__c.Account__c',
                         'AccountToOpportunity__c.Account__r.Brand__r.Name',
                         'AccountToOpportunity__c.Account__r.Brand__c',
                         'AccountToOpportunity__c.Account__r.Name',
                         'AccountToOpportunity__c.Account__r.BillingStreet',
                         'AccountToOpportunity__c.Account__r.BillingCity',
                         'AccountToOpportunity__c.Account__r.BillingPostalCode',
                         'AccountToOpportunity__c.CreatedDate',
                         'AccountToOpportunity__c.CreatedBy.Name',
                         'AccountToOpportunity__c.Source__c'];



export default class InvestorsListSidePanel extends NavigationMixin(LightningElement) {
    @api accountId;
    @api opportunityId;
    _selectedRecordId;

    contactToDelete;
    contactNameToDelete;

    // detail-tables
    @track selectedRecordActivities;
    @track selectedRecordContacts;
    @track selectedRecordHistory;
    @track selectedRecordActivityColumns = srColumns;
    @track selectedRecordContactColumns = ctColumns;
    @track selectedRecordHistoryColumns = hiColumns;
    @track selectedRecordOpporunityColumns = oppColumns;
    @track selectedRecordBrandOpporunityColumns = oppBrandColumns;

    @track searchColumns = searchColumns;
    @track activeSections = ['investorDetails','contacts', 'activities', 'statusHistory', 'openOpportunities', 'openBrandOpportunities'];

    // contact popover
    @track contactId;

    @track top;
    @track left;
    @track showConfirmDelete = false;

    // add contact
    wiredSearchResult;
    selectedContacts = [];
    excludeContacts = [];
    @track searchKey;
    @track prevSearchKey;
    @track searchResult;
    @track searchAll = false;
    @track selectedFilter = 'account';
    @track contactSearchLimit = CONTACT_SEARCH_LIMIT + 1;
    @track contactInfiniteLoadingEnabled = true;

    filterOptions = [
        { label: searchAccount, value: 'account' },
        { label: searchAll, value: 'global' }
    ];

    // UI-Labels
    label = {
        activities: tabActivities,
        contacts: tabContacts,
        statusHistory: tabHistory,
        close: cancelDeleteContact,
        delete: deleteContact,
        headline: confirmDeleteHeadline,
        confirm: confirmDeleteText,
        deleteError: deleteError,
        deleteSuccess: deleteSuccess,
        deleteSuccessTitle: deleteSuccessTitle,
        addContact: addContact,
        contactSearchPlaceholder: contactSearchPlaceholder,
        searchAccount: searchAccount,
        searchAll: searchAll,
        viewAll: viewAll,
        investorDetails : investorDetails,
        investorDetailsAddedOn : investorDetailsAddedOn,
        investorDetailsAddedBy : investorDetailsAddedBy,
        investorDetailsAddedThrough : investorDetailsAddedThrough,
        sectionOpenOpps : sectionOpenOpps,
        sectionStatusHistory : sectionStatusHistory,
        sectionActivities : sectionActivities,
        sectionContact : sectionContact,
        sectionOpenBrandOpps : sectionOpenBrandOpps,
        contactSearchMessageLabel : contactSearchMessageLabel
    };


    // Related List Parameters
    @track numberOfMaximumRows = 5;

    /********************
     * LIFECYCLE
     ********************/

    /********************
     * WIRE SERVICE
     ********************/

     // Data for investordetails
    @track investorAccountId;
    @track investorAccountName;
    @track investorAccountBrand;
    @track investorAccountBrandId;
    @track investorAccountStreet;
    @track investorAccountCity;
    @track investorAccountPostalCode;
    @track investorAccountCreatedBy;
    @track investorAccountCreatedDate;
    @track investorAccountSource;
    @wire(getRecord, { recordId: '$_selectedRecordId', fields: INVESTOR_FIELDS })
    wiredInvestor({ error, data }) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading contact',
                    message,
                    variant: 'error',
                }),
            );
        } else if (data) {
            this.investorAccountId = data.fields.Account__c.value;
            this.investorAccountName = data.fields.Account__r.displayValue;
            this.investorAccountBrand = data.fields.Account__r.value.fields.Brand__r.displayValue;
            this.investorAccountBrandId = data.fields.Account__r.value.fields.Brand__c.value;
            this.investorAccountCity = data.fields.Account__r.value.fields.BillingCity.value;
            this.investorAccountPostalCode = data.fields.Account__r.value.fields.BillingPostalCode.value;
            this.investorAccountStreet = data.fields.Account__r.value.fields.BillingStreet.value;
            if(this.investorAccountStreet !== null){
                this.investorAccountStreet += ',';
            }else{
                this.investorAccountStreet = '-';
            }

            this.investorAccountCreatedBy = data.fields.CreatedBy.displayValue;
            this.investorAccountCreatedDate = data.fields.CreatedDate.displayValue;
            this.investorAccountSource = data.fields.Source__c.displayValue;
            this.getOpenOpportunities(this.investorAccountId);
            this.getOpenBrandOpportunities(this.investorAccountBrandId);
            this.getStatusHistory(this._selectedRecordId);
        }
    }


    @track
    contactSearchMessage;

    @wire(searchContacts, {
        searchKey: '$searchKey',
        excludeContacts: '$excludeContacts',
        accountId: '$investorAccountId',
        globalSearch: '$searchAll',
        contactSearchLimit: '$contactSearchLimit'
    })
    wiredContacts(wiredSearchResult) {

        this.wiredSearchResult = wiredSearchResult;
        const { data, error } = wiredSearchResult;

        if (data) {
            const search = this.template.querySelectorAll(
                "lightning-input[type='search']"
            );
            if (search) {
                search.isLoading = false;
            }

            this.searchResult = [];

            let len = data.length > CONTACT_SEARCH_LIMIT ? CONTACT_SEARCH_LIMIT : data.length;
            this.contactSearchMessage = data.length > CONTACT_SEARCH_LIMIT ? this.label.contactSearchMessageLabel : '';
            console.log('...data.length: ' + data.length + ' CONTACT_SEARCH_LIMIT: ' + CONTACT_SEARCH_LIMIT);

            for (let i = 0; i < len; i++) {
                let result = Object.assign({}, data[i]);
                result.disabled = false;

                if (result.Account && result.Account.Name) {
                    result.accountName = result.Account.Name;
                }

                this.searchResult.push(result);
            }

            this.sortByValues(this.searchResult, ['accountName', 'Name']);
        } else if (error) {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        }
    }

    /********************
     * INTERNAL FUNCTIONS
     ********************/
    @track openOppData;
    getOpenOpportunities(selectedRecordId){
        search({
            list_fieldApiName: ['Opportunity__c', 'Opportunity__r.Name', 'toLabel(Status__c)', 'LastStatusDate__c'],
            string_objectApiName: 'AccountToOpportunity__c',
            string_searchTerm: selectedRecordId,
            string_whereApiName: 'Account__c',
            string_filter : 'Opportunity__r.IsClosed = false AND Opportunity__c != \'' + this.opportunityId + '\'',
            string_soqlOperator: '=',
            string_orderBy: 'Name',
            integer_limit: 5,
            integer_offset: 0
        })
        .then(result => {
            const tempResult = JSON.parse(JSON.stringify(result));
            let _openOppData = this._preprocessOpportunityData(tempResult);
            this.openOppData = JSON.parse(JSON.stringify(_openOppData));

        }).catch(error => {

            console.error(JSON.stringify(error, null, '\t'));
        })

    }

    @track openBrandOppData;
    getOpenBrandOpportunities(selectedBrandRecordId){
        if(selectedBrandRecordId != null){
            search({
                list_fieldApiName: ['Opportunity__c', 'Opportunity__r.Name', 'Account__r.Name', 'LastStatusDate__c', 'toLabel(Status__c)'],
                string_objectApiName: 'AccountToOpportunity__c',
                string_searchTerm: selectedBrandRecordId,
                string_whereApiName: 'Account__r.Brand__c',
                string_filter : 'Opportunity__r.IsClosed = false AND Opportunity__c != \'' + this.opportunityId + '\'',
                string_soqlOperator: '=',
                string_orderBy: 'Opportunity__r.CloseDate desc',
                integer_limit: 5,
                integer_offset: 0
            })
            .then(result => {
                console.log(JSON.stringify(result, null, '\t'));
                const tempResult = JSON.parse(JSON.stringify(result));
                let _openOppData = this._preprocessBrandOpportunityData(tempResult);
                this.openBrandOppData = JSON.parse(JSON.stringify(_openOppData));

            }).catch(error => {
                console.error(JSON.stringify(error, null, '\t'));
            })
        }
    }

    @track statusHistoryData;
    getStatusHistory(selectedRecordId){
        search({
            list_fieldApiName: ['toLabel(InvestorStatus__c)', 'LastModifiedBy.Name', 'StatusDate__c'],
            string_objectApiName: 'Status__c',
            string_searchTerm: selectedRecordId,
            string_whereApiName: 'Investor__c',
            string_filter : '',
            string_soqlOperator: '=',
            string_orderBy: 'CreatedDate desc',
            integer_limit: 5,
            integer_offset: 0
        })
        .then(result => {
            const tempResult = JSON.parse(JSON.stringify(result));
            let _statusHistoryData = this._preprocessStatusHistoryData(tempResult);
            this.statusHistoryData = JSON.parse(JSON.stringify(_statusHistoryData));

        }).catch(error => {
            console.error(error);
        })

    }

    _preprocessStatusHistoryData(result){
        let preprocessData = [];

        result.forEach(element => {

            if(typeof element.LastModifiedBy.Name !== 'undefined'){
                element.lastModifiedBy = element.LastModifiedBy.Name;
            }

            element.statusId = '/' + element.Id;
            element.statusName = element.InvestorStatus__c;

            preprocessData.push(element);
        });

       return preprocessData;
    }

    _preprocessOpportunityData(result){
        let preprocessData = [];
        result.forEach(element => {
            element.opportunityId = '/' + element.Opportunity__c;
            element.opportunityName = element.Opportunity__r.Name;

            preprocessData.push(element);
        });

       return preprocessData;
    }

    _preprocessBrandOpportunityData(result){
        let preprocessData = [];
        result.forEach(element => {
            console.log(JSON.stringify(element, null, '\t'));
            element.opportunityId = '/' + element.Opportunity__c;
            element.opportunityName = element.Opportunity__r.Name;
            element.accountName = element.Account__r.Name;

            preprocessData.push(element);
        });

       return preprocessData;
    }

    /**
     *
     * @param {*} selectedRecordId Id of the currently single selected record in the datatable
     *
     * @description handles row selection data fetch if there is a selection, otherwise it empties the selected record.
     */
    async selectRecordForDetails(selectedRecordId) {
        if (selectedRecordId) {
            // fetch data for Id
            const relatedData = await getRelatedData({
                recordId: selectedRecordId
            });

            let activity;
            let activities = [];

            for (let i = 0; i < relatedData.Event.length; i++) {
                activity = {};
                activity.type = 'Event';
                activity.subject = relatedData.Event[i].Subject;
                activity.date = relatedData.Event[i].Date__c;
                activity.id = relatedData.Event[i].Id;
                activity.activityid = '/' + activity.id;
                activity.objectType = 'Event';
                if(typeof relatedData.Event[i].WhoId !== 'undefined'){
                    let relations = [];
                    if (relatedData.Event[i].EventRelations != undefined)
                    {
                        for (let j = 0; j < relatedData.Event[i].EventRelations.length; j++)
                        {
                            if (relatedData.Event[i].EventRelations[j].Relation.Type === 'Contact')
                            {
                                relations.push({
                                    relationId: relatedData.Event[i].EventRelations[j].RelationId,
                                    link: '/' + relatedData.Event[i].EventRelations[j].RelationId,
                                    relationName: relatedData.Event[i].EventRelations[j].Relation.Name
                                });
                            }
                        }
                    }
                    activity.whoid = relations.sort((a,b) => (a.relationName > b.relationName) ? 1 : ((b.relationName > a.relationName) ? -1 : 0));
                }

                activities.push(activity);
            }

            for (let i = 0; i < relatedData.Task.length; i++) {
                activity = {};
                activity.type = 'Task';
                activity.subject = relatedData.Task[i].Subject;
                activity.date = relatedData.Task[i].ActivityDate;
                activity.id = relatedData.Task[i].Id;
                activity.activityid = '/' + activity.id;
                activity.objectType = 'Task';

                if(typeof relatedData.Task[i].WhoId !== 'undefined'){
                    let relations = [];
                    if (relatedData.Task[i].TaskRelations != undefined)
                    {
                        for (let j = 0; j < relatedData.Task[i].TaskRelations.length; j++)
                        {
                            if (relatedData.Task[i].TaskRelations[j].Relation.Type === 'Contact')
                            {
                                relations.push({
                                    relationId: relatedData.Task[i].TaskRelations[j].RelationId,
                                    link: '/' + relatedData.Task[i].TaskRelations[j].RelationId,
                                    relationName: relatedData.Task[i].TaskRelations[j].Relation.Name
                                });
                            }
                        }
                    }
                    //activity.whoid = '/' + relatedData.Task[i].WhoId;
                    activity.whoid = relations.sort((a,b) => (a.relationName > b.relationName) ? 1 : ((b.relationName > a.relationName) ? -1 : 0)); ;
                    activity.contactname = relatedData.Task[i].Who.Name;
                }
                activities.push(activity);
            }
            // sort activities by Date
            activities.sort(function(a, b) {
                return b.date - a.date;
            });

            let filteredActivities = [];
            let numberOfMaxRows = activities.length > 5 ? 5 : activities.length;
            for (let i = 0; i < numberOfMaxRows; i++) {
                filteredActivities.push(activities[i]);
            }
            this.selectedRecordActivities = filteredActivities;

            let contacts = [];
            this.selectedContacts = [];
            for (let i = 0; i < relatedData.Contact.length; i++) {
                let contact = {};
                contact.investorRelation = relatedData.Contact[i].Id;
                contact.id = relatedData.Contact[i].Contact__c;
                contact.name = relatedData.Contact[i].Contact__r.Name;
                contact.contactUrl = '/' + relatedData.Contact[i].Contact__c;
                contact.firstName = relatedData.Contact[i].Contact__r.FirstName;
                contact.lastName = relatedData.Contact[i].Contact__r.LastName;
                contact.email = relatedData.Contact[i].Contact__r.Email;
                contact.accountName =
                    relatedData.Contact[i].Contact__r.Account.Name;
                contact.accountUrl = '/' + relatedData.Contact[i].Contact__r.AccountId;
                contacts.push(contact);

                this.selectedContacts.push(contact.id);
            }
            this.sortByValues(contacts, ['lastName', 'name']);
            this.selectedRecordContacts = contacts;

            let stati = [];
            for (let i = 0; i < relatedData.Status__c.length; i++) {
                let status = {};
                status.id = relatedData.Status__c[i].Id;
                status.state = relatedData.Status__c[i].Stage__c;
                status.date = relatedData.Status__c[i].StatusDate__c;
                status.lastModifiedByName = relatedData.Status__c[i].LastModifiedBy.Name;
                stati.push(status);
            }
            this.selectedRecordHistory = stati;
        } else {
            this.selectedRecordActivities = undefined;
            this.selectedRecordContacts = undefined;
            this.selectedRecordHistory = undefined;
        }
    }

    /**
     * sort contact search result array
     * @param {*} data
     * @param {*} values
     */
    sortByValues(data, values) {
        return data.sort(function(a, b) {
            var i, iLen, aChain, bChain;

            i = 0;
            iLen = values.length;
            for (i; i < iLen; i++) {
                aChain += a[values[i]];
                bChain += b[values[i]];
            }

            return aChain.localeCompare(bChain);
        });
    }

    /********************
     * EXPOSED FUNCTIONS
     ********************/

    @api set selectedRecordId(value) {
        this._selectedRecordId = value;
        this.selectRecordForDetails(value);
    }

    get selectedRecordId() {
        return this._selectedRecordId;
    }

    /**
     * refresh component
     */
    @api
    refresh() {
        if (this.selectedRecordId) {
            this.selectRecordForDetails(this.selectedRecordId);
            this.getStatusHistory(this.selectedRecordId);
        }
    }

    /********************
     * EVENT HANDLING
     ********************/

    handleShowDetail(event) {
        if (this.contactId) {
            return;
        }

        if (event.detail) {
            this.contactId = event.detail.contactId;
            this.top = event.detail.top;
            this.left = event.detail.left;
        }
    }

    handleHideDetail() {
        this.contactId = null;
    }

    /**
     * open delete ContactToInvestor__c confirm modal
     * @param {*} event
     */
    handleDeleteContact(event) {
        this.contactToDelete = event.detail.contactId;

        if (this.contactToDelete) {
            let contact = this.selectedRecordContacts.find(
                obj => obj.investorRelation === this.contactToDelete
            );

            if (contact) {
                this.contactNameToDelete = contact.name;
            }
        }

        // open confirm modal
        this.showConfirmDelete = true;
    }

    handleAddActivity(event){
        this.dispatchEvent(
            CustomEvent('actionbutton', {
                composed: true,
                bubbles: true,
                cancelable: true,
                detail: {
                    contactId : event.detail.contactId,
                    investorId : this._selectedRecordId
                }
            })
        );
    }


    handleViewAllOppsClick(event){
        this.relatedListPageReference = {
            type: 'standard__recordRelationshipPage',
            attributes: {
                recordId: this.investorAccountId,
                objectApiName: 'Account',
                relationshipApiName: 'Account_to_opportunities__r',
                actionName: 'view'
            }
        };
        this[NavigationMixin.GenerateUrl](this.relatedListPageReference).then(url => window.open(url));
    }

    handleViewAllBrandOppsClick(event){
        this.relatedListPageReference = {
            type: 'standard__navItemPage',
            attributes: {
                apiName : 'Opportunities_of_Brand'
            },
            state:{
                c__recordId: this.investorAccountBrandId
            }
        };

        this[NavigationMixin.GenerateUrl](this.relatedListPageReference).then(url => window.open(url));
    }

    handleViewAllContactsClick(event){
        this.relatedListPageReference = {
            type: 'standard__recordRelationshipPage',
            attributes: {
                recordId: this.selectedRecordId,
                objectApiName: 'AccountToOpportunity__c',
                relationshipApiName: 'InvestorContacts__r',
                actionName: 'view'
            }
        };

        this[NavigationMixin.GenerateUrl](this.relatedListPageReference).then(url => window.open(url));
    }

    handleViewAllActivitiesClick(event){
        this.relatedListPageReference = {
            type: 'standard__navItemPage',
            attributes: {
                apiName : 'All_Activities'
            },
            state:{
                c__recordId: this.selectedRecordId
            }
        };

        this[NavigationMixin.GenerateUrl](this.relatedListPageReference).then(url => window.open(url));
    }

    handleViewAllStatusHistoryClick(event){
        this.relatedListPageReference = {
            type: 'standard__recordRelationshipPage',
            attributes: {
                recordId: this.selectedRecordId,
                objectApiName: 'AccountToOpportunity__c',
                relationshipApiName: 'Status__r',
                actionName: 'view'
            }
        };

        this[NavigationMixin.GenerateUrl](this.relatedListPageReference)
            .then(url => window.open(url));
    }



    /**
     * close delete ContactToInvestor__c record confirm modal
     */
    closeModal() {
        this.contactToDelete = null;
        this.contactNameToDelete = null;
        this.showConfirmDelete = false;
    }

    /**
     * delete selected ContactToInvestor__c record
     */
    async deleteContact() {
        if (this.contactToDelete) {
            deleteRecord(this.contactToDelete)
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: this.label.deleteSuccessTitle,
                            message: this.label.deleteSuccess,
                            variant: 'success'
                        })
                    );

                    this.selectRecordForDetails(this.selectedRecordId).then(
                        () => {
                            this.excludeContacts = this.selectedContacts;
                            this.refresh();
                        }
                    );
                    const contactsChanged = new CustomEvent('contactschanged');
                    this.dispatchEvent(contactsChanged);
                })
                .catch(error => {
                    console.log('...error: ' + JSON.stringify(error));
                    let message = error.body.message;
                    if (error.body.output != undefined)
                    {
                        message += ' ' + this.constructOneMessageFromErrors(error.body.output.errors)
                    }
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: this.label.deleteError,
                            message: message,
                            variant: 'error'
                        })
                    );
                })
                .finally(() => {
                    this.closeModal();
                });
        }
    }

    constructOneMessageFromErrors(errors)
    {
        let message = '';
        errors.forEach(function(error){
            message += error.message + ' ';
        });

        return message;
    }

    /**
     * search contacts
     * @param {*} event
     */
    handleSearch(event) {
        console.log('...handleSearch');
        // Debouncing this method: Do not update the reactive property as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;

        if (searchKey.length >= 3) {
             console.log('...handleSearch searchKey.length>=3');
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            this.delayTimeout = setTimeout(() => {
                event.target.isLoading = true;
                // process search
                this.searchKey = searchKey;
                this.excludeContacts = this.selectedContacts;
            }, DELAY);
        } else if (searchKey === '') {
            this.searchKey = null;
            this.searchResult = null;
        }
    }



    /**
     * handle contact search filter change
     * @param {*} event
     */
    handleSearchFilter(event) {
        const selected = event.detail.value;

        if (selected) {
            this.searchAll = selected === 'global';
        }

        if (this.wiredSearchResult.data && this.searchKey) {
            refreshApex(this.wiredSearchResult);
        }
    }

    /**
     * add selected contact
     * @param {*} event
     */
    async handleRowAction(event) {
        const record = event.detail.row;

        const fields = {
            Investor__c: this.selectedRecordId,
            Contact__c: record.Id
        };
        const recordInput = { apiName: 'ContactToInvestor__c', fields };

        try {
            // disable add buttons in datatable
            this.disableAddButton(true);

            await createRecord(recordInput);

            // remove from searchResult
            const recordIndex = this.searchResult.findIndex(
                obj => obj.Id === record.Id
            );

            if (recordIndex !== -1) {
                this.searchResult = this.searchResult
                    .slice(0, recordIndex)
                    .concat(this.searchResult.slice(recordIndex + 1));
            }

            // update contacts datatable
            this.selectRecordForDetails(this.selectedRecordId);

            // enable add buttons
            this.disableAddButton(false);
            const contactAdded = new CustomEvent('contactschanged');
            this.dispatchEvent(contactAdded);
        } catch (error) {
            this.disableAddButton(false);

            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error creating Investor Contact',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        }
    }

    /**
     * disable add buttons an add contacts datatable
     * @param {*} disabled
     */
    disableAddButton(disabled) {
        if (this.searchResult) {
            for (let i = 0; i < this.searchResult.length; i++) {
                this.searchResult[i].disabled = disabled;
            }
        }
    }
}