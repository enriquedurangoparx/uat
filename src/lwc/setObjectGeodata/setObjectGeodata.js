import { LightningElement, wire, api, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import { getRecord } from 'lightning/uiRecordApi';
import leafl from '@salesforce/resourceUrl/leaflet';

// import custom labels:
import setObjectGeodataSetLocation from '@salesforce/label/c.SetObjectGeodataSetLocation';
import setObjectGeodataSaveLocation from '@salesforce/label/c.SetObjectGeodataSaveLocation';
import setObjectGeodataInitialHelpMessage from '@salesforce/label/c.SetObjectGeodataInitialHelpMessage';
import setObjectGeodataSetGeoLocation from '@salesforce/label/c.SetObjectGeodataSetGeoLocation';
import generalLabelCancel from '@salesforce/label/c.GeneralLabelCancel';
import generalLabelLatitude from '@salesforce/label/c.GeneralLabelLatitude';
import generalLabelLongitude from '@salesforce/label/c.GeneralLabelLongitude';
import generalLabelError from '@salesforce/label/c.GeneralLabelError';

var pageReference;
var map;
var marker;

export default class SetObjectGeodata extends LightningElement
{
    @api recordId;

    label = {
        setObjectGeodataSetLocation,
        setObjectGeodataSaveLocation,
        setObjectGeodataInitialHelpMessage,
        setObjectGeodataSetGeoLocation,
        generalLabelCancel,
        generalLabelLatitude,
        generalLabelLongitude,
        generalLabelError
    }

    @wire(CurrentPageReference)
    wiredPageRef(pageRef) {
      this.pageRef = pageRef;
      pageReference = pageRef;
      if(this.pageRef) registerListener('SetObjectGeodata__setLocation', this.setLocation, this);
    }
    @track objectRecord;
    @track objectRecordLatitude;
    @track objectRecordLongitude;

    @wire(getRecord, { recordId: '$recordId', fields: ['PropertyObject__c.Geolocation__Latitude__s', 'PropertyObject__c.Geolocation__Longitude__s'] })
    wiredRecord({ error, data }) {
        if (error) 
        {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) 
            {
                message = error.body.map(e => e.message).join(', ');
            } 
            else if (typeof error.body.message === 'string') 
            {
                message = error.body.message;
            }
            this.dispatchEvent(
                new ShowToastEvent({
                    title: this.generalLabelError,
                    message,
                    variant: 'error',
                }),
            );
        } 
        else if (data) 
        {
            this.objectRecord = data;
            this.objectRecordLatitude = this.objectRecord.fields.Geolocation__Latitude__s.value;
            this.objectRecordLongitude = this.objectRecord.fields.Geolocation__Longitude__s.value;

            if (this.initialized) {
                return;
            }
            this.initialized = true;
            Promise.all([
                loadScript(this, leafl + '/leaflet-src.js'),
                loadStyle(this, leafl + '/leaflet.css'),
            ])
                .then(() => {
                    this.initializeLeafLet()
                })
                .catch(error => {
                    console.error(error);
                });
        }
    }

    @track helperMessage = this.label.setObjectGeodataInitialHelpMessage;

    @track setLocationButtonLabel = this.label.setObjectGeodataSetLocation;
    @track currentLocation = [];
    
    initialized = false;
    @track mapEditMode = false;

    @track newLatitude;
    @track newLongitude;

    get shwoNewLatitude()
    {
        return this.newLatitude;
    }

    get saveButtonIsActive()
    {
        return this.mapEditMode === false && this.newLatitude && this.newLongitude ? true : false;
    }

    get showHelperMessage()
    {
        return this.mapEditMode === false ? true : false;
    }

    get showLocation()
    {
        return this.mapEditMode && this.newLatitude && this.newLongitude ? true : false;
    }

    disconnectedCallback() {
		unregisterAllListeners(this);
    }

    initializeLeafLet() {
        const object = {
            lat: this.objectRecordLatitude,
            lng: this.objectRecordLongitude
        }
        fireEvent(pageReference, 'SetObjectGeodata__setLocation', object);
        let center = [];
        if (this.objectRecordLatitude && this.objectRecordLongitude)
        {
            center = [this.objectRecordLatitude, this.objectRecordLongitude];
        }
        else
        {
            center = [52.5200, 13.4050];
        }
        const mapRoot = this.template.querySelector(".map-root")
        map = L.map(mapRoot, {
			center: center,
			minZoom: 3,
			zoom: 18,
			setView: true
        });
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
			subdomains: ['a','b','c']
        }).addTo( map );
        
        if (this.objectRecordLatitude && this.objectRecordLongitude)
        {
            marker = L.marker([this.objectRecordLatitude, this.objectRecordLongitude]).addTo(map); // [latitude, longitude]
        }
        else
        {
            marker = null;
        }
    }

    onMapClick(e) {
        const object = {
            lat: e.latlng.lat,
            lng: e.latlng.lng
        }
        if (marker !== null) {
            map.removeLayer(marker);
        }
        marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);

        fireEvent(pageReference, 'SetObjectGeodata__setLocation', object);
    }

    setLocation(event)
    {
        this.newLatitude = event.lat;
        this.newLongitude = event.lng;
    }

    handleEditMode()
    {
        this.setLocationButtonLabel = this.setLocationButtonLabel === this.label.setObjectGeodataSetLocation ? this.label.generalLabelCancel : this.label.setObjectGeodataSetLocation;
        this.mapEditMode = !this.mapEditMode;
        if (this.mapEditMode)
        {
            map.on('click', this.onMapClick);
        }
        else
        {
            this.setInitialMapMarker();
            map.off('click');
        }
    }

    setInitialMapMarker()
    {
        if (marker !== null) {
            map.removeLayer(marker);
        }
        this.newLatitude = this.objectRecordLatitude;
        this.newLongitude = this.objectRecordLongitude;
        marker = L.marker([this.objectRecordLatitude, this.objectRecordLongitude]).addTo(map);
    }
    
    showSaveModal()
    {
        const child = this.template.querySelector('c-set-object-geodata-confirm-modal');

        child.openModal({'Id' : this.recordId, 'Geolocation__Latitude__s' : this.newLatitude, 'Geolocation__Longitude__s' : this.newLongitude});

    }

    handleUpdate()
    {
        this.setLocationButtonLabel = this.setLocationButtonLabel === this.label.setObjectGeodataSetLocation ? this.label.generalLabelCancel : this.label.setObjectGeodataSetLocation;
        this.mapEditMode = !this.mapEditMode;
        map.off('click');
    }
}