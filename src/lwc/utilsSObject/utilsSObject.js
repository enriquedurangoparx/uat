/**
 * @author Maksim Fedorneko <maksim.fedorenko@parx.com>
 * SObject utility functions
 */
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import successLabel from '@salesforce/label/c.GeneralLabelSuccess';
import errorLabel from '@salesforce/label/c.GeneralLabelError';

/**
 * Returns translated label for the specified fieldDescriptionObject
 * @param fieldDescriptionObject field description that comes from @salesforce/schema/OBJECT_NAME__c.FIELD_NAME__c
 * @param objectInfo object info that comes from getObjectInfo lightning/uiObjectInfoApi method
 * @returns {string} Translation for the specified field
 */
const getFieldLabel = (fieldDescriptionObject, objectInfo) => {
    let result = '';
    const fieldApiName = fieldDescriptionObject['fieldApiName'];
    if (objectInfo && objectInfo["data"] && fieldApiName) {
        result = objectInfo["data"]["fields"][fieldApiName]["label"];
    }
    return result;
};

/**
 * Returns the array of {attributes: null, label: "FIELD_LABEL", validFor: Array(0), value: "FIELD_VALUE"} objects
 * @param picklistValues object info that comes from getPicklistValues lightning/uiObjectInfoApi method
 * @returns {Array}
 */
const getPicklistOptions = (picklistValues) => {
    let result = [];
    if (picklistValues && picklistValues["data"] && picklistValues["data"]["values"]) {
        result = picklistValues["data"]["values"];
    }
    return result;
};

const updateSObject = (record, successMsg, callback) => {
    updateRecord({ fields: record })
    .then(() => {
        showToast(successLabel, successMsg, 'success');
        callback(true);
    })
    .catch(error => {
        showErrorToast(error, 'utilsSObject::updateSObject');
        callback(false);
    });
};

const showErrorToast = (error, sourceComponentName) => {
    let message = '';
    let title = 'Error';
    if (error['body']) {
        if (error['body']['exceptionType']) {
            // exception
            title = error['body']['exceptionType'];
            message = error['body']['message'] + '. ' + error['body']['stackTrace']
        } else if(error['body']['pageErrors']) {
            for (const pageError of error['body']['pageErrors']) {
                message += pageError['statusCode'] + ': ' + pageError['message'];
            }
        } else if (error['body']['message']) {
            message = error['body']['message'];
        } else {
            message = JSON.stringify(error);
        }
    }
    showToast(title, message, TOAST_VARIANT.ERROR, TOAST_MODE.STICKY);
    console.log('⛔ Error in ' + sourceComponentName + ' lwc component', JSON.parse(JSON.stringify(error)));
};

/**
 * The theme and icon displayed in the toast. Valid values are:
 * info—(Default) A gray box with an info icon.
 * success—A green box with a checkmark icon.
 * warning—A yellow box with a warning icon.
 * error—A red box with an error icon.
 */
const TOAST_VARIANT = { INFO: 'info', SUCCESS: 'success', WARNING: 'warning', ERROR: 'error' };

/**
 * Determines how persistent the toast is. Valid values are:
 * dismissable—(Default) Remains visible until the user clicks the close button or 3 seconds has elapsed, whichever comes first.
 * pester—Remains visible for 3 seconds.
 * sticky—Remains visible until the user clicks the close button.
 */
const TOAST_MODE = { DISMISSABLE: 'dismissable', PESTER: 'pester', STICKY: 'sticky' };

const showToast = (title, message, variant, mode) => {
    dispatchEvent(
        new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: mode ? mode : TOAST_MODE.DISMISSABLE
        }),
    );
};

export {
    getFieldLabel,
    getPicklistOptions,
    updateSObject,
    showToast,
    showErrorToast,
    TOAST_VARIANT,
    TOAST_MODE
};