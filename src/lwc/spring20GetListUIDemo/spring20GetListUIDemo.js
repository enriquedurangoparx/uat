import { LightningElement, track, wire } from 'lwc';
import { getListUi } from 'lightning/uiListApi';
import CONTACT_OBJECT from '@salesforce/schema/Contact';

export default class Spring20GetListUIDemo extends LightningElement 
{
   @track contactSelectedListView = '';
   @track testData;

   @wire(getListUi, { objectApiName: CONTACT_OBJECT,  listViewApiName: '$contactSelectedListView'})
   wiredContactsByListApiName({ error, data }) {
       if (data) {
            console.log('>>> wiredContactsByListApiName');
            console.log(JSON.stringify(data));
            this.testData = JSON.stringify(data);
            // this.searchObjects('Contact', data.records.records);
       } else if (error) {
           this.error = error;
       }
   }
}