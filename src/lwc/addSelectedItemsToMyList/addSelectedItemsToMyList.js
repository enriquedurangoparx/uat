import { LightningElement, api, track } from 'lwc';
import { reduceErrors } from 'c/lwcUtil';

import recordsSearch from '@salesforce/apex/SearchController.search';
import createInvestorListItem from '@salesforce/apex/LWCAddSelectedItemsToMyList.createInvestorListItemRecords';
import createInvestorListItemContact from '@salesforce/apex/LWCAddSelectedItemsToMyList.createInvestorListItemContactsRecords';

// import custom labels:
import applicationNameLabel from '@salesforce/label/c.AddToMyListApplicationName';
import addActionLabel from '@salesforce/label/c.AddToMyListAddAction';
import searchLabel from '@salesforce/label/c.AddToMyListSearchLabel';
import searchPlaceholderLabel from '@salesforce/label/c.AddToMyListSearchPlaceholderLabel';
import contactIncludedNotificationLabel from '@salesforce/label/c.AddToMyListContactIncludedNotification';
import selectAtLeastOneRecordLabel from '@salesforce/label/c.AddToMyListSelectAtLeastOneRecord';
import existingRecordsLabel from '@salesforce/label/c.AddToMyListExistingRecords';
import newRecordsLabel from '@salesforce/label/c.AddToMyListNewRecords';
import backLabel from '@salesforce/label/c.GeneralLabelBack';
// custom labels for new record window:
import generalLabelClose from '@salesforce/label/c.GeneralLabelClose';
import generalLabelCancel from '@salesforce/label/c.GeneralLabelCancel';
import generalLabelSave from '@salesforce/label/c.GeneralLabelSave';
import addToMyListNewInvestorsList from '@salesforce/label/c.AddToMyListNewInvestorsList';
import addToMyListRecordCreated from '@salesforce/label/c.AddToMyListRecordCreated';
import addToMyListHere from '@salesforce/label/c.AddToMyListHere';
import generalLabelError from '@salesforce/label/c.GeneralLabelError';
import generalLabelSuccess from '@salesforce/label/c.GeneralLabelSuccess';


export default class AddSelectedItemsToMyList extends LightningElement 
{
    @api objectApiName;
    @api selectedRecords;
    @api returnUrl;

    label = {
        applicationNameLabel,
        addActionLabel,
        searchLabel,
        searchPlaceholderLabel,
        contactIncludedNotificationLabel,
        selectAtLeastOneRecordLabel,
        existingRecordsLabel,
        newRecordsLabel,
        backLabel
    }

    // configuration-objects for lookup components
    searchString = {
        objectApiName: 'InvestorsList__c', 
        fieldApiName: 'Name', searchTerm: '', 
        isLikeSearch: true, 
        orderBy: 'CreatedDate DESC, Name ASC', 
        isMultiEntry: false, 
        addNewRecordModeIsOn: true,
        newRecordLabel: addToMyListNewInvestorsList
    };

    addRecordLabels = {
        close: generalLabelClose,
        cancel: generalLabelCancel,
        save: generalLabelSave,
        modalHeader: addToMyListNewInvestorsList,
        successMessage: addToMyListRecordCreated,
        success: generalLabelSuccess,
        error: generalLabelError,
        here: addToMyListHere
    };

    @track selectedRecordId;
    @track response;
    @track isLoading = false;

    // add new record functionality
    addRecordObjectApiName = 'InvestorsList__c';
    addRecordFields = [
        {fieldApiName: 'Name', required: true},
        {fieldApiName: 'Description__c', required: false},
        {fieldApiName: 'AccessType__c', required: false}
    ];

    get recordSelected()
    {
        return this.selectedRecordId ? false : true;
    }

    get showComponent()
    {
        return this.selectedRecords && this.selectedRecords.length > 0 ? true : false;
    }

    get showExistingRecords()
    {
        if (this.objectApiName === 'Account')
        {
            return this.response && this.response.existingInvestorsListItems && this.response.existingInvestorsListItems.length > 0 ? true : false;
        }
        else if (this.objectApiName === 'Contact')
        {
            return this.response && this.response.existingInvestorsListItemsContact && this.response.existingInvestorsListItemsContact.length > 0 ? true : false;
        }
        return false;
    }

    get showNewRecords()
    {
        if (this.objectApiName === 'Account')
        {
            return this.response && this.response.newInvestorsListItems && this.response.newInvestorsListItems.length > 0 ? true : false;
        }
        else if (this.objectApiName === 'Contact')
        {
            return this.response && this.response.newInvestorsListItemsContact && this.response.newInvestorsListItemsContact.length > 0 ? true : false;
        }
        return false;
    }

    get isAccount()
    {
        return this.objectApiName === 'Account' ? true : false;
    }

    get investorListURLName()
    {
        return this.response && this.response.investorListId ? '/' + this.response.investorListId : '';
    }

    get investorListName()
    {
        return this.response && this.response.investorListName ? this.response.investorListName : '';
    }

    handleRecordsSearch(event)
    {
        this.searchString.searchTerm = event.detail.searchTerm;
        let searchParam = {
            searchJSON: JSON.stringify(this.searchString)
        };

        recordsSearch(searchParam)
            .then(results => {
                this.template
                    .querySelector('[data-id="nameSearchInput"]')
                    .setSearchResults(results);
                if (event.detail.recordCreated)
                {
                    this.template.querySelector('[data-id="nameSearchInput"]').invokeHandleResultClick(event.detail.createdRecordId);
                }
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.\n' +
                        reduceErrors(error).join(', '),
                    'error'
                );
            });
    }

    handleRecordChange(event)
    {
        if(event.target.selection && event.target.selection.length > 0) {
            this.selectedRecordId = event.target.selection[0].id;
        } else {
            this.selectedRecordId = undefined;
        }
    }

    handleRecordSelectionChange(event)
    {
        if(event.target.selection && event.target.selection.length > 0) {
            for(let i=0; i<event.target.selection.length; i++) {
                this.selectedRecordId = event.target.selection[i].id;
            }
        } else {
            this.selectedRecordId = undefined;
        }
    }

    handleCreateRecord()
    {
        this.isLoading = true;

        let params = {
            recordIds: this.selectedRecords,
            investorListId: this.selectedRecordId
        }
        if (this.objectApiName === 'Account')
        {
            createInvestorListItem(params)
            .then(results => {
                this.response = JSON.parse(results);
                this.isLoading = false;
            })
        }
        else if (this.objectApiName === 'Contact')
        {
            createInvestorListItemContact(params)
            .then(results => {
                this.response = JSON.parse(results);
                this.isLoading = false;
            })
        }
    }

    handleSuccessNewRecordCreation(event)
    {
        let createdRecord = JSON.parse(event.detail);
        if (createdRecord && createdRecord.fields && createdRecord.fields.Name && createdRecord.fields.Name.value)
        {
            let evnt = {
                detail: {
                    searchTerm: '',
                    recordCreated: false,
                    createdRecordId: ''
                }
            }
            evnt.detail.searchTerm = createdRecord.fields.Name.value;
            evnt.detail.recordCreated = true;
            evnt.detail.createdRecordId = createdRecord.id;
            this.handleRecordsSearch(evnt);
        }
    }
}