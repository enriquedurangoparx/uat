import { LightningElement, api } from 'lwc';

export default class InvestorsListContactsList extends LightningElement 
{
    @api rowId;
    _listOfContacts;

    set listOfContacts(value)
    {
        let tempList = value ? value.split(";") : [];
        let tempListOfContacts = [];
        if (tempList.length > 0)
        {
            tempList.forEach(contactRow => {
                tempListOfContacts.push(contactRow.substring(20));
            });
        }
        this._listOfContacts = tempListOfContacts;
    }

    @api
    get listOfContacts()
    {
        return this._listOfContacts;
    }
}