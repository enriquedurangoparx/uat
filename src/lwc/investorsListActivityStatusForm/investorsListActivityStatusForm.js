import { LightningElement, wire, api, track } from 'lwc';

import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import STAGE_FIELD from '@salesforce/schema/AccountToOpportunity__c.Status__c';

// import custom label
import generalLabelNone from '@salesforce/label/c.GeneralLabelNone';
import status from '@salesforce/label/c.InvestorListInvestorStatus';
import generalLabelSelectAnOption from '@salesforce/label/c.GeneralLabelSelectAnOption';

const DEFAULT_RECORDTYPE = '012000000000000AAA';

export default class InvestorsListActivityStatusForm extends LightningElement {

    // UI Labels
    label = {
        status,
        generalLabelSelectAnOption
    }

    @wire(getPicklistValues, { recordTypeId: DEFAULT_RECORDTYPE, fieldApiName: STAGE_FIELD })
        picklistValues;

    // ema : US 2304 : added variable to handle situation when we try to invoke picklistValues before it was fetched
    @track
    isPicklistValuesFetched = false;

    // ema : US 2304 : copy of picklistValues variable with --None-- label
    @track
    _picklistValues = [];

    @api set selectedValue(value){
        this._selectedValue = value;
    }

    _selectedValue;
    get selectedValue(){
        return this._selectedValue;
    }

    handleComboboxChange(event){
        if (event.detail.value === generalLabelNone)
        {
            this._selectedValue = '';
        }
        else
        {
            this._selectedValue = event.detail.value;
        }

        let statusLabel = '';
        let statusValue = '';

        if (this._picklistValues)
        {
            this._picklistValues.forEach(function(picklistObj)
            {
                if (event.detail.value === picklistObj.value && event.detail.value !== generalLabelNone)
                {
                    statusLabel = picklistObj.label;
                    statusValue = picklistObj.value;
                }
            });
        }

        const evnt = CustomEvent('statuschange', {
            detail: {
                selectedstatus: statusLabel,
                selectedValue: statusValue
            },
        });
        this.dispatchEvent(evnt);
    }

    renderedCallback()
    {
        // ema : US 2304 adjustments starts
        let tempPicklistValues = [
            {
                label : generalLabelNone,
                value : generalLabelNone
            }
        ];

        if (this.picklistValues && this.picklistValues.data && this.isPicklistValuesFetched === false)
        {
            this.isPicklistValuesFetched = true;
        }

        if (this.isPicklistValuesFetched && this._picklistValues.length === 0 && this.picklistValues.data)
        {
            this.picklistValues.data.values.forEach(function(picklistObj)
            {
                tempPicklistValues.push(
                    {
                        label : picklistObj.label,
                        value : picklistObj.value
                    }
                )
            });
            this._picklistValues = tempPicklistValues;
        }
        // ema : US 2304 adjustments ends
    }

    @api
    set predifinedValue(label)
    {
        let tempSelectedValue = '';
        if (this._picklistValues)
        {
            this._picklistValues.forEach(function(picklistObj)
            {
                if (label === picklistObj.label && picklistObj.label !==generalLabelNone)
                {
                    tempSelectedValue = picklistObj.value;
                }
            });
        }
        this._selectedValue = tempSelectedValue;
    }

    get predifinedValue()
    {
        return this._selectedValue;
    }
}