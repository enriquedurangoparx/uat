import { LightningElement, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';

const DELAY = 350;


export default class PropertyFilter extends LightningElement {
    @track searchKey = '';
    actualYear = new Date().getFullYear();
    @track minYearOfConstruction = this._getMinYear();
    @track maxYearOfConstruction = this._getMaxYear();
    @track searchKey = '';

    @wire(CurrentPageReference) pageRef;
    
    connectedCallback() {
        registerListener(
            'ReturnRange__yearOfConstructionRangeChange',
            this.handleYearOfConstructionFilterChange,
            this
        );
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    handleYearOfConstructionFilterChange(filters) {
        this.minYearOfConstruction = filters.minValue;
        this.maxYearOfConstruction = filters.maxValue;
        
        this.fireChangeEvent();
    }

    handleReset() {
        this.searchKey = '';
        this.minYearOfConstruction = this._getMinYear();
        this.maxYearOfConstruction = this._getMaxYear();
     
        this.template.querySelector('c-return-range').resetMaxMinValue(this.minYearOfConstruction, this.maxYearOfConstruction);
        
        this.fireChangeEvent();
    }

    /**
     * Handle change event for search key.
     */
    changeHandler(event){
        const field = event.target.name;
        const value = event.target.value;
        
        switch(field){
            case 'searchKey':
                this.searchKey = value;
                break;
            default:
        }

        this.fireChangeEvent();
    }
 
    _getMinYear(){
        return this.actualYear - 50;
    }

    _getMaxYear(){
        return this.actualYear + 20;
    }

    fireChangeEvent() {
        // Debouncing this method: Do not actually fire the event as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex
        // method calls in components listening to this event.
        window.clearTimeout(this.delayTimeout);
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            const filters = {
                string_searchKey: this.searchKey,
                string_minYearOfConstruction: this.minYearOfConstruction,
                string_maxYearOfConstruction: this.maxYearOfConstruction
                
            };
            fireEvent(this.pageRef, 'PropertyFilter__filterChange', filters);
        }, DELAY);
    }
}