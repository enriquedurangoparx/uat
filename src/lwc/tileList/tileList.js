/**
 * @description Represents a list of tiles.
 * @class
 * @author Sebastian Meier
 * @history 
 * @param {object} tile - The title of the component.
 * @param {number} totalItemCount - The total number of items to show.
 */
import { LightningElement, api, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { unregisterAllListeners, fireEvent } from 'c/pubsub';
const PAGE_SIZE = 9;

export default class TileList extends LightningElement {
    @track pageNumber = 1;
    @track pageSize = PAGE_SIZE;

    @track error = '';
    @api tiles;
    @api totalItemCount;

    disconnectedCallback() {
        console.log('>>>TileList disconnectedCallback'); 
        unregisterAllListeners(this);
    }

    @wire(CurrentPageReference) pageRef;

    handlePreviousPage() {
        console.log('>>>TileList handlePreviousPage'); 
        this.pageNumber = this.pageNumber - 1;
        this.handlePageChange();
    }

    handleNextPage() {
        console.log('>>>TileList handleNextPage'); 
        this.pageNumber = this.pageNumber + 1;
        this.handlePageChange();
    }

    handlePageChange()
    {
        const pageChange = new CustomEvent(
            'pagechange',
            { 
                detail: {
                    pageNumber: this.pageNumber,
                }
            }
        );
        this.dispatchEvent(pageChange);
    }

    /**
     * Fires if a tile is selected.
     * 
     * @function
     * @param {event} event
     */
    handleTileSelected(event) {
        console.log('>>>TileList -- handleTileSelected');
        console.log('>>>event: ' + JSON.stringify(event, null, '\t'));
        fireEvent(this.pageRef, 'TileList__tileSelected', event.detail);
    }
}