import { LightningElement, wire, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';

import viewAll from '@salesforce/label/c.RelatedListViewAll';

import search from '@salesforce/apex/GenericSearch.search';

const URL_DELIMITER = '/';

// constants for sorting by default
const DEFAULT_SORT_FIELD = 'accountName';
const DEFAULT_SORT_DIRECTION = 'asc';

export default class RelatedList extends NavigationMixin(LightningElement) {
    @api relatedListTitle; // Title for lightning-card.
    @api parentObjectApiName; // Contains the apiname for the parent object you are searching on. For AccountToOpportunity__c the result is Opportunity.
    @api relationshipName; // Name of relationship for lookup relationship
    @api childObjectApiName; // Contains the apiname for the object you are searching on.
    @api parentId; // Id for parent record.
    @api columns; // columns which should be displayed.
    @api numberOfMaximumRows; // Define how many rows are displayed.
    @api filter; // For SOQL. Optional to filter records.
    @api whereApiName; // For SOQL. Define field on where clause.
    @api orderBy; // For SOQL. Sort order.

    @track isLoading; // Indicates if records are loading.
    @track hasTitle;
    @track data = 'undefined'; // Contains records for table.
    @track parentRecord = 'undefined'; // Contains parent record.
    @track childObjectIconUrl = 'undefined'; // Contains the iconName. For opportunity: custom:custom44
    @track _columns; //Internal state for columns on datatable

    // Table sorting
    @track sortedBy = DEFAULT_SORT_FIELD;
    @track sortedDirection = DEFAULT_SORT_DIRECTION;

    fieldApiNames = []; // Array with field apinames. Extracted from columns definition.
    parentObjectInfo = 'undefined'; // i.e. Opportunity
    childObjectInfo = 'undefined'; // i.e. AccountToOpportunity

    label = {
        viewAll: viewAll,
    };

    @wire(getObjectInfo, { objectApiName: '$parentObjectApiName' })
    wiredParentObjectInfo({ error, data }) {
        if (data) {
            this.parentObjectInfo = data;
            this.error = undefined;
            this._extractFieldRelationshipName();
        } else if (error) {
            this.error = error;
            this.record = undefined;
        }
    }

    @wire(getObjectInfo, { objectApiName: '$childObjectApiName' })
    wiredChildObjectInfo({ error, data }) {
        if(data){
            this.childObjectInfo = data;
            this.childObjectIconUrl = this._extractIconName();
            this.setDatableColumns();
        } else if (error){
            this.error = error;
            this.record = undefined;
        }
    }

    _extractIconName(){
        let iconUrl = (this.childObjectInfo ? this.childObjectInfo.themeInfo.iconUrl : '');
        let string_substringBeforeLast = this.substringBeforeLast(iconUrl, URL_DELIMITER);
        let string_substringAfterLast = this.substringAfterLast(iconUrl, URL_DELIMITER);

        let string_iconCategory = string_substringBeforeLast;
        let string_iconName = string_substringAfterLast.split('_')[0];
        let string_returnValue = string_iconCategory + ':' + string_iconName;

        return string_returnValue;
    }

    /*
    * Set the columns for datatable, based on the object metadata to prevent custom labels.
    */
   setDatableColumns(){
        let tempColumns = [];

        this.columns.forEach(column => {
            let tempColumn = JSON.parse(JSON.stringify(column));// Clone columns, because object is read-only.

            // Columns without links.
            if(typeof tempColumn.type !== 'undefined' && tempColumn.type !== 'url' && this.childObjectInfo.fields.hasOwnProperty(tempColumn.fieldName)){
                tempColumn.label = this.childObjectInfo.fields[tempColumn.fieldName].label;
            }

            tempColumns.push(tempColumn);
        });

        this._columns = tempColumns;
    }

    /**
     * @description Returns the substring that occurs before the last occurrence of the specified separator.
     *
     * @param {string} value
     * @param {string} separator
     *
     * @returns {string}
     */
    substringBeforeLast(value, separator) {
        let splittedElements = value.split(separator);
        let numberOfSplittedElements = splittedElements.length;
        let extractedValue = splittedElements[numberOfSplittedElements-2];

        return extractedValue;
    }

    /**
     * Returns the substring that occurs after the last occurrence of the specified separator.
     *
     * @param {string} value
     * @param {string} separator
     *
     * @returns {string}
     */
    substringAfterLast(value, separator) {
        let splittedElements = value.split(separator);
        let numberOfSplittedElements = splittedElements.length;
        let extractedValue = splittedElements[numberOfSplittedElements-1];

        return extractedValue;
    }

    connectedCallback(){
        this.initialize();
    }

    @wire(search, {
        list_fieldApiName: '$fieldApiNames',
        string_objectApiName: '$childObjectApiName',
        string_searchTerm: '$parentId',
        string_whereApiName: '$whereApiName',
        string_filter : '$filter',
        string_soqlOperator: '=',
        string_orderBy: '$orderBy',
        integer_limit: '$numberOfMaximumRows',
        integer_offset: 0
    })
    wiredRecords({ error, data }){
        if(data){
            this.error = undefined;
            const tempResult = JSON.parse(JSON.stringify(data));
            this.data = this._preprocessData(tempResult);// Raw unfiltered records
        } else if(error){
            this.error = error;
            this.data = 'undefined';
            console.error(JSON.stringify(this.error, null, '\t'));
        }
    }

    initialize(){
        console.log('initialize' + this.parentObjectApiName);
        this._extractFieldApiNames();

        // Check if title is passed
        this.hasTitle = typeof this.relatedListTitle !== 'undefined';

        this.isLoading = true;
        search({
            list_fieldApiName: this.fieldApiNames,
            string_objectApiName: this.childObjectApiName,
            string_searchTerm: this.parentId,
            string_whereApiName: this.whereApiName,
            string_filter : this.filter,
            string_soqlOperator: '=',
            string_orderBy: this.orderBy,
            integer_limit: this.numberOfMaximumRows,
            integer_offset: 0
        })
        .then(result => {
            this.error = undefined;
            const tempResult = JSON.parse(JSON.stringify(result));
            this.data = this._preprocessData(tempResult);// Raw unfiltered records

            console.log(JSON.stringify(this.data, null, '\t'));
        }).catch(error => {
            this.error = error;
            console.error(JSON.stringify(this.error, null, '\t'));
        })



        this.isLoading = false;
    }

    _preprocessData(result){
        let preprocessData = [];

        result.forEach(element => {
            this.columns.forEach(column => {
                if(column.type === 'url'){
                    const lookupFieldApiName = column.typeAttributes.label.lookupFieldApiName;
                    const lookupFieldRelationship = column.typeAttributes.label.lookupFieldApiName.replace('__c', '__r');
                    const parentFieldApiName = column.typeAttributes.label.parentFieldApiName;
                    const fieldName = column.typeAttributes.label.fieldName; // imaginary field

                    if(typeof element[lookupFieldRelationship] !== 'undefined'){
                        if(lookupFieldApiName == 'Id'){
                            element[fieldName] = element.Name;
                            element.linkToRecord = '/' + element.Id;
                        }else{
                            element[fieldName] = element[lookupFieldRelationship][parentFieldApiName];
                            element.linkToParentRecord = '/' + element[lookupFieldApiName];
                        }
                    }
                }
            });
            preprocessData.push(element);
        });

        return preprocessData;
    }

    _extractFieldRelationshipName(){
        this.parentObjectInfo.childRelationships.forEach(relationship => {

            if(relationship.childObjectApiName === this.childObjectApiName){
                this.relationshipName = relationship.relationshipName;
                return true;
            }
        })
    }

    _extractFieldApiNames(){
        this.columns.forEach(column => {

            if(column.type === 'url'){
                const label = column.typeAttributes.label;
                let lookupFieldApiName = label.lookupFieldApiName; // i.e.: Account__c
                const parentFieldApiName = label.parentFieldApiName; // i.e. Name

                if(lookupFieldApiName !== 'Id'){
                    const lookupFieldRelationship = lookupFieldApiName.replace('__c', '__r');
                    const fieldApiName = lookupFieldRelationship + '.' + parentFieldApiName;//this._firstCharacterToUppercase(parentFieldApiName);
                    this.fieldApiNames.push(fieldApiName);
                }else{
                    this.fieldApiNames.push(parentFieldApiName);
                }

                this.fieldApiNames.push(lookupFieldApiName);
            } else {
                const fieldApiName = column.fieldName;//this._firstCharacterToUppercase(column.fieldName);
                this.fieldApiNames.push(fieldApiName);
            }
        });
    }

    _firstCharacterToUppercase(value){
        let returnValue = '';

        if (typeof value !== 'string'){
            returnValue = '';
        } else {
            returnValue = value.charAt(0).toUpperCase() + value.slice(1);
        }

        return returnValue;
    }

    handleViewAllClick(event){
        this.navigateToRelatedList();
        //https://colliersde--dev.lightning.force.com/lightning/r/0061x000008Zc9zAAC/related/Account_to_opportunities__r/view
    }

    navigateToRelatedList(){
        // https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.use_navigate_page_types
        // Navigate to the CaseComments related list page
        // for a specific Case record.
        this.relatedListPageReference = {
            type: 'standard__recordRelationshipPage',
            attributes: {
                recordId: this.parentId,
                objectApiName: this.parentObjectApiName,
                relationshipApiName: this.relationshipName,
                actionName: 'view'
            }
        };

        this[NavigationMixin.GenerateUrl](this.relatedListPageReference)
            .then(url => window.open(url));

    }

    get title(){
        return this.relatedListTitle + ' (' + this.data.length + ')';
    }

    /**
     *
     * @param {*} event Datatable Sort Event
     *
     * @description event handler for sorting the header fields
     */
    updateColumnSorting(event) {
        // assign the latest attribute with the sorted column fieldName and sorted direction
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;

        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(this.sortedBy, this.sortedDirection);
   }

   /**
     *
     * @param {*} fieldname Name of the field to be sorted
     * @param {*} direction "asc" or "desc"
     *
     * @description function to sort the array
     */
    sortData(fieldname, direction){
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.data));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1 : -1;

        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        this.data = parseData;
    }
}