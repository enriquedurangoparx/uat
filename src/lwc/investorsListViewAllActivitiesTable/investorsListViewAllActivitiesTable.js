import LightningDatatable from 'lightning/datatable';
import { wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import activityListAction from './InvestorsListViewAllActivitiesTableAction.html';
import { fireEvent } from 'c/pubsub';

export default class InvestorsListViewAllActivitiesTable extends LightningDatatable {
    @wire(CurrentPageReference)
    pageRef;

    static customTypes = {
        activityListAction: {
            template: activityListAction,
            typeAttributes: ['action']
        }
    };
}