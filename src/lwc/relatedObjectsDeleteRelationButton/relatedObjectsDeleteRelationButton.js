import { LightningElement, api, track } from 'lwc';
// Accessibility module
//import { baseNavigation } from 'lightning/datatableKeyboardMixins';
// For the render() method
import template from './relatedObjectsDeleteRelationButton.html';

export default class RelatedObjectsDeleteRelationButton extends LightningElement 
{
    @api rowId;
    
    @track _relateToId;
    @track iconName;
    
    @api set relateToId(value)
    {
        this._relateToId = value;
        
        if(value)
        {
            this.iconName='utility:delete';
        }
        else
        {
            this.iconName = undefined;
        }
    }

    get showIcon()
    {
        return this.iconName ? true : false;
    }

    get relateToId()
    {
        return this._relateToId;
    }

    // Required for mixins
    render() {
        return template;
    }

    fireDeleteRelation() {
        ////window.console.log(baseNavigation);
        console.log('rowId: ', this.rowId);
        console.log('_relateToId: ', this._relateToId);
        
        const event = CustomEvent('deleterelation', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                objId: this.rowId,
                relationId: this._relateToId
            },
        });
        this.dispatchEvent(event);
    }
}