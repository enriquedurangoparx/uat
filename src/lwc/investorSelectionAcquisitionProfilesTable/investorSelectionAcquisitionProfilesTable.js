import LightningDatatable from 'lightning/datatable';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import { wire } from 'lwc';
import acquisitionProfileName from '@salesforce/label/c.investorSelectionAcquisitionProfileName';

export default class InvestorSelectionAcquisitionProfilesTable extends LightningDatatable {
    @wire(CurrentPageReference)
    pageRef;

    renderedCallback() {
        super.renderedCallback();
        var self = this;
        let apName = this.template.querySelectorAll(`lightning-primitive-cell-factory[data-label=\"${acquisitionProfileName}\"]`);
        if (apName.length > 0) {
            this.addMouseOverAndLeaveListeners(apName, self, 'AcquisitionProfile__c');
        }
    }

    addMouseOverAndLeaveListeners(nodes, self, objectType) {
        if ( nodes )
        {
            nodes.forEach( node => {
                node.addEventListener("mouseover", function(event) {
                    self.handleMouseOverEvent(event, self, objectType);
                });
                node.addEventListener("mouseleave", function (event) {
                    self.handleMouseLeaveEvent(event, self, objectType);
                });
            });
        }
    }

    disconnectedCallback() {}

    handleMouseOverEvent(event, self, objectType) {
        self.throwPayloadEvent(event, true, self, objectType);
    }

    handleMouseLeaveEvent(event, self, objectType) {
        self.throwPayloadEvent(event, false, self, objectType);
    }

    throwPayloadEvent(event, open, self, objectType) {
        if (event && event.target && event.target.parentNode) {
            var recordId = event.target.parentNode.parentNode.getAttribute('data-row-key-value');

            fireEvent(self.pageRef, 'payloadrender', {
                    recordId: recordId,
                    open: open,
                    left: event.clientX,
                    top: event.clientY
                });
        }
    }
}