import { LightningElement, api, track } from 'lwc';

export default class investorSelectionDeleteButton extends LightningElement {
    @api recordId;
    @track deleteModalOpen = false;

    handleClick(event){
        this.deleteModalOpen = true;

        const child = this.template.querySelector('c-investor-selection-delete-selected-contact-modal');
        child.openModal(this.recordId);
    }
}