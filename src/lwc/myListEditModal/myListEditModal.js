import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSharing from '@salesforce/apex/MyListController.getSharingEntriesForInvestorsList';
import saveList from '@salesforce/apex/MyListController.saveList';

import { NavigationMixin } from 'lightning/navigation';
import INVESTORS_LIST_OBJECT from '@salesforce/schema/InvestorsList__c';
import NAME_FIELD from '@salesforce/schema/InvestorsList__c.Name';
import DESCRIPTION_FIELD from '@salesforce/schema/InvestorsList__c.Description__c';
import ACCESSTYPE_FIELD from '@salesforce/schema/InvestorsList__c.AccessType__c';

import cancel from '@salesforce/label/c.MyListCancelEditList';
import myListEditList from '@salesforce/label/c.MyListEditList';
import save from '@salesforce/label/c.MyListSaveListEdit';
import close from '@salesforce/label/c.MyListCloseEditModal';
import headline from '@salesforce/label/c.MyListCloseEditModalHeadline';
import accessRead from '@salesforce/label/c.MyListEditModalAccessRead';
import accessEdit from '@salesforce/label/c.MyListEditModalAccessEdit';
import accessRemove from '@salesforce/label/c.MyListEditModalAccessRemove';
import access from '@salesforce/label/c.MyListEditModalAccess';
import listOwner from '@salesforce/label/c.MyListEditModalListOwner';
import userOrGroup from '@salesforce/label/c.MyListEditModalUserOrGroup';
import searchPlaceholder from '@salesforce/label/c.InvestorsSelectionObjectSearchPlaceholder';
import userOrGroupName from '@salesforce/label/c.MyListEditModalUserOrGroupName';
import generalLabelError from '@salesforce/label/c.GeneralLabelError';
import generalLabelNoAcess from '@salesforce/label/c.GeneralLabelNoAcess';



export default class myListEditModal extends NavigationMixin(LightningElement) {

    @api currentList;
    @api objUser;
    @api isClone;
    @api usersOrGroups;
    @api componentInfo = {};
    @track nameValue;
    @track descriptionValue;
    @track accessType;
    @track access;
    @track listId;
    @track userRecordUrl;
    @track showOwner;
    @track showAccessForm = false;
    @track showAccessTable = false;
    @track accessList = [];
    selectedUserOrGroup;

    label = {
        headline: headline,
        close: close,
        cancel: cancel,
        save: save,
        access,
        accessRead,
        accessEdit,
        accessRemove,
        listOwner,
        userOrGroup,
        searchPlaceholder,
        userOrGroupName,
        myListEditList,
        generalLabelError,
        generalLabelNoAcess
    };

    USER_AND_GROUPS_STR = 'usersandgroups';
    PUBLIC_ACCESS_STR = 'public';
    PRIVATE_ACCESS_STR = 'private';


    /********************
     * LIFECYCLE
     ********************/
    disconnectedCallback() {
        this.currentList = null;
    }

    connectedCallback(){
        this.predefineFields();
        this.initializeAccess();

        if(this.isClone)
        {
            this.initializeClone();
        }
        if(typeof this.currentList !== 'undefined' && this.currentList != null)
        {
            // Is edit or clone
            this.showAddAccess = this.currentList.accessType === 'usersandgroups' ? true : false;
        }
    }

    renderedCallback() {
        this.initDatalist();
    }

    predefineFields()
    {
        // prefill accessType picklist
        if (this.currentList && this.currentList.accessType)
        {
            this.accessType = this.currentList.accessType;
        }
        else
        {
            // Is new
            this.accessType = 'private';
        }

        // prefill name
        if (this.currentList && this.currentList.name)
        {
            this.nameValue = this.currentList.name;
        } 
        // prefill description
        if (this.currentList && this.currentList.description)
        {
            this.descriptionValue = this.currentList.description;
        }
    }

    get headerMessage()
    {
        return this.componentInfo && this.componentInfo.isEditMode ? this.label.myListEditList : this.label.headline;
    }


    /********************
     * EXPOSED FUNCTIONS
     ********************/
    get listApiName() {
        return INVESTORS_LIST_OBJECT.objectApiName;
    }

    get nameFieldApiName() {
        return NAME_FIELD.fieldApiName;
    }

    get descriptionFieldApiName() {
        return DESCRIPTION_FIELD.fieldApiName;
    }

    get accessTypeFieldApiName() {
        return ACCESSTYPE_FIELD.fieldApiName;
    }

    get accessReadOrWrite(){
        return [
            { label: accessRead, value: 'Read' },
            { label: accessEdit, value: 'Edit' }
        ];
    }

    initDatalist(){
        let inputDataList = this.template.querySelector('datalist');
        if(inputDataList != null){
            let listId = this.template.querySelector('datalist').id;
            this.template.querySelector("input").setAttribute("list", listId);
        }
    }

    /********************
     * INTERNAL FUNCTIONS
     ********************/
    disableButtons(disabled) {
        const cancelBtn = this.template.querySelector(
            '[data-id="cancel-button"]'
        );

        if (cancelBtn) {
            cancelBtn.disabled = disabled;
        }

        const submitBtn = this.template.querySelector(
            '[data-id="submit-button"]'
        );

        if (submitBtn) {
            submitBtn.disabled = disabled;
        }
    }

    /********************
     * EVENT HANDLING
     ********************/

    handleSubmit(event) {
        event.preventDefault();
        this.disableButtons(true);

        let newAccessMap = {};
        if(typeof this.accessList !== 'undefined'){
            for (var i = 0; i < this.accessList.length; i++) {
                newAccessMap[this.accessList[i].UserOrGroupId] = this.accessList[i].AccessLevel;
            }
        }

        if (this.accessType && this.accessType !== this.USER_AND_GROUPS_STR)
        {
            if (this.usersOrGroups)
            {
                this.usersOrGroups.forEach(element => {
                    switch(this.accessType) 
                    {
                        case this.PUBLIC_ACCESS_STR:
                            newAccessMap[element.key] = element.key.startsWith('005') ? 'Read' : this.PRIVATE_ACCESS_STR;
                            break;
                        case this.PRIVATE_ACCESS_STR:
                            newAccessMap[element.key] = this.PRIVATE_ACCESS_STR;
                            break;
                        default:
                    }
                });
            }
        }

        saveList({
            isClone : this.isClone, 
            listToSave: JSON.stringify(this.currentList), 
            nameValue : event.detail.fields.Name, 
            descriptionValue : event.detail.fields.Description__c, 
            accessType : event.detail.fields.AccessType__c, 
            sharingMap : JSON.stringify(newAccessMap)
        })
        .then(result => {
            if(this.isClone){
                this[NavigationMixin.Navigate]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: result,
                        actionName: 'view'
                    }
                });
            }else{
                this.disableButtons(false);
                this.listId = result;
                this.dispatchEvent(new CustomEvent('refresh'));
            }
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: this.label.generalLabelError,
                    message: this.label.generalLabelNoAcess,
                    variant: 'error'
                })
            );
        });
    }

    /**
     * handle change of access type
     */
    handleAccessTypeChange(event) {
        this.accessType = event.detail.value;
        this.showAddAccess = this.accessType === 'usersandgroups' ? true : false;

        if(this.showAddAccess){
            this.initDatalist();
        }
    }

    /**
     * handle change of access
     */
    handleAccessReadOrWriteChange(event) {
        this.access = event.detail.value;
    }

    handleAddAccessClick(event){
        if(typeof this.accessList === 'undefined' || this.accessList.length === 0){
            this.accessList = [];
            this.showAccessTable = true;
        }

        let selectedUserOrGroupId;
        for(var i = 0; i < this.usersOrGroups.length; i++){
            if(this.usersOrGroups[i].value === this.selectedUserOrGroup){
                selectedUserOrGroupId = this.usersOrGroups[i].key;
            }
        }
        if(typeof selectedUserOrGroupId !== 'undefined'){
            let accessElement = {
                "AccessLevel": this.access,
                "optionValueEdit" : 'edit-' + selectedUserOrGroupId,
                "optionValueRead" : 'read-' + selectedUserOrGroupId,
                "optionValueRemove" : 'remove-' + selectedUserOrGroupId,
                "UserOrGroupId": selectedUserOrGroupId,
                "UserOrGroup": {
                    "Name": this.selectedUserOrGroup,
                    "Id": selectedUserOrGroupId
                }
            };

            if(this.access === 'Read'){
                accessElement.isRead = "selected";
            }

            if(this.access === 'Edit'){
                accessElement.isEdit = "selected";
            }

            this.accessList.push(accessElement);
        }
    }

    handleUserOrGroupSelected(event){
        this.selectedUserOrGroup = event.target.value;
    }

    handleAccesSelectListChange(event){
        let optionValue = event.target.value;
        let access = optionValue.split('-')[0];
        let recordId = optionValue.split('-')[1];

        for (var i = 0; i < this.accessList.length; i++) {
            if(recordId === this.accessList[i].UserOrGroupId){
                this.accessList[i].AccessLevel = access;
            }
        }
    }

    /**
     * close edit modal
     */
    cancel() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    /**
     * prefill form on clone
    */
    initializeClone(){
        this.isClone = true;
        this.nameValue = 'Kopie von ' + this.currentList.name;

        if(typeof this.currentList.description !== 'undefined'){
            this.descriptionValue = 'Kopie von ' + this.currentList.description;
        }
    }

    /**
     * initialize access data
    */
    initializeAccess(){
        if(this.objUser.ProfileName === 'Systemadministrator' 
            || this.objUser.ProfileName === 'System Administrator' 
            || (this.currentList && this.currentList.ownerId === this.objUser.Id) 
            || (typeof this.currentList === 'undefined' || this.currentList == null))
        {
            this.showAccessForm = true;
        }

        if(this.showAccessForm){
            if(typeof this.currentList !== 'undefined' && this.currentList != null){
                this.listId = this.currentList.id;
                this.generateURLToUser();

                this.showOwner = true;
                getSharing({investorsListId: this.listId})
                .then(result => {
                    const tempResult = JSON.parse(JSON.stringify(result));

                    let _accessList = this._preprocessData(tempResult);
                    this.accessList = JSON.parse(JSON.stringify(_accessList));


                    if(this.accessList.length > 0){
                        this.showAccessTable = true;
                    }
                });
            }
        }
    }


    _preprocessData(result){
        let preprocessData = [];
        result.forEach(element => {
            if(element.AccessLevel === 'Edit'){
                element.isEdit = 'selected';
            }
            if(element.AccessLevel === 'Read'){
                element.isRead = 'selected';
            }
            element.optionValueRead = 'read-' + element.UserOrGroupId;
            element.optionValueEdit = 'edit-' + element.UserOrGroupId;
            element.optionValueRemove = 'remove-' + element.UserOrGroupId;

            preprocessData.push(element);
        });

       return preprocessData;
    }

    generateURLToUser(){
        // Generate a URL to a User record page
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.currentList.ownerId,
                actionName: 'view',
            },
        }).then(url => {
            this.userRecordUrl = url;
        });
    }
}