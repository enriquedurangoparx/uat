import { LightningElement, track, api, wire} from 'lwc';
import { refreshApex } from '@salesforce/apex';
import getAcquisitionProfiles from '@salesforce/apex/MyListController.getRelatedAcquisitionProfiles';
import getAccountAcquisitionProfiles from '@salesforce/apex/MyListController.getAccountRelatedAcquisitionProfiles';
import getListItems from '@salesforce/apex/MyListController.getListItems';

export default class MyListDetailComponent extends LightningElement {
    @track searchResultList;
    @api recordId;
    @track listItems;
    @track wiredResult;
    @track showDataTable = true;

    // lazy load variables
    @track
    dataTableIsLoading = false;
    
    investorsLimitSize = 25;
    investorContactsLimitSize = 25;

    @wire(getListItems, { recordId: '$recordId', investorsLimitSize: '$investorsLimitSize', investorContactsLimitSize: '$investorContactsLimitSize'})
    getListItems(result){
        if(result && result.data){
            this.wiredResult = result;
            this.listItems = result.data;
        } else {
            this.listItems = undefined;
        }
    }

    searchResultFoundHandler(event){
        if(event.detail){
            this.searchResultList = event.detail.result;
        }
        const detailComponent = this.template.querySelector('c-my-list-detail-data-component');
        detailComponent.searchResult = event.detail.result;
        detailComponent.objectType = event.detail.objectType;

        detailComponent.mode = event.detail.result?'search':'list';
    }

    myListSelectedHandler(event){
        const recordDetailComponent = this.template.querySelector('c-my-list-detail-recorddetail-component');
        // fetch Acquisition-Profiles separately
        
        if (event.detail)
        {
            console.log('...event.detail.selectedRow.accountId: ' + event.detail.selectedRow.accountId);
            if (event.detail.selectedRow.hasContact)
            {
                console.log('...event.detail.selectedRow.contactId: ' + event.detail.selectedRow.contactId);
                console.log('...recordDetailComponent: ' + recordDetailComponent);
                this.fetchAcquisitionProfiles(event.detail.selectedRow.contactId, recordDetailComponent);
            }

            else if (event.detail.selectedRow.accountId != undefined)
            {
                this.fetchAcquisitionProfilesForAccount(event.detail.selectedRow.accountId, recordDetailComponent);
            }
            else
            {
                recordDetailComponent.setAcquisitionProfiles([]);
            }

            recordDetailComponent.setDetailsObject(event.detail.selectedRow);
        } 
        else 
        {
            recordDetailComponent.setDetailsObject(undefined);
        }
    }

    async fetchAcquisitionProfiles(contactId, recordDetailComponent){
        if (contactId)
        {
            const acquisitionProfiles = await getAcquisitionProfiles( {contactId: contactId} );
            if (acquisitionProfiles.length > 0)
            {
                recordDetailComponent.setAcquisitionProfiles(acquisitionProfiles);
            } 
            else 
            {
                recordDetailComponent.setAcquisitionProfiles([]);
            }
        }
    }

	async fetchAcquisitionProfilesForAccount(accountId, recordDetailComponent){
		if (accountId)
		{
			const acquisitionProfiles = await getAccountAcquisitionProfiles( {accountId: accountId} );
			if (acquisitionProfiles.length > 0)
			{
				recordDetailComponent.setAcquisitionProfiles(acquisitionProfiles);
			}
			else
			{
				recordDetailComponent.setAcquisitionProfiles([]);
			}
		}
	}

    refreshHandler(){
        refreshApex(this.wiredResult);

        this.showDataTable = false;
        setTimeout(() => {
            this.showDataTable = true;
        }, 50);

        try {
            const searchComponent = this.template.querySelector('c-my-list-detail-search-component');
            searchComponent.clearSearchTerm();
        } catch (e) {
            console.warn("MyListDetailComponent:refreshHandler", e.message);
        }
    }

}