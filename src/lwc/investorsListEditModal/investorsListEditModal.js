import { LightningElement, api, track } from 'lwc';

// import custom labels
import close from '@salesforce/label/c.InvestorsListCloseLabel';
import cancel from '@salesforce/label/c.investorsListCancelLabel';

export default class InvestorsListEditModal extends LightningElement {
    @track showModal = false;
    @api recordId;

    label = {
        close,
        cancel,
        header: "Aktivitätenliste"
    }

    /********************
     * EXPOSED FUNCTIONS
     ********************/
    /**
     * open objective selection modal
     */
    @api
    openModal(recordId) {
        window.console.log("recordId", recordId);
        this.recordId = recordId;
        //this.loadReports().then(() => {
            this.showModal = true;
        //});
    }

    /**
     * close objective selection modal
     */
    closeModal() {
        this.showModal = false;
    }

}