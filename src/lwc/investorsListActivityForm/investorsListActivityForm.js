import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { reduceErrors } from 'c/lwcUtil';
import Id from '@salesforce/user/Id';
import {fireEvent} from 'c/pubsub';
import { CurrentPageReference} from 'lightning/navigation';


import lookupSearch from '@salesforce/apex/LookupController.search';
import createTask from '@salesforce/apex/InvestorsListController.createTask';
import updateInvestorsCall from '@salesforce/apex/InvestorsListController.updateInvestors';

// custom labels
import date from '@salesforce/label/c.InvestorsListActionFormDate';
import topic from '@salesforce/label/c.InvestorsListActionFormTopic';
import comment from '@salesforce/label/c.InvestorsListActionFormComment';
import dueDate from '@salesforce/label/c.InvestorsListActionFormDueDate';
import contacts from '@salesforce/label/c.InvestorsListActionFormContacts';
import enquiry from '@salesforce/label/c.InvestorsListActionFormEnquiry';
import assignedTo from '@salesforce/label/c.InvestorsListActionFormAssignedTo';
import contactsErrorMessage from '@salesforce/label/c.InvestorsListActionFormContactsErrorMessage';
import toastTitle from '@salesforce/label/c.InvestorsListActionFormToastTitle';
import startDate from '@salesforce/label/c.InvestorsListActionFormStartDate';
import endDate from '@salesforce/label/c.InvestorsListActionFormEndDate';
import amount from '@salesforce/label/c.InvestorsListActionFormAmount';
import activityBidSubjectLabel from '@salesforce/label/c.InvestorsListActionFormActivityBidSubjectLabel';
import callSubject from '@salesforce/label/c.InvestorsListActionFormCallSubject';
import declination from '@salesforce/label/c.InvestorsListDeclinationLabel';
import open from '@salesforce/label/c.InvestorsListActivityStatusOpen';
import closed from '@salesforce/label/c.InvestorsListActivityStatusClosed';

const DEFAULT_RECORDTYPE = '012000000000000AAA';

export default class InvestorsListActivityForm extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @api investorId;
    @api contactId;
    @api listOfContacts;

    @track declinationSelected;
    @track defaultRecordTypeId = DEFAULT_RECORDTYPE;
    @track investorRowStatus;
    @track _selectedDeclinations = [];
    _investorRow;

    @api
    set investorRow(value){
        this._investorRow = value;
        this.investorRowStatus = this._investorRow.rowStatus ? this._investorRow.rowStatus : '';
    }

    get investorRow(){
        return this._investorRow;
    }

    validStatus = new Set(['Angebot', 'Offer', 'Indicative Offer', 'Confirmatory Offer', 'Binding Offer']);

    @track selectedActivityStatusValue = 'Completed';
    get activityStatusOptions() {
        return [
            { label: closed, value: 'Completed' },
            { label: open, value: 'Open' }
        ];
    }

    handleActivityStatusChange(event) {
        this.selectedActivityStatusValue = event.detail.value;
    }

    handleStatusChange(event){
        this.disableButton(false);
        this.investorRowStatus = event.detail.selectedstatus;
        this.investorRowValue = event.detail.selectedValue;
        this.declinationSelected = this.investorRowValue == 'Declination' ? true : false;

        if(this.declinationSelected && this._selectedDeclinations.length == 0){
            this.disableButton(true);
        }
    }

    get showAmount(){
        return this.validStatus.has(this.investorRowStatus) ? true : false;
    }

    // setter for activityType, sets track variables for managing which fields will be displayed and stored
    _activityType;
    @api set activityType(value) {
        this._activityType = value;
        if(value === "call"){
            this.typeIsCall=true;
            this.typeIsTask = this.typeIsOffer = this.typeIsEvent = false;
            this.callSubject = this.label.callSubject;
        } else if(value === "task"){
            this.typeIsTask = true;
            this.typeIsCall = this.typeIsOffer = this.typeIsEvent = false;
        } else if(value === "event"){
            this.typeIsEvent = true;
            this.typeIsCall = this.typeIsOffer = this.typeIsTask = false;
        } else if(value === "offer"){
            this.typeIsOffer = true;
            this.typeIsCall = this.typeIsEvent = this.typeIsTask = false;
        }
    }
    get activityType(){
        return this._activityType;
    }

    @track userId = Id;
    @track dateValue;
    @track callSubject;

    // values to show/hide fields depending on activityType
    @track typeIsEvent=false;
    @track typeIsTask=false;
    @track typeIsOffer=false;
    @track typeIsCall=false;

    // UI Labels
    label = {
        date,
        topic,
        comment,
        dueDate,
        contacts,
        enquiry,
        assignedTo,
        contactsErrorMessage,
        toastTitle,
        toastMessage: "",
        startDate,
        endDate,
        amount,
        activityBidSubjectLabel,
        callSubject,
        declination,
        open,
        closed
    }

    // configuration-objects for lookup components
    contactField = {objectApiName: "Task", apiName: "WhoId", isMultiEntry: true};
    enquiryField = {objectApiName: "Task", apiName: "AcquisitionProfile__c", isMultiEntry: false};

    // fields to hold values from the lookup fields
    selectedContactIds = [];
    acqouisitionProfileId;

    renderedCallback(){
        var localDate = new Date();
        this.dateValue = localDate.toISOString().substring(0,10);

        if(typeof this.contactId !== 'undefined' && !this.selectedContactIds.includes(this.contactId)){
            this.selectedContactIds.push(this.contactId);
        }
    }

    // US 2698 npo
    @track predefinedContacts;

    connectedCallback()
    {
        console.log('...connectedCallback.listOfContactes: ' + this.listOfContacts);
        let selection = [];
        if (this.listOfContacts)
        {
            let splittedList = this.listOfContacts.split(';');
            splittedList.forEach(function(contact){
                console.log('...contact: ' + contact);
                if (contact)
                {
					let result = contact.split(':');
					console.log('...result: ' + result);
					let singleSelection =
					{
						id: result[0].trim(),
						sObjectType: 'Contact',
						icon: 'standard:contact',
						title: result[1].trim()
					}
					selection.push(singleSelection);
                }
            });
            this.predefinedContacts = selection;
//			this.template
//				.querySelector('[data-id="nameSearchInput"]')
//				.setSelection(selection);
        }

    }


	handleContactsLookupSearch(event) {
        var searchParam = {
            parentObjectApiName: this.contactField.objectApiName,
            lookupApiName: this.contactField.apiName,
            searchTerm: event.detail.searchTerm
        };

        lookupSearch(searchParam)
            .then(results => {
                this.template
                    .querySelector('[data-id="nameSearchInput"]')
                    .setSearchResults(results);
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.\n' +
                        reduceErrors(error).join(', '),
                    'error'
                );
            });
    }

    handleContactSelectionChange(event){
        if(event.target.selection && event.target.selection.length > 0) {
            this.selectedContactIds = [];
            for(let i=0; i<event.target.selection.length; i++) {
                this.selectedContactIds.push(event.target.selection[i].id);
            }
        } else {
            this.selectedContactIds = undefined;
        }
    }

    @api
    async submit(){
        let allValid = this.validateForm();
        if(!allValid)
            return;

        const activityStatusForm = this.template.querySelector('c-investors-list-activity-status-form');
        const selectedStatusValue = activityStatusForm.selectedValue;

        let description = this.template.querySelector('[data-id="commentInput"]').value;
        let dateC = this.template.querySelector('[data-id="dateInput"]').value;
        let activity = {ActivityDate: dateC,
                    Description: description,
                    AcquisitionProfile__c: this.acqouisitionProfileId,
                    InvestorStatus__c: selectedStatusValue,
                    Status: this.selectedActivityStatusValue,
                    OwnerId: this.userId};

        if(this.showAmount){
            let lastBidAmount = this.template.querySelector('[data-id="amountInput"]').value;
            activity.Amount__c = lastBidAmount;
        }
        activity.Subject = this.template.querySelector('[data-id="topicInput"]').value;
        activity.WhatId = typeof this._investorRow.investorId !== 'undefined' ? this._investorRow.investorId : this.investorId;

        let createdObject;
        createdObject = await createTask({investorStatus: selectedStatusValue,data: activity, taskWhoIds : this.selectedContactIds});

        let investorsUpdated = false;
        if(createdObject){
            if(selectedStatusValue){
               let decValue = this._selectedDeclinations ? this._selectedDeclinations.join(';') : '';
               let investors = [{Status__c: selectedStatusValue, Id: activity.WhatId, ReasonForRejection__c : decValue}];
                const updateInvestorsResult = await updateInvestorsCall({
                    investors : investors,
                    statusDate : dateC,
                    createNewStatus : false
                });
                investorsUpdated = true;

                // refresh picklist
                fireEvent(this.pageRef, 'investorsListStatusPicklist_refresh', investors);
            }
        }

        const event = CustomEvent('activitycreated', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                toastTitle: this.label.toastTitle,
                toastMessage: this.label.toastMessage,
                toastVariant: 'success',
                refreshMainTable: investorsUpdated,
                investor: {Id: activity.WhatId, Status__c: selectedStatusValue}
            },
        });
        this.dispatchEvent(event);
    }

    validateForm(){
        const allValidInput = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
            const allValidTextArea = [...this.template.querySelectorAll('lightning-textarea')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
            const allValidLookup = [...this.template.querySelectorAll('c-lookup')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
        let allValid = (allValidInput && allValidTextArea && allValidLookup);
        return allValid;
    }


    notifyUser(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }

    handleDeclinationReasonChange(event) {
        this._selectedDeclinations = event.detail;
        if(this.declinationSelected && this._selectedDeclinations.length == 0){
            this.disableButton(true);
        }else{
            this.disableButton(false);
        }
    }

    disableButton(buttonDisabled){
        const thisEvent = new CustomEvent("disablebutton", {
            detail: buttonDisabled
          });

          // Dispatches the event.
          this.dispatchEvent(thisEvent);
    }
}