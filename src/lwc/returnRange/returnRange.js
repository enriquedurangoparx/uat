/**
 * Represents a noUiSlider form a 3th party JS library (https://refreshless.com/nouislider/)
 * 
 * @class
 * @var {string} eventname unique eventname in the format of <classname>__<whatHappened>. i.e.: "ReturnRange__yearOfConstructionRangeChange"
 * @var {string} label that should appear under the slider
 * @var {string} mode could be "number" or "percentage".
 * @var {number} min
 * @var {number} max
 * @var {number} start defines the starting position
 * @var {number} end defines the ending position
 * @var {number} step 
 */
import { LightningElement, api, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import NO_UI_SLIDER from '@salesforce/resourceUrl/nouislider';

export default class ReturnRange extends LightningElement {
    @api eventname;
    @api label;
    @api mode; 
    @api min = '0';
    @api max = '1000';
    @api start = null;
    @api end = null;
    @api step = '10';
    @api disabled;
    
    _isRendered;
    _overlayElement = null;

    @wire(CurrentPageReference) pageRef;

    @api
    resetMaxMinValue(min, max) {
        const slider = this.template.querySelector('.slider');
        
        slider.noUiSlider.set([min, max]);
    }

    get isDisabled() {
        return this.disabled !== undefined ? this.disabled : false;
    }

    renderedCallback() {
        if (this._isRendered) return;
        this._isRendered = true;
        this._overlayElement = this.template.querySelector('[data-id="overlay"]');

        Promise.all([
            loadStyle(this, NO_UI_SLIDER + '/nouislider.css'),
            loadScript(this, NO_UI_SLIDER + '/nouislider.js')
        ]).then(() => {
            const slider = this.template.querySelector('.slider');
            let _isPercentMode = (this.mode === "percentage" ? true : false);

            let currentPosition = [this.start, this.end];
            if (!this.start || !this.end) {
                currentPosition = [this.min, this.max];
            }

            window.noUiSlider.create(slider, {
                start: currentPosition,
                connect: true,
                tooltips: true,
                step: Number.parseInt(this.step, 10),
                format: {
                    to(value) {
                        let roundedValue = Math.round(value);
                        roundedValue = roundedValue + (_isPercentMode ? '%' : '');

                        return roundedValue;
                    },
                    from(value) {                       
                        return value;
                    }
                },
                range: {
                    min: Number.parseInt(this.min, 10),
                    max: Number.parseInt(this.max, 10)
                }
            });

            slider.noUiSlider.on('change', range => {
                let minRange = (_isPercentMode ? range[0].replace('%', '') : range[0]);
                let maxRange = (_isPercentMode ? range[1].replace('%', '') : range[1]);

                fireEvent(this.pageRef, this.eventname, {
                    minValue: minRange,
                    maxValue: maxRange
                });
            });
            this.setDisabled(this.isDisabled);
        });
    }

    @api setDisabled(state) {
        console.log('ReturnRange:setDisabled', state);
        if (this._overlayElement) {
            state ? this._overlayElement.classList.remove("slds-hide") : this._overlayElement.classList.add("slds-hide");

            const sliderHandlerElement = this.template.querySelector('.noUi-connect');
            if (sliderHandlerElement) {
                sliderHandlerElement.style.backgroundColor = state ? "#B8B8B8" : "#3FB8AF";
            }
        }
    }
}