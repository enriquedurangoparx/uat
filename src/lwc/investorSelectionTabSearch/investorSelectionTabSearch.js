/**
 * Is used to search for contact, brand, opportunity in the investorselection.
 *
 * @author Christian Schwabe (Christian.Schwabe@colliers.com)
 * @var {string}    searchTerm      Contains the text to search for.
 * @var {string}    error           Generic error message for all errors.
 * @var {string}    objectApiName   Hold the current value for radio-group searchType
 *
 *
 */
import { LightningElement, track, wire, api } from 'lwc';
import { registerListener} from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

import { getListUi } from 'lightning/uiListApi'
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import OPPORTUNITY_OBJECT from '@salesforce/schema/Opportunity';

// import custom labels:
import searchDescription from '@salesforce/label/c.InvestorsSelectionSearchDescription';
import objToSearchFor from '@salesforce/label/c.InvestorsSelectionObjectToSearchLabel';
import searchPlaceholder from '@salesforce/label/c.InvestorsSelectionObjectSearchPlaceholder';
import accountLabel from '@salesforce/label/c.InvestorsSelectionAccount';
import contactLabel from '@salesforce/label/c.InvestorsSelectionContact';
import opportunityLabel from '@salesforce/label/c.InvestorsSelectionOpportunity';
import selectListViewLabel from '@salesforce/label/c.SelectListView';
import contactListViewLabel from '@salesforce/label/c.ContactListView';
import accountListViewLabel from '@salesforce/label/c.AccountListView';


export default class InvestorSelectionTabSearch extends LightningElement {
    @wire(CurrentPageReference) pageRef;

    @track searchTerm;
    @track error;

    @track objectApiName = 'Contact';
    @track radioButtonValue = '';

    label = {
        searchDescription,
        objToSearchFor,
        searchPlaceholder,
        selectListViewLabel,
        contactListViewLabel,
        accountListViewLabel
    }


    connectedCallback()
    {
        registerListener('InvestorSelectionDatatable__recordadded', this.cleanSearchText, this);
    }

    renderedCallback()
    {
        this.template.querySelector('[data-name=searchTerm]').focus();
    }


    /*
    * List View functionalities
    */

    // Contact
   @track contactListViewOptions;
   @track contactSelectedListView = '';
   @wire(getListUi, { objectApiName: CONTACT_OBJECT })
   wiredContactListViews({ error, data }) {
       if (data) {
           this.contactListViewOptions = this.convertListViewsToPicklist(data);
       } else if (error) {
           this.error = error;
       }
   }

   handleContactListViewChange(event) {
       this.clearSearchTerm();
       fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : true});

       this.contactSelectedListView = event.detail.value;
       if(this.contactSelectedListView === ''){
            this.cancelSearch();
       }
   }

   @wire(getListUi, { objectApiName: CONTACT_OBJECT,  listViewApiName: '$contactSelectedListView', pageSize : 1000})
   wiredContactsByListApiName({ error, data }) {
       if (!this.searchTerm)
       {
		   if (data) {
				if (data.lists){
					this.searchObjects('Contact', data.lists);
				}
				else if (data.records){
					this.searchObjects('Contact', data.records.records);
				}
		   } else if (error) {
			   this.error = error;
		   }
       }
   }



    // Account
    @track accountListViewOptions;
    @track accountSelectedListView = '';
    @wire(getListUi, { objectApiName: ACCOUNT_OBJECT })
    wiredAccountListViews({ error, data }) {
        if (data) {

            this.accountListViewOptions = this.convertListViewsToPicklist(data);
        } else if (error) {
            this.error = error;
        }
    }

    handleAccountListViewChange(event) {
        this.clearSearchTerm();
        fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : true});

        this.accountSelectedListView = event.detail.value;
        if(this.accountSelectedListView === ''){
            this.cancelSearch();
       }
    }
    @wire(getListUi, { objectApiName: ACCOUNT_OBJECT,  listViewApiName: '$accountSelectedListView', pageSize : 1000})
    wiredAccountsByListApiName({ error, data }) {
	    if (!this.searchTerm)
	    {
			if (data) {
				if (data.lists)
				{
					this.searchObjects('Account', data.lists);
				}
				else if (data.records)
				{
					this.searchObjects('Account', data.records.records);
				}
			} else if (error) {
				this.error = error;
			}
        }
    }



    // Opportunity
    @track opportunityListViewOptions;
    @track opportunitySelectedListView = '';
    @wire(getListUi, { objectApiName: OPPORTUNITY_OBJECT })
    wiredOpportunityListViews({ error, data }) {
        if (data) {
            this.opportunityListViewOptions = this.convertListViewsToPicklist(data);
        } else if (error) {
            this.error = error;
        }
    }

    handleOpportunityListViewChange(event) {
        console.log('...handleOpportunityListViewChange');
        this.clearSearchTerm();
        fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : true});

        this.opportunitySelectedListView = event.detail.value;
        if(this.opportunitySelectedListView === ''){
            this.cancelSearch();
       }
    }
    @wire(getListUi, { objectApiName: OPPORTUNITY_OBJECT,  listViewApiName: '$opportunitySelectedListView', pageSize : 1000})
    wiredOpportunitiesByListApiName({ error, data }) {
	    if (!this.searchTerm)
	    {
			if (data) {
				if (data.lists)
				{
					this.searchObjects('Opportunity', data.lists);
				}
				else if (data.records)
				{
					this.searchObjects('Opportunity', data.records.records);
				}
			} else if (error) {
				this.error = error;
			}
        }
    }



   convertListViewsToPicklist(data){
        let listViews = data;
//        let options = [{label: this.label.selectListViewLabel, value:''}];
		let options = [];
        if(typeof listViews !== 'undefined'){
            for (var i = 0; i < listViews.lists.length; i++) {
                let element = {label:listViews.lists[i].label, value:listViews.lists[i].apiName};
                options.push(element);
            }
        }
        return options;
   }

   searchObjects(objectName, records){
       console.log('...searchObjects objectName: ' + objectName);
        const searchEvent = new CustomEvent(
            'listviewchanged',
            {
                detail: {
                    objectName: objectName,
                    records: records
                }
            }
        );
        this.dispatchEvent(searchEvent);
    }

    cancelSearch(){
        console.log('...cancelSearch:' + !this.searchTerm);
//        if (!this.searchTerm)
//        {
        	this.dispatchEvent(new CustomEvent('searchancelled'));
//        }
    }



    /**
     * Return radio group values for searchType.
     */
    get searchTypeOptions() {
        return [
            { label: contactLabel, value: 'Contact' },
            { label: accountLabel, value: 'Account' }
            // { label: opportunityLabel, value: 'Opportunity' }
        ];
    }

    /**
     * This is triggered when the user hits ENTER.
     */
    handleKeyUp(event){
        const isEnterKey = event.keyCode === 13;

        if (isEnterKey) {
//            US2693 npo
			this.clearSelectedListViews();

            fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : true});

            this.searchTerm = this.template.querySelector('[data-name=searchTerm]').value;
            this.eventDispatchHandler();
        }
    }

    /**
     * This is triggered when the value in the input field changes
    */
   handleInputChange(event){
       if(event.target.value === ''){
            this.dispatchEvent(new CustomEvent('searchancelled'));
       }
   }

    /**
     * This is triggered when the input or radio-group has changed.
     */
    eventDispatchHandler(){
        const searchEvent = new CustomEvent(
            'searchchange',
            {
                detail: {
                    searchTerm: this.searchTerm,
                    objectApiName: this.objectApiName
                }
            }
        );
        this.dispatchEvent(searchEvent);
    }
    /**
     * This is triggerd when a radio button is selected
     */
	radioGroupChangeHandler(event) {
		fireEvent(this.pageRef, 'InvestorSelectionDatatable__isLoading', {isLoading : true});

		this.objectApiName = event.detail.value;
		console.log('>>>InvestorSelectionTabSearch -- radioGroupChangeHandler(): ' + this.objectApiName);

		this.eventDispatchHandler();
	}

	cleanSearchText(event){
		this.searchTerm = '';
	}

    @api
    get isOpportunitySelected()
    {
        return this.objectApiName === 'Opportunity';
    }

	@api
	get isContactSelected()
	{
		return this.objectApiName === 'Contact';
	}

	@api
	get isAccountSelected()
	{
		return this.objectApiName === 'Account';
	}

	clearSelectedListViews()
	{
		this.contactSelectedListView = '';
		this.accountSelectedListView = '';
		this.opportunitySelectedListView = '';
 	}

 	clearSearchTerm()
 	{
 	    this.searchTerm = '';
	}
}