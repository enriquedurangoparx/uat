import { LightningElement, track, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import { CurrentPageReference } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import { NavigationMixin } from 'lightning/navigation';
import { unregisterAllListeners, registerListener, fireEvent} from 'c/pubsub';
import { updateRecord } from 'lightning/uiRecordApi';
import updateInvestorsCall from '@salesforce/apex/InvestorsListController.updateInvestorsPriority';
import updateInvestorsCallWithStatus from '@salesforce/apex/InvestorsListController.updateInvestors';
import getUnderbidderPicklistEntries from '@salesforce/apex/InvestorsListController.getUnderbidderPicklistEntries';
import { getFieldLabel, getPicklistOptions } from 'c/utilsSObject';

import OPPORTUNITY_NAME_FIELD from '@salesforce/schema/Opportunity.Name';
import OPPORTUNITY_ACCOUNT_ID_FIELD from '@salesforce/schema/Opportunity.AccountId';
import STAGE_FIELD from '@salesforce/schema/AccountToOpportunity__c.Status__c';
import PRIORITY_FIELD from '@salesforce/schema/AccountToOpportunity__c.Priority__c';

// controller methods
import findInvestors from '@salesforce/apex/InvestorsListController.findInvestors';
import setPinned from '@salesforce/apex/InvestorsListController.setPinnedOnOpportunity';
import loadCampaigns from '@salesforce/apex/NewMailingController.loadCampaigns';
import lookupSearch from '@salesforce/apex/LookupController.search';


// import custom labels:
import searchPlaceholder from '@salesforce/label/c.InvestorsListSearchPlaceholder';
import investorsReport from '@salesforce/label/c.InvestorsListInvestorsReport';
import updateLabel from '@salesforce/label/c.InvestorsListUpdate';
import prioUpdate from '@salesforce/label/c.InvestorsListPriorityUpdate';
import advisorUpdate from '@salesforce/label/c.InvestorsListAdvisorUpdate';
import statusUpdate from '@salesforce/label/c.InvestorsListStatusUpdate';
import approvalUpdate from '@salesforce/label/c.InvestorsListApprovalUpdate';
import approvalUpdateReject from '@salesforce/label/c.InvestorsListApprovalUpdateReject';
import removeFromSelection from '@salesforce/label/c.InvestorsListRemoveFromSelection';
import pinToastTitle from '@salesforce/label/c.InvestorsListPinToastTitle';
import unpinToastTitle from '@salesforce/label/c.InvestorsListUnpinToastTitle';
import noInvestorsFoundTitle from '@salesforce/label/c.InvestorsListNoInvestorsFound';
import noInvestorsFoundMessage from '@salesforce/label/c.InvestorsListNoInvestorsFoundMessage';
import back from '@salesforce/label/c.InvestorsListBackLinkLabel';
import nameLabel from '@salesforce/label/c.InvestorsListName';
import statusLabel from '@salesforce/label/c.InvestorsListStatusLabel';
import priorityLabel from '@salesforce/label/c.InvestorsListPriorityLabel';
import approvedLabel from '@salesforce/label/c.InvestorsListApprovedLabel';
import statusSince from '@salesforce/label/c.InvestorsListLastStatusLabel';
import lastBid from '@salesforce/label/c.InvestorsListLastBidLabel';
import lastBidAmount from '@salesforce/label/c.InvestorsListLastBidAmount';
import listOfContacts from '@salesforce/label/c.InvestorsListListOfContacts';
import lastActivities from '@salesforce/label/c.InvestorsListLastActivitiesLabel';
import actionLabel from '@salesforce/label/c.InvestorsListActionLabel';
import declination from '@salesforce/label/c.InvestorsListDeclinationLabel';
import toSelection from '@salesforce/label/c.InvestorsListToInvestorsSelectionLabel';
import resetFilter from '@salesforce/label/c.InvestorsListResetFilterLabel';
import underbidder from '@salesforce/label/c.InvestorsListUnderbidderLabel';
import underbidders from '@salesforce/label/c.InvestorsListUnderbiddersLabel';
import newMailing from '@salesforce/label/c.InvestorsListNewMailing';
import existingMailing from '@salesforce/label/c.InvestorsListExistingMailing';
import statusFilterPlaceholderLabel from '@salesforce/label/c.InvestorsListStatusFilterPlaceholder';
import priorityFilterPlaceholderLabel from '@salesforce/label/c.InvestorsListPriorityFilterPlaceholder';
import advisorFilterPlaceholderLabel from '@salesforce/label/c.InvestorsListAdvisorFilterPlaceholder';

// Took from Mobile Application because values are the same
import available from '@salesforce/label/c.mobilePropertyExplorerAvailable';
import chosen from '@salesforce/label/c.mobilePropertyExplorerChosen';
import saveContactInvestorsListItems from '@salesforce/apex/MyListController.saveContactInvestorsListItems';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';




/***********************/
/****** Constants ******/
/***********************/

// constants for sorting by default
const DEFAULT_SORT_FIELD = 'Name';
const DEFAULT_SORT_DIRECTION = 'asc';

/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 300;
const DEFAULT_RECORDTYPE = '012000000000000AAA';

/**
 * Component for handling Investors on an overview
 *
 * @version 1.0.0
 * @author andreas.reichert@youperience.com
 */
export default class InvestorsListComponent extends NavigationMixin(LightningElement) {
    @track columns = [
        {label: '',type: 'pinButtonRow', fieldName: 'Id',fixedWidth: 36,typeAttributes: {isPinned: { fieldName: 'isPinned', accountId: 'accountId' }},cellAttributes: { class: { fieldName: 'bgClass' }}},
        {label: actionLabel, type: 'actionButtonRow', fieldName: 'Id', fixedWidth: 80,  typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}},
        {label: priorityLabel, type: 'priorityPicklist', sortable: 'true', fieldName: 'Id',fixedWidth: 120,typeAttributes: {priority: { fieldName: 'priority'}},cellAttributes: { class: { fieldName: 'bgClass' }}},
        {label: nameLabel, fieldName: 'accountLink',initialWidth: 150,type: 'url',sortable: 'true',typeAttributes: { label: { fieldName: 'Name' }, target: '_blank' },cellAttributes: { class: { fieldName: 'bgClass' }}},
        {label: listOfContacts, type: 'contactsList', fieldName: 'Id', initialWidth: 150, typeAttributes: {listOfContacts: { fieldName: 'listOfContacts'}}, cellAttributes: { class: { fieldName: 'bgClass' }} },
        {label: advisorUpdate, fieldName: 'advisorLink',type: 'url',sortable: 'true',typeAttributes: { label: { fieldName: 'advisorName' }, target: '_blank' },cellAttributes: { class: { fieldName: 'bgClass' }}},
        {label: statusLabel, type: 'statusPicklist', fieldName: 'Id',fixedWidth: 160,typeAttributes: {status: { fieldName: 'status'}},cellAttributes: { class: { fieldName: 'bgClass' }}},
        {label: statusSince, fieldName: 'statusSince',initialWidth: 105, type: 'date', sortable: 'true', cellAttributes: { class: { fieldName: 'bgClass' }}},
        {label: approvedLabel, type: 'approvedPicklist', fieldName: 'Id',fixedWidth: 160,typeAttributes: {approved: { fieldName: 'approved'}},cellAttributes: { class: { fieldName: 'bgClass' }}},
        {label: lastActivities, type: 'lastActivitiesList', fieldName: 'Id',initialWidth: 350, wrapText: true, typeAttributes: {listOfLastActivities: { fieldName: 'listOfLastActivities'}}, cellAttributes: { class: { fieldName: 'bgClass' }} },
        {label: lastBid, fieldName: 'lastBidDate', type: 'date', sortable: 'true', cellAttributes: { class: { fieldName: 'bgClass' }},},
        {label: lastBidAmount, fieldName: 'lastBidAmount',initialWidth: 180, type: 'currency',sortable: 'true', cellAttributes: { class: { fieldName: 'bgClass', alignment: 'left' }},},
        {label: underbidder, type: 'underbidderPicklist', fieldName: 'investor',fixedWidth: 120, typeAttributes: {underbidderValues: { fieldName: 'underbidderValues'}}, sortable: 'true', cellAttributes: { class: { fieldName: 'bgClass' }}},
        {label: declination, fieldName: 'declination', type: 'text', cellAttributes: { class: { fieldName: 'bgClass' }}}
    ];

    @wire(CurrentPageReference) pageRef;
    @track isLoading = false;

    // opportunity Id
    @track recordId;

    // raw result of Investors data - do not use for datatable!
    @track data;
    @track error;
    // Table sorting
    @track sortBy = DEFAULT_SORT_FIELD;
    @track sortDirection = DEFAULT_SORT_DIRECTION;

    @track disableAllMassEditButtons = true;
    @track showSidebar = false;

    _records;
    // table data
    @track records;
    // Account search key
    @track searchKey = '';
    @track selectedRecordId;
    @track resetFilterDisabled = true;

    @track _selectedStatus = [];
    @track defaultRecordTypeId = DEFAULT_RECORDTYPE;

    accountFilter;
    stageFilter;
    priorityFilter;
    advisorFilter;
	advisorField = {objectApiName: "AccountToOpportunity__c", apiName: "Advisor__c", isMultiEntry: true, labelHidden: true};
	existingAdvisors = new Set();

    underbidderPicklistEntries = null;
    keyToDisableCaching;

    // opportunity Name for Header
    get name() {
        let returnValue;
        if (this.opportunity && this.opportunity.data) {
            returnValue = this.opportunity.data.fields.Name.value;
        } else {
            returnValue = null;
        }
        return returnValue;
    }

    get accountId() {
        let returnValue;
        if (this.opportunity && this.opportunity.data) {
            returnValue = this.opportunity.data.fields.AccountId.value;
        } else {
            returnValue = null;
        }
        return returnValue;
    }

    get showOpenbutton() {
        return this.selectedRecordId && !this.showSidebar;
    }

    // array for semicolon separated investor ids
    pinnedInvestorIds;

    // UI-Labels, lateron import from custom labels
    label = {
        searchPlaceholder,
        investorsReport,
        statusUpdate,
        approvalUpdate,
        approvalUpdateReject,
        removeFromSelection,
        pinToastTitle,
        unpinToastTitle,
        pinToastMessage: '',
        noInvestorsFoundTitle,
        noInvestorsFoundMessage,
        back,
        toSelection,
        resetFilter,
        prioUpdate,
        advisorUpdate,
        available,
        chosen,
        statusLabel,
        underbidders,
        newMailing,
        updateLabel,
        existingMailing,
        statusFilterPlaceholderLabel,
        priorityFilterPlaceholderLabel,
        advisorFilterPlaceholderLabel
    };

    eventChannel;
    taskChannel;
    contactChannel;
    statusChannel;

    @wire(CurrentPageReference) pageRef;
    connectedCallback() {
        this.keyToDisableCaching = new Date().getTime();
        if (this.pageRef && this.pageRef.state.c__recordId) {
            this.recordId = this.pageRef.state.c__recordId;
        }

        registerListener('payloadrender', this.handlePayloadRender, this);
        registerListener('investorsListPriorityPicklist_priorityChange', this.updateRowWithPriority, this);
        registerListener('investorsListApprovedPicklist_approvedChange', this.updateRowWithApproved, this);
        registerListener('investorsListStatusPicklist_statusChange', this.updateRowWithStatus, this);
        registerListener('investorsList_underbidderUpdated', this.updateUnderbidder, this);
        registerListener('investorsListUnderbiddersModal_recordsUpdated', this.updateUnderbidders, this);
        registerListener('investorsListUnderbiddersModal_updateHasBeenCancelled', this.closingHasBeenCancelled, this);

        this.subscribeToChangeEvent('/data/EventChangeEvent', this.eventChannel);
        this.subscribeToChangeEvent('/data/TaskChangeEvent', this.taskChannel);
        this.subscribeToChangeEvent('/data/ContactChangeEvent', this.contactChannel);
        this.subscribeToChangeEvent('/data/Status__ChangeEvent', this.statusChannel);
        this.calculateTableHeight();
    }

    @track
    tableStyle; // is for dynamic height for our table
    calculateTableHeight()
    {
        let windowheight = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight;
        if (windowheight)
        {
            // 470 is a height of control elements
            windowheight = windowheight - 470;
        }
        else
        {
            windowheight = 600;
        }
        this.tableStyle = `padding-top: 7px; height: ${windowheight}px;`;
    }

    subscribeToChangeEvent(channelName, channel)
    {
        let self = this;
        const messageCallback = function(response) {
            console.log('...New message received : ', JSON.stringify(response));
            self.refreshTable();
        };
        subscribe(channelName, -1, messageCallback).then(response => {
            console.log('...Successfully subscribed to : ', JSON.stringify(response));
            channel = response;
        });

    }

    disconnectedCallback() {
        unregisterAllListeners(this);
        unsubscribe(this.eventChannel, response => {});
		unsubscribe(this.taskChannel, response => {});
        unsubscribe(this.contactChannel, response => {});
		unsubscribe(this.statusChannel, response => {});
    }

    @wire(getRecord, {
        recordId: '$recordId',
        fields: [OPPORTUNITY_NAME_FIELD, OPPORTUNITY_ACCOUNT_ID_FIELD]
    })
    opportunity;

    @wire(getPicklistValues, {
        recordTypeId: DEFAULT_RECORDTYPE,
        fieldApiName: STAGE_FIELD
    })
    stagePicklistValues;

	@wire(getPicklistValues, {
		recordTypeId: DEFAULT_RECORDTYPE,
		fieldApiName: PRIORITY_FIELD
	})
	priorityPicklistValues;

    // lazy load variables
    @track
    dataTableIsLoading = false;
    
    initialLimitSize = 25;
    dataLimitSizeStep = 10;
    dataLoadingFinished = false;
    lastLoadedDataSize = 0;
    currentLoadedDataSize = 0;
    /**
     *
     * @param {*} result
     *
     * @description
     */
    @wire(findInvestors, { searchKey: '$searchKey', recordId: '$recordId', keyToDisableCaching: '$keyToDisableCaching', limitSize: '$initialLimitSize'})
    async findInvestors(result) {
        if (result.data) {
            if (result.data.length === 0) {
                // Generate a URL to a User record page
                this[NavigationMixin.GenerateUrl]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: this.recordId,
                        actionName: 'view'
                    }
                }).then(url => {
                    const event = new ShowToastEvent({
                        title: this.label.noInvestorsFoundTitle,
                        message: this.label.noInvestorsFoundMessage,
                        messageData: [
                            this.name,
                            {
                                url,
                                label: this.label.back
                            }
                        ],
                        variant: 'warning',
                        mode: 'pester'
                    });
                    this.dispatchEvent(event);
                });
            }

            if (!this.underbidderPicklistEntries) {
                this.underbidderPicklistEntries = JSON.parse(await getUnderbidderPicklistEntries());
            }

            let resultArray = this.prepareData(result);
            console.log('...result from findInvestors: ' + JSON.stringify(result));

            this._records = resultArray;
            this.records = resultArray;

            // new statuses should be shown after refresh
			let newStatuses = [];
			for (let i = 0; i < this.records.length; i++) {
				newStatuses.push({Status__c: this.records[i]['status'], Id: this.records[i]['Id']})
			}
			fireEvent(this.pageRef, 'investorsListStatusPicklist_refresh', newStatuses);

            // we should not override initialLoading
            this.filterAccounts(true);

            // variable to prevent lazy load when we have no any records
            this.currentLoadedDataSize = this._records ? this._records.length : 0;

            // On initial load -> custom sorting
            this.sortData('initialLoading', this.sortDirection);
            this.data = result;
            console.log('...this.data from findInvestors: ' + JSON.stringify(this.data));
            this.error = undefined;
            this.dataTableIsLoading = false;
        } else if (result.error) {
            this.error = result.error;
            this.records = undefined;
            this.dataTableIsLoading = false;
        }
    }

    loadMoreDataRecords()
    {
        if (this.accountFilter)
        {
            return;
        }

        if (this.stageFilter)
        {
            return;
        }

        if (this.dataLoadingFinished)
        {
            this.dataTableIsLoading = false;
            return;
        }

        if (this.lastLoadedDataSize < this.currentLoadedDataSize)
        {
            this.lastLoadedDataSize = this._records ? this._records.length : 0;
            
            this.dataTableIsLoading = true;
            this.initialLimitSize = this.initialLimitSize + this.dataLimitSizeStep;
            this.keyToDisableCaching = new Date().getTime();
            refreshApex(this.data);
        }
        else
        {
            this.dataTableIsLoading = false;
            this.dataLoadingFinished = true;
        }
    }


    @wire(loadCampaigns, {opportunityId: '$recordId', loadOnlyActive: false})
    async loadCampaigns(result) {
        if (result.data) {
            console.log('...' + result.data.length);
            this.existingMailingDisabled = result.data.length === 0;
        }
    }

    @track
    existingMailingDisabled = true;

    handleKeyChange(event) {
        window.clearTimeout(this.delayTimeout);
        this.accountFilter = event.target.value;
        this.resetFilterDisabled = false;

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.filterAccounts();
        }, DELAY);
    }

    /**
     * filter accounts
     */
     filterAccounts(withoutSort) {
        console.log('...filterAccounts:');
        const accountFilter = this.accountFilter ? this.accountFilter.toLowerCase() : null;
        const stageFilter = this.stageFilter ? this.stageFilter.toLowerCase() : null;
        const priorityFilter = this.priorityFilter ? this.priorityFilter.toLowerCase() : null;
        const advisorFilter = this.advisorFilter ? this.advisorFilter : null;


         if (!accountFilter && !stageFilter && !priorityFilter && !advisorFilter) {
             this.records = this._records;
             return;
         }

         let stageFilterValues = [];
         if(stageFilter != null){
            stageFilterValues = stageFilter.split(',');
         }

         let advisorFilterValues = [];
         if (advisorFilter != null)
         {
             advisorFilterValues = advisorFilter.split(',');
             console.log('...advisorFilterValues: ' + advisorFilterValues);
         }

         let priorityFilterValues = [];
         if (priorityFilter != null)
         {
             priorityFilterValues = priorityFilter.split(',');
         }

         let self = this;
         this.records = this._records.filter(function(el) {
             let stageMatch;
             let priorityMatch;

             priorityMatch = priorityFilter ? self.checkPicklistMatch(priorityFilterValues, 'priority', el) : true;
             stageMatch = stageFilter ? self.checkPicklistMatch(stageFilterValues, 'status', el) : true;

             let advisorMatch = advisorFilter ? self.checkIdMatch(advisorFilterValues, 'advisorId', el) : true;
				console.log('...advisorFilter:' + advisorFilter + ' advisorMatch:' + advisorMatch);
             let accountNameMatch = accountFilter ? el.Name.toLowerCase().includes(accountFilter) : true;
             if (!accountNameMatch && accountFilter)
             {
                 console.log('...search contact match');
                 let accountContactMatch;
                  for (let i = 0; i < el.contacts.length; i++) {
                      if (el.contacts[i].firstName
                          && el.contacts[i].lastName
                          && el.contacts[i].firstName
                              .toLowerCase()
                              .indexOf(accountFilter) > -1 ||
                          el.contacts[i].lastName
                              .toLowerCase()
                              .indexOf(accountFilter) > -1
                      ) {
                          accountContactMatch = true;
                      }
                  }
                  accountNameMatch = accountContactMatch;
             }

             return accountNameMatch && stageMatch && priorityMatch && advisorMatch;
         });
		console.log('...this.records: ' + JSON.toString(this.records));
		if (!withoutSort)
		{
		    console.log('...sorted in filter accs');
        	this.sortData(this.sortBy, this.sortDirection);
        }
     }

     checkPicklistMatch(filterValues, fieldName, el)
     {
         let match = false;
		 for(let i = 0; i < filterValues.length; i++){
			 if(el[fieldName] || el[fieldName] !== undefined){
				 if(filterValues[i] != '' && filterValues[i] == el[fieldName].toLowerCase()){
					 match = true;
					 break;
				 }
			 }
		 }
		 return match;
     }

	  checkIdMatch(filterValues, fieldName, el)
	  {
		 let match = false;
		 for(let i = 0; i < filterValues.length; i++){
			 if(el[fieldName] || el[fieldName] !== undefined){
				 if(filterValues[i] != '' && filterValues[i].toString() === el[fieldName].toString()){
					 match = true;
					 break;
				 }
			 }
		 }
		 return match;
	}

    /**
     *
     * @param {*} result Investors data fetchied from InvestorsListController.getInvestorsList or InvestorsListController.findInvestors
     *
     * @description creates data object for datatable
     */
    prepareData(result) {
        let pinnedInvestorIds;
        let resultArray = [];
        // store data for refreshApex method
        this.data = result;
        for (let i = 0; i < result.data.length; i++) {
            // create data object for table row
            let dataObject = {};
            dataObject.Id = result.data[i].Id;
            dataObject.Name = result.data[i].Account__r.Name;
            dataObject.listOfContacts = result.data[i].ListOfContacts__c;
            dataObject.listOfLastActivities = result.data[i].LastActivitiesHistory__c;
            dataObject.status = result.data[i].Status__c;
            dataObject.lastStatus = result.data[i].LastStatus__c;
            dataObject.statusSince = result.data[i].LastStatusDate__c;
            dataObject.approved = result.data[i].Approved__c;
            dataObject.lastActivityDate = result.data[i].LastActivityDate__c;
            dataObject.lastVisitDate = result.data[i].LastVisitDate__c;
            dataObject.lastBidDate = result.data[i].LastBidDate__c;
            dataObject.lastBidAmount = result.data[i].LastBidAmount__c;
            dataObject.priority = result.data[i].Priority__c;
            dataObject.investor = {
                Id: result.data[i].Id,
                LastBidDate__c: result.data[i].LastBidDate__c,
                LastBidAmount__c: result.data[i].LastBidAmount__c,
                Underbidder__c: result.data[i].Underbidder__c
            };
            dataObject.underbidder = result.data[i].Underbidder__c;
            dataObject.underbidderValues = this.underbidderPicklistEntries;
            dataObject.declination = result.data[i].ReasonForRejection__c
                ? result.data[i].ReasonForRejection__c
                : '-';
            // set the id's array
            if (!pinnedInvestorIds) {
                if (result.data[i].Opportunity__r.Pinned_Investors__c) {
                    pinnedInvestorIds =
                        result.data[i].Opportunity__r.Pinned_Investors__c;
                    this.pinnedInvestorIds = pinnedInvestorIds.split(';');
                } else {
                    this.pinnedInvestorIds = [];
                }
            }
            dataObject.contacts = [];
            if (result.data[i].InvestorContacts__r) {
                for (
                    let j = 0;
                    j < result.data[i].InvestorContacts__r.length;
                    j++
                ) {
                    let contact = {};
                    contact.firstName =
                        result.data[i].InvestorContacts__r[
                            j
                        ].Contact__r.FirstName;
                    contact.lastName =
                        result.data[i].InvestorContacts__r[
                            j
                        ].Contact__r.LastName;
                    contact.accountName =
                        result.data[i].InvestorContacts__r[
                            j
                        ].Contact__r.Account.Name;
                    dataObject.contacts.push(contact);
                }
            }
            dataObject.accountLink = '/lightning/r/Account/' + result.data[i].Account__c + '/view';
            dataObject.accountId = result.data[i].Account__c;
            // create a "pinned" field if Id is contained in the pinned list
            dataObject.isPinned = this.pinnedInvestorIds.indexOf(result.data[i].Id) > -1;

            if(dataObject.declination !== '-' || dataObject.approved === 'No'){
                dataObject.bgClass = 'slds-theme--shade slds-theme--alert-texture';
            }

            if(typeof result.data[i].Advisor__c !== 'undefined'){
                dataObject.advisorLink = '/lightning/r/User/' + result.data[i].Advisor__c + '/view';
                dataObject.advisorName = result.data[i].Advisor__r.Name;
                dataObject.advisorId = result.data[i].Advisor__c;
                this.existingAdvisors.add(result.data[i].Advisor__c.toString());
            }

            resultArray.push(dataObject);
        }
        return resultArray;
    }
    /**
     *
     * @param {*} event Datatable Sort Event
     *
     * @description event handler for sorting the header fields
     */
    handleSortData(event) {
        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    /**
     *
     * @param {*} fieldname Name of the field to be sorted
     * @param {*} direction "asc" or "desc"
     *
     * @description function to sort the array
     */
    sortData(fieldname, direction) {
        this.sortBy = fieldname;
        this.sortDirection = direction;

        // we need to remap fieldname because of that columns data is a hyperlink
        if (fieldname === 'accountLink') {
            fieldname = 'Name';
        } else if (fieldname === 'Id') {
            fieldname = 'priority';
        } else if (fieldname === 'investor') {
            fieldname = 'underbidder';
        }

        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.records));
        let sortedList = [];
        let unpinnedRecords = [];
        for (let i = 0; i < parseData.length; i++) {
            let element = parseData[i];
            if (element.Id && this.pinnedInvestorIds.indexOf(element.Id) > -1) {
                sortedList.push(element);
            }else{
                unpinnedRecords.push(element);
            }
        }

        let keyValue = (a, b) => {return a[fieldname];};
        let isReverse = direction === 'asc' ? 1 : -1;

        if(this.sortBy !== 'initialLoading'){
            // sorting data
            unpinnedRecords.sort((x, y) => {
                x = keyValue(x, isReverse) ? keyValue(x, isReverse) : ''; // handling null values
                y = keyValue(y, isReverse) ? keyValue(y, isReverse) : '';
                return isReverse * ((x > y) - (y > x));
            });

            for (let i = 0; i < unpinnedRecords.length; i++) {
                sortedList.push(unpinnedRecords[i]);
            }
        }else{
            // Special sorting on init load:
            for (let i = 0; i < unpinnedRecords.length; i++) {
                if(unpinnedRecords[i].declination === '-' && unpinnedRecords[i].approved !== 'No'){
                    sortedList.push(unpinnedRecords[i]);
                }
            }
            for (let i = 0; i < unpinnedRecords.length; i++) {
                if(unpinnedRecords[i].declination === '-' && unpinnedRecords[i].approved === 'No'){
                    sortedList.push(unpinnedRecords[i]);
                }
            }
            for (let i = 0; i < unpinnedRecords.length; i++) {
                if(unpinnedRecords[i].declination !== '-'){
                    sortedList.push(unpinnedRecords[i]);
                }
            }
        }

        // set the sorted data to data table data
        this.records = sortedList;
    }


    /**
     *
     * @param {*} event Custom event dispatched by the pin button (investorsListPinButton)
     *
     * @description stores the "pinned" value in a comma separated list
     */
    async handlePinRow(event) {
        const { rowId, isPinned } = event.detail;

        await setPinned({
            opportunityId: this.recordId,
            accountId: rowId,
            pin: !isPinned
        });
        let pinToastMsg = isPinned
            ? this.label.unpinToastTitle
            : this.label.pinToastTitle;
        this.dispatchToast(pinToastMsg, this.label.pinToastMessage, 'success');

        refreshApex(this.data);
    }

    /**
     *
     * @param {*} event Custom event dispatched by the activity button (investorsListActivityButton)
     *
     * @description opens the Activity Modal Window
     */
    handleRowAction(event) {
        const row = event.detail;
        const child = this.template.querySelector(
            'c-investors-list-activity-modal'
        );
        let listOfContacts = '';
        this.records.forEach(function(item){
            if (item.Id === event.detail.rowId)
            {
                listOfContacts = item.listOfContacts;
            }
        });
        console.log('...handleRowAction.listOfContacts: ' + listOfContacts);
        child.listOfContacts = listOfContacts;
        child.openModal(row);

    }

    /**
     *
     * @param {*} event Event thrown by datatable component if checkbox is selected or deselected
     *
     * @description Handles the selection of a single row (no selection or multiple selections are skipped)
     */
    handleRowSelection(event) {
        if (event.detail.selectedRows.length === 1) {
            this.disableAllMassEditButtons = false;
        } else {
            this.disableAllMassEditButtons = event.detail.selectedRows.length === 0;
        }
    }
    handleShowdetails(event){
        this.selectedRecordId = event.detail.rowId;
        this.showSidebar = true;
        // US2378 npo: Display Investor details with a Sidenav
        setTimeout(() => {
            let sidebar = this.template.querySelector('.sidenav');
            sidebar.focus();
            window.scrollTo(0, 0);
        })

    }

    closePanel(event){
        this.showSidebar = false;
    }

    openPanel(event){
        this.showSidebar = true;
    }

    /**
     * @description handles click on updateStatus button. Calls investorsListStateModal component
     */
    updateStatus() {
        const sm = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = sm.getSelectedRows();
        if (selectedRows && selectedRows.length > 0) {
            const child = this.template.querySelector(
                'c-investors-list-state-modal'
            );
            child.openModal(selectedRows, '', '');
        }
    }

    /**
     * @description handles click on updateApproval button
     */
    massApprove() {
        this.massUpdateRecords('Yes');
    }

    massReject() {
        this.massUpdateRecords('No');
    }

    massUpdateRecords(approvalValue){
        this.isLoading = true;
        const sm = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = sm.getSelectedRows();
        if (selectedRows && selectedRows.length > 0) {
            let investors = [];

            for(let i = 0; i < selectedRows.length; i++){
                investors.push({Id: selectedRows[i].Id, Approved__c: approvalValue});
            }

            updateInvestorsCall({investors : investors})
            .then(result => {
                this.isLoading = false;
                fireEvent(this.pageRef, 'investorsListApprovedPicklist_refresh', investors);
                return refreshApex(this.data);
            })
            .catch(error => {
                console.log('Error:' + error);
            });
        }
    }

    /**
     * @description handles click on updatePrio button
     */
    updatePriority() {
        const sm = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = sm.getSelectedRows();
        if (selectedRows && selectedRows.length > 0) {
            const child = this.template.querySelector(
                'c-investors-list-priority-modal'
            );

            child.openModal(selectedRows);
        }
    }

    updateAdvisor(){
        const sm = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = sm.getSelectedRows();
        if (selectedRows && selectedRows.length > 0) {
            const child = this.template.querySelector(
                'c-investors-list-advisor-modal'
            );

            child.openModal(selectedRows);
        }
    }

    underbiddersSelection() {
        const sm = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = sm.getSelectedRows();
        if (selectedRows && selectedRows.length > 0) {
            this.showUnderbiddersModal(selectedRows);
        }
    }

    /**
     * @description handles click on the deleteInvestor Button. Calls the investorsListDeleteModal component
     */
    removeFromSelection() {
        const dt = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = dt.getSelectedRows();
        if (selectedRows && selectedRows.length > 0) {
            const child = this.template.querySelector(
                'c-investors-list-delete-modal'
            );

            child.openModal(selectedRows);
        }
    }

    /**
     *
     * @param {*} event Customevent called by modal windows
     *
     * @description callback function for the modal windows to refresh the data in the datatable and show a toast message.
     */
    handleRefreshInvestors(event) {
        const { toastTitle, toastMessage, toastVariant } = event.detail;
        this.dispatchToast(toastTitle, toastMessage, toastVariant);
        this.refreshTable();
    }

    refreshTable()
    {
        this.keyToDisableCaching = new Date().getTime();
        refreshApex(this.data);
        console.log('...this.data:' + JSON.stringify(this.data));
        const sp = this.template.querySelector('c-investors-list-side-panel');
        if (sp) {
            sp.refresh();
        }
    }

    handleStageChange(event) {
        this._selectedStatus = event.detail;
        this.resetFilterDisabled = false;

        this.stageFilter = '';
        for(let i = 0; i < this._selectedStatus.length; i++){
           for(let j= 0; j < this.stagePicklistValues.data.values.length; j++){
                if(this._selectedStatus[i] === this.stagePicklistValues.data.values[j].value){
                    this.stageFilter +=  this.stagePicklistValues.data.values[j].value + ',';
                }
           }
        }

        this.filterAccounts();
    }

	handlePriorityChange(event) {
		this._selectedPriority = event.detail;
		this.resetFilterDisabled = false;

		this.priorityFilter = '';
		for(let i = 0; i < this._selectedPriority.length; i++){
		   for(let j= 0; j < this.priorityPicklistValues.data.values.length; j++){
				if(this._selectedPriority[i] === this.priorityPicklistValues.data.values[j].value){
					this.priorityFilter +=  this.priorityPicklistValues.data.values[j].value + ',';
				}
		   }
		}

		this.filterAccounts();
	}

    handleInvestorsDeleted(event){
        refreshApex(this.data);

        window.clearTimeout(this.delayTimeout);

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.filterAccounts();
        }, DELAY);
    }

    handleResetFilter() {
        this.resetFilterDisabled = true;
        let filters = this.template.querySelectorAll('c-autocomplete-combobox');
		filters.forEach(function(filter) {
		    filter.reset();
		});
		this.template.querySelector('[data-id="advisorFilter"]').selection = [];
		this.advisorFilter = undefined;
        // get account search
        const search = this.template.querySelector(
            '[data-field="account-filter"]'
        );
        if (search) {
            search.value = null;
            this.accountFilter = null;
        }

        this.records = this._records;
    }

    handleActivityCreated(event)
    {
        this.handleRefreshSidePanel(event);
        this.handleRefreshInvestors(event);
    }

    handleRefreshSidePanel(event) {
        console.log('...handleRefreshSidePanel');
        const actModal = this.template.querySelector(
            'c-investors-list-activity-modal'
        );
        actModal.closeModal();
        // show toast message
        const { toastTitle, toastMessage, toastVariant } = event.detail;
        this.dispatchToast(toastTitle, toastMessage, toastVariant);

        refreshApex(this.data);
        // refresh sidepanel if needed
        const sp = this.template.querySelector('c-investors-list-side-panel');
        if (sp) {
            sp.refresh();
        }

        const investorRecord = event.detail.investor;
        if (investorRecord.Status__c === 'Closing') {
            for (let i = 0; i < this.records.length; i++) {
                if (this.records[i]['Id'] === investorRecord.Id) {
                    this.records[i]['status'] = investorRecord.Status__c;
                    break;
                }
            }
            this.showUnderbiddersModal(this.records);
        }
    }

    handleOnselect(event){
        let selectedItemValue = event.detail.value;
        if(selectedItemValue == 'prioUpdate'){
            this.updatePriority();
        }

        if(selectedItemValue == 'advisorUpdate'){
            this.updateAdvisor();
        }
        if(selectedItemValue == 'statusUpdate'){
            this.updateStatus();
        }
        if(selectedItemValue == 'approvalUpdate'){
            this.massApprove();
        }
        if(selectedItemValue == 'approvalUpdateReject'){
            this.massReject();
        }
        if(selectedItemValue == 'underbidders'){
            this.underbiddersSelection();
        }
    }

    showUnderbiddersModal(records, investorWhichIsGettingClosed, prevStatus) {
        const child = this.template.querySelector('c-investors-list-underbidders-modal');
        child.investorPreviousStatus = prevStatus;
        child.openModal(records, investorWhichIsGettingClosed);
    }

    /**
     *
     * @param {*} title Title of the toast displayed
     * @param {*} message Message displayed in the toast
     * @param {*} variant Variant of the toast (info (default), success, warning, and error)
     */
    dispatchToast(title, message, variant) {
        this.dispatchEvent(new ShowToastEvent({title: title,message: message,variant: variant}));
    }

    // Update from Priotity picklist
    updateRowWithPriority(data){
        this.isLoading = true;
        const fields = {};
        fields['Id'] = data.id;
        fields['Priority__c'] = data.priority;
        this.updateRow(fields);
    }

    // Update from Approved picklist
    updateRowWithApproved(data){
        this.isLoading = true;
        const fields = {};
        fields['Id'] = data.id;
        fields['Approved__c'] = data.approved;
        this.updateRow(fields);
    }

    // General update
    updateRow(fields){
        const recordInput = { fields };
        updateRecord(recordInput)
        .then(() => {
            this.isLoading = false;
            return refreshApex(this.data);
        })
        .catch(error => {
            this.dispatchToast('Error updating record', error.body.message, 'error');
            this.isLoading = false;
        });
    }

    // Update from Status picklist (other update method to populate status record)
    updateRowWithStatus(data){
        let prevStatus = '';
        for (let i = 0; i < this.records.length; i++) {
            if (this.records[i]['Id'] === data.id) {
                prevStatus = this.records[i]['status'];
                break;
            }
        }
        if (data.status === 'Closing') {
            for (let i = 0; i < this.records.length; i++) {
                if (this.records[i]['Id'] === data.id) {
                    prevStatus = this.records[i]['status'];
                    this.records[i]['status'] = data.status;
                    break;
                }
            }
            this.showUnderbiddersModal(this.records, {...data, Id: data.id, Status__c: data.status}, prevStatus);
            return;
        }

        this.isLoading = true;
        let investors = [{Id: data.id, Status__c: data.status}];
        let today = new Date();

        updateInvestorsCallWithStatus({investors : investors, statusDate : today, createNewStatus : true})
        .then(result => {
            const sp = this.template.querySelector('c-investors-list-side-panel');
            if (sp) {
                sp.refresh();
            }

            this.isLoading = false;

            refreshApex(this.data);
        })
        .catch(error => {
            this.dispatchToast('Error updating record', error.body ? error.body.message : JSON.stringify(error), 'error');
            this.isLoading = false;
        });
    }

    updateUnderbidder(record) {
        for (let i = 0; i < this.records.length; i++) {
            if (this.records[i]['Id'] === record['Id']) {
                this.records[i]['underbidder'] = record['Underbidder__c'];
                break;
            }
        }
    }

    updateUnderbidders(records) {
        if (records.underbidders) {
            for (const record of records.underbidders) {
                this.updateUnderbidder(record);
            }
        }
    }

    @track currentPayloadRequestId;
    handlePayloadRender(event) {
        var recordId = event.recordId;
        var shouldOpen = event.open;
        var top = event.top;
        var left = event.left;

        //render Request JSON data as table
        if ( shouldOpen && !this.currentPayloadRequestId && recordId)
        {
            this.currentPayloadRequestId = recordId;
            this.currentPayloadTop = top;
            this.currentPayloadLeft = left + 25;
        }
        else if ( !shouldOpen && this.currentPayloadRequestId && recordId)
        {
            this.currentPayloadRequestId = undefined;
            this.currentPayloadTop = 0;
            this.currentPayloadLeft = 0;
        }
    }

    /**
    *  US2378 npo: Display Investor details with a Sidenav
    */
    handleKeyPress({code}) {
        if ('Escape' === code) {
            this.showSidebar = false;
        }
    }

    handleNewMailingClick(event)
    {
        this.handleShowNewMailingModal();
    }
    handleExistingMailingClick(event)
    {
        this.handleShowExistingMailingModal();
    }
    /**
    *  US1910 npo
    */
    takeSelectedInvestors()
    {
        const sm = this.template.querySelector('c-investors-list-datatable');
        let selectedRows = sm.getSelectedRows();
        let investors = [];
        if (selectedRows && selectedRows.length > 0)
        {
            for(let i = 0; i < selectedRows.length; i++)
            {
                investors.push(selectedRows[i].Id);
            }
        }
        return investors;
    }

     handleShowNewMailingModal() {
        const modal = this.template.querySelector('c-investors-list-new-mailing-modal');
        modal.selectedInvestors = this.takeSelectedInvestors();
        modal.show();
    }

     handleShowExistingMailingModal() {
        const modal = this.template.querySelector('c-investors-list-existing-mailing-modal');
        modal.selectedInvestors = this.takeSelectedInvestors();
        modal.opportunityId = this.recordId;
        modal.show();
    }

    handleCancelNewMailingModal() {
        const modal = this.template.querySelector('c-investors-list-new-mailing-modal');
        modal.hide();
    }

    closingHasBeenCancelled(data)
    {
        let investorWhichIsGettingClosed = data.investorWhichIsGettingClosed.Id;
        let investorPreviousStatus = data.investorPreviousStatus + '';
        for (let i = 0; i < this.records.length; i++) {
            if (this.records[i]['Id'] === investorWhichIsGettingClosed) {
                this.records[i]['status'] = investorPreviousStatus;
                break;
            }
        }
    }

	handleAdvisorLookupSearch(event) {
		var searchParam = {
			parentObjectApiName: this.advisorField.objectApiName,
			lookupApiName: this.advisorField.apiName,
			searchTerm: event.detail.searchTerm
		};

		let self = this;
		lookupSearch(searchParam)
			.then(results => {
			    let filteredResults = results.filter(function(el){
			        console.log('...el:' + JSON.stringify(el));
			        return self.existingAdvisors.has(el.id);
				});

				this.template
					.querySelector('[data-id="advisorFilter"]')
					.setSearchResults(filteredResults);
			})
			.catch(error => {
			    const event = new ShowToastEvent({
					title: 'Advisor search error',
					message: 'Advisor search error',

					variant: 'error',
					mode: 'pester'
				});
				this.dispatchEvent(event);
			});
	}

	handleAdvisorFilterChanged(event)
	{
	    this.resetFilterDisabled = false;
	    let advisorFilter = [];
        if(event.target.selection && event.target.selection.length > 0) {
            for(let i = 0; i < event.target.selection.length; i++)
            {
                advisorFilter.push(event.target.selection[i].id);
            }
        }
        this.advisorFilter = advisorFilter.toString();
        this.filterAccounts();
    }
}