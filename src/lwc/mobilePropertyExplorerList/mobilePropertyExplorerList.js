import { LightningElement, api, track } from 'lwc';

import numberOfResults from '@salesforce/label/c.mobilePropertyExplorerNumberOfResults';

export default class MobilePropertyExplorerList extends LightningElement {

    @track properties;
    numberOfResults;

    label = {
        numberOfResults
    };

    @api setProperties(properties){
        this.properties = properties;
        this.numberOfResults = this.properties.length;
    }

}