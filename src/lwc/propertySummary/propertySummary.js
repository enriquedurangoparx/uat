import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';

import NAME_FIELD from '@salesforce/schema/PropertyObject__c.Name';
import YEAR_OF_CONSTRUCTION_FIELD from '@salesforce/schema/PropertyObject__c.YearofConstruction__c';
import ROOM_DEPTH_FROM_FIELD from '@salesforce/schema/PropertyObject__c.RoomDepthFrom__c';
import ROOM_DEPTH_TO_FIELD from '@salesforce/schema/PropertyObject__c.RoomDepthTo__c';
import SQM_OFFICE_FIELD from '@salesforce/schema/PropertyObject__c.SqmOffice__c';
import SQM_TOTAL_FIELD from '@salesforce/schema/PropertyObject__c.SqmInTotal__c';
import SQM_RESIDENTIAL_FIELD from '@salesforce/schema/PropertyObject__c.sqmResidential__c';
import PICTURE_FIELD from '@salesforce/schema/PropertyObject__c.Picture__c';

export default class PropertySummary extends NavigationMixin(LightningElement) {
    @api recordId;
    @track propertyFields = [NAME_FIELD,
        YEAR_OF_CONSTRUCTION_FIELD,
        ROOM_DEPTH_FROM_FIELD,
        ROOM_DEPTH_TO_FIELD,
        SQM_OFFICE_FIELD,
        SQM_TOTAL_FIELD,
        SQM_RESIDENTIAL_FIELD];

    @wire(CurrentPageReference) pageRef;

    @wire(getRecord, {
        recordId: '$recordId',
        fields: [NAME_FIELD, PICTURE_FIELD]
    })
    property;
    
    connectedCallback() {
        console.log('>>>PropertySummary -- connectedCallback');

        registerListener(
            'TileList__tileSelected',
            this.handleTileSelected,
            this
        );
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    get propertyName() {
        return getFieldValue(this.property.data, NAME_FIELD);
    }

    get pictureURL() {
        return getFieldValue(this.property.data, PICTURE_FIELD);
    }

    handleTileSelected(propertyId) {
        console.log('>>>PropertySummary -- handleTileSelected');
        console.log('>>>propertyId: ' + JSON.stringify(propertyId, null, '\t'));

        this.recordId = propertyId;
    }

    handleNavigateToRecord() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: 'PropertyObject__c',
                actionName: 'view'
            }
        });
    }
}