/**
 * Provide records for map (markers) and tiles.
 * 
 * @class
 * @param {array} properties contains records from propertyobject__c.
 * @param {array} markers contains marker-objects for markers on the map-element in second tab.
 * @param {number} pageNumber declared first page for pagination.
 * @param {number} pageSize number of tiles on one page.
 * @param {array} tileList contains tiles, see function createTile().
 * @param {number} totalItemCount number of total items for every pages.
 */

import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners  } from 'c/pubsub';
import getPagedPropertyList from '@salesforce/apex/PropertyController.getPagedPropertyList';
import getPropertyForMap from '@salesforce/apex/PropertyController.getPropertyForMap';
import { createMarker } from 'c/mapObject';
import { minYearOfConstruction, maxYearOfConstruction } from 'c/propertyFilter';

// import custom labels for datatable columns:
import propertiesLabel from '@salesforce/label/c.PropertyExplorerPropertiesLabel';
import mapLabel from '@salesforce/label/c.PropertyExplorerMapLabel';

const PAGE_SIZE = 9;

export default class PropertyTab extends LightningElement {
    @track properties = [];
    @track markers = [];
    @track pageNumber = 1;
    @track pageSize = PAGE_SIZE;
    @track tileList;
    @track totalItemCount;
    @track selectedTab = propertiesLabel;

    string_searchKey = '';
    string_minYearOfConstruction = minYearOfConstruction;
    string_maxYearOfConstruction = maxYearOfConstruction;

    @wire(CurrentPageReference) pageRef;

    label = {
        propertiesLabel,
        mapLabel
    }

    connectedCallback() {
        registerListener(
            'PropertyFilter__filterChange',
            this.handleFilterChange,
            this
        );
        this.handleLoadData();
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    handleLoadData(){
        console.log('handleLoadData: ' + this.selectedTab);
        switch (this.selectedTab) 
        {
            case this.label.mapLabel:
                getPropertyForMap({
                    string_searchKey: this.string_searchKey,
                    string_minYearOfConstruction: this.string_minYearOfConstruction, 
                    string_maxYearOfConstruction: this.string_maxYearOfConstruction, 
                })
                .then(result => {
                    let res = result;
                
                    this.totalItemCount = res.totalItemCount;
                    this.createMarkers(res.records);
                })
                .catch(error => {
                    this.error = error;
                });
                break;
            default:
                getPagedPropertyList({
                    string_searchKey: this.string_searchKey,
                    string_minYearOfConstruction: this.string_minYearOfConstruction, 
                    string_maxYearOfConstruction: this.string_maxYearOfConstruction, 
                    pageSize: this.pageSize, 
                    pageNumber: this.pageNumber
                })
                .then(result => {
                    this.properties = result;
                    this.tileList = [];
                
                    this.totalItemCount = this.properties.totalItemCount;
                    this.createTile(this.properties.records);
                })
                .catch(error => {
                    this.error = error;
                });
                break;
        }
    }

    handlePageChange(event)
    {
        this.pageNumber = event.detail.pageNumber;
        this.handleLoadData();
    }

    createTile(records){
          
        records.forEach(property => {
            let mapData= [];

            mapData.push({key: property.Id + 'YearofConstruction__c', fieldLabel: 'Year of construction: ', fieldValue: property.YearofConstruction__c});
            mapData.push({key: property.Id + 'sqmOffice__c', fieldLabel: 'sqm office space: ', fieldValue: property.sqmOffice__c});
            let tile = { 
                id : property.Id,
                title : property.Name,
                City : property.City__c,
                backgroundImageUrl : property.Thumbnail__c,
                mapData
                }
               
            this.tileList.push(tile);
        });

    }

    handleFilterChange(filters) {
        this.string_searchKey = filters.string_searchKey;
        this.string_maxYearOfConstruction = filters.string_maxYearOfConstruction;
        this.string_minYearOfConstruction = filters.string_minYearOfConstruction;

        this.handleLoadData();
    }

    createMarkers(records){
        let tempMarkers = [];
        if (records) {
            records.forEach(record => {
                if(typeof record !== undefined){
                    let marker = createMarker(
                        record.Id, 
                        record.Geolocation__Latitude__s,
                        record.Geolocation__Longitude__s,
                        typeof record.Name !== undefined ? record.Name : '', 
                        typeof record.PropertyDescriptionDe__c !== undefined ? record.PropertyDescriptionDe__c : '', 
                        '', 
                        record.Street__c, 
                        record.PostalCode__c, 
                        record.Country__c);

                    tempMarkers.push(marker); 
                }
            }); 
        }
        this.markers = tempMarkers;
        console.log('>>>PropertyTab createMarkers' + JSON.stringify(this.markers, null, '\t'));
    }  

    handleActiveTab(event)
    {
        this.selectedTab = event.target.label;
        switch (this.selectedTab) 
        {
            case this.label.mapLabel:
                getPropertyForMap({
                    string_searchKey: this.string_searchKey,
                    string_minYearOfConstruction: this.string_minYearOfConstruction, 
                    string_maxYearOfConstruction: this.string_maxYearOfConstruction
                })
                .then(result => {
                    let res = result;
                
                    this.totalItemCount = res.totalItemCount;
                    this.createMarkers(res.records);
                })
                .catch(error => {
                    this.error = error;
                });
                break;
            default:
        }
    }
}