import { LightningElement, wire, track, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

// import custom labels:
import search from '@salesforce/label/c.InvestorsSelecetionSearch';
import matching from '@salesforce/label/c.InvestorsSelectionMatching';
import underbidder from '@salesforce/label/c.InvestorsSelectionUnderbidder';
import mylists from '@salesforce/label/c.InvestorsSelectionMyLists';
import brandLabel from '@salesforce/label/c.InvestorsSelectionBrand';
import representativeLabel from '@salesforce/label/c.InvestorsSelectionRepresentative';
import streetLabel from '@salesforce/label/c.InvestorsSelectionStreet';
import cityLabel from '@salesforce/label/c.InvestorsSelectionCity';
import accountNameLabel from '@salesforce/label/c.InvestorsSelectionAccountName';
import representativeOfInvestorLabel from '@salesforce/label/c.InvestorsSelectionRepresentativeOfInvestorLabel';
import firstnameLabel from '@salesforce/label/c.InvestorsSelectionFirstname';
import lastnameLabel from '@salesforce/label/c.InvestorsSelectionLastname';
import emailLabel from '@salesforce/label/c.InvestorsSelectionEmail';

const KEY_FIELD = 'Id';
const ACCOUNT_FORM_FIELDS = ['Name', 'Brand__c', 'BillingStreet', 'BillingCity'];
const CONTACT_FORM_FIELDS = ['Title', 'FirstName', 'LastName', 'Position__c', 'Email'];

export default class InvestorSelectionTab extends LightningElement {

    // Table Metadata
    keyField = KEY_FIELD;
    @track contactColumns = [
        {label: ' ', fieldName: 'Id', type: 'checkboxContainer', cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: {disabledandchecked: {fieldName : 'disabledandchecked'}}, fixedWidth: 45, sortable: true},
        { label: accountNameLabel, fieldName: '_InvestorId',  type: 'url',  cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: { label: {fieldName: 'InvestorName'}, target:'_blank'}, sortable: true },
        { label: firstnameLabel, fieldName: '_ContactIdForName',  type: 'url',  cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: { label: {fieldName: 'FirstName'}, target:'_blank'}, sortable: true },
        { label: lastnameLabel, fieldName: '_ContactIdForSurname',  type: 'url', cellAttributes: { class: { fieldName: 'bgClass' }}, typeAttributes: { label: {fieldName: 'LastName'}, target:'_blank'}, sortable: true },
        { label: representativeLabel, fieldName: 'isRepresentative',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: streetLabel, fieldName: 'InvestorBillingStreet',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: cityLabel, fieldName: 'InvestorBillingCity',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: emailLabel, fieldName: 'Email',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: brandLabel, fieldName: '_InvestorBrandId',  type: 'url',  cellAttributes: { class: { fieldName: 'bgClass' }}, typeAttributes: { label: {fieldName: 'InvestorBrandName'}, target:'_blank'}, sortable: true },
        { label: '', type: 'actionButtonRow', fieldName: 'Id', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
    ];

    @track accountColumns = [
        { label: ' ', fieldName: 'Id', type: 'checkboxContainer', cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: {disabledandchecked: {fieldName : 'disabledandchecked'}}, fixedWidth: 45, sortable: true},
        { label: accountNameLabel, fieldName: '_InvestorId',  type: 'url',  cellAttributes: { class: { fieldName: 'bgClass' }},typeAttributes: { label: {fieldName: 'InvestorName'}, target:'_blank'}, sortable: true },
        { label: streetLabel, fieldName: 'InvestorBillingStreet',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: cityLabel, fieldName: 'InvestorBillingCity',  cellAttributes: { class: { fieldName: 'bgClass' }},sortable: true },
        { label: brandLabel, fieldName: '_InvestorBrandId',  type: 'url',  cellAttributes: { class: { fieldName: 'bgClass' }}, typeAttributes: { label: {fieldName: 'InvestorBrandName'}, target:'_blank'}, sortable: true },
        { label: '', type: 'actionButtonRow', fieldName: 'Id', fixedWidth: 46, typeAttributes: {rowStatus: { fieldName: 'Stage'}}, cellAttributes: { class: { fieldName: 'bgClass' }}}
    ];

    accountFormFields = ACCOUNT_FORM_FIELDS;
    contactFormFields = CONTACT_FORM_FIELDS;

    label = {
        search,
        matching,
        mylists,
        underbidder
    }

    @wire(CurrentPageReference) pageRef;

    // Text search
    handleSearchChangeHandler(event){
        const searchTerm = event.detail.searchTerm;
        const objectApiName = event.detail.objectApiName;

        let columnsCopy = JSON.parse(JSON.stringify(this.contactColumns));
        columnsCopy.forEach(function (item, index) {
            if(item.fieldName === '_InvestorId'){
                if(objectApiName === 'Opportunity'){
                    item.label = representativeOfInvestorLabel;
                }else{
                    item.label = accountNameLabel;
                }
            }
        });
        this.contactColumns = columnsCopy;


        const object = {
            tableMetadata: {
                columns: objectApiName === 'Account' ? this.accountColumns : this.contactColumns,
                keyField: this.keyField,
                sortedBy: this.CONTACT_SORTED_BY
            },
            formMetadata: {
                fields: {
                    account: this.accountFormFields,
                    contact: this.contactFormFields
                },
                columns: 2
            },
            searchParameter: {
                string_objectApiName: objectApiName,
                string_searchTerm: searchTerm,
            }
        }

        //Notify investorSelectionDatatable.js to search for records.
        fireEvent(this.pageRef, 'InvestorSelectionTab__searchRecords', object);

        //Notify
        const searchEvent = new CustomEvent(
            'searchchange',
            {
                detail: {
                    objectApiName: objectApiName
                }
            }
        );
        this.dispatchEvent(searchEvent);
    }



    // List view search
    handleListviewChanged(event){
        // Get Contact Objects
        let objectName = event.detail.objectName;
        let records = event.detail.records;
        let recordIds = [];
        for (var i = 0; i < records.length; i++) {
            recordIds.push(records[i].id);
        }

        // Change column if obj is Opportunity
        let columnsCopy = JSON.parse(JSON.stringify(this.contactColumns));
        columnsCopy.forEach(function (item, index) {
            if(item.fieldName === '_InvestorId'){
                if(objectName === 'Opportunity'){
                    item.label = representativeOfInvestorLabel;
                }else{
                    item.label = accountNameLabel;
                }
            }
        });
        this.contactColumns = columnsCopy;


        const object = {
            tableMetadata: {
                columns: this.contactColumns,
                keyField: this.keyField,
                sortedBy: this.CONTACT_SORTED_BY
            },
            formMetadata: {
                fields: {
                    account: this.accountFormFields,
                    contact: this.contactFormFields
                },
                columns: 2
            },
            searchParameters: {
                objectName: objectName,
                recordIds: recordIds
            }
        }
        //Notify investorSelectionDatatable.js to search for records.
        fireEvent(this.pageRef, 'InvestorSelectionTab__searchRecordsbylistview', object);

    }

    handleSearchCancelled(){
        fireEvent(this.pageRef, 'InvestorSelection__resetPage', null);
        fireEvent(this.pageRef, 'InvestorSelection__handleNoRowIsSelectedHandler', null);
    }

    handleActiveTab(event)
    {
        console.log('=== handleActiveTab ===');
        if (event.target.label === this.label.underbidder)
        {
            // fire event to refresh data on the tab
            fireEvent(this.pageRef, 'InvestorSelectionTabBidder__bidderTabInitData', null);
        }
        //Notify investorSelectionDatatable.js to show / hide dependent information.
        fireEvent(this.pageRef, 'InvestorSelection__toggleTabInfo', event.target.label);
        fireEvent(this.pageRef, 'InvestorSelection__resetPage', null);
    }
}