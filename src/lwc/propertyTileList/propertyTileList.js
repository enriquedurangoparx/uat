import { LightningElement, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';
import getPagedPropertyList from '@salesforce/apex/PropertyController.getPagedPropertyList';

const PAGE_SIZE = 9;

export default class PropertyTileList extends LightningElement {
    @track pageNumber = 1;

    @track pageSize = PAGE_SIZE;

    searchKey = '';
    minYearOfConstruction = 0;
    maxYearOfConstruction = 0;
    //maxPrice = 9999999;
    //minBedrooms = 0;
    //minBathrooms = 0;

    connectedCallback() {
        console.log('>>>PropertyTileList -- connectedCallback called.');
        registerListener(
            'propertyExplorer__filterChange',
            this.handleFilterChange,
            this
        );
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    @wire(CurrentPageReference) pageRef;

    @wire(getPagedPropertyList, {
        searchKey: '$searchKey',
        minYearOfConstruction: '$minYearOfConstruction',
        maxYearOfConstruction: '$maxYearOfConstruction',
        //numberOfRooms: '$numberOfRooms',
        //numberOfSquaremeter: '$numberOfSquaremeter',
        pageSize: '$pageSize',
        pageNumber: '$pageNumber'
    })
    properties;

    handleFilterChange(filters) {
        this.searchKey = filters.searchKey;
        /*this.yearOfConstruction = filters.yearOfConstruction;
        this.numberOfRooms = filters.numberOfRooms;
        this.numberOfSquaremeter = filters.numberOfSquaremeter;*/
    }

    handlePreviousPage() {
        this.pageNumber = this.pageNumber - 1;
    }

    handleNextPage() {
        this.pageNumber = this.pageNumber + 1;
    }

    handlePropertySelected(event) {
        fireEvent(this.pageRef, 'propertyExplorer__propertySelected', event.detail);
    }
}