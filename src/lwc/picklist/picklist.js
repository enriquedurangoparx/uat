/**
*
*	@author npo
*	@copyright PARX
*/
import { LightningElement, api, track, wire } from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';


export default class Picklist extends LightningElement {
	@api
	options;

	@api
	selectedValue;

	@track
	values;

	@api
	fieldName;

	@api
	recordId;

	@api
	isDisabled;

	connectedCallback()
	{
		if (this.options !== undefined)
		{
		    let self = this;
		    this.values = [];
		    let selectedValue = self.selectedValue ? self.selectedValue.toString() : '';
			this.options.forEach(function(item) {
				let selected = (selectedValue === item.value.toString());
				self.values.push({label: item.label, value: item.value, selected: selected});
			});
		}
	}

	handleValueChange(event)
	{
	    	    let self = this;
        		let data = {
        			fieldName: self.fieldName,
        			value: event.target.value,
        			recordId: self.recordId
        		};
        		const dataUpdateEvent = new CustomEvent('picklistchange', {
        				composed: true,
        				bubbles: true,
        				detail: data
        		});
        		self.dispatchEvent(dataUpdateEvent);
 	}

    sendChangeEvent (fieldName, value, recordId)
	{
	    console.log('...picklist.async(event) start');
	    let self = this;
		let data = {
			fieldName: fieldName,
			value: value,
			recordId: recordId
		};
		const dataUpdateEvent = new CustomEvent('picklistchange', {
				composed: true,
				bubbles: true,
				detail: data
		});
		self.dispatchEvent(dataUpdateEvent);
		console.log('...picklist.async(event) end');
 	}
}