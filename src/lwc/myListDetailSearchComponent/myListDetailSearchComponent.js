import { LightningElement, track, api, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { registerListener, fireEvent } from 'c/pubsub';

import searchAccountOrContact from '@salesforce/apex/MyListController.searchAccountOrContact';

// custom labels
import accountLabel from '@salesforce/label/c.MyListDetailSearchComponentAccountLabel';
import contactLabel from '@salesforce/label/c.MyListDetailSearchComponentContactLabel';
import searchPlaceholder from '@salesforce/label/c.MyListDetailSearchComponentSearchPlaceholder';
import title from '@salesforce/label/c.MyListDetailSearchComponentTitle';
import searchDescription from '@salesforce/label/c.InvestorsSelectionSearchDescription';


const options = [{ label: contactLabel, value: 'Contact' },{ label: accountLabel, value: 'Account' }];

export default class MyListDetailSearchComponent extends NavigationMixin(LightningElement) {
    @wire(CurrentPageReference) pageRef;

    @track options;
    @track results;
    @track searchObject = 'Contact';
    @api recordId;

    label = {
        searchPlaceholder: searchPlaceholder,
        title: title,
        searchDescription
    }

    connectedCallback(){
        registerListener('MyListDetailSearchComponent__loadMoreData', this.loadMoreData, this);

        this.options = options;
    }

    limitSize = 25;
    limitSizeStep = 10;
    loadedDataSize = 0;
    dataLoadingFinished = false;

    async searchAccountOrContact(){
        this.initLazyLoadState();
        const searchInput = this.template.querySelector('lightning-input');
        const searchTerm = searchInput.value;
        const radioGroup = this.template.querySelector('lightning-radio-group');
        const objectType = radioGroup.value;
        if(searchTerm && searchTerm.length > 2){
            const searchAccountOrContactResult = await searchAccountOrContact({
                objectType: objectType, 
                searchTerm: searchTerm,
                investorsListId: this.recordId,
                limitSize: this.limitSize,
            });
            this.loadedDataSize = searchAccountOrContactResult ? searchAccountOrContactResult.length : 0;
            const event = CustomEvent('searchresultfound', {
                composed: true,
                bubbles: true,
                cancelable: true,
                detail: {
                    objectType: objectType,
                    result: searchAccountOrContactResult
                },
            });
            this.dispatchEvent(event);
        } else if(searchTerm.length === 0){
            const event = CustomEvent('searchresultfound', {
                composed: true,
                bubbles: true,
                cancelable: true,
                detail: {
                    objectType: objectType,
                    result: undefined
                },
            });
            this.dispatchEvent(event);
        }
    }

    initLazyLoadState()
    {
        this.limitSize = 25;
        this.limitSizeStep = 10;
        this.loadedDataSize = 0;
        this.dataLoadingFinished = false;
    }

    loadMoreData()
    {
        if (this.dataLoadingFinished)
        {
            fireEvent(this.pageRef, 'MyListDetailDataComponent__tableIsLoading', null);
            return;
        }
        this.limitSize = this.limitSize + this.limitSizeStep;

        const searchInput = this.template.querySelector('lightning-input');
        const searchTerm = searchInput.value;
        const radioGroup = this.template.querySelector('lightning-radio-group');
        const objectType = radioGroup.value;

        searchAccountOrContact({
            objectType: objectType, 
            searchTerm: searchTerm,
            investorsListId: this.recordId,
            limitSize: this.limitSize,
        })
        .then(result => {
            if (this.loadedDataSize < result.length)
            {
                this.loadedDataSize = result.length;

                const event = CustomEvent('searchresultfound', {
                    composed: true,
                    bubbles: true,
                    cancelable: true,
                    detail: {
                        objectType: objectType,
                        result: result
                    },
                });
                this.dispatchEvent(event);
            }
            else
            {
                fireEvent(this.pageRef, 'MyListDetailDataComponent__tableIsLoading', null);
                this.dataLoadingFinished = true;
            }
            
        }).catch(error => {
        })
    }

    changeHandler(event) {
        if (event.target.value === '') {
            this.searchAccountOrContact();
        }
    }

    /**
     * This is triggered when the user hits a key and is filtered by enter key.
     */
    keyUpHandler(event){
        if (event.key === 'Enter') {
            this.searchAccountOrContact();
        }
    }

    @api clearSearchTerm() {
        try {
            const input = this.template.querySelector('lightning-input');
            input.value = '';
        } catch (e) {
            console.warn("MyListDetailSearchComponent:clearSearchTerm", e.message);
        }
    }
}