/**
 * Created by ille on 2019-04-03.
 */

trigger onTimeCard on TimeCard__c (before insert, before update, before delete, after insert, after update, after delete)
{
    new TimeCardTrigger().execute();
}