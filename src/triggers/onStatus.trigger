/**
*	
*	@author npo
*	@copyright PARX
*/
trigger onStatus on Status__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    StatusTrigger handler = new StatusTrigger();
    handler.disableForUserWithPermission(ATrigger.CAN_SKIP_TRIGGERS_PERMISSION_NAME);
    handler.execute();
}