/**
* Apex Trigger for custom object SubCommissions__c.
*
* @author: <Christian Schwabe> (Christian.Schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 01.07.2019	| Christian Schwabe (Christian.Schwabe@colliers.com)	| initial version.
*/
trigger onSubCommission on SubCommissions__c (before insert, before update, before delete,
        after insert, after update, after delete)
{
	new SubCommissionTrigger().execute();
}