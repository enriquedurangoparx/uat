/**
* Apex Trigger for Task.
*
* @author: <Egor Markuzov> (egor.markuzov@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 05.11.2019	| Egor Markuzov (egor.markuzov@colliers.com)	        | initial version.
* 0.2 03.03.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | added the disableForUserWithPermission() invocation to bypass the trigger
*/
trigger onTask on Task (before insert, before update, before delete, after insert, after update, after delete) {
    ActivityTrigger handler = new ActivityTrigger();
    handler.disableForUserWithPermission(ATrigger.CAN_SKIP_TRIGGERS_PERMISSION_NAME);
    handler.execute();
}