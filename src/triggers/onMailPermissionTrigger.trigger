/**
* Apex Trigger for MailPermission__c records.
*
* @author: <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 18.02.2020	| <Nadzeya Polautsava> (nadzeya.polautsava@parx.com)    | initial version.
*/
trigger onMailPermissionTrigger on MailPermission__c (before insert, before update )
{
	MailPermissionHandler handler = new MailPermissionHandler();
	handler.disableForUserWithPermission(ATrigger.CAN_SKIP_TRIGGERS_PERMISSION_NAME);
	handler.execute();

}