/**
* Apex Trigger for PropertyObject__c records.
*
* @author: <Maksim Fedorenko> (maksim.fedorenko@parx.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 31.01.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | initial version.
* 0.2 03.03.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | added the disableForUserWithPermission() invocation to bypass the trigger
*/
trigger onPropertyObject on PropertyObject__c (before insert, before update, before delete, after insert, after update, after delete)
{
    PropertyObjectTrigger handler = new PropertyObjectTrigger();
    handler.disableForUserWithPermission(ATrigger.CAN_SKIP_TRIGGERS_PERMISSION_NAME);
    handler.execute();
}