/**
* Apex Trigger for Account records.
*
* @author: <Christian Schwabe> (christian.schwabe@colliers.com)
*
* @history:
* version		    | author				                                | changes
* ====================================================================================
* 0.1 18.02.2020	| Christian Schwabe (christian.schwabe@colliers.com)    | initial version.
* 0.2 03.03.2020	| Maksim Fedorenko (maksim.fedorenko@parx.com)          | added the disableForUserWithPermission() invocation to bypass the trigger
*/
trigger onAccount on Account (before insert, before update, before delete, after insert, after update, after delete)
{
    AccountTrigger handler = new AccountTrigger();
    handler.disableForUserWithPermission(ATrigger.CAN_SKIP_TRIGGERS_PERMISSION_NAME);
    handler.execute();
}