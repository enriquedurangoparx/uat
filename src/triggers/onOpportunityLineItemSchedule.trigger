/**
 * Created by ille on 2019-05-29.
 */

trigger onOpportunityLineItemSchedule on OpportunityLineItemSchedule (before insert, before update, before delete,
        after insert, after update, after delete)
{
    new OpportunityLineItemScheduleTrigger().execute();
}