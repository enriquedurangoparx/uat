/**
 * All Trigger handlings on ONB2 Invoice object
 * @author Tejashree
 * @copyright Parx
*/
trigger InvoiceTrigger on ONB2__Invoice__c (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
   new TriggerTemplate.TriggerManager(new InvoiceTriggerHandler()).runHandlers();
}