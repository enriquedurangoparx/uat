/**
 * @author ile
 * @copyright PARX
 */

trigger onOpportunityProduct on OpportunityLineItem (before insert, before update, before delete,
        after insert, after update, after delete)
{
    new OpportunityProductTrigger().execute();
}