<!--Created by Enrique Durango - 2019-->
<project name="Retrieve and Deploy SFDC metadata" default="deployEmptyCheckOnly" basedir=".." xmlns:sf="antlib:com.salesforce">
    <property name="buildFolder" value="build"/>

    <taskdef uri="antlib:com.salesforce"
        resource="com/salesforce/antlib.xml"
        classpath="${buildFolder}/lib/ant-salesforce.jar"/>
    <taskdef resource="net/sf/antcontrib/antlib.xml"
        classpath="${buildFolder}/lib/ant-contrib-1.0b3.jar"/>

    <property file="${buildFolder}/build.properties"/>
    <property environment="env"/>

    <!-- Retrieve an unpackaged set of metadata from your org -->
    <!-- The file ${sf.packageFolder}/package.xml lists what is to be retrieved -->
    <target name="getCode">
      <echo level="info">Retrieving the server's version of code</echo>
      <mkdir dir="${basedir}/${sf.retrieveTarget}"/>
      <!-- Retrieve the contents into another directory -->
      <sf:retrieve
        retrieveTarget="${basedir}/${sf.retrieveTarget}"
        username="${sf.username}"
        password="${sf.password}"
        serverurl="${sf.serverurl}"
        unpackaged="${sf.packageFolder}/package.xml"/>
      <fileset dir="${basedir}/${sf.retrieveTarget}" id="differences">
        <different targetdir="${sf.deployRoot}" ignoreFileTimes="true"/>
      </fileset>
      <propertyregex property="string.out" input="${toString:differences}" regexp=";" replace="&#10;"/>
      <echo message="These are the files that will be updated"/>
      <echo message="${string.out}"/>
      <if>
        <equals arg1="${sf.commitsavechanges}" arg2="true" trim="true"/>
        <then>
          <echo message="Changed files will be set to commit" />
          <move file="${basedir}/${sf.retrieveTarget}" tofile="${sf.deployRoot}"/>
          <antcall target="delete_metadata_files"/>
        </then>
        <else>
          <echo message="Changed files will not be set to commit!!!" />
          <echo message="If you want to commit the changes set the global variable ${sf.commitsavechanges} = true" />
        </else>
      </if>
    </target>

    <!-- Shows deploying code & running tests for code in directory -->
    <target name="deployCode" depends="delete_metadata_files">
      <echo level="info">Performing the deploy username: ${sf.username} url: ${sf.serverurl}</echo>
      <echo level="info">Displaying package.xml file used for the deployment : ${sf.deployRoot}/package.xml</echo>
      <loadfile property="package" srcFile="${sf.deployRoot}/package.xml"/>
      <for param="line" list="${package}" delimiter="${line.separator}">
        <sequential>
          <echo message="@{line}"/>
        </sequential>
      </for>
      <!-- Upload the contents of the "codepkg" directory, running the tests for just 1 class -->
      <sf:deploy
        username="${sf.username}"
        password="${sf.password}"
        serverurl="${sf.serverurl}"
        deployRoot="${sf.deployRoot}"
        pollWaitMillis="${sf.pollWaitMillis}"
        rollbackOnError="true"
        maxPoll="${sf.maxPoll}"/>
    </target>

    <!-- Make sure the tests all pass on this instance. Shows check only; never actually saves to the server -->
    <target name="deployEmptyCheckOnly" depends="delete_metadata_files">
      <echo level="info">Testing the deploy</echo>
      <echo level="info">Displaying package.xml file used for the deployment : ${sf.deployRoot}/package.xml</echo>
      <loadfile property="package" srcFile="${sf.deployRoot}/package.xml"/>
      <for param="line" list="${package}" delimiter="${line.separator}">
        <sequential>
          <echo message="@{line}"/>
        </sequential>
      </for>
      <sf:deploy
          checkOnly="true"
          logType="Debugonly"
          username="${sf.username}"
          password="${sf.password}"
          serverurl="${sf.serverurl}"
          deployRoot="${sf.deployRoot}"
          pollWaitMillis="${sf.pollWaitMillis}"
          maxPoll="${sf.maxPoll}"
          testLevel="RunLocalTests">
       </sf:deploy>
    </target>

    <target name="undeployCode">
      <echo level="info">Undeploying metadata stored in folder : ${sf.removecodepkgFolder}</echo>
      <sf:deploy
        username="${sf.username}"
        password="${sf.password}"
        serverurl="${sf.serverurl}"
        deployRoot="${sf.removecodepkgFolder}"
        pollWaitMillis="${sf.pollWaitMillis}"
        maxPoll="${sf.maxPoll}"/>
    </target>

    <target name="remove_xml_references">
      <echo message="removing references that cannot be migrated from all profiles" />
      <if>
        <available file="${sf.deployRoot}/profiles" type="dir" />
        <then>
          <echo message="The file is on the root folder, therefore is not metadata" />
          <replaceregexp
              match="^    &lt;layoutAssignments&gt;\n        &lt;layout&gt;(EntityMilestone-Object Milestone Layout|Question-Question Layout|Reply-Reply Layout|SocialPost-Social Post Layout)&lt;/layout&gt;\n    &lt;/layoutAssignments&gt;$"
              replace=""
              flags="gm"
              byline="false">
              <fileset
              dir="${sf.deployRoot}/profiles"
              includes="**/*.profile"
              />
          </replaceregexp>
          <replaceregexp
            match="^    &lt;tabVisibilities&gt;\n        &lt;tab&gt;(standard-MessagingEndUser|standard-QuickText|standard-MessagingSession|standard-SocialPersona)&lt;/tab&gt;\n        &lt;visibility&gt;(DefaultOn|DefaultOff)&lt;/visibility&gt;\n    &lt;/tabVisibilities&gt;$"
            replace=""
            flags="gm"
            byline="false">
            <fileset
            dir="${sf.deployRoot}/profiles"
            includes="**/*.profile"
            />
        </replaceregexp>
        </then>
      </if>
    </target>

    <target name="delete_metadata_files" depends="remove_xml_references">
        <echo message="removing files that cannot be migrated" />
        <delete failonerror="false" dir="${sf.deployRoot}/cleanDataServices"/>
        <delete failonerror="false" dir="${sf.deployRoot}/profilePasswordPolicies"/>
        <delete failonerror="false" dir="${sf.deployRoot}/profileSessionSettings"/>
        <delete failonerror="false" dir="${sf.deployRoot}/settings"/>
        <delete failonerror="false" file="${sf.deployRoot}/layouts/EntityMilestone-Object Milestone Layout.layout"/>
        <delete failonerror="false" file="${sf.deployRoot}/layouts/Question-Question Layout.layout"/>
        <delete failonerror="false" file="${sf.deployRoot}/layouts/Reply-Reply Layout.layout"/>
        <delete failonerror="false" file="${sf.deployRoot}/layouts/SocialPost-Social Post Layout.layout"/>
        <delete failonerror="false" file="${sf.deployRoot}/workflows/Question.workflow"/>
        <delete failonerror="false" file="${sf.deployRoot}/workflows/SocialPost.workflow"/>
        <delete failonerror="false" file="${sf.deployRoot}/workflows/Reply.workflow"/>
        <delete failonerror="false" file="${sf.deployRoot}/objects/PaymentGateway.object"/>
        <delete failonerror="false" file="${sf.deployRoot}/objects/CommSubscriptionChannelType.object"/>
        <delete failonerror="false" file="${sf.deployRoot}/objects/PaymentGroup.object"/>
        <delete failonerror="false" file="${sf.deployRoot}/objects/ServiceAppointment.object"/>
    </target>

    <scriptdef name="extractFolderAndFileName" language="javascript">
       <attribute name="text"/>
       <attribute name="buildFolder"/>
       <![CDATA[
         var text = attributes.get("text");
         var buildFolder = attributes.get("buildFolder");
         var fileName = "empty";
         var folder = "empty";

         if(text.lastIndexOf("build/") === -1) {
             var end = text.lastIndexOf("/");
             if(end !== -1){
                fileName = text.substring(end + 1, text.length);
                if(fileName !== "package.xml"){
                    var substracted = text.substring(0, end);
                    var start = substracted.lastIndexOf("/");
                    folder = substracted.substring(start + 1 , end);
                } else {
                    fileName = "empty";
                }
             }

             if(text.lastIndexOf("/lwc/") !== -1) {
                folder = "lwc/" + folder ;
                var end = fileName.lastIndexOf(".");
                fileName = fileName.substring(0, end);
             }
             if(text.lastIndexOf("/aura/") !== -1) {
                folder = "aura/" + folder ;
                var end = fileName.lastIndexOf(".");
                fileName = fileName.substring(0, end);
             }
             if(text.lastIndexOf("/reports/") !== -1) {
                folder = "reports/" + folder ;
                var end = fileName.lastIndexOf(".");
                fileName = fileName.substring(0, end);
             }
        }

        project.setProperty("folder", folder);
        project.setProperty("fileName", fileName);

      ]]>
    </scriptdef>

    <!-- This target can be used to achieve the incremental deployment -->
    <target name="gatherCommitedMetadata">
      <echo level="info">Gathering changed metadata</echo>
      <loadfile property="filesToCopy" srcFile="${deployFolder}/${file}"/>
      <for param="line" list="${filesToCopy}" delimiter="${line.separator}">
        <sequential>
          <echo message="@{line}"/>
          <extractFolderAndFileName text="@{line}"/>
          <echo level="info">Filename = ${fileName}</echo>
          <if>
            <equals arg1="${fileName}" arg2="empty" />
            <then>
              <echo message="The file is the package.xml file or is a file from the build folder, therefore will be ignored" />
            </then>
            <else>
              <if>
                <equals arg1="${folder}" arg2="empty" />
                <then>
                  <echo message="The file is on the root folder, therefore is not metadata" />
                </then>
                <else>
                  <mkdir dir="${deployFolder}/${folder}"/>
                  <copy todir="${deployFolder}/${folder}">
                    <fileset dir="${sf.deployRoot}/${folder}">
                      <include name="**/${fileName}*"/>
                    </fileset>
                  </copy>
                </else>
              </if>
            </else>
          </if>
        </sequential>
      </for>
    </target>

    <!-- This target is for deleting the files generated during the incremental deployment -->
    <target name="deleteDeploymentFiles">
      <echo level="info">Deleting deployment files</echo>
      <delete dir="${deployFolder}"/>
    </target>

</project>
<!--Created by Enrique Durango - 2019-->
